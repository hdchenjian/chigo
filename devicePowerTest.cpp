#include "3rdParty/V4L2ImageCap/jni/capture.h"
#include <./3rdParty/V4L2ImageCap/jni/cap_types.h>
#include <stdio.h>  
#include <unistd.h>
#include "zUtil.h"
#include "zLog.h"
#include "devicePowerTest.h"
#include "zVideoThread.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "zImage.h"
#include "device_test.h"

#define USB_STORAGE 0 // 1: 存储到U盘， 0: 存储到SDCARD

#define USB_MIN_FREE_SIZE   1024*1024*1024  //1G
#define XML_FILE_PATH       "/data/powerOn.xml"
#define POWER_ON_MAX_DURATION 60*60
#define SAVE_IMAGE_INTERVAL 1   // 保存图像间隔 -- 每十次开机，保存一次图片
#define MAX_SAVE_IMAGE_TIME 360 // 指定时间内没有抓到图像，不再重试。单位: 秒
#define SLEEP_DURATION 100000   // 睡眠时长， 单位: 毫秒

static std::string loc_storage_path = "/sdcard";

DtPower_OnAndOff::DtPower_OnAndOff(int width,int height)
    : m_isUsbExist(false)
    , m_lastPowerOnTime(0)
    , m_powerOnTimes(0)
    , m_nWidth(width)
    , m_nHeight(height)
    , m_nThreadCount(0)
    , m_start_time(0)
{
    m_fileManager = zFileUtil::zFileManager::getInstance();
}

DtPower_OnAndOff::~DtPower_OnAndOff()
{
    zThread::Close();
}

void DtPower_OnAndOff::Start()
{
    if ( init())
    {
        if (0 == (m_powerOnTimes % SAVE_IMAGE_INTERVAL))
        {
            LOG_I("DtPower_OnAndOff: thread started.\n");
            m_start_time = zBaseUtil::getSecond();
            zThread::Start();
        }
    }
}

void DtPower_OnAndOff::close_internal()
{
    LOG_I("zThread::Close befor lock \n");
    std::unique_lock<std::mutex> lock(m_mutex);
    LOG_I("zThread::Close end lock \n");

    m_threadState = DEAD_THREAD;
    LOG_I("thread id=%u\n", zBaseUtil::getThreadId());
    return;
}

void DtPower_OnAndOff::proc()
{
    //LOG_D("DtPower_OnAndOff::proc: m_isUsbExist=%d\n", m_isUsbExist);
    std::int64_t curr_time = zBaseUtil::getSecond();
    if ((curr_time - m_start_time) >= MAX_SAVE_IMAGE_TIME)
    {
        LOG_I("## timeout. curr_time=%lld, m_start_time=%lld\n", curr_time, m_start_time);
        close_internal();
        return;
    }

#if USB_STORAGE
    if (!m_isUsbExist)
    {
        check_usb_storage();
    }
    
    if (m_isUsbExist)
#endif
    {
        //LOG_D("DtPower_OnAndOff::proc: m_isUsbExist=%d, usbdir = %s", m_isUsbExist, m_usbDir.c_str());
#if USB_STORAGE
        std::uint64_t free_Size = m_fileManager->getDiskFreeSize(m_usbDir);
#else
        std::uint64_t free_Size = m_fileManager->getDiskFreeSize(loc_storage_path);
#endif
        if (free_Size >= USB_MIN_FREE_SIZE)
        {
            LOG_I("free_Size = %lldM \n", (free_Size >> 20));
            if (doAction())
            {
                LOG_I("save image ok. uptime=%lld.\n", zBaseUtil::get_up_time());
                close_internal();
            }
        }
        else
        {
            LOG_I("storage not enough. free=%dM.\n", (free_Size >> 20));
        }
    }

    std::this_thread::sleep_for(std::chrono::microseconds(SLEEP_DURATION));
    return;
}

bool DtPower_OnAndOff::init()
{
    if (access(XML_FILE_PATH,0) != 0)
    {
        createXml(XML_FILE_PATH);
    }
    LOG_I("%s is exist!\n", XML_FILE_PATH);
    
    if (parseXml(XML_FILE_PATH))
    {
        changValueXml(XML_FILE_PATH,"times", getPowerOntimes() + 1);
        return true;
    }
    return false;
}

bool DtPower_OnAndOff::doAction()
{
    std::int64_t curr_time = zBaseUtil::getSecond();
    LOG_I("### Power on times is %ld. curr_time=%lld\n", m_powerOnTimes, curr_time);
    changValueXml(XML_FILE_PATH, "lastTime", curr_time);
    return saveImageToStorage();
}

bool DtPower_OnAndOff::saveImageToStorage()
{
    char szBuf[256] = { 0 };
    sprintf(szBuf, "%ld", m_powerOnTimes / SAVE_IMAGE_INTERVAL);

#if USB_STORAGE
    std::string tmp_dir = m_usbDir;
#else
    std::string tmp_dir = loc_storage_path;
#endif
    (void)m_fileManager->appendDirectory(tmp_dir, "poweron/");
    std::string filename = m_fileManager->getNewFilename(tmp_dir, szBuf, "jpg");
    LOG_D("filename = %s\n", filename.c_str());

    S_VCBuf imageBuf = { 0 };
    zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();
    bool nReturn = pManage->getFrame(imageBuf, zAlgManager::Threads::THREADANONYMOUS);
    if (!nReturn)
    {
        LOG_E("Get image failed !\n");
        zAlgManager::free_frame(imageBuf);
        return false;
    }

    zImageUtil::zImage::yuv_to_jpg_file(filename,imageBuf.buf,imageBuf.BytesUsed,m_nWidth,m_nHeight,2);
    LOG_I("Save Image:%s\n", filename.c_str());

    zAlgManager::free_frame(imageBuf);
    return true;
}

long DtPower_OnAndOff::getPowerOntimes() const
{
    return m_powerOnTimes;
}

void DtPower_OnAndOff::setPowerOntimes(const long& times)
{
    m_powerOnTimes = times;
}

long DtPower_OnAndOff::getLastPowerOnTime() const
{
    return m_lastPowerOnTime;
}

void DtPower_OnAndOff::setLastPowerOnTime(const long& time)
{
    m_lastPowerOnTime = time;
}

bool DtPower_OnAndOff::createXml(const std::string& filename)
{
    LOG_I("create Xml !");
    TiXmlDocument* xmlDoc = new TiXmlDocument();

    TiXmlElement elemt("PowerOn");

    elemt.SetAttribute("times",0);
    elemt.SetAttribute("lastTime",0);

    xmlDoc->InsertEndChild(elemt);

    xmlDoc->SaveFile(filename.c_str());

    delete xmlDoc;

    return true;
}

bool DtPower_OnAndOff::parseXml(const std::string& filename)
{
    TiXmlDocument* xmlDoc = new TiXmlDocument(filename.c_str());

    if (xmlDoc->LoadFile())
    {
        TiXmlElement* elemt;
        elemt = xmlDoc->FirstChildElement("PowerOn");
        if (elemt)
        {
            m_powerOnTimes = (long)atoi(elemt->Attribute("times"));
            m_lastPowerOnTime = (long)atoi(elemt->Attribute("lastTime"));
            LOG_I("m_powerOnTimes=%d \n", m_powerOnTimes);
        }
        LOG_I("success!\n");
    }
    else
    {
        LOG_E("parse Xml failed !\n");
        delete xmlDoc;
        return false;
    }

    delete xmlDoc;
    return true;
}

bool DtPower_OnAndOff::changValueXml(const std::string& filename,const std::string& attribute,long value)
{
    TiXmlDocument* xmlDoc = new TiXmlDocument(filename.c_str());

    if (xmlDoc->LoadFile())
    {
        TiXmlElement* elemt;
        elemt = xmlDoc->FirstChildElement("PowerOn");
        if (elemt)
        {
            elemt->SetAttribute(attribute.c_str(),value);
        }
    }
    else
    {
        LOG_I("LoadFile failed !\n");
        delete xmlDoc;
        return false;
    }
    xmlDoc->SaveFile();
    delete xmlDoc;

    return true;
}

/*****************************************************************************
 * 函 数 名  : DtPower_OnAndOff.check_usb_storage
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月23日
 * 函数功能  : 检测U盘是否存在, 设置U盘路径
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtPower_OnAndOff::check_usb_storage()
{
    // OTG_DEVICE状态，无U盘
    if (OTG_MODE_DEVICE == DtThread::dt_get_otg_mode())
    {
        return;
    }

    char mountpoint[255] = { 0 };
    char label[] = "usbhost1";
    if (0 == zBaseUtil::get_mountpoint(label, mountpoint))
    {
        m_usbDir = mountpoint;
        m_isUsbExist = true;
        LOG_D("usb storage. %s\n", mountpoint);
    }
    else
    {
        m_isUsbExist = false;
        m_usbDir = "";
        LOG_D("no usb storage.\n");
    }
    return;
}
        
