#ifndef __ZTHREAD_H__
#define __ZTHREAD_H__

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <cstdint>

class zThreadMsg
{
public:
    std::uint32_t msg;                                      // MSG id
    void* from;                                             // MSG 来源
    void* pMsg;                                             // MSG 内容指针，POST MSG 最好不要传送结构体指针
    std::chrono::steady_clock::time_point  time;            // MSG 创建时间
    zThreadMsg()
        : msg(0)
        , from(NULL)
        , pMsg(NULL)
    {
        time = std::chrono::steady_clock::now();
    }

    bool operator< (const zThreadMsg& that) const
    {
        //std::chrono::steady_clock::duration timeDiff = this->time - that.time;
        //return (timeDiff.count() > 0);
        return this->time > that.time;
    }

};

class zThread
{
public:
    enum ThreadState
    {
        NEW_THREAD,
        RUNABLE_THREAD,
        RUNNING_THREAD,
        BLOCKED_THREAD,
        DEAD_THREAD
    };
    zThread();
    virtual ~zThread();

    bool Is(ThreadState state);
    void BreakPoint();
    virtual void Start();
    virtual void Close();
    virtual void Wait();
    virtual void WakeUp();
    virtual void proc();

    virtual int OnMsg(const zThreadMsg& msg);
    int addMsg(const zThreadMsg& msg);
    int getMsg(zThreadMsg& msg);
    int peerMsg(zThreadMsg& msg);

    static int SendThreadMsg(zThread* pThread, const zThreadMsg& msg);
    static int PostThreadMsg(zThread* pThread, const zThreadMsg& msg);          // do not post msg with struct pointer. if necessary
                                                                            // ensure pointer exist before msg be deal with
    static int GetThreadMsg(zThread* pThread, zThreadMsg& msg);
    static int PeerThreadMsg(zThread* pThread, zThreadMsg& msg);
private:
    static void threadProc(void* param);

protected:
    ThreadState m_threadState;
    std::thread*  m_pThread;
    std::mutex m_mutex;
    std::condition_variable m_cond;
    std::mutex m_msgMutex;

private:
    std::priority_queue<zThreadMsg> m_msgQueue;
};

#endif
