#ifndef __ZVIDEOTHREAD_H__
#define __ZVIDEOTHREAD_H__

#include <./3rdParty/V4L2ImageCap/jni/capture.h>
#include <./3rdParty/V4L2ImageCap/jni/cap_types.h>
#include "zThread.h"
#include "I2CTag.h"
#include "zUtil.h"
#include "devicePowerTest.h"
//#include "zAlgorithmManager.h"
//#include "zEakonThread.h"
//#include "tamaneThread.h"

#define DEFAULT_H264_W (320)
#define DEFAULT_H264_H (240)

class zRokugaThread;
class zTamaneThread;
class zAlgManager;
class zVideoThread : public zThread
{
public:
    static zVideoThread* getInstance();
    static void destroyInstance();

    void init(int dev_id = 0, int width = 1280, int height = 720,
        int pixel_fmt = VIDEO_CAP_FMT_NV21, int frame_rate = 0,
        int reverse = 0);

    void Start();
    void proc();

    std::string version();
    void chgModel(zBaseUtil::zEakonModel model);
    zBaseUtil::zEakonModel getEAKonModel();
    void setEakonDev(ZDEVICE device);
    void setVideoState(zBaseUtil::zVideoState state);
    zBaseUtil::zVideoState getVideoState();
    void setEAKonState(int state);
    int getEAKonState();
    int getRokuState();
    void SetNeedEnc(bool need);
    bool NeedEnc();

    int onEvent(int nEvent);
    int onMsg(const zBaseUtil::zMsg& msg);

    zAlgManager* getAlgManager();
    zRokugaThread* getRokugaThread();
    
    //////////////////////////////////////////////////////////////////////////
    bool findRokuFile(const std::string& fileName, std::string& fullPath);
    bool downloadFile(const zBaseUtil::DownloadParam& param);
    bool removeFile(const std::string& fileName);
    int change_resolution(zBaseUtil::resolution_t& resolution);
    int get_encode_resolution(int& width, int& height);
    //////////////////////////////////////////////////////////////////////////
    int gamma_correct(bool bOpen);

private:
    zVideoThread(/*int dev_id = 0, int width = 1280, int height = 720,
                 int pixel_fmt = VIDEO_CAP_FMT_NV12, int frame_rate = 0,
                 int reverse = 0*/);
    ~zVideoThread();

    int do_video_capture(S_VCBuf& frameBuf, int need_encode);
    int do_video_encode(int bufLen);

    int InitH264Enc(int width, int height);
    int UninitH264Enc();
    void redo_init();
    
    //int GetFrame(unsigned char* szBuf, unsigned int nLen);
    void proc_h264_enc_result(int result);
    int gamma_correct_execute(bool bOpen);

protected:
    void switchEEKonState(int step = 1);
    void AnalyzeFrame(S_VCBuf& buf);
    void DealFrame(S_VCBuf& buf);
    void DealEncFrame(S_H264EncBuf& buf, std::int64_t TimeStamp);

public:
    void save_raw_video(const S_H264EncBuf& h264_vbuf);
    void reset_raw_video();
    int m_nWidth;
    int m_nHeight;
    
private:
    //bool m_bInited;
    //bool m_bOnline;               // 是否是直播、如果不是则不上传摄像头当前帧
    bool m_bNeedUpload;
    static zVideoThread* m_instance;
    volatile zBaseUtil::zVideoState m_nVideoState;

    zRokugaThread* m_pRokugaThread;
    zAlgManager* m_pManager;
    DtPower_OnAndOff* m_DtPower;
    bool m_poweron_image;

    //int m_nDevID;
    int m_nPixelFmt;
    int m_nFmtRate;
    int m_nReverse;
    std::mutex m_ctrlMutex;
    std::string m_strVersion;

    bool m_video_initialize_fail_flag;
    bool m_h264_enc_fail_flag;
    int m_h264_enc_frm_cnt;
    int m_frame_threshold;

    int m_getframe_fail_continuous_count; // 连续抓取视频失败次数统计

    bool m_encode_inited_flag; // H264编码初始化标志
    volatile int m_h264_encode_w; // H264编码宽度
    volatile int m_h264_encode_h; // H264编码高度
    std::mutex m_encode_mutex;    // 编码参数锁

    FILE* m_fp; // 裸视频文件指针
    int m_total_frame; 
    std::int64_t m_prev_frame_time;

    volatile bool m_need_gamma_correct; // gamma 矫正标志
    bool m_gamma_correct_state; 
};
#endif
