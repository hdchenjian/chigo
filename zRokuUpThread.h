#ifndef __ZROKUUPTHREAD_H__
#define __ZROKUUPTHREAD_H__

#include "zSingletonThread.h"
#include "zUtil.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <3rdParty/ffmpeg/libavutil/timestamp.h>
#include <3rdParty/ffmpeg/libavformat/avformat.h>
#ifdef __cplusplus
}
#endif

// 录像上传线程
class zRokuUpThread : public zSingletonThread
{
public:
    static zRokuUpThread* getInstance();
    static void destroyInstance();

    void init(std::string filename, std::uint64_t nGap = 83);       // 因为是单例 这里通过每次init 更新上传的文件， 如果正在上传则啥都不干
                                                                    // nGap 是上传视频帧的间隔时间 ms
    void unInit();                                                  // 反初始化
    bool proc();
    int onMsg(const zBaseUtil::zMsg& msg);                          // 处理相关消息
    std::string filename();
    void onFileEnd();

    // 20151014
protected:
    zRokuUpThread();
    ~zRokuUpThread();
    void setFilename(std::string filename);                         // 设置上传文件名
    void initReader(std::string filename);                          // 初始化取H264视频帧相关东西
    void uninitReader();                                            // 反初始化取H264视频帧相关东西

    void dealFrame(const AVPacket& pkt_);                           // 处理取出来的视频帧，（直接调用p2p上传） 默认p2p已经跑起来（跑不起来也不会报错）
private:
    static zRokuUpThread* instance_;
    bool needUp_;                                                   // 是否需要上传、当设为flase的时候 proc 返回false，线程结束 否则 一直循环
                                                                    // 这个其实是跟 videoThread的online 状态互斥的（要么传录像 要么直播）
    std::string upFilename_;                                        // 上传文件名
    std::mutex ctrlMutex_;
    //
    //FILE *fp_video_/* = NULL*/;
    AVFormatContext *ifmt_ctx_ /*= NULL*/;                          // 读帧相关
    AVPacket pkt_;                                                  // 读帧相关
};

#endif
