#include "ZoneRecorder.h"

ZONE_POS ZoneRecorder::calZone(const zBaseUtil::v2Hand& handRect) {
    const double dx = ((double)handRect.right_+handRect.left_)/2 - startCX;
    const double dy = ((double)handRect.bottom_+handRect.top_)/2 - startCY;
    const double zoneScaled = 1.2;
    ZONE_POS tmpZone = zone;
    // 判断所在区域
    if (dy < -zoneWidth*zoneScaled)
    {
        tmpZone = TOP_ZONE;
    }
    else if (dy > zoneWidth*zoneScaled)
    {
        tmpZone = BOTTOM_ZONE;
    }
    else if (dx > zoneWidth*zoneScaled)
    {
        tmpZone = RIGHT_ZONE;
    }
    else if (dx < -zoneWidth*zoneScaled) 
    {
        tmpZone = LEFT_ZONE;
    }
    else if ((-zoneWidth < dx && dx < zoneWidth)
        && (-zoneWidth < dy && dy < zoneWidth))
    {
        tmpZone = CENTER_ZONE;
    } else {
        // 防止跳变
        ZONE_POS regularZone = getRegularZone(dx, dy);
        if (regularZone != tmpZone && tmpZone != CENTER_ZONE) {
            tmpZone = regularZone;
        }
    }
    return tmpZone;
}

/**
 * 更新进去区域时间、驻留时长并设置所在区域。
 */
void ZoneRecorder::updateZone(ZONE_POS futureZone) {
    // 通过判断是否区域发生变化，并更新进入区域的时间、驻留时长。
    if (futureZone != zone) {
        isChangedZone = true;
        zoneStartMillis = zBaseUtil::getMicroSecond()/1000;
        zoneStayMillis = 0;
    } else {
        isChangedZone = false;
        zoneStayMillis = zBaseUtil::getMicroSecond()/1000
                - zoneStartMillis;
    }
    zone = futureZone;
}

ZONE_POS ZoneRecorder::getRegularZone(double dx, double dy) {
    ZONE_POS tempZone = zone;

    if (dy <= -zoneWidth)
    {
        tempZone = TOP_ZONE;
    }
    else if (dy >= zoneWidth)
    {
        tempZone = BOTTOM_ZONE;
    }
    else if (dx >= zoneWidth) 
    {
        tempZone = RIGHT_ZONE;
    }
    else if (dx <= -zoneWidth) 
    {
        tempZone = LEFT_ZONE;
    }
    else if ((-zoneWidth < dx && dx < zoneWidth)
        && (-zoneWidth < dy && dy < zoneWidth))
    {
        tempZone = CENTER_ZONE;
    } 
    return tempZone;
}


