#include <cmath>
#include <string>
#include <list>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#ifdef _WINDOWS
#else
#include <unistd.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
#include <3rdParty/ffmpeg/libavutil/avassert.h>
#include <3rdParty/ffmpeg/libavutil/channel_layout.h>
#include <3rdParty/ffmpeg/libavutil/opt.h>
#include <3rdParty/ffmpeg/libavutil/mathematics.h>
#include <3rdParty/ffmpeg/libavutil/timestamp.h>
#include <3rdParty/ffmpeg/libavformat/avformat.h>
//#include <3rdParty/ffmpeg/libswscale/swscale.h>
#include <3rdParty/ffmpeg/libswresample/swresample.h>
#include <3rdParty/ffmpeg/libavutil/time.h>
#include <3rdParty/ffmpeg/libavcodec/avcodec.h>
#ifdef __cplusplus
}
#endif

#include "zAlgorithmManager.h"
#include "p2pThread.h"
//#include "zWifiConnect.h"
#include "zRokuUpThread.h"
#include "zWifiStateThread.h"
#include "config_mgr.h"
#include "zFile.h"
#include "zRokugaThread.h"
#include "zVideoThread.h"
#include "zLog.h"
#include "zUtil.h"
#include "zImage.h"

#define STREAM_FRAME_RATE 12 /* 25 images/s */
#define ROKU_TIME (3*60)
#define TANINN_ROKU_TIME (3*60)

#define SAVE_IMAGE_GAP      (300) 
#define SAVE_IMAGE_CNT      (10)
#define SAVE_IMAGE_TIMERID  (1008) 
#define USB_TEST_TIMERID    (1009)

#define MIN_RECORD_FILE_SIZE (100*1024)     // 最小录像文件大小
#define STORAGE_RETAIN_SIZE_DEFAULT (50)    // 默认存储空间预留大小，单位: M

#define IMAGE_SUFFIX ("jpg")
#define MAX_GESTURE_IAMGE_COUNT 1000   // 手势图像最大张数
#define MAX_ALARM_IAMGE_COUNT   1000   // 告警图像最大张数

#define VIDEO_CLEAR_MEMORY_SIZE 100    // 视频存储清理大小，单位: M
#define GESTURE_IMAGE_CLEAR_SIZE 100   // 手势图像存储清理大小，单位: 张数
#define ALARM_IMAGE_CLEAR_SIZE 100     // 告警图像存储清理大小，单位: 张数

//////////////////////////////////////////////////////////////////////////
//#include "capture.h"

#define STREAM_DURATION   10.0
#define STREAM_PIX_FMT    AV_PIX_FMT_YUV420P /* default pix_fmt */

#define SCALE_FLAGS SWS_BICUBIC
#define MAX_MANNUAL_RECORD_DURATION (12*60*60*1000) // 单位: 毫秒

// 录像标志文件名
static const char * recording_preffix = "recording_";


// a wrapper around a single output AVStream
typedef struct OutputStream {
    AVStream *st;

    /* pts of the next frame that will be generated */
    int64_t next_pts;
    int samples_count;

    AVFrame *frame;
    AVFrame *tmp_frame;

    float t, tincr, tincr2;

    //struct SwsContext *sws_ctx;
    struct SwrContext *swr_ctx;
} OutputStream;

/* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
typedef struct
{
    zRokugaThread *p_roku;
    S_VCBuf buf;
    int gesture_type;
}gesture_thread_t;

const char * file_type_string_table[zRokugaThread::FILE_TYPE_BUTT + 1] = 
{
    "video_alarm", "video_normal", "image", "image", "unkonwn"
};

int zRokugaThread::write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt)
{
    /* rescale output packet timestamp values from codec to stream timebase */
    av_packet_rescale_ts(pkt, *time_base, st->time_base);
    pkt->stream_index = st->index;

    /* Write the compressed frame to the media file. */
    //log_packet(fmt_ctx, pkt);
    return av_interleaved_write_frame(fmt_ctx, pkt);
}

/* Add an output stream. */
void zRokugaThread::add_stream(
    OutputStream *ost, AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id)
{
    /* find the encoder */
    *codec = avcodec_find_encoder(codec_id);
    if (!(*codec)) {
        fprintf(stderr, "Could not find encoder for '%s'\n",
            avcodec_get_name(codec_id));
        exit(1);
    }

    ost->st = avformat_new_stream(oc, *codec);
    //LOG_D("ost->st=%p, oc->st=%p \n", ost->st, oc->streams[oc->nb_streams - 1]);
    if (!ost->st) {
        fprintf(stderr, "Could not allocate stream\n");
        exit(1);
    }
    ost->st->id = oc->nb_streams - 1;
    AVCodecContext *c = ost->st->codec;

    switch ((*codec)->type) {
    case AVMEDIA_TYPE_AUDIO:
        c->sample_fmt = (*codec)->sample_fmts ?
            (*codec)->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
        c->bit_rate = 64000;
        c->sample_rate = 44100;
        if ((*codec)->supported_samplerates) {
            c->sample_rate = (*codec)->supported_samplerates[0];
            for (int i = 0; (*codec)->supported_samplerates[i]; i++) {
                if ((*codec)->supported_samplerates[i] == 44100)
                    c->sample_rate = 44100;
            }
        }
        c->channels = av_get_channel_layout_nb_channels(c->channel_layout);
        c->channel_layout = AV_CH_LAYOUT_STEREO;
        if ((*codec)->channel_layouts) {
            c->channel_layout = (*codec)->channel_layouts[0];
            for (int i = 0; (*codec)->channel_layouts[i]; i++) {
                if ((*codec)->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                    c->channel_layout = AV_CH_LAYOUT_STEREO;
            }
        }
        c->channels = av_get_channel_layout_nb_channels(c->channel_layout);
        ost->st->time_base = (AVRational){ 1, c->sample_rate };
        break;

    case AVMEDIA_TYPE_VIDEO:
        avcodec_get_context_defaults3(c, *codec);
        c->codec_id = codec_id;

        c->bit_rate = 400000;
        /* Resolution must be a multiple of two. */
        c->width = m_encode_width;
        c->height = m_encode_height;
        LOG_D("width=%d, height=%d \n", c->width, c->height);

        /* timebase: This is the fundamental unit of time (in seconds) in terms
        * of which frame timestamps are represented. For fixed-fps content,
        * timebase should be 1/framerate and timestamp increments should be
        * identical to 1. */
        ost->st->time_base = (AVRational){ 2, 25 };
        c->time_base = ost->st->time_base;

        c->gop_size = 12; /* emit one intra frame every twelve frames at most */
        c->pix_fmt = STREAM_PIX_FMT;
        if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
            /* just for testing, we also add B frames */
            c->max_b_frames = 2;
        }
        if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
            /* Needed to avoid using macroblocks in which some coeffs overflow.
            * This does not happen with normal video, it just happens here as
            * the motion of the chroma plane does not match the luma plane. */
            c->mb_decision = 2;
        }
        break;

    default:
        break;
    }

    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
}

/**************************************************************/
/* audio output */

AVFrame* zRokugaThread::alloc_audio_frame(enum AVSampleFormat sample_fmt,
    uint64_t channel_layout,
    int sample_rate, int nb_samples)
{
    AVFrame *frame = av_frame_alloc();
    int ret;

    if (!frame) {
        fprintf(stderr, "Error allocating an audio frame\n");
        exit(1);
    }

    frame->format = sample_fmt;
    frame->channel_layout = channel_layout;
    frame->sample_rate = sample_rate;
    frame->nb_samples = nb_samples;

    if (nb_samples) {
        ret = av_frame_get_buffer(frame, 0);
        if (ret < 0) {
            fprintf(stderr, "Error allocating an audio buffer\n");
            exit(1);
        }
    }

    return frame;
}

void zRokugaThread::open_audio(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg)
{
}

/* Prepare a 16 bit dummy audio frame of 'frame_size' samples and
* 'nb_channels' channels. */
AVFrame* zRokugaThread::get_audio_frame(OutputStream *ost)
{
    AVFrame *frame = ost->tmp_frame;
    int j, i, v;
    int16_t *q = (int16_t*)frame->data[0];

    /* check if we want to generate more frames */
    if (av_compare_ts(ost->next_pts, ost->st->codec->time_base,
        STREAM_DURATION, (AVRational){ 1, 1 }) >= 0)
        return NULL;

    for (j = 0; j <frame->nb_samples; j++) {
        v = (int)(sin(ost->t) * 10000);
        for (i = 0; i < ost->st->codec->channels; i++)
            *q++ = v;
        ost->t += ost->tincr;
        ost->tincr += ost->tincr2;
    }

    frame->pts = ost->next_pts;
    ost->next_pts += frame->nb_samples;

    return frame;
}

/*
* encode one audio frame and send it to the muxer
* return 1 when encoding is finished, 0 otherwise
*/
int write_audio_frame_cnt = 0;
int zRokugaThread::write_audio_frame(AVFormatContext *oc, OutputStream *ost)
{
    return 0;
}

/**************************************************************/
/* video output */
AVFrame* zRokugaThread::alloc_picture(enum AVPixelFormat pix_fmt, int width, int height)
{
    AVFrame *picture;
    int ret;

    picture = av_frame_alloc();
    if (!picture)
        return NULL;

    picture->format = pix_fmt;
    picture->width = width;
    picture->height = height;

    /* allocate the buffers for the frame data */
    ret = av_frame_get_buffer(picture, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate frame data.\n");
        exit(1);
    }

    return picture;
}

void zRokugaThread::open_video(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg)
{
    int ret;
    AVCodecContext *c = ost->st->codec;
    AVDictionary *opt = NULL;

    av_dict_copy(&opt, opt_arg, 0);

    /* open the codec */
    ret = avcodec_open2(c, codec, &opt);
    av_dict_free(&opt);
    if (ret < 0) {
        LOG_E("Could not open video codec \n");
        //fprintf(stderr, "Could not open video codec: %s\n", av_err2str(ret));
        //exit(1);
    }

    /* allocate and init a re-usable frame */
    ost->frame = alloc_picture(c->pix_fmt, c->width, c->height);
    if (!ost->frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }

    /* If the output format is not YUV420P, then a temporary YUV420P
    * picture is needed too. It is then converted to the required
    * output format. */
    ost->tmp_frame = NULL;
    if (c->pix_fmt != AV_PIX_FMT_YUV420P) {
        ost->tmp_frame = alloc_picture(AV_PIX_FMT_YUV420P, c->width, c->height);
        if (!ost->tmp_frame) {
            fprintf(stderr, "Could not allocate temporary picture\n");
            exit(1);
        }
    }
}

S_H264EncBuf* zRokugaThread::get_video_frame(OutputStream *ost)
{
    return NULL;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.write_muxer_header
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月12日
 * 函数功能  : 写MP4复用头
 * 输入参数  : const uint8_t *extradata  sps && pps
               int extradata_size        长度
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zRokugaThread::write_muxer_header(
    const uint8_t *extradata, int extradata_size)
{
    if (    (extradata_size > 0)
        &&  (NULL != extradata) )
    {
        AVCodecContext *c = video_st_.st->codec;
        int extra_size = extradata_size + FF_INPUT_BUFFER_PADDING_SIZE;

        c->extradata_size = extradata_size;
        c->extradata = (uint8_t *)av_mallocz(extra_size);
        if (NULL == c->extradata)
        {
            LOG_E("av_mallocz failed.\n");
        }
        else
        {
            (void)memcpy(c->extradata, extradata, extradata_size);
        }
    }

    // 添加MP4复用选项，这样生成的MP4文件没有尾部数据，也可以正常播放。
    AVDictionary *opt = NULL;
    /* delete by 刘春龙, 2016-03-07, 原因: 有些播放器不兼容，无法拖动 */
    //av_dict_set(&opt, "movflags", "frag_keyframe", 0);
    //av_dict_set(&opt, "movflags", "empty_moov", 0);

    // 将MOOV BOX 写在文件前面
    av_dict_set(&opt, "movflags", "+faststart", 0);

    /* Write the stream header, if any. */
    int ret = avformat_write_header(oc_, &opt);
    if (ret < 0) {
        LOG_E("Error occurred when opening output file %s \n", m_strFileName.c_str());
        return -1;
    }

    /* add by 刘春龙, 2015-12-03, 注: 调用avformat_write_header 可能已经释放掉了 */
    if (NULL != opt)
    {
        av_dict_free(&opt);
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.write_video_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 获取H264编码帧，写录像文件
 * 输入参数  : AVFormatContext *oc  复用上下文
               OutputStream *ost    输出流上下文
 * 输出参数  : 无
 * 返 回 值  : int: 1: 写失败 0: 成功
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zRokugaThread::write_video_frame(AVFormatContext *oc, OutputStream *ost)
{
    if (m_pManager->getEncFrames(m_frms, m_frms_size))
    {
        for (int i=0; i<m_frms_size; i++)
        {
            encoded_frame_t &frm = m_frms[i];
            int ret = write_video_frame_impl(oc, ost, frm.m_enc_img, frm.m_enc_time_stamp);
            if (0 != ret)
            {
                LOG_E("ret=%d\n", ret);
                return ret;
            }
        }
    }
    return 0;
}

/*
* encode one video frame and send it to the muxer
* return 1 when encoding is finished, 0 otherwise
*/
int zRokugaThread::write_video_frame_impl(
    AVFormatContext *oc, OutputStream *ost, S_H264EncBuf &h264frame, std::int64_t TimeStamp)
{
    if (NULL == oc || NULL == ost)
    {
        return 1;
    }

    if (!h264frame.buf || (0 == h264frame.len))
    {
        return 0;
    }

    if (m_bFirstFrame && !h264frame.IsKeyFrame)
    {
        return 0;
    }

    AVCodecContext *c = ost->st->codec;
    AVPacket pkt = { 0 };
    av_init_packet(&pkt);
    pkt.flags |= h264frame.IsKeyFrame ? AV_PKT_FLAG_KEY : 0;
    pkt.stream_index = ost->st->index;
    pkt.data = (uint8_t *)h264frame.buf;//(uint8_t *)frame;
    pkt.size = h264frame.BytesUsed;//sizeof(AVPicture);

    int64_t pts;
    if (m_bFirstFrame)
    {
        pts_ = 0;
        TimeStampBase_ = TimeStamp;

        (void)write_muxer_header((uint8_t *)h264frame.buf, h264frame.KeyInfoLen);
        m_bFirstFrame = false;
    }

    int64_t gap = TimeStamp - TimeStampBase_ + ROKUGAG_FRAME_GAP/4;//防止时间戳误差导致pts不准
    //pts = gap/ROKUGAG_FRAME_GAP;
    ++pts_;
    pts = pts_;
    pkt.pts = pkt.dts = pts;
    LOG_D("TimeStamp=%lld, TimeStampBase=%lld, gap=%lld, pts=%lld \n", 
        TimeStamp, TimeStampBase_, gap, pts);

    int ret = write_frame(oc, &c->time_base, ost->st, &pkt);
    if (ret < 0) 
    {
        LOG_E("Error while writing video frame!\n");
        // if Failed may need end roku
        return 1;
    }

    return 0;
}

void zRokugaThread::close_stream(AVFormatContext *oc, OutputStream *ost)
{
    avcodec_close(ost->st->codec);
    av_frame_free(&ost->frame);
    av_frame_free(&ost->tmp_frame);
    //sws_freeContext(ost->sws_ctx);
    swr_free(&ost->swr_ctx);
}

//////////////////////////////////////////////////////////////////////////
//id_time_(T or F)
static bool getSyncFromName(std::string& name)
{
    if (name.empty()) 
    {
        return false;
    }
    return (name.rfind("_T") != std::string::npos);
}

static int getIDFromName(std::string name)
{    
    if (name.empty()) return -1;
    // file name id_time.suffix
    int id = -1;
    size_t nPos = name.find("_");
    if (nPos != std::string::npos)
    {
        name = name.substr(0, nPos);
        id = atoi(name.c_str());
    }
    return id;
}

// filename == dir + prefix+"_" + time + "_T" or "_F" + "." + suffix
static std::string getStridFromName(std::string name)
{
    std::string strResult;
    size_t nPos = name.find("_");
    if (nPos != std::string::npos)
    {
        strResult = name.substr(0, nPos);
    }
    return strResult;
}

// filename == dir + prefix+"_" + time + "_T" or "_F" + "." + suffix
static std::string getTimeFromeName(std::string name)
{
    std::string  strResult;
    size_t nPos = name.find("_");
    size_t nEnd = name.rfind("_");
    size_t nLen = nEnd - nPos - 1;
    if (nPos != std::string::npos
        && nLen > 0)
    {
        strResult = name.substr(nPos + 1, nLen);
    }
    return strResult;
}

static bool FileNameIDCmp(const std::string& srcIt, const std::string& desIt)
{
    int srcid = getIDFromName(srcIt);
    int desid = getIDFromName(desIt);
    return srcid < desid;
}

static bool FileNameIDCmp2(const zRokugaThread::file_info_t& srcIt, 
    const zRokugaThread::file_info_t& desIt)
{
    int srcid = getIDFromName(srcIt.file_name);
    int desid = getIDFromName(desIt.file_name);
    return srcid < desid;
}

static bool UsbTest(void* param)
{
    if (NULL == param) return false;
    zRokugaThread* pThread = (zRokugaThread*)param;

    pThread->findAndTryUSB();
    return true;
}

static std::string getFilenameByFullPath(const std::string& filename, const std::string& rootDir)
{
    LOG_I("getFilenameByFullPath filename == %s, rootDir == %s \n", filename.c_str(), rootDir.c_str());
    size_t nPos = filename.find(rootDir);
    if (nPos == std::string::npos)
    {
        LOG_E("getFilenameByFullPath failed! \n");
        return "";
    }
    std::string str = filename.substr(nPos + rootDir.length(), filename.length() - rootDir.length());
    LOG_I("result == %s \n", str.c_str());
    return str;
}
//////////////////////////////////////////////////////////////////////////
//
//static bool endRokutaninn_(void* param)
//{
//  if (NULL == param) return true;
//  zRokugaThread* pThread = (zRokugaThread*)param;
//  pThread->onRokutaninnEnd();
//  return true;
//}
//////////////////////////////////////////////////////////////////////////
// roku and size
zRokugaThread::zRokugaThread(zAlgManager* palg, int nWidth, int nHeight)
    : gSaveImage(false)
    , m_pManager(palg)
    , m_bFirstFrame(true)
    , m_bLocst(true)
    , m_bNeedRename(false)
    , m_latestVideoid(0)
    , m_latestImageid(0)
    , m_nWidth(nWidth)
    , m_nHeight(nHeight)
    , m_ctrState(UNKNOWN_ROKU)
    , TimeStampBase_(0)
    , pts_(0)
    , m_curRokuType(zRokugaThread::rokuStoreType::st_default)
    , m_preFrame(0)
    , m_nRoku(ROKU_TIME)
    , m_nTaninnRoku(TANINN_ROKU_TIME)
    , m_nCurRokuTime(0)
    , m_nCurTaninnRokuTime(0)
    , m_latest_gesture_image_id(0)
    , oc_(NULL)
    , m_pSaveImageThread(NULL)
    , m_encode_width(0)
    , m_encode_height(0)
    , m_manual_record_max_duration(MAX_MANNUAL_RECORD_DURATION)
{
    (void)memset(m_frms, 0, sizeof(m_frms));

    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
    std::string val = conf.getValue("retainSize");
    LOG_D(" retainSize == %s \n");
    int size = STORAGE_RETAIN_SIZE_DEFAULT;
    if (!val.empty())
    {
        size = atoi(val.c_str());
        LOG_D("atoi(val.c_str()) == %d \n", size);
    }
    m_retainSize = std::abs(size);
    LOG_D(" disk retain size is %s == %llu \n", val.c_str(), m_retainSize);

    val = conf.getValue("avDir");
    if (val.empty()) val = "/sdcard/";
    //m_locDir = val;
    setDir(val, zRokugaThread::rokuStoreType::st_loc);
    dirChg(zRokugaThread::rokuStoreType::st_loc);

    // alarm image dir
    //val = conf.getValue("imageDir");
    //if (val.empty()) val = "/sdcard/alarmImage/";
    //setDir( val, rokuStoreType::st_image);                // default alarmImage

    findAndTryUSB();
    m_timer.setTimer(USB_TEST_TIMERID, -1, -1, 10000, 0, UsbTest, this);
    LOG_D("stDir == %s \n", stDir().c_str());
    val = conf.getValue("avSuffix");
    if (val.empty()) val = "av";
    m_strVideoSuffix = val;
    //
    val = conf.getValue("rokuTime");
    if (!val.empty())
    {
        m_nRoku = atoi(val.c_str());
    }

    val = conf.getValue("taninnRokuTime");
    if (!val.empty())
    {
        m_nTaninnRoku = atoi(val.c_str());
    }

    val = conf.getValue("manual_record_max_duration");
    if (!val.empty())
    {
        m_manual_record_max_duration = atoi(val.c_str()) * 60 * 1000;
    }
    LOG_D("m_manual_record_max_duration=%lld", m_manual_record_max_duration);

    //
    memset(&video_st_, 0, sizeof(OutputStream));

    // image
    //zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    //if (!pm->isDirectoryExist(m_alarmImgDir)) pm->createDirectory(m_alarmImgDir);

    //updateImageList(m_alarmImgDir, m_alarmImage);
    updateImageList(getDir(rokuStoreType::st_loc_image), m_locAlarmImage, m_latestImageid);
    updateImageList(getDir(rokuStoreType::st_usb_image), m_usbAlarmImage, m_latestImageid);

    /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
    (void)updateImageList(getDir(rokuStoreType::st_loc_gesture_image), m_local_gesture_image_list, m_latest_gesture_image_id);
    (void)updateImageList(getDir(rokuStoreType::st_usb_gesture_image), m_usb_gesture_image_list, m_latest_gesture_image_id);

    //LOG_D(" alarm image dir == %s, list size == %zu \n", getDir(rokuStoreType::st_image).c_str(), m_alarmImage.size());
    // video
    updateVideolist(getDir(rokuStoreType::st_usb_alarm), m_usbAlarmVideo);
    updateVideolist(getDir(rokuStoreType::st_loc_alarm), m_locAlarmVideo);

    updateVideolist(getDir(rokuStoreType::st_usb_normal), m_usbNormalVideo);
    updateVideolist(getDir(rokuStoreType::st_loc_normal), m_locNormalVideo);                // 默认存本地
    LOG_D("m_usbVideo.size == %d, m_locVideo.size == %d, avDir == %s\n", 
        m_usbNormalVideo.size(), m_locNormalVideo.size(), stDir().c_str());

    /* add by 刘春龙, 2016-03-18, 原因: 未经美的测试，先屏蔽掉 */
    // 更新文件ID值，防止ID值超过max(int). 涉及文件名变化，需重新执行updateVideolist等。
    //update_all_file_ID();

    save_curr_record_file_info();
}

zRokugaThread::~zRokugaThread()
{
    if (gSaveImage)
    {
        gSaveImage = false;
        m_pSaveImageThread->join();
        delete m_pSaveImageThread;
        m_pSaveImageThread = NULL;
    }
    
    m_timer.removeTimer(USB_TEST_TIMERID);
    if (rokuState() & rokuCtrlCode::ROKU_TANINN)
    {
        endTaninnRoku();
    }
    else if (rokuState() & rokuCtrlCode::ROKU) 
    {
        endRoku();
    }

    for (int i=0; i<ENCODED_FRAMES_COUNT; i++)
    {
        zAlgManager::free_encoded_frame(m_frms[i].m_enc_img);
    }
    (void)memset(m_frms, 0, sizeof(m_frms));

    //uninitRokuga();
}

int zRokugaThread::onMsg(const zBaseUtil::zMsg& msg)
{
    LOG_I("zRokugaThread::onMsg == %d \n", msg.msg);
    if (ZM_QUERYVIDEO == msg.msg)
    {
        std::list<file_info_t> listResult;
        std::string strResult;
        zBaseUtil::QueryParam qmsg = *(zBaseUtil::QueryParam*)msg.pparam;
        LOG_D("on query video list top == %d, skip == %d, render == %s, type == %s \n"
            , qmsg.top, qmsg.skip, qmsg.render.c_str(), qmsg.type.c_str());
        {
            std::unique_lock<std::mutex> lock(m_ctrlMutex);
            QueryVideoList(qmsg, listResult);
        }
        videoList2Jsonstr(listResult, strResult);
        LOG_D(" videoList2Jsonstr 0: result == %s, list size == %zu \n", strResult.c_str(), listResult.size());
        sendMessageWebrtc(qmsg.render, strResult);
    }
    else if (ZM_QUERYVIDEO_BASEONTIME == msg.msg)
    {
        std::list<file_info_t> listResult;
        std::string strResult;
        zBaseUtil::QueryParamBaseOnTime qmsg = *(zBaseUtil::QueryParamBaseOnTime*)msg.pparam;
        LOG_D("on query video list bgn_date == %s, end_date == %s, render == %s, type == %s, max_count=%d \n"
            , qmsg.bgn_date.get_date_string().c_str()
            , qmsg.end_date.get_date_string().c_str()
            , qmsg.render.c_str()
            , qmsg.type.c_str(),  qmsg.max_count);
        {
            std::unique_lock<std::mutex> lock(m_ctrlMutex);
            QueryVideoListBaseOnTime(qmsg, listResult);
        }
        videoList2Jsonstr(listResult, strResult);
        LOG_D("videoList2Jsonstr 1: result == %s, list size == %zu \n", strResult.c_str(), listResult.size());
        sendMessageWebrtc(qmsg.render, strResult);
    }
    else
    {
        // 没参数的
        onEvent(msg.msg);
    }

    return 0;
}

static bool SaveImageProc(void* param)
{
    if (NULL == param) return false;
    zRokugaThread* pThread = (zRokugaThread*)param;

    std::int64_t preProc = zBaseUtil::getMilliSecond();
    std::int64_t nElapse = 0;
    int nCnt = SAVE_IMAGE_CNT;
    LOG_D(" begin SaveImageProc at %lld \n", preProc);
    while (pThread->gSaveImage && nCnt > 0)
    {
        std::int64_t nCur = zBaseUtil::getMilliSecond();
        std::int64_t nGap = nCur - preProc;
        nElapse += nGap;

        if (nElapse >= SAVE_IMAGE_GAP)
        {
            nElapse -= SAVE_IMAGE_GAP;
            --nCnt;
            pThread->saveImage();
        }

        preProc = nCur;
    }
    LOG_D(" end SaveImageProc at %lld \n", preProc);
    return true;
}

//static bool SaveImage(void* param)
//{
//  if (NULL == param) return false;
//  zRokugaThread* pThread = (zRokugaThread*)param;
//  pThread->saveImage();
//  return true;
//}


// 不管是结束 陌生人录像 还是普通录像
// 都把当前的oc_清空 在下一帧proc 会因为 filename = "" 新建一个！
int zRokugaThread::onEvent(int nEvent)
{
    //LOG_D("onEvent: befor mutex \n");
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("onEvent: after mutex \n");
    //if (m_ctrState == nEvent) return 0;
    LOG_I("zRokugaThread::onEvent nEvent == %d, ctrState == %d \n", nEvent, m_ctrState);
    if (nEvent == ZM_ROKU
        && !(m_ctrState & rokuCtrlCode::ROKU) 
        && !(m_ctrState & rokuCtrlCode::ROKU_TANINN))
    {
        LOG_D("start record...\n");
        m_ctrState = m_ctrState & (~rokuCtrlCode::STOPED_ROKU);
        m_ctrState = m_ctrState | rokuCtrlCode::ROKU;
        dirChg(stType());
        rokuNewfile();
        m_manual_record_begin_time = zBaseUtil::getMilliSecond();
        LOG_D("end record...\n");
    }

    if (nEvent == ZM_BEGIN_ROKUSTRANGER
        && !(m_ctrState & rokuCtrlCode::ROKU_TANINN) )
    {
        m_nCurTaninnRokuTime = 0;
        //m_ctrState = m_ctrState & ~( rokuCtrlCode::STOPED_ROKU );
        m_ctrState = rokuCtrlCode::ROKU_TANINN;
        //m_rokuTimer.removeTimer(ZM_END_ROKUSTRANGER);
        //if (m_ctrState & rokuCtrlCode::ROKU)
        {
            // end roku 
            LOG_D("start record taninn...\n");
            dirChg(stType());
            rokuNewfile();
            //m_timer.setTimer(SAVE_IMAGE_TIMERID, 1, 10, 300, 0, SaveImage, this);
            gSaveImage = true;
            m_pSaveImageThread = new std::thread(SaveImageProc, this);
            /* add by 刘春龙, 2015-10-31, 原因: 线程分离，否则会产生僵尸线程 */
            m_pSaveImageThread->detach();

            LOG_D("end record taninn...\n");
        }
    }
    if ( nEvent == ZM_END_ROKUSTRANGER)
    {
        // delay 30s and then stop taninn roku
        //m_rokuTimer.setTimer(ZM_END_ROKUSTRANGER, 1, 1, 100, 30 * 1000, endRokutaninn_, this);
        //m_timer.removeTimer(SAVE_IMAGE_TIMERID);
        gSaveImage = false;

        onRokutaninnEnd();
    }
    if (nEvent == ZM_ROKUSTOP)
    {
        if (m_ctrState & rokuCtrlCode::ROKU_TANINN
            & !(m_ctrState & rokuCtrlCode::ROKU))
        {
            endTaninnRoku();
        }
        else
        {
            m_ctrState = m_ctrState & (~rokuCtrlCode::ROKU_TANINN);
        }
        
        if (m_ctrState & rokuCtrlCode::ROKU)
        {
            endRoku();
        }
        m_ctrState = rokuCtrlCode::STOPED_ROKU;
        //uninitRokuga();
    }
    // 时间同步
    if (nEvent == ZM_TIMESYNC)
    {
        //std::uint64_t diff = zWifiConnect::getInstance()->getTimeDiff();
        m_bNeedRename = true;
        if (! (m_ctrState & (rokuCtrlCode::ROKU | rokuCtrlCode::ROKU_TANINN)))
        {
            update_all_filename();
        }
    }
    //m_ctrState = nEvent;
    LOG_I("zRokugaThread::onEvent end \n");
    return 0;
}

zRokugaThread::rokuStoreType zRokugaThread::getDir2Clear()
{
    if (m_usbDir.empty()) return zRokugaThread::rokuStoreType::st_loc;
    if (m_locDir.empty()) return zRokugaThread::rokuStoreType::st_usb;
    if (m_locNormalVideo.empty() && m_locAlarmVideo.empty()) return zRokugaThread::rokuStoreType::st_loc;
    if (m_usbNormalVideo.empty() && m_usbAlarmVideo.empty()) return zRokugaThread::rokuStoreType::st_usb;

    std::string strLocNormal, strLocAlarm;
    std::string strUsbNormal, strUsbAlarm;
    int locId = std::numeric_limits<int>::max();
    int usbId = std::numeric_limits<int>::max();

    if (!m_locNormalVideo.empty())
    {
        std::string strLoc = m_locNormalVideo.front();
        int id = getIDFromName(strLoc);
        if (id < locId) locId = id;
    }

    if (!m_locAlarmVideo.empty())
    {
        std::string strLoc = m_locAlarmVideo.front();
        int id = getIDFromName(strLoc);
        if (id < locId) locId = id;
    }

    if (!m_usbNormalVideo.empty())
    {
        std::string strUsb = m_usbNormalVideo.front();
        int id = getIDFromName(strUsb);
        if (id < usbId) usbId = id;
    }

    if (!m_usbAlarmVideo.empty())
    {
        std::string strUsb = m_usbAlarmVideo.front();
        int id = getIDFromName(strUsb);
        if (id < usbId) usbId = id;
    }

    if (locId < usbId) return zRokugaThread::rokuStoreType::st_loc;
    else return zRokugaThread::rokuStoreType::st_usb;
}

bool zRokugaThread::checkDisk(const std::string& dir)
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    if (    dir.empty()
        ||  !pm->isDirectoryExist(dir))
    {
        return true;
    }

    std::uint64_t sz = pm->getDiskFreeSize(dir);
    LOG_D("free size == %lld \n", sz);
    sz = sz >> 20;
    LOG_D("zRokugaThread::rokuNewfile Free size == %lld, retainsize == %lldM \n", sz, m_retainSize);

    return (static_cast<int>(sz) < m_retainSize);
}

// 新建一个文件
// 清理录制环境相关的东西
// 判断磁盘大小、如果有则清理一半的当前文件
// 获取一个文件名（根据时间获得）
// 初始化一个新的录制环境
// filename == dir + prefix+"_" + time + "_T" or "_F" + "." + suffix
bool zRokugaThread::rokuNewfile()
{
    uninitRokuga();

    bool bFull = checkDisk(stDir());
    if (bFull)
    {
        LOG_D("video_storage_clear enter.\n");
        video_storage_clear();
        LOG_D("video_storage_clear exit.\n");
    }

    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

    // assert free size > m_nRokuTime size
    ++m_latestVideoid;
    char szBuf[256] = { 0 };
    sprintf(szBuf, "%s%d", recording_preffix, m_latestVideoid);

    std::string strDir = stDir();
    std::string filename = pm->getNewFilename(strDir, szBuf, m_strVideoSuffix);
    while (filename.empty())
    {
        memset(szBuf, 0, 256);
        ++m_latestVideoid;
        sprintf(szBuf, "%s%d", recording_preffix, m_latestVideoid);
        filename = pm->getNewFilename(strDir, szBuf, m_strVideoSuffix);
    }
    LOG_D("filename=%s\n", filename.c_str());

    int nTry = 3;
    while (nTry-- && initRokuga(filename) )
    {
        pm->removeFile(filename);
        uninitRokuga();
    }

    return true;
}

void zRokugaThread::addRokuFile(const std::string& filename)
{
    zRokugaThread::rokuStoreType nType = m_curr_record_file_type;

    LOG_D(" addRokuFile %s nType == %d, m_curr_record_file_state=%d, m_ctrState == %d\n", 
        filename.c_str(), nType, (int)m_ctrState, m_curr_record_file_state);
    if (rokuStoreType::st_usb == nType)
    {
        if (m_curr_record_file_state & rokuCtrlCode::ROKU_TANINN)
        {
            //LOG_D(" addRokuFile m_usbAlarmVideo %s", filename.c_str());
            m_usbAlarmVideo.push_back(filename);
        }
        else
        {
            //LOG_D(" addRokuFile m_usbNormalVideo %s", filename.c_str());
            m_usbNormalVideo.push_back(filename);
        }
    }
    else
    {
        if (m_curr_record_file_state & rokuCtrlCode::ROKU_TANINN)
        {
            //LOG_D(" addRokuFile m_locAlarmVideo %s", filename.c_str());
            m_locAlarmVideo.push_back(filename);
        }
        else
        {
            //LOG_D(" addRokuFile m_locNormalVideo %s", filename.c_str());
            m_locNormalVideo.push_back(filename);
        }
    }
}

// 录制录像，根据m_nCurRokuTime 判断当前录制时间
// 超过则新建文件
bool zRokugaThread::roku(std::int64_t nElapse)
{
    int nReturn = 0;
    {
        nReturn = write_video_frame(oc_, &video_st_);
    }

    m_nCurRokuTime += nElapse;
    if (    (m_nCurRokuTime > static_cast<int>(m_nRoku * 1000))
        ||  m_strFileName.empty()
        ||  nReturn)
    {
        // may block if can not find a suitable filename
        LOG_D(" roku new file cur roku time == %lld, file name == %s, write video frame return == %d \n"
            , m_nCurRokuTime, m_strFileName.c_str(), nReturn);
        rokuNewfile();
    }

    //S_H264EncBuf buf = { 0 };
    //bool bResult = m_pManager->getEncFrame(buf, zAlgManager::Threads::ROKUGATHREAD);
    return true;
}

// 录制陌生人
// 如果之前在普通录制则继续用那个文件、能保证陌生人录制完毕后文件关闭
bool zRokugaThread::rokuTaninn(std::int64_t nElapse)
{
    LOG_D("zRokugaThread::rokuTaninn nElapse == %lld\n", nElapse);
    int nReturn = 0;
    {
        nReturn = write_video_frame(oc_, &video_st_);
    }

    m_nCurTaninnRokuTime += nElapse;
    if (    (m_nCurTaninnRokuTime > static_cast<int>(m_nTaninnRoku * 1000))
        ||  nReturn )
    {
        //m_ctrState = m_ctrState & (~rokuCtrlCode::ROKU_TANINN);
        //m_ctrState |= END_ROKU_TANIN;
        LOG_D(" roku new file cur roku time == %lld, file name == %s, write video frame return == %d \n"
            , m_nCurTaninnRokuTime, m_strFileName.c_str(), nReturn);
        rokuNewfile();
    }

    LOG_D("zRokugaThread::rokuTaninn end \n");
    return true;
}

// 反初始化录制相关环境
// 结束普通录制、将当先状态设置为 STOPED_ROKU
bool zRokugaThread::endRoku()
{
    //std::unique_lock<std::mutex> lock(m_ctrlMutex);
    uninitRokuga();
    m_ctrState = rokuCtrlCode::STOPED_ROKU;
    return true;
}

// 反初始化录制相关环境
// 清空陌生人录制相关标志
bool zRokugaThread::endTaninnRoku()
{
    uninitRokuga();
    m_ctrState = m_ctrState & ~(rokuCtrlCode::END_ROKU_TANIN | rokuCtrlCode::ROKU_TANINN);
    m_nCurTaninnRokuTime = 0;

    return true;
}

void zRokugaThread::update_all_filename()
{
    if (m_bNeedRename)
    {
        std::int64_t nGap = zWifiStateThread::getInstance()->getTimeDiff();
        LOG_I("nGap= %lld\n", nGap);

        updateFilename(nGap, rokuStoreType::st_loc_normal);
        updateFilename(nGap, rokuStoreType::st_usb_normal);
        
        updateFilename(nGap, rokuStoreType::st_loc_alarm);
        updateFilename(nGap, rokuStoreType::st_usb_alarm);

        updateFilename(nGap, rokuStoreType::st_loc_image);
        updateFilename(nGap, rokuStoreType::st_usb_image);

        m_bNeedRename = false;
    }
    return;
}

// ro ku 
bool zRokugaThread::doAction(std::int64_t nElapse)
{
    bool bResult = false;
    //LOG_D("doAction: befor mutex \n");
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("doAction: after mutex \n");
    if (m_ctrState & rokuCtrlCode::ROKU_TANINN)
    {
        bResult = rokuTaninn(nElapse);
    }
//    else if (m_ctrState & rokuCtrlCode::END_ROKU_TANIN)
//    {
//        endTaninnRoku();
//    }
    else if (m_ctrState & rokuCtrlCode::ROKU)
    {
        std::int64_t curr_time = zBaseUtil::getMilliSecond();
        if ((curr_time - m_manual_record_begin_time) >= m_manual_record_max_duration)
        {
            bResult = endRoku(); // 强制停止手动录像
        }
        else
        {
            bResult = roku(nElapse);
        }
    }
    else if (m_ctrState & rokuCtrlCode::STOPED_ROKU)
    {
        //
    }

    return bResult;
}

void zRokugaThread::proc()
{
    std::int64_t cur = zBaseUtil::getMilliSecond();
    m_preFrame = (0 == m_preFrame) ? cur : m_preFrame;    // for first time
    std::int64_t nElapse = cur - m_preFrame;
    m_preFrame = cur;
    
    LOG_D("befor m_timer.elapse \n");
    m_timer.elapse(nElapse);
    //LOG_D("after m_timer.elapse == %lld \n", zBaseUtil::getMilliSecond() - cur);

    //LOG_D("befor doAction \n");
    (void)doAction(nElapse);
    //LOG_D("after doAction == %lld \n", zBaseUtil::getMilliSecond() - cur);

    std::int64_t cur2 = zBaseUtil::getMilliSecond();
    std::int64_t nGap = cur2 - cur;

/*
    LOG_D("m_preFrame=%lld, nGap=%lld, nElapse=%lld, ROKUGAG_FRAME_GA=%d\n ",
        m_preFrame, nGap, nElapse, ROKUGAG_FRAME_GAP);
*/
    const int64_t ROKUGAG_GET_FRAME_GAP = ROKUGAG_FRAME_GAP - 20;
    if (ROKUGAG_GET_FRAME_GAP > (nGap))
    {
        //LOG_D("will sleep %lld \n", ROKUGAG_FRAME_GAP - nGap);
        std::this_thread::sleep_for(std::chrono::milliseconds(ROKUGAG_GET_FRAME_GAP - nGap));        // 12
    }
}

int zRokugaThread::rokuState()
{
    if (NULL == this) return -1;
    //LOG_D("rokuState: befor mutex \n");
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("rokuState: after mutex \n");
    if (m_ctrState & rokuCtrlCode::ROKU_TANINN) return 2;
    else if (m_ctrState & rokuCtrlCode::ROKU) return 1;
    return 0;
}

void zRokugaThread::onRokutaninnEnd()
{
    if (NULL == this) return ;
    //std::unique_lock<std::mutex> lock(m_ctrlMutex);
    endTaninnRoku();
}

// gap secs
static std::string replaceTimeStr(const std::string& str, std::int64_t withGap)
{
    struct tm tp = { 0 };
    sscanf(str.c_str(), "%d-%02d-%02d_%02d-%02d-%02d",
        &tp.tm_year, &tp.tm_mon, &tp.tm_mday,
        &tp.tm_hour, &tp.tm_min, &tp.tm_sec);
    tp.tm_year -= 1900;
    tp.tm_mon -= 1;
    time_t tt = mktime(&tp);
    tt += withGap;

    struct tm* tmTmp = localtime(&tt);
    char date[60] = { 0 };
    sprintf(date, "%d-%02d-%02d_%02d-%02d-%02d",
        tmTmp->tm_year + 1900, tmTmp->tm_mon + 1, tmTmp->tm_mday,
        tmTmp->tm_hour, tmTmp->tm_min, tmTmp->tm_sec);
    
    return std::string(date);
}

void zRokugaThread::dirChg(zRokugaThread::rokuStoreType nType)
{
    //if (m_curRokuType == nType) return;taninn

    m_curRokuType = nType;
    std::string rootDir;
    std::string subDir = "normal/";
    if (zRokugaThread::rokuStoreType::st_default == nType)
    {
        rootDir = m_locDir;
    }
    else if (zRokugaThread::rokuStoreType::st_loc == nType)
    {
        rootDir = m_locDir;
    }
    else if (zRokugaThread::rokuStoreType::st_usb == nType)
    {
        rootDir = m_usbDir;
    }

    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

    m_curDir = rootDir;
    {
        if (m_ctrState & rokuCtrlCode::ROKU_TANINN)
            subDir = "alarm/";
        else subDir = "normal/";
        if (!pm->appendDirectory(m_curDir, subDir))
            LOG_E("Error: cannot append %s directory with %s \n", m_curDir.c_str(), subDir.c_str());

        // try add alarmImage dir
        std::string tmp_dir = rootDir;
        if (!pm->appendDirectory(tmp_dir, "alarmImage/"))
            LOG_E("Error: cannot append %s directory with %s \n", rootDir.c_str(), subDir.c_str());

        /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
        tmp_dir = rootDir;
        if (!pm->appendDirectory(tmp_dir, "gestureImage/"))
            LOG_E("Error: cannot append %s directory with %s \n", rootDir.c_str(), subDir.c_str());
    }
    
    LOG_D("dirChg m_curDir == %s nType == %d m_latestId == %d\n", m_curDir.c_str(), nType, m_latestVideoid);
}

zRokugaThread::rokuStoreType zRokugaThread::stType()
{
    return m_curRokuType;
}
std::string zRokugaThread::stDir()
{
    return m_curDir;
}

std::string zRokugaThread::getDir(zRokugaThread::rokuStoreType nType)
{
    std::string dir = "";
    if (rokuStoreType::st_default == nType)
    {
        dir = m_locDir;
    }
    else if (rokuStoreType::st_loc == nType)
    {
        dir = m_locDir;
    }
    else if (rokuStoreType::st_usb == nType)
    {
        dir = m_usbDir;
    }
    //else if (rokuStoreType::st_image == nType)
    //{
    //  dir = m_alarmImgDir;
    //}
    else if (rokuStoreType::st_loc_normal == nType)
    {
        if (!m_locDir.empty()) dir = m_locDir + "normal/";
    }
    else if (rokuStoreType::st_loc_alarm == nType)
    {
        if (!m_locDir.empty()) dir = m_locDir + "alarm/";
    }
    else if (rokuStoreType::st_loc_image == nType)
    {
        if (!m_locDir.empty()) dir = m_locDir + "alarmImage/";
    }
    else if (rokuStoreType::st_loc_gesture_image == nType)
    {
        /* add by 刘春龙, 2015-10-31, 原因: 手势图片*/
        if (!m_locDir.empty()) dir = m_locDir + "gestureImage/";
    }
    else if (rokuStoreType::st_usb_normal == nType)
    {
        if (!m_usbDir.empty()) dir = m_usbDir + "normal/";
    }
    else if (rokuStoreType::st_usb_alarm == nType)
    {
        if (!m_usbDir.empty()) dir = m_usbDir + "alarm/";
    }
    else if (rokuStoreType::st_usb_image == nType)
    {
        if (!m_usbDir.empty()) dir = m_usbDir + "alarmImage/";
    }
    else if (rokuStoreType::st_usb_gesture_image == nType)
    {
        /* add by 刘春龙, 2015-10-31, 原因: 手势图片*/
        if (!m_usbDir.empty()) dir = m_usbDir + "gestureImage/";
    }

    return dir;
}

std::string zRokugaThread::setDir(const std::string& dir, zRokugaThread::rokuStoreType nType)
{
    //zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    //if (dir.empty()
    //  || !pm->isDirectoryExist(dir)) return "";
    LOG_D("webrtc setDir %d == %s \n", nType, dir.c_str());
    std::string oldDir;
    if (zRokugaThread::rokuStoreType::st_default == nType)
    {
        oldDir = m_locDir;
        m_locDir = dir;
    }
    else if (zRokugaThread::rokuStoreType::st_loc == nType)
    {
        oldDir = m_locDir;
        m_locDir = dir;
    }
    else if (zRokugaThread::rokuStoreType::st_usb == nType)
    {
        oldDir = m_usbDir;
        m_usbDir = dir;
    }
    //else if (rokuStoreType::st_image == nType)
    //{
    //  oldDir = m_alarmImgDir;
    //  m_alarmImgDir = dir;
    //}
    return oldDir;
}

//int zRokugaThread::getLatestId(std::list<std::string>& list)
//{
//  int nID = 0;
//  if (!list.empty())
//  {
//      list.sort(FileNameIDCmp);
//      std::string  strid = list.back();
//      // file name id_time.suffix
//      int id = getIDFromName(strid);
//      if (-1 != id)
//      {
//          nID = id;
//      }
//  }
//  return nID;
//}

int zRokugaThread::updateImageList(
    std::string rootDir, std::list<std::string>& list, int& img_id)
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

    if (rootDir.empty()
        || !pm->isDirectoryExist(rootDir)) return 0;
    pm->EnumDirFiles(pm->dir2Path(rootDir), list, IMAGE_SUFFIX);

    if (!list.empty())
    {
        list.sort(FileNameIDCmp);
        remove_null_files(rootDir, list);

        if (!list.empty())
        {
            std::string  strid = list.back();
            // file name id_time.suffix
            int id = getIDFromName(strid);
            LOG_D("updateImageList id = getIDFromName(strid) == %d \n", id);
            if ((id >= 0) && (id > img_id))
            {
                img_id = id;
            }
        }
    }

    return list.size();
}

int zRokugaThread::updateVideolist(std::string rootDir, std::list<std::string>& list)
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    if (rootDir.empty()
        || !pm->isDirectoryExist(rootDir)) return 0;
    pm->EnumDirFiles(pm->dir2Path(rootDir), list, m_strVideoSuffix);
    
    //std::sort(m_listVideo.begin(), m_listVideo.end(), FileNameCmp);
    if (!list.empty())
    {
         std::list<std::string>::iterator iter = list.begin();
         while(iter != list.end())
         {
            size_t nPos = iter->find(recording_preffix);
            if (nPos != std::string::npos)
            {
                pm->removeFile(rootDir + *iter);
                list.erase(iter++);
            }else ++iter;
        }

        #if 0
        for(iter=list.begin(); iter != list.end(); iter++)
        {
            LOG_D("videolist: %s\n", iter->c_str());
        }
        #endif

        list.sort(FileNameIDCmp);
        remove_null_files(rootDir, list);

        if (!list.empty())
        {
            std::string  strid = list.back();
            // file name id_time.suffix
            int id = getIDFromName(strid);
            LOG_D(" id = getIDFromName(strid) == %d \n", id);
            if (id > m_latestVideoid) m_latestVideoid = id;
        }
    }

    LOG_D("updateFilelist rootDir = %s nCnt == %d \n", rootDir.c_str(), list.size());
    return list.size();
}

// filename == dir + prefix+"_" + time + "_T" or "_F" + "." + suffix
int zRokugaThread::updateFilename( std::int64_t nGap, rokuStoreType type)
{
    // 添加文件重命名策略
    std::list<std::string>* list = nullptr;
    if ( rokuStoreType::st_loc_normal == type)
    {
        list = &m_locNormalVideo;
    }
    else if (rokuStoreType::st_loc_alarm == type)
    {
        list = &m_locAlarmVideo;
    }
    else if (rokuStoreType::st_usb_normal == type)
    {
        list = &m_usbNormalVideo;
    }
    else if (rokuStoreType::st_usb_alarm == type)
    {
        list = &m_usbAlarmVideo;
    }
    //else if (rokuStoreType::st_image == type)
    //{
    //  list = &m_alarmImage;
    //}
    else if (rokuStoreType::st_loc_image == type)
    {
        list = &m_locAlarmImage;
    }
    else if (rokuStoreType::st_usb_image == type)
    {
        list = &m_usbAlarmImage;
    }
    else
    {
        LOG_E("unkonwn rename type \n");
    }
    
    if (list->empty()) 
    {
        return 0;
    }

    int nCnt = 0;
    std::string oldName;
    std::string name;
    std::string suffix = (rokuStoreType::st_loc_image == type 
        || rokuStoreType::st_usb_image == type) ? IMAGE_SUFFIX : m_strVideoSuffix;

    // 最后一个文件在读写、、、
    for (auto it = list->begin(); it != list->end(); it++)
    {
        oldName = *it;
        int nPos = oldName.length() - 1 - ( suffix.empty() ? 0 : (suffix.length() + 1));
        char chFlag = oldName.at(nPos);
        if ('T' == chFlag) continue;

        std::string strTm = getTimeFromeName(oldName);
        std::string strId = getStridFromName(oldName);
        //LOG_D("oldName=%s, strTm=%s, strId=%s, chFlag=%c\n", oldName.c_str(), strTm.c_str(), strId.c_str(), chFlag);
        name = strId + "_" + replaceTimeStr(strTm, nGap) + "_T";
        if (!suffix.empty())
        {
            name = name + "." + suffix;
        }

        if (oldName != name)
        {
            LOG_D("line=%d, oldName=%s, name=%s, nGap=%lld\n", __LINE__, oldName.c_str(), name.c_str(), nGap);
            zFileUtil::zFileManager::getInstance()->renameFile(getDir(type), oldName, name);

            *it = name; // 修改队列中的文件名
            ++nCnt;
        }
    }

    return nCnt;
}

int zRokugaThread::video2Jsonstr(std::string& str, int nTop /* = 10 */, int nSkip /* = 1 */)
{
    std::list<std::string>& list = (zRokugaThread::rokuStoreType::st_usb == stType()) ? m_usbNormalVideo : m_locNormalVideo;
    LOG_D("zRokugaThread::video2Jsonstr 001, m_usbVideo.size == %d, m_locVideo.size == %d stType == %d \n"
        , m_usbNormalVideo.size(), m_locNormalVideo.size(),(int)stType());
    int n = rokuState();
    LOG_D("%d %d %d\n", n, nTop, nSkip);
    n = n > 0 ? 1 : 0;
    if (static_cast<int>(list.size()) < (1+n+nSkip))
    {
        str = "{ \"act\":\"video_query\", \"videos\":[  ] }";
        return 0;
    }

    //////////////////////////////////////////////////////////////////////////
    int nCnt = n + nSkip;
    std::list<std::string>::reverse_iterator it = list.rbegin();
    if (n > 0)
    {
        it++;               // skip the roku file
    }
    while (nCnt--) it++;
    //assert(!nCnt);
    nCnt = 0;
    //////////////////////////////////////////////////////////////////////////
    std::string strJson = "{ \"act\":\"video_query\", \"videos\":[ ";

    while (it != list.rend()
        && nCnt < nTop)
    {
        std::string filename = *it;

        std::string strid = getStridFromName(filename);

        std::string strTime = getTimeFromeName(filename);

        if (strid.empty()) continue;;
        strJson += "{ \"id\":\""+ strid + "\",\"time\": \"" + strTime + "\", \"filename\":\""+filename +"\" },";

        ++it;
        ++nCnt;
    }
    strJson = strJson.substr(0, strJson.length() - 1);

    strJson += " ] }";
    str = strJson;
    return nCnt;
}

//////////////////////////////////////////////////////////////////////////
time_t zRokugaThread::str2time(std::string str)
{
    time_t result = 0;
    std::string strTime = getTimeFromeName(str);
    if (strTime.empty()) return result;

    struct tm tp = { 0 };
    sscanf(str.c_str(), "%d-%02d-%02d_%02d-%02d-%02d",
        &tp.tm_year, &tp.tm_mon, &tp.tm_mday,
        &tp.tm_hour, &tp.tm_min, &tp.tm_sec);
    tp.tm_year -= 1900;
    tp.tm_mon -= 1;
    result = mktime(&tp);

    return result;
}

void zRokugaThread::findAndTryUSB()
{
    char mountpoint[255] = { 0 };
    char label[] = "usbhost1";
    LOG_D(" before get_mountpoint \n");
    if (zBaseUtil::get_mountpoint(label, mountpoint) == 0) 
    {
        LOG_D("usb_demo2: Mountpoint of %s is %s\n", label, mountpoint);

        //val = mountpoint;
        zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
        std::string usbRootDir = pm->path2Dir(mountpoint);
        std::string oldDir = setDir(usbRootDir, rokuStoreType::st_usb);
        if (usbRootDir != oldDir)
        {
            /* add by 刘春龙, 2015-10-31, 原因: 加锁 */
            std::unique_lock<std::mutex> lock(m_ctrlMutex);

            dirChg(zRokugaThread::rokuStoreType::st_usb);
            // clear usb list
            m_usbNormalVideo.clear();
            m_usbAlarmVideo.clear();
            m_usbAlarmImage.clear();
            // update
            updateImageList(getDir(rokuStoreType::st_usb_image), m_usbAlarmImage, m_latestImageid);
            /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
            (void)updateImageList(getDir(rokuStoreType::st_usb_gesture_image), 
                m_usb_gesture_image_list, m_latest_gesture_image_id);

            updateVideolist(getDir(rokuStoreType::st_usb_alarm), m_usbAlarmVideo);
            updateVideolist(getDir(rokuStoreType::st_usb_normal), m_usbNormalVideo);
        }
    }
    else 
    {
        if (rokuStoreType::st_usb == stType())
        {
            setDir("", rokuStoreType::st_usb);
            dirChg(rokuStoreType::st_loc);
            // clear usb list
            m_usbNormalVideo.clear();
            m_usbAlarmVideo.clear();
            m_usbAlarmImage.clear();
        }
        LOG_D("usb_demo2: %s has not been mounted\n", label);
    }
    //m_nNeedSearch = USB_TEST_FREQUENCY;
}

std::list<std::string> zRokugaThread::QueryVideoListByTime(time_t from, time_t to)
{
    std::list<std::string> result;
    std::list<std::string>::iterator itFirst;
    std::list<std::string>::iterator itLast;

    itFirst = lower_bound(m_locNormalVideo.begin(), m_locNormalVideo.end(), from);
    itLast = upper_bound(m_locNormalVideo.begin(), m_locNormalVideo.end(), to);
    result.insert(result.end(), itFirst, itLast);

    itFirst = lower_bound(m_usbNormalVideo.begin(), m_usbNormalVideo.end(), from);
    itLast = upper_bound(m_usbNormalVideo.begin(), m_usbNormalVideo.end(), to);
    result.insert(result.end(), itFirst, itLast);
    return result;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.fill_file_list
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月8日
 * 函数功能  : 填充文件信息列表
 * 输入参数  : std::list<std::string>& filename_list   文件列表
               FILE_TYPE_E file_type                   文件类型
               rokuStoreType store_type                存储类型
               std::list<file_info_t>& file_info_list  文件信息列表
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::fill_file_list(
    std::list<std::string>& filename_list, 
    FILE_TYPE_E file_type, 
    rokuStoreType store_type,
    std::list<file_info_t>& file_info_list)
{
    std::list<std::string>::iterator it = filename_list.begin();
    while (it != filename_list.end())
    {
        if (getSyncFromName(*it))
        {
            file_info_t file_info;
            file_info.file_type = file_type;
            file_info.store_type = store_type;
            file_info.file_name = *it;

            file_info_list.push_back(file_info);
        }
        else
        {
            // 未同步的文件不查询
            LOG_D("filename=%s\n", it->c_str());
        }
        it++;
    }
    return;
}

// 将查询结果，存入result队列中。删除掉文件大小为0的文件。
void zRokugaThread::reverse_query(
    std::reverse_iterator<std::list<file_info_t>::iterator> r_pos,
    std::reverse_iterator<std::list<file_info_t>::iterator> r_end,
    int max_count, 
    std::list<file_info_t>& file_info_list)
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    int count = 0;
    for ( ;(r_pos != r_end) && (count < max_count); ++r_pos)
    {
        std::string file_path = getDir(r_pos->store_type);
        file_path += r_pos->file_name;
        int file_size = pm->getFileSize(file_path);
        if (file_size <= 0)
        {
            pm->removeFile(file_path); // 删除文件
            std::list<std::string>* list = getList(r_pos->store_type);
            if (NULL != list)
            {
                list->remove(r_pos->file_name); // 更新文件列表
            }
            else
            {
                LOG_E("list is NULL. store_type=%d\n", r_pos->store_type);
            }
            LOG_D("file_path=%s, file_size=%d\n", file_path.c_str(), file_size);
        }
        else
        {
            file_info_list.push_front(*r_pos);
            ++count;
        }
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.get_list
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年6月28日
 * 函数功能  : 获取录像文件链表
 * 输入参数  : const zBaseUtil::QueryParam& param  参数类型等
               std::list<file_info_t> &temp_list   链表
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::get_list(
    const std::string &type, std::list<file_info_t> &temp_list)
{
    if ("video_alarm" == type)
    {
        if( !m_locAlarmVideo.empty())
        {
            fill_file_list(m_locAlarmVideo, VIDEO_ALARM, rokuStoreType::st_loc_alarm, temp_list);
        }

        if (!m_usbAlarmVideo.empty()) 
        {   
            fill_file_list(m_usbAlarmVideo, VIDEO_ALARM, rokuStoreType::st_usb_alarm, temp_list);
        }
    }
    else if ("video_normal" == type)
    {
        if (!m_locNormalVideo.empty()) 
        {
            fill_file_list(m_locNormalVideo, VIDEO_NORMAL, rokuStoreType::st_loc_normal, temp_list);
        }
        
        if (!m_usbNormalVideo.empty()) 
        {
            fill_file_list(m_usbNormalVideo, VIDEO_NORMAL, rokuStoreType::st_usb_normal, temp_list);
        }
    }
    else if ("image" == type)
    {
        //LOG_D(" QueryVideoList image size == %zu \n ", m_alarmImage.size());
        if (!m_locAlarmImage.empty()) 
        {
            fill_file_list(m_locAlarmImage, IMAGE_NORMAL, rokuStoreType::st_loc_image, temp_list);
        }
        
        if (!m_usbAlarmImage.empty())
        {
            fill_file_list(m_usbAlarmImage, IMAGE_NORMAL, rokuStoreType::st_usb_image, temp_list);
        }
    }
    else if ("gesture_image" == type)
    {
        /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
        if (!m_local_gesture_image_list.empty()) 
        {
            fill_file_list(m_local_gesture_image_list, IMAGE_NORMAL, 
                rokuStoreType::st_loc_gesture_image, temp_list);
        }
        
        if (!m_usb_gesture_image_list.empty()) 
        {
            fill_file_list(m_usb_gesture_image_list, IMAGE_NORMAL, 
                rokuStoreType::st_usb_gesture_image, temp_list);
        }
    }
    else if ("video_all" == type || type.empty())
    {
        if( !m_locAlarmVideo.empty()) 
        {
            fill_file_list(m_locAlarmVideo, VIDEO_ALARM, rokuStoreType::st_loc_alarm, temp_list);
        }
        
        if (!m_usbAlarmVideo.empty()) 
        {   
            fill_file_list(m_usbAlarmVideo, VIDEO_ALARM, rokuStoreType::st_usb_alarm, temp_list);
        }

        if (!m_locNormalVideo.empty()) 
        {
            fill_file_list(m_locNormalVideo, VIDEO_NORMAL, rokuStoreType::st_loc_normal, temp_list);
        }
        
        if (!m_usbNormalVideo.empty()) 
        {
            fill_file_list(m_usbNormalVideo, VIDEO_NORMAL, rokuStoreType::st_usb_normal, temp_list);
        }
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.QueryVideoList
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月8日
 * 函数功能  : 获取文件信息列表
 * 输入参数  : const zBaseUtil::QueryParam& param  查询参数
               std::list<file_info_t>& list        输出文件信息列表
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::QueryVideoList(
    const zBaseUtil::QueryParam& param, std::list<file_info_t>& file_info_list)
{
    file_info_list.clear();
    // 不需要查询了, 返回结果空。
    if (param.top <= 0)
    {
        return;
    }

    std::list<file_info_t> temp_list;
    get_list(param.type, temp_list);
    for(std::list<file_info_t>::iterator it = temp_list.begin(); it != temp_list.end(); it++) {
    	LOG_I("webrtc %s\n", (*it).file_name.c_str());
    }
    //LOG_D("temp_list size=%d\n", temp_list.size());
    if (temp_list.empty())
    {
        LOG_I("webrtc temp_list.empty()\n");
        return;
    }

    // 按文件ID 从小到大排列。
    temp_list.sort(FileNameIDCmp2);

    // 二分算法，查找第一个大于等于ID值的位置。temp_list队列已按ID从小到大排序。
    std::list<file_info_t>::iterator low;
    if (param.id < 0)
    {
        // 没有找到ID值，或接受的ID值非法，或为空，从最后一个有效的值开始查询。
        low = temp_list.end();
        std::advance(low, -1);
        //LOG_D("filename = %s\n", low->file_name.c_str());
    }
    else
    {
        // 查询第一个大于等于 param.id 的文件位置。
        low = std::lower_bound (temp_list.begin(), temp_list.end(), param.id, 
             [](const file_info_t &info, int id) {return (getIDFromName(info.file_name) < id);});

        if (low == temp_list.end())
        {
            // 没有找到ID值，或接受的ID值非法，从最后一个有效的值开始查询。
            std::advance(low, -1); 
        }
        //LOG_D("filename = %s\n", low->file_name.c_str());
    }

    int left_dist = std::distance(temp_list.begin(), low);  // 到链表头的距离
    int right_dist = std::distance(low, temp_list.end());   // 到链表尾的距离
    //LOG_D("left_dist=%d, right_dist=%d\n", left_dist, right_dist);

    typedef std::list<file_info_t>::iterator iter_type;
    std::reverse_iterator<iter_type> r_pos;

    int skip = param.skip;
    if (skip < 0 && -skip >= right_dist)
    {
        // skip为负数时，向后跳转，如果超过队列尾，从尾部第一个有效值开始查询。
        r_pos = temp_list.rbegin();
    }
    else if (skip > 0 && skip > left_dist)
    {
        // skip为正数时，向前跳转。如果超过队列头，设置为反向队列尾。相当于查询结果为空。
        r_pos = temp_list.rend();
    }
    else
    {
        // 反向队列，跳转到指定的位置，开始查询。
        r_pos = std::list<file_info_t>::reverse_iterator(++low);
        std::advance(r_pos, skip); // r_pos += skip;
    }

    // 将查询结果，存入result队列中。删除掉文件大小为0的文件。
    reverse_query(r_pos, temp_list.rend(), param.top, file_info_list);

    LOG_D("query size = %d\n", file_info_list.size());
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.QueryVideoListBaseOnTime
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年6月29日
 * 函数功能  : 按时间段，查询录像文件
 * 输入参数  : const zBaseUtil::QueryParamBaseOnTime& param  参数
               std::list<file_info_t>& file_info_list        结果
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::QueryVideoListBaseOnTime(
    const zBaseUtil::QueryParamBaseOnTime& param, std::list<file_info_t>& file_info_list)
{
    file_info_list.clear();
    // 不需要查询了, 返回结果空。
    bool bgn_valid = param.bgn_date.is_valid();
    bool end_valid = param.end_date.is_valid();
    if (    (!bgn_valid && !end_valid)
        ||  (bgn_valid && end_valid && (param.bgn_date.compare(param.end_date) > 0))
        ||  (0 == param.max_count))
    {
        return;
    }

    std::list<file_info_t> temp_list;
    get_list(param.type, temp_list);
    //LOG_D("temp_list size=%d\n", temp_list.size());
    if (temp_list.empty())
    {
        return;
    }

    // 按文件ID 从小到大排列。
    temp_list.sort(FileNameIDCmp2);

    // 二分算法，查找第一个大于等于ID值的位置。temp_list队列已按ID从小到大排序。
    std::list<file_info_t>::iterator bgn_it = temp_list.begin();
    std::list<file_info_t>::iterator end_it = temp_list.end();

    // 日期比较函数
    auto f_comp = [](const file_info_t &info, const RW::file_date &date)
    {
        RW::file_date d( getTimeFromeName(info.file_name));
        return (d.compare(date) < 0);
    };

    if (bgn_valid)
    {
        // 查询第一个大于等于 param.bgn_date 的文件位置。
        bgn_it = std::lower_bound (temp_list.begin(), temp_list.end(), param.bgn_date, f_comp);
        if (bgn_it == temp_list.end())
        {
            // 起始位置，超过队列尾，直接返回。
            return;
        }
    }

    if (end_valid)
    {
        end_it = std::lower_bound (temp_list.begin(), temp_list.end(), param.end_date, f_comp);
    }

    // 注意: 查找范围为[bgn_date, end_date).
    typedef std::list<file_info_t>::iterator iter_type;
    std::reverse_iterator<iter_type> r_pos = std::list<file_info_t>::reverse_iterator(end_it);
    std::reverse_iterator<iter_type> r_end = std::list<file_info_t>::reverse_iterator(bgn_it);

    //LOG_D("r_pos: filename = %s\n", r_pos->file_name.c_str());
    //LOG_D("r_end: filename = %s\n", r_end->file_name.c_str());

    // 如果max_count 小于0，查询[bgn_date, end_date)范围内的所有文件。
    int max_count = param.max_count;
    if (max_count < 0) 
    {
        max_count = temp_list.size();
    }

    // 将查询结果，存入result队列中。删除掉文件大小为0的文件。
    reverse_query(r_pos, r_end, max_count, file_info_list);

    LOG_D("query size = %d\n", file_info_list.size());
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.videoList2Jsonstr
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月8日
 * 函数功能  : 转化文件信息列表，为JSON串
 * 输入参数  : const std::list<file_info_t>& list  文件信息列表
               std::string& str                    输出，json格式
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::videoList2Jsonstr(const std::list<file_info_t>& list, std::string& str)
{
    LOG_D(" videoList2Jsonstr: list.size == %u \n", list.size());
    if (list.size() < 1)
    {
        str = "{ \"act\":\"file_query\", \"files\":[  ] }";
        return ;
    }

    std::list<file_info_t>::const_reverse_iterator it = list.rbegin();
    std::string strJson = "{ \"act\":\"file_query\", \"files\":[ ";

    while (it != list.rend())
    {
        std::string filename = it->file_name;
        std::string filetype = file_type_string_table[it->file_type];
        ++it;

        std::string strid = getStridFromName(filename);

        std::string strTime = getTimeFromeName(filename);

        if (strid.empty()) continue;;
        strJson += "{ \"id\":\"" + strid + "\",\"time\": \"" + strTime + "\", \"filename\":\"" + filename + "\",\"type\": \"" + filetype + "\" },";

    }
    strJson = strJson.substr(0, strJson.length() - 1);

    strJson += " ] }";
    str = strJson;
    return;
}

//bool zRokugaThread::removeVideo(std::string str)
//{
//  std::list<std::string>::iterator it;
//  zFileUtil::zFileManager* fm = zFileUtil::zFileManager::getInstance();
//
//  removeListVideo(m_locNormalVideo, str);
//  removeListVideo(m_locAlarmVideo, str);
//
//  if (!getDir(rokuStoreType::st_usb).empty())
//  {
//      removeListVideo(m_usbNormalVideo, str);
//      removeListVideo(m_usbAlarmVideo, str);
//  }
//
//  return true;
//}

//bool zRokugaThread::removeListVideo(std::list<std::string>& list, const std::string& str)
//{
//  std::list<std::string>::iterator it;
//  zFileUtil::zFileManager* fm = zFileUtil::zFileManager::getInstance();
//  do
//  {
//      it = std::find(list.begin(), list.end(), str);
//      if (it == list.end()) break;
//      fm->removeFile(getDir(rokuStoreType::st_loc) + str);
//      it = list.erase(it);
//  } while (it != list.end());
//  
//  return true;
//}

bool zRokugaThread::findFile(const std::string& fileName, std::string& fullPath)
{
    if (NULL == this) return false;
    //LOG_D("findFile: befor mutex \n");
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("findFile: after mutex \n");
    bool result = true;

    do 
    {
        std::string strLowerCasePath(fileName);
        for (unsigned int i = 0; i < strLowerCasePath.length(); ++i)
        {
            strLowerCasePath[i] = tolower(fileName[i]);
        }

        if (    std::string::npos != strLowerCasePath.find(".png")
            ||  std::string::npos != strLowerCasePath.find(".bmp")
            ||  std::string::npos != strLowerCasePath.find(".jpg"))
        {
            //if (std::find(m_alarmImage.begin(), m_alarmImage.end()
            //  , fileName) != m_alarmImage.end())
            //{
            //  fullPath = getDir(rokuStoreType::st_image) + fileName;
            //}
            //else result = false;
            if (std::find(m_locAlarmImage.begin(), m_locAlarmImage.end()
                , fileName) != m_locAlarmImage.end())
            {
                fullPath = getDir(rokuStoreType::st_loc_image) + fileName;
            }
            else if (std::find(m_usbAlarmImage.begin(), m_usbAlarmImage.end()
                , fileName) != m_usbAlarmImage.end())
            {
                fullPath = getDir(rokuStoreType::st_usb_image) + fileName;
            }
            else result = false;
            break;
        }

        if (std::find(m_locNormalVideo.begin(), m_locNormalVideo.end()
            , fileName) != m_locNormalVideo.end()) fullPath = getDir(rokuStoreType::st_loc_normal) + fileName;
        else if (std::find(m_locAlarmVideo.begin(), m_locAlarmVideo.end()
            , fileName) != m_locAlarmVideo.end()) fullPath = getDir(rokuStoreType::st_loc_alarm) + fileName;
        else if (std::find(m_usbNormalVideo.begin(), m_usbNormalVideo.end()
            , fileName) != m_usbNormalVideo.end()) fullPath = getDir(rokuStoreType::st_usb_normal) + fileName;
        else if (std::find(m_usbAlarmVideo.begin(), m_usbAlarmVideo.end()
            , fileName) != m_usbAlarmVideo.end()) fullPath = getDir(rokuStoreType::st_usb_alarm) + fileName;
        else result = false;
    } while (false);
    return result;
}

bool zRokugaThread::removeFile_(const std::string& fileName)
{
    if (NULL == this) return false;
    //LOG_D("removeFile_: befor mutex \n");
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("removeFile_: after mutex \n");
    std::list<std::string>::iterator it;
    std::string fullPath;
    bool result = true;

    do
    {
        zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

        std::string strLowerCasePath(fileName);
        for (unsigned int i = 0; i < strLowerCasePath.length(); ++i)
        {
            strLowerCasePath[i] = tolower(fileName[i]);
        }

        if (    std::string::npos != strLowerCasePath.find(".png")
            ||  std::string::npos != strLowerCasePath.find(".bmp")
            ||  std::string::npos != strLowerCasePath.find(".jpg"))
        {
            //if ((it = std::find(m_alarmImage.begin(), m_alarmImage.end()
            //  , fileName)) != m_alarmImage.end())
            //{
            //  result = pm->removeFile(getDir(rokuStoreType::st_image) + fileName);
            //  if(result) it = m_alarmImage.erase(it);
            //}
            //else result = false;
            if ((it = std::find(m_locAlarmImage.begin(), m_locAlarmImage.end()
                , fileName)) != m_locAlarmImage.end())
            {
                result = pm->removeFile(getDir(rokuStoreType::st_loc_image) + fileName);
                if (result) it = m_locAlarmImage.erase(it);
            }
            else if ((it = std::find(m_usbAlarmImage.begin(), m_usbAlarmImage.end()
                , fileName)) != m_usbAlarmImage.end())
            {
                result = pm->removeFile(getDir(rokuStoreType::st_usb_image) + fileName);
                if (result) it = m_usbAlarmImage.erase(it);
            }
            else result = false;
            break;
        }

        if ((it = std::find(m_locNormalVideo.begin(), m_locNormalVideo.end()
            , fileName)) != m_locNormalVideo.end())
        {
            result = pm->removeFile(getDir(rokuStoreType::st_loc_normal) + fileName);
            if (result) it = m_locNormalVideo.erase(it);
        }
        else if ((it = std::find(m_locAlarmVideo.begin(), m_locAlarmVideo.end()
            , fileName)) != m_locAlarmVideo.end())
        {
            result = pm->removeFile(getDir(rokuStoreType::st_loc_alarm) + fileName);
            if (result) it = m_locNormalVideo.erase(it);
        }

        else if ((it = std::find(m_usbNormalVideo.begin(), m_usbNormalVideo.end()
            , fileName)) != m_usbNormalVideo.end())
        {
            result = pm->removeFile(getDir(rokuStoreType::st_usb_normal) + fileName);
            if (result) it = m_locNormalVideo.erase(it);
        }
        else if ((it = std::find(m_usbAlarmVideo.begin(), m_usbAlarmVideo.end()
            , fileName)) != m_usbAlarmVideo.end())
        {
            result = pm->removeFile(getDir(rokuStoreType::st_usb_alarm)+fileName);
            if(result) it = m_locNormalVideo.erase(it);
        }
        else result = false;
    } while (false);

    return result;
}


bool zRokugaThread::downloadFile(const zBaseUtil::DownloadParam& param)
{
    bool result = false;
    if (NULL == this) return result;
    std::string fullPath;
    LOG_I("webrtc:downloadFile %s \n ", param.filename.c_str());
    if (    ("" != zP2PThread::m_deviceId)
        &&  findFile(param.filename, fullPath))
    {
        LOG_I("webrtc:downloadFile %s \n ", param.filename.c_str());
        int nResult = fileAcceptWebrtc(param.render, fullPath);
        if (0 == nResult)
        {
            result = true;
        }
        else
        {
            LOG_E("Error: pgLiveFileAccept Failed! errorno == %d, filePath == %s, renderPath == %s \n",
                nResult, fullPath.c_str(), param.renderPath.c_str());
        }
        result = (0 == nResult);
    }
    else
    {
        LOG_E("webrtc: Error: file %s do not exist anymore ! \n", param.filename.c_str());

        fileRejectWebrtc(param.render);

        char szBuf[256] = { 0 };
        sprintf(szBuf, "{ \"act\":\"download\", \"filename\":\"%s\", \"result\":\"not_exist\"}", param.filename.c_str());
        notifyWebrtc(szBuf);
    }
    return result;
}


bool zRokugaThread::removeFile(const std::string& fileName)
{
    bool result = false;
    if (NULL == this) return result;

    result = removeFile_(fileName);

    char szBuf[256] = { 0 };
    sprintf(szBuf, "{ \"act\":\"delete_file\", \"filename\":\"%s\", \"result\":\"%s\", \"msg\":\"%s\"}"
        , fileName.c_str(), result ? "true" : "false", result? "succ" : "file not exist");
    notifyWebrtc(szBuf);
    return result;
}

/*****************************************************************************
 * 函 数 名  : image_free
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月31日
 * 函数功能  : 释放资源
 * 输入参数  : S_VCBuf& src_buf  图像
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static void image_free(S_VCBuf& src_buf)
{
    free(src_buf.buf);
    src_buf.buf = NULL;
    src_buf.len = 0;
    return;
}

/*****************************************************************************
 * 函 数 名  : image_copy
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月31日
 * 函数功能  : 图像拷贝
 * 输入参数  : const S_VCBuf& src_buf  源图像
               S_VCBuf& dst_buf        目的图像
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static bool image_copy(const S_VCBuf& src_buf, S_VCBuf& dst_buf)
{
    if (src_buf.len <= 0)
    {
        return false;
    }

    unsigned char* pBuf = (unsigned char*)malloc(src_buf.len);
    if (NULL == pBuf)
    {
        LOG_E("malloc failed!\n");
        return false;
    }

    (void)memcpy(&dst_buf, &src_buf, sizeof(S_VCBuf));
    (void)memcpy(pBuf, src_buf.buf, src_buf.len);
    dst_buf.buf = pBuf;

    return true;
}

/*****************************************************************************
 * 函 数 名  : save_gesture_image_proc
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月31日
 * 函数功能  : 存手势图像
 * 输入参数  : void* param  : gesture_thread_t
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static void save_gesture_image_proc(void* param)
{
    gesture_thread_t *p_thread_param = (gesture_thread_t *)param;
    if ((NULL != p_thread_param) && (NULL != p_thread_param->p_roku))
    {
        p_thread_param->p_roku->save_gesture_image(p_thread_param->buf, p_thread_param->gesture_type);
    }

    // 释放内存
    image_free(p_thread_param->buf);
    free(p_thread_param);
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.save_gesture_image_thread
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月31日
 * 函数功能  : 创建线程，存储图像
 * 输入参数  : const S_VCBuf& buf  图像
               int gesture_type    手势开 或 关
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::save_gesture_image_thread(const S_VCBuf& buf, int gesture_type)
{
    gesture_thread_t *p_thread_param = (gesture_thread_t *)malloc(sizeof(gesture_thread_t));
    if (!image_copy(buf, p_thread_param->buf))
    {
        free(p_thread_param);
        return;
    }
    
    p_thread_param->p_roku = this;
    p_thread_param->gesture_type = gesture_type;

    std::thread thd(save_gesture_image_proc, p_thread_param);
    thd.detach();
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.save_gesture_image
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月31日
 * 函数功能  : 获取文件名，存JPG图像
 * 输入参数  : const S_VCBuf& buf  图像
               int gesture_type    手势
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zRokugaThread::save_gesture_image(const S_VCBuf& buf, int gesture_type)
{
    if ((NULL == buf.buf) || (buf.len <= 0))
    {
        return -1;
    }

    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    rokuStoreType type;
    std::string rootDir;
    std::list<std::string>* list = NULL;
    (void)get_gesture_image_info(type, rootDir, &list);
    if (NULL == list)
    {
        return 0;
    }

    int imageID = 0;
    std::string fileName;

    // disk
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        image_storage_clear(list, rootDir, MAX_GESTURE_IAMGE_COUNT, GESTURE_IMAGE_CLEAR_SIZE);

        ++m_latest_gesture_image_id;
        imageID = m_latest_gesture_image_id;
    }

    bool bSucc = false;
    // image
    {
        char szBuf[256] = { 0 };
        sprintf(szBuf, "%d_type-%d", imageID, gesture_type);

        fileName = pm->getNewFilename(rootDir, szBuf, IMAGE_SUFFIX);
        if (fileName.empty())
        {
            LOG_E(" getNewFilename failed! rootDir == %s, szBuf == %s \n", rootDir.c_str(), szBuf);
            return -1;
        }

        LOG_D("save image len == %u, width = %d, height = %d, file name = %s \n", 
            buf.BytesUsed, m_nWidth, m_nHeight, fileName.c_str());
        bSucc = zImageUtil::zImage::yuv_to_jpg_file(fileName, buf.buf, buf.BytesUsed, m_nWidth, m_nHeight, 2);
        if (false == bSucc)
        {
            LOG_E("encode jpg failed.\n");
        }
    }

    if (bSucc)
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        std::string str = getFilenameByFullPath(fileName, rootDir);
        list->push_back(str);
    }
    return 0;
}

void zRokugaThread::saveImage()
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

    rokuStoreType type;
    std::string rootDir;
    std::list<std::string>* list = NULL;
    (void)get_alarm_image_info(type, rootDir, &list);
    if (NULL == list)
    {
        return;
    }

    int imageID = 0;
    std::string fileName;

    // disk
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        image_storage_clear(list, rootDir, MAX_ALARM_IAMGE_COUNT, ALARM_IMAGE_CLEAR_SIZE);

        ++m_latestImageid;
        imageID = m_latestImageid;
    }

    bool bSucc = false;
    // image
    {
        char szBuf[256] = { 0 };
        sprintf(szBuf, "%d", imageID);

        fileName = pm->getNewFilename(rootDir, szBuf, IMAGE_SUFFIX);

        if (fileName.empty())
        {
            LOG_E(" getNewFilename failed! rootDir == %s, szBuf == %s \n", rootDir.c_str(), szBuf);
            return;
        }
        //zImageUtil::zImage image;
        S_VCBuf buf;
        memset(&buf, 0, sizeof(S_VCBuf));

        bool result = m_pManager->getFrame(buf, zAlgManager::Threads::THREADANONYMOUS);

        if (!result) return;

        LOG_D("save image len == %u, width = %d, height = %d, file name = %s \n", 
            buf.BytesUsed, m_nWidth, m_nHeight, fileName.c_str());

        bSucc = zImageUtil::zImage::yuv_to_jpg_file(fileName, buf.buf, buf.BytesUsed, m_nWidth, m_nHeight, 2);
        //if (image.initWithNV12Data(buf.buf, buf.BytesUsed, m_nWidth, m_nHeight, 4, false))
        //{
        //  if (!image.save2File(fileName.c_str()))
        //  {
        //      LOG_D("save image %s failed! \n", fileName.c_str());
        //  }
        //  else bSucc = true;
        //}

        free(buf.buf);
        buf.buf = NULL;
        buf.len = 0;
    }

    if (bSucc)
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        std::string str = getFilenameByFullPath(fileName, rootDir);
        list->push_back(str);
    }
}
//////////////////////////////////////////////////////////////////////////

void  zRokugaThread::save_curr_record_file_info()
{
    m_curr_record_file_type = m_curRokuType;
    m_curr_record_file_state = m_ctrState;
    m_curr_record_file_dir = m_curDir;
    return;
}

int zRokugaThread::initRokuga( const std::string& filename)
{
    LOG_D("zRokugaThread::initRokuga filename = %s \n", filename.c_str());
    m_nCurRokuTime = 0;
    //////////////////////////////////////////////////////////////////////////
    AVOutputFormat *fmt;

    AVCodec *video_codec;
    int ret;
    int have_video = 0;
    AVDictionary *opt = NULL;

    /* Initialize libavcodec, and register all codecs and formats. */
    av_register_all();

    /* allocate the output media context */
    avformat_alloc_output_context2(&oc_, NULL, NULL, filename.c_str());
    if (!oc_) {
        LOG_E("Could not deduce output format from file extension: using MPEG.\n");
        avformat_alloc_output_context2(&oc_, NULL, "mpeg", filename.c_str());
    }
    if (!oc_)
        return 1;

    fmt = oc_->oformat;
    fmt->video_codec = AV_CODEC_ID_H264;

    /* Add the audio and video streams using the default format codecs
    * and initialize the codecs. */
    if (fmt->video_codec != AV_CODEC_ID_NONE) {
        add_stream(&video_st_, oc_, &video_codec, fmt->video_codec);
        have_video = 1;
    }

    /* Now that all the parameters are set, we can open the audio and
    * video codecs and allocate the necessary encode buffers. */
    if (have_video)
        open_video(oc_, video_codec, &video_st_, opt);

    //if (have_audio)
    //  open_audio(oc_, audio_codec, &audio_st_, opt);

    av_dump_format(oc_, 0, filename.c_str(), 1);

    /* open the output file, if needed */
    if (!(fmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&oc_->pb, filename.c_str(), AVIO_FLAG_WRITE);
        if (ret < 0) {
            LOG_E("Could not open %s\n", filename.c_str());
            return 1;
        }
    }

    #if 0
    // 添加MP4复用选项，这样生成的MP4文件没有尾部数据，也可以正常播放。
    av_dict_set(&opt, "movflags", "frag_keyframe", 0);
    av_dict_set(&opt, "movflags", "empty_moov", 0);

    /* Write the stream header, if any. */
    ret = avformat_write_header(oc_, &opt);
    if (ret < 0) {
        LOG_E("Error occurred when opening output file %s \n", filename.c_str());
        return 1;
    }
    #endif

    /* add by 刘春龙, 2015-12-03, 注: 调用avformat_write_header 可能已经释放掉了 */
    if (NULL != opt){
        av_dict_free(&opt);
    }

    m_bFirstFrame = true;
    m_strFileName = filename;

    /* delete by 刘春龙, 2016-01-15, 原因: 修复0文件问题 */
    //std::string str = getFilenameByFullPath(filename, stDir());
    //addRokuFile(str) ;
    save_curr_record_file_info();

    time_t curr_time = zBaseUtil::ntp_time();
    zBaseUtil::write_sync_time(curr_time);

    return 0;
}

void zRokugaThread::uninitRokuga()
{
    if (NULL == oc_ || NULL == video_st_.st) return;

    AVOutputFormat *fmt = oc_->oformat;
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();

    int file_size = pm->getFileSize(m_strFileName);
    //LOG_D("m_strFileName=%s, file_size=%d\n", m_strFileName.c_str(), file_size);

    /* add by 刘春龙, 2015-12-31, 原因: SD卡不可写，导致程序崩溃 */
    if ((NULL != oc_->pb) && (file_size > 0))
    {
        av_write_trailer(oc_);
        avio_flush(oc_->pb);
    }

    /* Close each codec. */
    //if (m_ctrState == ROKU)
    //{
    //}
    close_stream(oc_, &video_st_);
    //close_stream(oc_, &audio_st_);

    if (!(fmt->flags & AVFMT_NOFILE))
    {
        /* Close the output file. */
        avio_closep(&oc_->pb);
    }

    /* free the stream */
    avformat_free_context(oc_);

    video_st_.reset();
    oc_ = NULL;
    //memset(&audio_st_, 0, sizeof(OutputStream));

    m_nCurRokuTime = 0; 
    m_nCurTaninnRokuTime = 0;

    file_size = pm->getFileSize(m_strFileName);
    if (file_size < MIN_RECORD_FILE_SIZE)
    {
        LOG_D("file_size=%d, file=%s\n", file_size, m_strFileName.c_str());
        (void)(pm->removeFile(m_strFileName));

        // 没有加入队列, m_latestVideoid需减回来
        if (m_latestVideoid > 0)
        {
            m_latestVideoid--;
        }
    }
    else
    {
        std::string str_recording = getFilenameByFullPath(m_strFileName, m_curr_record_file_dir);
        if (!str_recording.empty())
        {
            zBaseUtil::file_sync(m_strFileName); // 文件关闭的时候，不一定将文件写入SDCARD，执行该函数，同步到SDCARD.

            std::string recorded_filename = str_recording.substr(strlen(recording_preffix));
            zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
            if (    !recorded_filename.empty()
                &&  (NULL != pm) )
            {
                if (pm->renameFile(m_curr_record_file_dir, str_recording, recorded_filename))
                {
                    LOG_D("recorded_filename=%s\n", recorded_filename.c_str());
                    addRokuFile(recorded_filename);

                    time_t curr_time = zBaseUtil::ntp_time();
                    zBaseUtil::write_sync_time(curr_time);
                }
            }
        }
    }

    // 复位为空
    m_strFileName = "";

    // 时间同步了 在关闭所有文件的时候 尝试 update所有的
    update_all_filename();
    
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.set_record_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 设置录像分辨率
 * 输入参数  : int width   宽度
               int height  高度
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::set_record_resolution(int width, int height)
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    
    m_encode_width = width;
    m_encode_height = height;
    LOG_I("m_encode_width=%d, m_encode_height=%d\n", m_encode_width, m_encode_height);
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.joint_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 转存文件列表和属性
 * 输入参数  : std::list<file_property_t>& dst_files  目的文件列表
               std::list<std::string>& src_files      源文件列表
               enum rokuStoreType storage_type        文件存储类型
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::joint_files(
    std::list<file_property_t>& dst_files,
    std::list<std::string>& src_files, 
    enum rokuStoreType storage_type )
{
    std::list<std::string>::iterator it = src_files.begin();
    std::list<std::string>::iterator it_end = src_files.end();
    while (it != it_end)
    {
        file_property_t property;
        property.filename = *it;
        property.storage_type = storage_type;

        dst_files.push_back(property);
        ++it;
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : FileNameIDCmp3
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 比较文件ID值
 * 输入参数  : const file_property_t& s0  文件属性
               const file_property_t& s1  文件属性
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static bool FileNameIDCmp3(
    const zRokugaThread::file_property_t& s0, 
    const zRokugaThread::file_property_t& s1)
{
    int srcid = getIDFromName(s0.filename);
    int desid = getIDFromName(s1.filename);
    return srcid < desid;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.sort_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 按ID值，从小到大排序
 * 输入参数  : std::list<file_property_t>& files  文件列表
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::sort_files(std::list<file_property_t>& files)
{
    (void)files.sort(FileNameIDCmp3);
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.rename_file_ID
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 重命名文件ID值
 * 输入参数  : file_property_t property  文件属性
               int id                    ID值
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::rename_file_ID(file_property_t property, int id)
{
    std::string dir = getDir(property.storage_type);
    if (dir.empty())
    {
        LOG_E("empty storage directory. storage_type =%d\n", property.storage_type);
        return;
    }

    std::string &old_filename = property.filename;
    size_t nPos = old_filename.find("_");
    if (nPos == std::string::npos)
    {
        LOG_E("old_filename = %s\n", old_filename.c_str());
        return;
    }

    char id_buf[64];
    (void)sprintf(id_buf, "%d", id);
    std::string new_filename = id_buf + old_filename.substr(nPos);

    zFileUtil::zFileManager *pm = zFileUtil::zFileManager::getInstance();
    pm->renameFile(dir, old_filename, new_filename);

    //LOG_D("old_filename=%s, new_filename=%s\n", old_filename.c_str(), new_filename.c_str());
    return ;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.fill_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 合并文件列表
 * 输入参数  : std::list<file_property_t>& files  文件列表
               enum FILE_TYPE_E file_type         文件类型
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::fill_files(
    std::list<file_property_t>& files, FILE_TYPE_E file_type)
{
    if (VIDEO_ALARM == file_type)
    {
        joint_files(files, m_locAlarmVideo, st_loc_alarm);
        joint_files(files, m_usbAlarmVideo, st_usb_alarm);
    }
    else if (VIDEO_NORMAL == file_type)
    {
        joint_files(files, m_locNormalVideo, st_loc_normal);
        joint_files(files, m_usbNormalVideo, st_usb_normal);
    }
    else if (IMAGE_NORMAL == file_type)
    {
        joint_files(files, m_locAlarmImage, st_loc_image);
        joint_files(files, m_usbAlarmImage, st_usb_image);
    }
    else if (IMAGE_DEBUG == file_type)
    {
        joint_files(files, m_local_gesture_image_list, st_loc_gesture_image);
        joint_files(files, m_usb_gesture_image_list, st_usb_gesture_image);
    }

    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.rename_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 重命名文件ID值
 * 输入参数  : std::list<file_property_t>& files  文件列表
               int& next_id                       下次获取文件名时使用的ID值
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::rename_files(
    std::list<file_property_t>& files, int& next_id)
{
    if (0 == files.size())
    {
        return;
    }

    // 按ID排序
    sort_files(files);

    int file_id = 0;
    std::list<file_property_t>::iterator it = files.begin();
    std::list<file_property_t>::iterator it_end = files.end();
    while(it != it_end)
    {
        rename_file_ID(*it, file_id++);
        ++it;
    }

    next_id = file_id;
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.update_all_filename
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 更新所有文件的ID
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::update_all_file_ID()
{
    std::list<file_property_t> files;

    fill_files(files, VIDEO_ALARM);
    fill_files(files, VIDEO_NORMAL);
    rename_files(files, m_latestVideoid);

    files.clear();
    fill_files(files, IMAGE_NORMAL);
    rename_files(files, m_latestImageid);

    files.clear();
    fill_files(files, IMAGE_DEBUG);
    rename_files(files, m_latest_gesture_image_id);

    LOG_D("m_latestVideoid=%d, m_latestImageid=%d, m_latest_gesture_image_id=%d\n",
        m_latestVideoid, m_latestImageid, m_latest_gesture_image_id);
    return;
}

std::list<std::string>* zRokugaThread::getList(zRokugaThread::rokuStoreType curType)
{
    std::list<std::string>* list = getVideoList(curType);
    if (NULL == list)
    {
        list = getImageList(curType);
    }
    return list;
}

std::list<std::string>* zRokugaThread::getImageList(zRokugaThread::rokuStoreType curType)
{
    if (st_loc_image == curType)
    {
       return &m_locAlarmImage;
    }
    else if (st_usb_image == curType)
    {
       return &m_usbAlarmImage;
    }
    else if (st_loc_gesture_image == curType)
    {
       return &m_local_gesture_image_list;
    }
    else if (st_usb_gesture_image == curType)
    {
       return &m_usb_gesture_image_list;
    }

    return NULL;
}

/*****************************************************************************
 * 函 数 名  : getVideoList
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月25日
 * 函数功能  : 获取视频列表指针
 * 输入参数  : zRokugaThread::rokuStoreType curType  存储类型
 * 输出参数  : 无
 * 返 回 值  : std::list<std::string>*
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::list<std::string>* zRokugaThread::getVideoList(
    zRokugaThread::rokuStoreType curType)
{
    if (st_loc_normal == curType)
    {
        return &m_locNormalVideo;
    }
    else if (st_loc_alarm == curType)
    {
        return &m_locAlarmVideo;
    }
    else if (st_usb_normal == curType)
    {
        return &m_usbNormalVideo;
    }
    else if (st_usb_alarm == curType)
    {
        return &m_usbAlarmVideo;
    }

    return NULL;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.joint_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月18日
 * 函数功能  : 转存文件列表和属性
 * 输入参数  : std::list<file_property_t>& dst_files  目的文件列表
               std::list<std::string>& src_files      源文件列表
               enum rokuStoreType storage_type        文件存储类型
               int max_size                           最大转存大小，单位: M
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::joint_files_2(
    std::list<file_property_t>& dst_files,
    std::list<std::string>& src_files, 
    enum rokuStoreType storage_type, int max_size)
{
    zFileUtil::zFileManager *pm = zFileUtil::zFileManager::getInstance();
    if (NULL == pm)
    {
        return;
    }

    std::string store_dir = getDir(storage_type);
    int total_file_size = 0;

    std::list<std::string>::iterator it = src_files.begin();
    std::list<std::string>::iterator it_end = src_files.end();
    while ( (it != it_end)
        &&  ((total_file_size >> 20) <= max_size))
    {
        file_property_t property;
        property.filename = *it;
        property.storage_type = storage_type;

        int file_size = pm->getFileSize(store_dir + *it);
        total_file_size += file_size;

        dst_files.push_back(property);
        ++it;
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.video_storage_clear
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月25日
 * 函数功能  : 告警录像和手动录像，一起清理，按ID从小开始清理，不改变当前存储类型。
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::video_storage_clear()
{
    zRokugaThread::rokuStoreType curType = stType();
    std::list<file_property_t> files;

    if (zRokugaThread::rokuStoreType::st_usb == curType)
    {
        joint_files_2(files, m_usbNormalVideo, st_usb_normal, VIDEO_CLEAR_MEMORY_SIZE);
        joint_files_2(files, m_usbAlarmVideo, st_usb_alarm, VIDEO_CLEAR_MEMORY_SIZE);
    }
    else // zRokugaThread::rokuStoreType::st_loc
    {
        joint_files_2(files, m_locNormalVideo, st_loc_normal, VIDEO_CLEAR_MEMORY_SIZE);
        joint_files_2(files, m_locAlarmVideo, st_loc_alarm, VIDEO_CLEAR_MEMORY_SIZE);
    }

    if (0 == files.size())
    {
        LOG_I("This should not happened.\n");
        return;
    }

    // 按ID从小到大排序
    sort_files(files);
    zFileUtil::zFileManager *pm = zFileUtil::zFileManager::getInstance();
    if (NULL == pm)
    {
        return;
    }

    int total_size = 0;
    auto it = files.begin();
    auto it_end = files.end();
    while( (it != it_end)
        && ((total_size >> 20) < VIDEO_CLEAR_MEMORY_SIZE))
    {
        std::string full_filename = getDir(it->storage_type) + it->filename;
        int file_size = pm->getFileSize(full_filename);
        pm->removeFile(full_filename);

        auto pVideo = zRokugaThread::getVideoList(it->storage_type);
        pVideo->remove(it->filename);

        total_size += file_size;
        ++it;
    }

    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.image_storage_clear
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月26日
 * 函数功能  : 清理图片存储空间
 * 输入参数  : std::list<std::string>* image_list  图片文件列表
               std::string& root_dir               图片文件路径
               int max_image_count                 最大的图片数量
               int image_clear_size                清理的图像数量
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::image_storage_clear(
    std::list<std::string>* image_list, std::string& root_dir,
    int max_image_count, int image_clear_size)
{
    if (NULL == image_list)
    {
        LOG_D("image_list is null.\n");
        return;
    }

    zFileUtil::zFileManager *pm = zFileUtil::zFileManager::getInstance();
    if (NULL == pm)
    {
        return;
    }

    if (static_cast<int>(image_list->size()) >= max_image_count)
    {
        int total_removed = 0;
        auto it = image_list->begin();
        auto it_end = image_list->end();
        while ((it != it_end)
            && (total_removed < image_clear_size))
        {
            std::string full_filename = root_dir + *it;
            pm->removeFile(full_filename);
            image_list->erase(it++);

            ++total_removed;
            LOG_D("full_filename=%s, total_removed=%d\n", full_filename.c_str(), total_removed);
        }
    }
    else // 如果存储满了，应该是视频满了造成的，所以清理视频存储
    {
        bool bFull = checkDisk(root_dir);
        if (bFull)
        {
            video_storage_clear();
        }
    }

    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.get_alarm_image_info
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月26日
 * 函数功能  : 获取告警图片相关的，类型，文件列表等信息
 * 输入参数  : zRokugaThread::rokuStoreType &dirType  存储类型
               std::string& root_dir                  存储路径
               std::list<std::string>** ppList        文件列表
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zRokugaThread::get_alarm_image_info(
    zRokugaThread::rokuStoreType &roku_type,
    std::string& root_dir,
    std::list<std::string>** ppList)
{
    zRokugaThread::rokuStoreType curType = stType();
    if (zRokugaThread::rokuStoreType::st_loc == curType)
    {
        roku_type = zRokugaThread::rokuStoreType::st_loc_image;
        *ppList = &m_locAlarmImage;
    }
    else
    {
        roku_type = zRokugaThread::rokuStoreType::st_usb_image;
        *ppList = &m_usbAlarmImage;
    }
    root_dir = getDir(roku_type);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.get_gesture_image_info
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月26日
 * 函数功能  : 获取手势图片相关的，类型，文件列表等信息
 * 输入参数  : zRokugaThread::rokuStoreType &dirType  存储类型
               std::string& root_dir                  存储路径
               std::list<std::string>** ppList        文件列表
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zRokugaThread::get_gesture_image_info(
    zRokugaThread::rokuStoreType &roku_type,
    std::string& root_dir,
    std::list<std::string>** ppList)
{
    zRokugaThread::rokuStoreType curType = stType();
    if (zRokugaThread::rokuStoreType::st_loc == curType)
    {
        roku_type = zRokugaThread::rokuStoreType::st_loc_gesture_image;
        *ppList = &m_local_gesture_image_list;
    }
    else
    {
        roku_type = zRokugaThread::rokuStoreType::st_usb_gesture_image;
        *ppList = &m_usb_gesture_image_list;
    }
    root_dir = getDir(roku_type);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.storage_clear
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月26日
 * 函数功能  : 手动清理存储空间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::storage_clear()
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);

    // 清理视频， 注意: 需先清理视频空间
    video_storage_clear();

    // 清理图片
    rokuStoreType type;
    std::string rootDir;
    std::list<std::string>* list = NULL;

    (void)get_alarm_image_info(type, rootDir, &list);
    image_storage_clear(list, rootDir, MAX_ALARM_IAMGE_COUNT, ALARM_IMAGE_CLEAR_SIZE);

    (void)get_gesture_image_info(type, rootDir, &list);
    image_storage_clear(list, rootDir, MAX_GESTURE_IAMGE_COUNT, GESTURE_IMAGE_CLEAR_SIZE);

    return;
}

/*****************************************************************************
 * 函 数 名  : zRokugaThread.remove_null_files
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年6月24日
 * 函数功能  : 从链表后开始扫描，文件大小为0的文件，删除掉
 * 输入参数  : std::string rootDir           
               std::list<std::string>& list
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zRokugaThread::remove_null_files(
    std::string rootDir, std::list<std::string>& list)
{
    static const int MAX_CNT = 10;

    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    int count = 0;
    std::list<std::string>::reverse_iterator rit = list.rbegin();
    while((rit!=list.rend()) && (count < MAX_CNT))
    {
        std::string file_path = rootDir + *rit;
        int file_size = pm->getFileSize(file_path);
        if (file_size <= 0)
        {
            pm->removeFile(file_path); // 删除文件
            rit = std::list<std::string>::reverse_iterator( list.erase((++rit).base())); // 更新文件列表
            LOG_D("remove filename=%s\n", file_path.c_str());
        }
        else {
            ++rit;
        }
        ++count;
    }
    return;
}

