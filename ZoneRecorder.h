#ifndef _ZONE_RECORDER_H_
#define _ZONE_RECORDER_H_

#include "zUtil.h"

/**
 * 根据HandRect来判断当前手势在那个区域内。
 * 基本策略如下：
 * 1. 记录下第一帧检测到手心为 中心位置
 * 2. 计算当前手中心位置与上述中心距离
 * 3. 根据这个距离判断是否所在区域（需要缓冲区防抖动）
 * 
 * @author guluxixi
 *
 */
class ZoneRecorder {
    /** 第一帧手心x轴位置 */
    double startCX; // 第一帧手心位置。
    double startCY; // 第一帧手心位置。
    
    /** 区域宽度变化宽度 */
    double zoneWidth;
    
    /** 当前区域*/
    ZONE_POS zone;
    
    /** 手势进入当前区域的开始时间。 */
    long zoneStartMillis;
    
    /** 手势在该区域的时长   */
    long zoneStayMillis;
    
    /** 是否发生区域变化  */
    bool isChangedZone;

public:
    ZoneRecorder()
        : startCX(0)
        , startCY(0)
        , zoneWidth(0)
        , zone(CENTER_ZONE)
        , zoneStartMillis(0)
        , zoneStayMillis(0)
        , isChangedZone(false)
    {
    }

    void clearStayMillis() {
        zoneStartMillis = static_cast<long>(zBaseUtil::getMicroSecond()/1000);
        zoneStayMillis = 0;
    }
    
    long getZoneStayMillis() {
        return zoneStayMillis;
    }

    bool isZoneChanged() {
        return isChangedZone;
    }

    ZONE_POS getZone() {
        return zone;
    }

    void init(const zBaseUtil::v2Hand& handRect) {
        startCX = ((double)handRect.left_+handRect.right_)/2;
        startCY = ((double)handRect.top_+handRect.bottom_)/2;
        zoneWidth = (handRect.right_ - handRect.left_) / 2.5;
        zoneStartMillis = static_cast<long>(zBaseUtil::getMicroSecond()/1000);
        zoneStayMillis = 0;
        isChangedZone = false;
    }
    
    void record(const zBaseUtil::v2Hand& handRect) {
        ZONE_POS futureZone = calZone(handRect);
        updateZone(futureZone);
    }

    /**
     * 计算对应手势所在区域。
     */
    ZONE_POS calZone(const zBaseUtil::v2Hand& handRect);
    
private:    
    /**
     * 更新进去区域时间、驻留时长并设置所在区域。
     */
    void updateZone(ZONE_POS futureZone);
    
    ZONE_POS getRegularZone(double dx, double dy);
};

#endif

