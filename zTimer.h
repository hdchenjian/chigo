#ifndef __ZTIMER_H_
#define __ZTIMER_H_

#include <list>
#include <stddef.h>

namespace zBaseUtil
{
    typedef bool (*timerFun)(void* param);
    typedef void(*onEnd)(void* param);

    typedef struct tagzTimerEvent
    {
        int id_;
        int type_;
        int cnt_;
        int gap_;
        int delay_;
        int elapse_;
        timerFun EventFunc_;
        void* param_;
        onEnd endFunc_;
        void* endParam_;
    }zTimerEvent;

    // -1 0 n
    class zTimer
    {
    public:
        zTimer();
        ~zTimer();
        int setTimer(int nID, int nType, int nCnt, int nGap, int wait, timerFun func, void* param = NULL, onEnd endFunc = NULL, void* endParam = NULL);
        int removeTimer(int nID);
        int clearTimer();

        int elapse(int nElapse);
    private:
        std::list<zTimerEvent> timerList_;
    };
}
#endif