#include "zEakonThread.h"
#include "zWifiStateThread.h"
#include "zUtil.h"
#include "config_mgr.h"
#include "zLog.h"
#include "zAlgorithmManager.h"
#include "zVideoThread.h"
#include "zFile.h"
#ifdef _WINDOWS
#else 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#endif
#define HEARTBEAT (3) // seconeds

zWifiStateThread* zWifiStateThread::instance_ = NULL;

zWifiStateThread* zWifiStateThread::getInstance()
{
    if (NULL == instance_)
    {
        instance_ = new zWifiStateThread();
    }
    return instance_;
}

void zWifiStateThread::destroyInstance()
{
    if (NULL != instance_)
    {
        delete instance_;
        instance_ = NULL;
    }

    return;
}

zWifiStateThread::zWifiStateThread()
    : bNeed_(true)
    , m_wifiState(-1)
    , m_beatGap(HEARTBEAT)
    , notify_(NULL)
    , timeDiff_(0)
    , wifi_connected_flag(true)
{
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");

    std::string strBeatGap = conf.getValue("beatGap");
    if (!strBeatGap.empty())
    {
        m_beatGap = atoi(strBeatGap.c_str());
    }

    zSingletonThread::setThreadGap(m_beatGap * 1000);
}

zWifiStateThread::~zWifiStateThread()
{

}

void zWifiStateThread::init(void* pParent)
{
    m_pEakonThread = (zEakonThread*)pParent;

}

void zWifiStateThread::eventNotify(void* pNoti)
{
    notify_ = pNoti;
}

/*****************************************************************************
 * 函 数 名  : zWifiStateThread.getTimeDiff
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月17日
 * 函数功能  : 获取同步时间差，单位: 秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t zWifiStateThread::getTimeDiff()
{
    // need lock
    //std::unique_lock<std::mutex> lock(ctrlMutex_);
    return timeDiff_;
}

bool zWifiStateThread::wifi_connected()
{
    // 同步时间
    std::string befor_sync_time = zFileUtil::getTimeString();
    time_t pre_time = zBaseUtil::ntp_time();

    std::int64_t secs = zBaseUtil::getSecond();
    if (!zBaseUtil::ntp_sync_time())
    {
        LOG_E("syncTime failed!!");
        return false;
    }

    std::int64_t sync_duration = zBaseUtil::getSecond() - secs;
    std::int64_t ndiff = zBaseUtil::ntp_time() - pre_time - sync_duration;
    if (abs(ndiff) >= 5){
        LOG_I("before time=%s, after time=%s, sync_duration=%lld, ndiff=%lld\n", 
            befor_sync_time.c_str(), 
            zFileUtil::getTimeString().c_str(), sync_duration, ndiff);
    }

    (void)zBaseUtil::create_synced_time_flag_file();
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    if (    (NULL != pm) 
        &&  (!(pm->get_time_sync())) )
    {
        timeDiff_ = ndiff;
        pm->timeSync();

        /* add by 刘春龙, 2016-01-17, 原因: 看起来可以删掉了，暂保留吧! */
        if (notify_)
        {
            ((zAlgManager*)notify_)->onEvent(ZM_TIMESYNC);
        }

        zVideoThread::getInstance()->onEvent(ZM_TIMESYNC);
    }

    /* add by 刘春龙, 2016-01-17, 原因: 看起来可以删掉了，暂保留吧! */
    zVideoThread::getInstance()->onEvent(ZM_WIFICONNECTED);
    return true;
}

bool zWifiStateThread::proc()
{
    //LOG_D("wifi state thread. enter. \n");
    unsigned char ret = 0;
    unsigned char state = 0;

    ret = zBaseUtil::connect_router();
    if (0 != ret)
    {
        m_pEakonThread->set_router_state(ret);

        state = 2;
        if (    (0 == m_pEakonThread->get_quick_check_mode())
            &&  (false == m_pEakonThread->get_device_test_mode()))
        {
            ret = zBaseUtil::connect_wan();
            if (0 != ret)
            {
                state = 1;
            }
        }
    }

    std::unique_lock<std::mutex> lock(ctrlMutex_);
    if (m_wifiState != state)
    {
        m_pEakonThread->WifiState(state);
        m_wifiState = state;
        if (1 == m_wifiState)
        {
            LOG_I("wifi state change to connected \n");
            wifi_connected_flag = wifi_connected();
        }
    }
	else if ((1 == state) && !wifi_connected_flag)
	{
		// 网络联网成功时，如果前一次wifi_connected失败的话，继续执行同步。
		wifi_connected_flag = wifi_connected();
	}

    //std::this_thread::sleep_for(std::chrono::seconds(m_beatGap));
    return bNeed_;
}

void zWifiStateThread::reset_wifi_state()
{
    std::unique_lock<std::mutex> lock(ctrlMutex_);
    m_wifiState = -1;
    if (NULL != m_pEakonThread)
    {
        m_pEakonThread->WifiState(0);
    }
    return;
}

