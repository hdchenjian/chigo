#include <thread>
#include <mutex>
#include <stdlib.h>
#include <chrono>

#ifdef _WINDOWS
#else 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#endif
#include "zWifiConnect.h"
#include "config_mgr.h"
#include "zLog.h"
#include "zAlgorithmManager.h"
#include "zVideoThread.h"
#include "zFile.h"

#define CONNECTGAP (15)

zWifiConnect* zWifiConnect::pInstance_ = NULL;
//int zWifiConnect::nReference_ = 0;

zWifiConnect* zWifiConnect::getInstance( )
{
    if (NULL == pInstance_)
    {
        pInstance_ = new zWifiConnect( );
    }

    //pInstance_->refresh(ssid, pswd);
    //++nReference_;

    return pInstance_;
}

int zWifiConnect::destroyInstance()
{
    if (NULL != pInstance_)
    {
        delete pInstance_;
        pInstance_ = NULL;
    }
    return 0;
}

void zWifiConnect::init(std::string ssid, std::string pswd /* = "" */)
{
    refresh(ssid, pswd);
}

zWifiConnect::zWifiConnect( )
    : bC_(false)
    , ConnectGap_(CONNECTGAP)
    , latestConnectTime_(0)
{
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");

    std::string strConnectGap = conf.getValue("connectGap");
    if (!strConnectGap.empty())
    {
        ConnectGap_ = atoi(strConnectGap.c_str());
    }
}

zWifiConnect::~zWifiConnect()
{

}

void zWifiConnect::refresh(std::string ssid, std::string pswd /* = "" */)
{
    LOG_D("zWifiConnect::refresh \n");
    std::unique_lock<std::mutex> lock(mutex_);
    strPswd_ = pswd;
    strSsid_ = ssid;
    LOG_D("zWifiConnect::refresh end strPswd == %s, strSsid == %s \n", strPswd_.c_str(), strSsid_.c_str());
    return;
}

void zWifiConnect::beginC()
{
    LOG_D("zWifiConnect::beginC \n");
    std::unique_lock<std::mutex> lock(mutex_);
    bC_ = true;
    LOG_D("zWifiConnect::beginC end \n");
    return;
}

void zWifiConnect::endC()
{
    LOG_D("zWifiConnect::endC \n");
    std::unique_lock<std::mutex> lock(mutex_);
    bC_ = false;
    latestConnectTime_ = zBaseUtil::getSecond();
    LOG_D("zWifiConnect::endC end\n");
    return;
}

bool zWifiConnect::IsConnecting()
{
    LOG_D("zWifiConnect::IsConnecting \n");
    std::unique_lock<std::mutex> lock(mutex_);

    std::int64_t nGap = ConnectGap_;
    if (0 != latestConnectTime_)
    {
        nGap = zBaseUtil::getSecond() - latestConnectTime_;
    }

    bool ret = (bC_ || (nGap < ConnectGap_));
    LOG_D("connect state == %s, nGap == %llu\n",
        (ret ? "will connect" : "connecting"), nGap);
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zWifiConnect.is_wifi_connect_done
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月6日
 * 函数功能  : wifi_connect是否执行完毕
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zWifiConnect::is_wifi_connect_done()
{
    LOG_D("enter. \n");
    std::unique_lock<std::mutex> lock(mutex_);
    return !bC_;
}

static void connectProc(void* param)
{
    zWifiConnect* instance = (zWifiConnect*)param;
    instance->connect();
    instance->endC();
}

int zWifiConnect::startConnect()
{
    if (IsConnecting())
    {
        return -1;
    }

    beginC();
    (void)disconnect(); // wifi重连前，先断网
    
    std::thread thd(connectProc, this);
    thd.detach();
    return 0;
}

int zWifiConnect::connect()
{
    LOG_D("zEakonThread:: connected wifi\n");
    std::string strPswd;
    std::string strSsid;
    {
        std::unique_lock<std::mutex> lock(mutex_);
        strPswd = strPswd_;
        strSsid = strSsid_;
    }

    LOG_D("strPswd == %s, strSsid == %s \n", strPswd.c_str(), strSsid.c_str());
    if (strSsid.empty())
    {
        LOG_E(" strSsid.empty() \n");
        return -1;
    }

    char szBuf[128] = { 0 };
    sprintf(szBuf, "wifi_connect -s \"%s\" -p \"%s\"", 
        transform_string(strSsid).c_str(), 
        transform_string(strPswd).c_str());

    LOG_D("try connect to wifi enter. szBuf=%s\n", szBuf);
    int nReturn = zBaseUtil::RW_System(szBuf);
    int nResult = -1;
    if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
    {
        nResult = 0;
    }
    LOG_D("try connect to wifi exit. nReturn=%d, nResult=%d\n", nReturn, nResult);

    return nResult;
}

int zWifiConnect::disconnect()
{
    int nResult = -1;
    const char *szBuf = "wifi_connect disconnect";

    LOG_D("try disconnect to wifi enter.\n");
    int nReturn = -1;
    nReturn = zBaseUtil::RW_System(szBuf);
    if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
    {
        nResult = 0;
    }

    return nResult;
}

std::string zWifiConnect::transform_string(
    const std::string &src)
{
    std::string dst = "";
    for (std::string::const_iterator it=src.begin(); it!=src.end(); ++it)
    {
        if (is_special_character(*it))
        {
            dst += '\\';
        }
        dst += *it;
    }
    return dst;
}

bool zWifiConnect::is_special_character(char ch)
{
    static const char special_characters[] = {'$', '`', '\\', '"'};
    for (uint8_t i=0; i<sizeof(special_characters); i++)
    {
        if (special_characters[i] == ch)
        {
            return true;
        }
    }
    return false;
}
