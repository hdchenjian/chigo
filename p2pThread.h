#ifndef __P2PTHREAD_H__
#define __P2PTHREAD_H__

#include "zThread.h"
#include <map>
#include <unordered_map>
#include <string>
#include <list>
#include <vector>

#include "3rdParty/webrtc/include/websocket_client.h"

typedef struct client_info_s
{
    std::string render;         // render 名称
    std::int64_t login_time;    // 登录时间, 单位: 毫秒
    std::string token;          // 登录凭证
}client_info_t;

//class zAlgManager;
//class zRokugaThread;
class zEakonThread;
class zP2PThread : public zThread
{
public:
    typedef struct tagp2pMsg{ // onRender msg 传来的消息结构
        std::string msgType;
        std::unordered_map<std::string, std::string> msgParam;
    }p2pMsg;

public:
    zP2PThread(zEakonThread* pParent, const char* szDevID = "abcac");
    ~zP2PThread();
public:
    static zP2PThread* getInstance();
    static void DestroyInstance();
    static int video_mode_from_resolution(int width, int height);
    static int video_mode_to_resolution(int mode, int& width, int& height);
    static int set_video_mode(int width, int height);
    static bool is_support_resolution(int width, int height);
    static std::vector<std::string> & split_with_delimiter(
        const std::string &str, char delimiter, std::vector<std::string> &elems);
    static void print_msg(const std::string &info);

    void Close();
    void proc();

    bool bDebug();
    void setDebug(bool bd);
    void NotifyRokuFileEnd(const std::string& filename);
    void NotifyDebugInfo(const std::string& debugInfo);
    
    int get_client_count();
    //void onEvent(int nEvent);
    void stop_peer_video_replay();

private:
    std::string to_string_muki();
    std::string get_state_impl(uint32_t index);
    
    std::string mkP2pMsg(const std::string& strAct, const std::string& strModel
        , const std::string& videoModel, const std::string& etc, const std::string& strDebug
        , const std::string& strRoku, const std::string& strVersion, const std::string& strWind
        , const std::string& strResolution);

    void OnRenderMSG_impl(std::string strRender, std::string strMsg, bool &changed);
    void OnRenderMSG(std::string strRender, std::string strMsg);

    void notify_response_impl(const std::string& response);
    void notifyP2P_change_resolution(const std::string& strAct, int roku_state);
    void notifyP2P(const std::string& strAct);
    void stop_video_live_response(
        const std::string& strAct, const std::string& render);
    bool client_erase(std::string& render);
    bool client_is_logined(std::string& render);

    void notify_process_start_info(const std::string& strAct);
    inline void state_changed(bool &changed);

public:
    //static unsigned int m_guInstID;
    static std::string m_deviceId;
private:
    static zP2PThread* m_instance;
    bool m_bDebug;
    std::mutex m_threadMutex;
    std::list<client_info_t> m_client_list;

    //zAlgManager* m_pManager;
    zEakonThread* m_pEakonThread;
    //bool m_pgLiveForwardAlloced;
    bool m_p2p_enable;
    std::string m_video_replay_peer_name;
    bool m_server_shutdown_retry = false;
    std::string m_deviceName;
    std::string strSrvAddr;
};

#endif
