#ifndef __ZWIFICONNECT_H__
#define __ZWIFICONNECT_H__

#include <string>
#include <mutex>
#include <chrono>

class zWifiConnect
{
public: 
    enum wifiEvent
    {
        wifi_c = 0,
        wifi_d,
    };
public:
    static zWifiConnect* getInstance( );
    static int destroyInstance();
    void init(std::string ssid, std::string pswd = "");
    //void eventNotify(void* pNoti);
    int startConnect();
    int connect();
    int disconnect();
    void beginC();
    void endC();
    bool IsConnecting();
    bool is_wifi_connect_done();

private:
    void refresh(std::string ssid, std::string pswd = "");
    std::string transform_string(const std::string &src);
    bool is_special_character(char ch);

private:
    zWifiConnect( );
    ~zWifiConnect();
private:
    bool bC_;

    //static int nReference_;
    static zWifiConnect* pInstance_;

    std::string strSsid_;
    std::string strPswd_;
    std::mutex mutex_;
    std::int64_t ConnectGap_;
    std::int64_t latestConnectTime_;
};

#endif // !__ZWIFICONNECT_H__
