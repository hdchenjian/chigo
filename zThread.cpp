#include "zThread.h"
#include "zLog.h"
#include "zUtil.h"

zThread::zThread()
    : m_threadState(NEW_THREAD)
    , m_pThread(NULL)
{
}

zThread::~zThread()
{
    LOG_I("zThread::~zThread() start \n");
    Close();
    LOG_I("zThread::~zThread() end \n");
}

void zThread::Start()
{
    LOG_D(" zThread::Start() \n");
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_threadState = RUNABLE_THREAD;
    }
    m_pThread = new std::thread(threadProc, this);
    LOG_D(" zThread::Start() end\n");
    return;
}

void zThread::Close()
{ 
    if (NULL == m_pThread) return;

    {
        LOG_I("zThread::Close befor lock \n");
        std::unique_lock<std::mutex> lock(m_mutex);
        LOG_I("zThread::Close end lock \n");
        m_threadState = DEAD_THREAD;
    }

    LOG_I("before join. thread id=%u\n", zBaseUtil::getThreadId());
    m_pThread->join();
    LOG_I("after join. thread id=%u\n", zBaseUtil::getThreadId());

    delete m_pThread;
    m_pThread = NULL;
    return;
}

void zThread::Wait()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    m_threadState = BLOCKED_THREAD;
}

void zThread::WakeUp()
{
    std::unique_lock <std::mutex> lock(m_mutex);
    m_threadState = RUNNING_THREAD;
    m_cond.notify_one();
    return;
}

bool zThread::Is(ThreadState state)
{
    std::unique_lock <std::mutex> lock(m_mutex);
    return m_threadState == state;
}
void zThread::BreakPoint()
{
    std::unique_lock <std::mutex> lock(m_mutex);
    while (/*Is(BLOCKED_THREAD)*/BLOCKED_THREAD == m_threadState)
    {
        m_cond.wait(lock);
    }
}
void zThread::proc()
{
    zThreadMsg msg;
    if (getMsg(msg))
    {
        OnMsg(msg);
    }

    return;
}

void zThread::threadProc(void* param)
{
    LOG_I("threadProc: param=%0x thread id == %u\n", param, zBaseUtil::getThreadId());
    zThread* pThread = (zThread*)param;
    while (!pThread->Is(ThreadState::DEAD_THREAD))
    {
        pThread->BreakPoint();
        pThread->proc();
        //std::this_thread::sleep_for(std::chrono::microseconds(1000));
    }
    LOG_I("threadProc: param=%0x thread id == %u end \n", param, zBaseUtil::getThreadId());
    return;
}

int zThread::SendThreadMsg(zThread* pThread, const zThreadMsg& msg)
{
    pThread->OnMsg(msg);
    return 0;
}

int zThread::PostThreadMsg(zThread* pThread, const zThreadMsg& msg)
{
    return pThread->addMsg(msg);
}

int zThread::GetThreadMsg(zThread* pThread, zThreadMsg& msg)
{
    return pThread->getMsg(msg);
}

int zThread::PeerThreadMsg(zThread* pThread, zThreadMsg& msg)
{
    return pThread->peerMsg(msg);
}

int zThread::OnMsg(const zThreadMsg& msg)
{
    // do msg
    std::unique_lock<std::mutex> lock(m_msgMutex);

    return 0;
}

int zThread::addMsg(const zThreadMsg& msg)
{
    std::unique_lock<std::mutex> lock(m_msgMutex);
    m_msgQueue.push(msg);

    return 0;
}
int zThread::getMsg(zThreadMsg& msg)
{
    std::unique_lock<std::mutex> lock(m_msgMutex);
    msg = m_msgQueue.top();
    m_msgQueue.pop();

    return 0;
}

int zThread::peerMsg(zThreadMsg& msg)
{
    std::unique_lock<std::mutex> lock(m_msgMutex);
    msg = m_msgQueue.top();

    return 0;
}