
#include "3rdParty/FaceRecov2/cFaceProc.h"

#include "zAlgorithmManager.h"
#include "zWifiConnect.h"
#include "zRokugaThread.h"
#include "zEakonThread.h"
#include "zAlgorithmDetectThread.h"
#include "zVideoThread.h"
#include "config_mgr.h"
#include "p2pThread.h"
#include "device_test.h"
#include "zLog.h"
#include "zUtil.h"

#define FACE_SUCC (1)
#define FACE_BODY_SUCC (2)
#define TEMANE_SUCC (4)
#define CB_SUCC (8)

static int beforInit()
{
    //zBaseUtil::RW_System("echo 1 > /proc/rst_state");
    zBaseUtil::RW_Echo("1", strlen("1"), "/proc/rst_state");
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    return 0;
}

zAlgManager::zAlgManager(int nWidth, int nHeight
    , std::string strDevName /* = "/dev/mcu_dev" */)
    : m_cameraType(DEFAULT_CAMERA)
    , m_nImgWidth(nWidth)
    , m_nImgHeght(nHeight)
    , m_strDevName(strDevName)
    , m_temaneThread(NULL)
    , m_faceBodyThread(NULL)
    , m_algDetThread(NULL)
    , m_eakonThread(NULL)
    , m_ieModel(zBaseUtil::zEakonModel::IE_MODEL)
    , m_videoState(zBaseUtil::zVideoState::CLOSED_VIDEO)
    , m_eakonState(zAlgManager::EakonState::OPENED_EAKON)
    , m_latestCBFrameTime(0)
    , m_curCBFrameTime(0)
{
    memset(&m_curFrame, 0, sizeof(S_VCBuf));
    encoded_frame_queue_init();

    LOG_I("zAlgManager::zAlgManager nWidth == %d, nHeight == %d\n", nWidth, nHeight);
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
    std::string val = conf.getValue("camerType");
    if (!val.empty())
    {
        m_cameraType = atoi(val.c_str());
    }
    //////////////////////////////////////////////////////////////////////////

    for (int i = 0; i < (int)zAlgManager::Threads::THREADSIZE; i++) g_bFresh[i] = false;

    //g_bFresh[0] = g_bFresh[1] = g_bFresh[2] = false;

    LOG_I("ChipsetConfig_LicenseParamInit before w = %d, h= %d\n", nWidth, nHeight); 
    beforInit();
    bool bResult = ChipsetConfig_LicenseParamInit("/dev/ttyS2", 9600);
    if (!bResult)
    {
        int nErr = zEakonThread::getLastErr();
        if ( !(nErr & EAKON_CHIP_ERR))
        {
            nErr |= EAKON_CHIP_ERR;
            zEakonThread::setLastErr(nErr);
        }
        LOG_E("ChipsetConfig_LicenseParamInit() failed!\n");
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        if (nErr & EAKON_CHIP_ERR)
        {
            nErr &= ~EAKON_CHIP_ERR;
            zEakonThread::setLastErr(nErr);
        }
    }

    m_algDetThread = new zAlgorithmDetectThread(this, m_cameraType, nWidth, nHeight);
    m_eakonThread = new zEakonThread(this, nWidth, nHeight);

    (void)zBaseUtil::write_status_file();
    return;
}

zAlgManager::~zAlgManager()
{
    if (NULL != m_algDetThread)
    {
        delete m_algDetThread;
        m_algDetThread = NULL;
    }

    if (NULL != m_eakonThread)
    {
        delete m_eakonThread;
        m_eakonThread = NULL;
    }
    FaceProcessor_FaceFinalize();
    BodyFaceProcessor_BodyFaceFinalize();
    GestureProcessor_GestureFinalize();

    zWifiConnect::destroyInstance();
    LOG_I("zAlgManager::~zAlgManager() end \n");

    encoded_frame_queue_end();
}

int zAlgManager::StartThreads()
{
    int nThreads = 0;
    if (NULL != m_eakonThread)
    {
        m_eakonThread->Start();
        ++nThreads;
    }

    if (NULL != m_algDetThread)
    {
        m_algDetThread->Start();
        ++nThreads;
    }
    
    //return nThreads == 2 ? 0 : -1;
    //zWifiConnect::getInstance()->eventNotify((void*)this);
    return 0;
}

int zAlgManager::p2pEvent(int nEvent)
{
    return 0;
}

bool zAlgManager::addFrame(const S_VCBuf& buf)
{
    if (NULL == this) return false;

    std::unique_lock<std::mutex> lock(m_bufMutex);
    unsigned char* pBuf = m_curFrame.buf;
    if (buf.len != m_curFrame.len || NULL == m_curFrame.buf)
    {
        //LOG_D("(buf.len != m_curFrame.len )\n");
        if (NULL != m_curFrame.buf)
        {
            LOG_I("NULL != m_curFrame.buf \n");
            free(m_curFrame.buf);
            m_curFrame.buf = NULL;
        }

        //LOG_D("(unsigned char*)malloc( %d ) \n", buf.len);
        pBuf = (unsigned char*)malloc(buf.len);
        if (NULL == pBuf)
        {
            LOG_I("addFrame failed!\n");
            return false;
        }
    }
    else
    {
        pBuf = m_curFrame.buf;
    }
    
    memcpy(&m_curFrame, &buf, sizeof(S_VCBuf));
    m_curFrame.buf = pBuf;

    // convert nv21 to yuv420
    int y_size = m_nImgWidth * m_nImgHeght;
    (void)memcpy(pBuf, buf.buf, y_size);

    unsigned char* uv_src = buf.buf + y_size;
    unsigned char* uv_src_end = buf.buf + y_size + y_size/2;
    unsigned char* u_dst = pBuf + y_size;
    unsigned char* v_dst = pBuf + y_size + y_size/4;
    while (uv_src < uv_src_end)
    {
        *v_dst++ = *uv_src++;
        *u_dst++ = *uv_src++;
    }

    //m_bFresh = true;
    //g_bFresh[0] = g_bFresh[1] = g_bFresh[2] = g_bFresh[3] = true;

    for (int i = 0; i < (int)zAlgManager::Threads::THREADSIZE; i++) g_bFresh[i] = true;

    m_curTemaneFrameTime = zBaseUtil::getMilliSecond();
    m_curCBFrameTime = zBaseUtil::getMilliSecond();

    //LOG_D("zAlgManager::addFrame end \n");
    return true;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.free_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 释放帧
 * 输入参数  : S_VCBuf& src  帧结构
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::free_frame(S_VCBuf& src)
{
    if (NULL != src.buf)
    {
        free(src.buf);
        src.buf = NULL;
    }
    
    (void)memset(&src, 0, sizeof(src));
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.alloc_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 分配帧，如果已分配内存，先释放，再分配
 * 输入参数  : S_VCBuf& src    帧结构
               int frame_size  帧大小
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::alloc_frame(S_VCBuf& src, int frame_size)
{
    free_frame(src);    
    unsigned char* pBuf = (unsigned char*)malloc(frame_size);
    if (NULL == pBuf)
    {
        LOG_E("malloc failed!");
        return false;
    }

    src.buf = pBuf;
    src.len = src.BytesUsed = frame_size;
    return true;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.copy_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 复制帧数据
 * 输入参数  : S_VCBuf& src  源帧
               S_VCBuf& dst  目的帧
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::copy_frame(S_VCBuf& dst, S_VCBuf& src)
{
    if (dst.len != src.len)
    {
        if (!alloc_frame(dst, src.len))
        {
            return false;
        }
    }

    unsigned char* pBuf = dst.buf;
    (void)memcpy(&dst, &src, sizeof(S_VCBuf));
    (void)memcpy(pBuf, src.buf, src.len);
    dst.buf = pBuf;
    return true;
}

bool zAlgManager::getFrame(S_VCBuf& buf, zAlgManager::Threads who /* = TEMANETHREAD */)
{
    if (NULL == this) return false;

    std::unique_lock<std::mutex> lock(m_bufMutex);

    if (Threads::THREADANONYMOUS != who && !g_bFresh[(int)who])
    {
        //LOG_D("zAlgManager::getFrame  !g_bFresh[(int)who] \n");
        return false;
    }

    if (!copy_frame(buf, m_curFrame))
    {
        LOG_I("copy_frame failed. size=%d\n", m_curFrame.len);
        return false;
    }

    if (Threads::THREADANONYMOUS != who) g_bFresh[(int)who] = false;

    if (who == zAlgManager::TEMANETHREAD)
    {
        buf.TimeStamp = zBaseUtil::getMilliSecond() - m_lastestTemaneFrame;
        m_lastestTemaneFrame = m_curTemaneFrameTime;
    }
    else if (who == zAlgManager::CBTHREAD)
    {
        buf.TimeStamp = zBaseUtil::getMilliSecond() - m_latestCBFrameTime;
        m_latestCBFrameTime = m_curCBFrameTime;
    }

    return true;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.getFrame2
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月4日
 * 函数功能  : 获取YUV图像
 * 输入参数  : S_VCBuf& buf              获取图像及宽高
               int &width                返回的宽度
               int &height               返回的高度
               zAlgManager::Threads who  线程名
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::getFrame2(S_VCBuf& buf,  
    int &width, int &height, zAlgManager::Threads who)
{
    width = m_nImgWidth;
    height = m_nImgHeght;
    return getFrame(buf, who);
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.free_encoded_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 释放编码帧内存
 * 输入参数  : S_H264EncBuf &encoded_frm  编码帧
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::free_encoded_frame(S_H264EncBuf &encoded_frm)
{
    if (NULL != encoded_frm.buf)
    {
        free(encoded_frm.buf);
        encoded_frm.buf = NULL;
    }

    (void)memset(&encoded_frm, 0, sizeof(S_H264EncBuf));
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.encoded_framee_queue_init
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : H264编码帧队列初始化
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::encoded_frame_queue_init()
{
    (void)memset(m_encoded_frame_queue, 0, sizeof(m_encoded_frame_queue));

    m_encoded_frame_rindex = 0;
    m_encoded_frame_windex = 0;
    m_encoded_frame_queue_size = 0;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.encoded_framee_queue_end
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : H264编码帧队列清理
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::encoded_frame_queue_end()
{
    for (int i=0; i < ENCODED_FRAMES_COUNT; i++)
    {
        zAlgManager::free_encoded_frame(m_encoded_frame_queue[i].m_enc_img);
    }
    encoded_frame_queue_init();
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.copy_encoded_frame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 拷贝编码帧
 * 输入参数  : encoded_frame_t &dst     目的编码帧
               const S_H264EncBuf &src_buf  源编码帧
               std::int64_t TimeStamp   时间戳
 * 输出参数  : 无
 * 返 回 值  : bool: true:成功， false: 失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::copy_encoded_frame(
    encoded_frame_t &dst, const S_H264EncBuf &src_buf, std::int64_t TimeStamp)
{
    S_H264EncBuf &dst_buf = dst.m_enc_img;
    unsigned char* pBuf = dst_buf.buf;

    if (    (dst_buf.len < src_buf.len)
        ||  (NULL == dst_buf.buf) )
    {
        if (NULL != dst_buf.buf)
        {
            free(dst_buf.buf);
            dst_buf.buf = NULL;
        }

        pBuf = (unsigned char*)malloc(src_buf.len);
        if (NULL == pBuf)
        {
            LOG_E("malloc failed! size=%d\n", src_buf.len);
            return false;
        }
    }

    (void)memcpy(&dst_buf, &src_buf, sizeof(S_H264EncBuf));
    (void)memcpy(pBuf, src_buf.buf, src_buf.BytesUsed);
    dst_buf.buf = pBuf;
    dst.m_enc_time_stamp = TimeStamp;

    return true;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.addEncFrame
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 向队列中添加一帧编码图像
 * 输入参数  : const S_H264EncBuf& buf  H264编码帧
               std::int64_t TimeStamp   时间戳
 * 输出参数  : 无
 * 返 回 值  : bool: true: 成功添加，false: 添加失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::addEncFrame(
    const S_H264EncBuf& buf, std::int64_t TimeStamp)
{
    std::unique_lock<std::mutex> lock(m_enc_frm_mutex);

    while (m_encoded_frame_queue_size >= ENCODED_FRAMES_COUNT)
    {
        //LOG_D("abandon frame. m_encoded_frame_rindex=%d\n", m_encoded_frame_rindex);

        // 读索引前移，相当于丢弃一帧编码图像
        ++m_encoded_frame_rindex;
        if (m_encoded_frame_rindex >= ENCODED_FRAMES_COUNT)
        {
            m_encoded_frame_rindex = 0;
        }

        --m_encoded_frame_queue_size;
    }

    int ret = false;
    encoded_frame_t* pAddBuf = &(m_encoded_frame_queue[m_encoded_frame_windex]);
    if (copy_encoded_frame(*pAddBuf, buf, TimeStamp))
    {
        ++m_encoded_frame_windex;
        if (m_encoded_frame_windex >= ENCODED_FRAMES_COUNT)
        {
            m_encoded_frame_windex = 0;
        }

        ++m_encoded_frame_queue_size;
        ret = true;
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.getEncFrames
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 获取编码帧
 * 输入参数  : encoded_frame_t frms[ENCODED_FRAMES_COUNT]  返回的编码帧数组
               int &frms_size                              返回编码帧个数
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::getEncFrames(
    encoded_frame_t frms[ENCODED_FRAMES_COUNT], int &frms_size)
{
    std::unique_lock<std::mutex> lock(m_enc_frm_mutex);

    frms_size = 0;
    while (m_encoded_frame_queue_size > 0)
    {
        encoded_frame_t &src = m_encoded_frame_queue[m_encoded_frame_rindex];
        if (copy_encoded_frame(frms[frms_size], src.m_enc_img, src.m_enc_time_stamp))
        {
            ++frms_size;
        }

        // 读索引前移，读走一帧编码图像
        ++m_encoded_frame_rindex;
        if (m_encoded_frame_rindex >= ENCODED_FRAMES_COUNT)
        {
            m_encoded_frame_rindex = 0;
        }

        --m_encoded_frame_queue_size;
    }

    return (0 != frms_size);
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.clrEncFrames
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月13日
 * 函数功能  : 复位编码帧缓存队列
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::clrEncFrames()
{
    std::unique_lock<std::mutex> lock(m_enc_frm_mutex);
    m_encoded_frame_rindex = 0;
    m_encoded_frame_windex = 0;
    m_encoded_frame_queue_size = 0;
    return;
}

void zAlgManager::onEvent(int nEvent)
{
    if (NULL == this) return;
    LOG_D("zAlgManager::onEvent %d enter \n", nEvent);
    
    m_algDetThread->onEvent(nEvent);

    if (ZM_NEEDREAD == nEvent
        || ZM_WIFICONNECTED == nEvent
        || ZM_OPEN == nEvent
        || ZM_CLOSE == nEvent
        || ZM_IEMODEL == nEvent
        || ZM_DEKAKEMODEL == nEvent)
    {
        m_eakonThread->onEvent(nEvent);
    }
    //if (ZM_TIMESYNC == nEvent)
    //{
    //  // 
    //}
    ////连上网了
    //if (ZM_WIFICONNECTED == nEvent)
    //{
    //  m_eakonThread->onEvent(nEvent);
    //}
    //
    //if (ZM_OPEN == nEvent
    //  || ZM_CLOSE == nEvent)
    //{
    //  m_eakonThread->refreshState();
    //}
    LOG_D("zAlgManager::onEvent %d end \n", nEvent);
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.setEakonModel
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月22日
 * 函数功能  : 设置安防模式
 * 输入参数  : zBaseUtil::zEakonModel model  安防模式
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::setEakonModel(zBaseUtil::zEakonModel model)
{
    /* add by 刘春龙, 2016-01-22, 原因: 开启安防时，可能存在两次入侵告警 */
    int nEvent = (model == zBaseUtil::zEakonModel::IE_MODEL) ? ZM_IEMODEL : ZM_DEKAKEMODEL;
    onEvent(nEvent);

    {
        std::unique_lock<std::mutex> lock(m_modelMutex);
        if (model != m_ieModel) 
        {
            m_ieModel = model;
            LOG_I("zAlgorithmTrehad::chgModel %d\n", (int)model);
        }
    }
    return;
}

zBaseUtil::zEakonModel zAlgManager::getEakonModel()
{
    if (NULL == this) return zBaseUtil::zEakonModel::UNKNOWN_MODEL;
    std::unique_lock<std::mutex> lock(m_modelMutex);
    //LOG_D("zAlgorithmTrehad::getEAKonModel %d\n", (int)m_IEModel);
    return m_ieModel;
}

void zAlgManager::setVideoState(zBaseUtil::zVideoState state)
{
    m_videoState = state;
    return;
}

zBaseUtil::zVideoState zAlgManager::getVideoState()
{
    return m_videoState;
}

void zAlgManager::setEakonState(int state)
{
    if (NULL == this) return;
    std::unique_lock<std::mutex> lock(m_stateMutex);
    if (state == m_eakonState) return;

    if (state == EakonState::CLOSED_EAKON) onEvent(ZM_CLOSE);
    else if (state == EakonState::OPENED_EAKON) onEvent(ZM_OPEN);

    LOG_I("eakon state =%d\n", state);
    m_eakonState = state;
    return;
}

int zAlgManager::getEakonState()
{
    if (NULL == this) return 0;
    std::unique_lock<std::mutex> lock(m_stateMutex);
    return m_eakonState;
}

int zAlgManager::getKazeParama(int& x, int& y)
{
    if (NULL == this) return -1;
    return m_eakonThread->getKazeParama(x, y);
}

bool zAlgManager::bFocus()
{
    if (NULL == this) return true;
    return m_eakonThread->bFocus();
}

int zAlgManager::KazemukiCtrl(int* arr, bool gesture_flag)
{
    if (NULL == this) return -1;
    return m_eakonThread->KazemukiCtrl(arr, gesture_flag);
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.get_smart_wind
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 获取智能风状态 -- 是否禁止
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgManager::get_smart_wind()
{
    if (NULL == this) return -1;
    return m_eakonThread->getKazeMuki();
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.get_gesture_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 获取手势功能标志
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgManager::get_gesture_flag()
{
    if (NULL == this) return -1;
    return m_eakonThread->getGestureFlag();
}

int zAlgManager::TaninnFind_BgnStranger()
{ 
    if (NULL == this) 
    {
        return 0;
    }
    
    zVideoThread* video_thread = zVideoThread::getInstance();
    if (NULL != video_thread)
    {
        return (video_thread->onEvent(ZM_BEGIN_ROKUSTRANGER));
    }

    LOG_E("may be error.\n");
    return -1;
}

int zAlgManager::TaninnLose_EndStranger()
{
    if (NULL == this)
    {
        return 0;
    }

    zVideoThread* video_thread = zVideoThread::getInstance();
    if (NULL != video_thread)
    {
        return (video_thread->onEvent(ZM_END_ROKUSTRANGER));
    }

    LOG_E("may be error.\n");
    return -1;
}

int zAlgManager::TaninnLose_EakonNotify()
{
    if (NULL == this)
    {
        return 0;
    }

    return m_eakonThread->TaninnLose();
}

int zAlgManager::TaninnFind_EakonNotify()
{ 
    if (NULL == this) 
    {
        return 0;
    }
    
    return m_eakonThread->TaninnFind();
}

void zAlgManager::get_lighting_mode(bool &auto_mode, bool &day)
{
    m_algDetThread->get_lighting_mode(auto_mode, day);
}

bool zAlgManager::autoLighting(bool bAuto)
{
    return m_algDetThread->autoLighting(bAuto);
}

void zAlgManager::chgLighting(bool bOpen)
{
    m_algDetThread->chgLighting(bOpen);
    return;
}
//////////////////////////////////////////////////////////////////////////

int zAlgManager::eakonCtrl(bool bOpen)
{
    if (NULL == this) return -1;
    
    bool bOpened = getEakonState() & zAlgManager::EakonState::OPENED_EAKON;
    //LOG_I("zAlgorithmTrehad::eakonCtrl, bOpened=%d, bOpen=%d \n", bOpened, bOpen);
    if (bOpened == bOpen)
    {
        LOG_I("zAlgorithmTrehad::eakonCtrl bOpened == bOpen\n");
        return -1;
    }
    int nReturn = bOpen ? m_eakonThread->OpenAirConditioner() : m_eakonThread->CloseAirConditioner();
    if (-1 != nReturn)
    {
        int state = bOpen ? EakonState::OPENED_EAKON : EakonState::CLOSED_EAKON;
        setEakonState(state);
    }
    //pgLiveNotify(zP2PThread::m_guInstID, bOpen ? "eakon open" : "eakon close");
    LOG_I("#####%s %lld\n", bOpen ? "eakon open" : "eakon close", zBaseUtil::getMilliSecond());
    return nReturn;
}

int zAlgManager::TemaneCtrl(bool bFocus)
{
    if (NULL == this) return -1;
    int nResult = m_eakonThread->TemaneCtrl(bFocus);
    return nResult;
}

int zAlgManager::TemaneCtrl2(bool bFocus)
{
    if (NULL == this) return -1;
    int nResult = m_eakonThread->TemaneCtrl2(bFocus);
    return nResult;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.auto_swipe_wind
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 自动扫风
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgManager::auto_swipe_wind()
{
    if (NULL == this) return -1;
    int nResult = m_eakonThread->auto_swipe_wind();
    return nResult;
}

zEakonThread* zAlgManager::getEakon()
{
    if (NULL == this) return NULL;
    //std::unique_lock<std::mutex> lock(m_stateMutex);
    return m_eakonThread;
}

zAlgorithmDetectThread* zAlgManager::getAlgorithmDetect()
{
    if (NULL == this) return NULL;
    //std::unique_lock<std::mutex> lock(m_stateMutex);
    return m_algDetThread;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.get_live_video_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月5日
 * 函数功能  : 获取"远程实时视频功能"开关配置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::get_live_video_state()
{
    if (NULL == this) return true;
    return m_eakonThread->get_live_video_state();
}

/*****************************************************************************
 * 函 数 名  : get_p2Pclient_count
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月15日
 * 函数功能  : 获取登录的P2P客户端数目
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int : 登录数
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgManager::get_p2pclient_count()
{
    int client_size = 0;
    zP2PThread * p2p_thread = zP2PThread::getInstance();
    if (NULL != p2p_thread)
    {
        client_size = p2p_thread->get_client_count();
    }
    return client_size;
}

/*****************************************************************************
 * 函 数 名  : get_dt_enable
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月15日
 * 函数功能  : 是否进入工装模式
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgManager::get_dt_enable()
{
    DtThread *pDt = DtThread::getInstance();
    if (NULL != pDt)
    {
        return pDt->dt_is_started();
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : get_gesture_flags
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 获取手势功能状态
 * 输入参数  : uint32_t &gesture      手势功能标志
               uint32_t &raise_count  举手次数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgManager::get_gesture_flags(uint32_t &gesture, uint32_t &raise_count)
{
    m_algDetThread->get_gesture_flags(gesture, raise_count);
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgManager.ZoneStayTimeup
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月27日
 * 函数功能  : 执行相应的动作
 * 输入参数  : ZONE_POS zone  悬停区域
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgManager::ZoneStayTimeup(ZONE_POS zone)
{
    if (NULL == this) return -1;
    int nResult = m_eakonThread->ZoneStayTimeup(zone);
    return nResult;
}

