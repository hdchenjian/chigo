#ifndef __RW_PROCESS_START_HPP__
#define __RW_PROCESS_START_HPP__

#include <cstdint>
#include "zUtil.h"

class ProcessStartCount 
{
public:
    static ProcessStartCount * get_instance()
    {
        if (NULL == instance_)
        {
            instance_ = new ProcessStartCount();
        }
        return instance_;
    }

    void destroy_instance()
    {
        if (NULL != instance_)
        {
            delete instance_;
            instance_ = NULL;
        }
    }

    uint32_t count() { return count_; }
    std::int64_t begin_time() { return begin_time_; }

private:
    ProcessStartCount() {
        (void)parse_xml();
        begin_time_ = zBaseUtil::getMicroSecond();
    }

    ~ProcessStartCount() {
    }    

    bool file_exist(const char * full_filename);
    bool create_xml();
    bool parse_xml();

private:
    static ProcessStartCount * instance_;
    static const char * persistence_store_;

    unsigned int count_ = 1;
    std::int64_t begin_time_ = 0;
};

#endif
