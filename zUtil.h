#ifndef __PARSERXML_H_
#define __PARSERXML_H_
#define MAX_NAME (256)

#define FACE_H (-1) // 头
#define FACE_F (0)
#define FACE_L (1)
#define FACE_R (2)

#define CAMERA_90 (90)
#define CAMERA_60 (60)
#define DEFAULT_CAMERA (90)
#define DEFAULT_FACE_F (250)
#define DEFAULT_FACE_C (500)

#define DETECT_INTERVAL (30)        // ms
#define DETECT_BODY_TH  (0.6)
#define DETECT_FACE_TH  (0.9)
//////////////////////////////////////////////////////////////////////////

#define G2IXX       (1000)
#define G2DXX       (1000.0)
//#define G2IMILSEC (1000)
#define G2ISEC  (1000 * 1000)
#define G2DSEC  (1000 * 1000.0)
//////////////////////////////////////////////////////////////////////////
#define ZM_OPEN (0)                 // 需要与空调交互的消息， 打开空调
#define ZM_CLOSE (1)                // 关闭空调
#define ZM_TEMANEIN (2)             // 发现手
#define ZM_TEMANEOUT (3)            // 丢失手
#define ZM_KAZECTRL (4)             // 风向控制
#define ZM_TANINN (5)               // 发现陌生人
#define ZM_UNKNOWN (6)              // 未知状态
#define ZM_WIFI (7)                 // wifi 信息（连上 断开之类）
#define ZM_DEVICEID (8)             // 查询deice id
#define ZM_SSID (9)
#define ZM_TANINNLOSE (10)
#define ZM_TEMPERATURE (11)         // 温度加，温度减
#define ZM_SIZE (12)                // 这是空调相关信息的大小（有多少个设置多大值）

// for etc
#define ZM_INVALID (-1)
#define ZM_IEMODEL (100)                // 在家模式
#define ZM_DEKAKEMODEL (101)            // 出门模式
#define ZM_NEEDREAD (102)               // MCU上报的 消息，空调相关（需要立马读，也就是下一个proc读
#define ZM_TIMESYNC (103)               // 时间同步了，（在wificonnect 线程连上外网之后会进行时间同步 并发出这个消息）
#define ZM_ONLINE (104)                 // 在线直播消息（zvideothread 以及 zRokuUpThread::getinstance 处理相关消息） 
#define ZM_ROKUUP (105)                 // 上传录像消息（zvideothread 以及 zRokuUpThread::getinstance 处理相关消息） 
#define ZM_ROKUUPSTOP (106)             // 停止上传录像（传直播？）
#define ZM_ROKU (107)                   // 开始普通录制
#define ZM_BEGIN_ROKUSTRANGER (108)         // 录制陌生人
#define ZM_ROKUSTOP (109)               // 停止普通录制
#define ZM_QUERYVIDEO (110)             // 查询录像文件
#define ZM_WIFICONNECTED (111)          // WIFI连上之后发消息
#define LIGHTSTATE (112)
#define ZM_CLOSELIGHT (112)
#define ZM_BLUELIGHT (114)
#define ZM_WHITELIGHT (115)
#define ZM_WBGLINT (116)
#define ZM_BGLINT (117)
#define ZM_WGLINT (118)
#define ZM_END_ROKUSTRANGER (120)
#define ZM_QUERYVIDEO_BASEONTIME (130)  // 按时间段，查询录像文件

#define ZM_JUST_SEND 400 // 直接发送 
//////////////////////////////////////////////////////////////////////////
// err type
// 0x04
#define EAKON_ERR_0x04 (1)
#define EAKON_ERR_0xa0 (2)
// val
#define EAKON_NO_ERR (0)
// for 0x04
#define EAKON_COMMU_ERR (1)
#define EAKON_NO_RUTURN (2)
#define EAKON_NO_ROUTERINFO (4)
#define EAKON_NOT_CONNECT_AP (8)
// for 0xa0
#define  EAKON_ERR_EXIST (1 << 8)
#define  EAKON_VIDEO_ERR (2 << 8)
#define  EAKON_WIFI_ERR (4 << 8)
#define  EAKON_CHIP_ERR (8 << 8)
//////////////////////////////////////////////////////////////////////////

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <chrono>
#include <vector>
#include <mutex>
#include <sstream>

#include "I2CTag.h"
#include "file_date.h"

#define ROKUGAG_FRAME_GAP 80

enum ZONE_POS
{
    NULL_ZONE = 0,
    CENTER_ZONE,
    LEFT_ZONE,
    RIGHT_ZONE,
    TOP_ZONE,
    BOTTOM_ZONE
};

namespace zBaseUtil
{
    extern std::mutex g_resolution_video_mutex;  // 切分辨率和抓图，互斥锁
    extern std::mutex g_resolution_record_mutex; // 切分辨率和录像，互斥锁
    extern const char *sync_time_flag_file;      // 是否有同步标志

    enum zVideoState{
        UNKNOWN_VIDEO = 0,
        CLOSED_VIDEO = 1,
        //OPENED_VIDEO = 2,
        UPROKU_VIDEO = 4,
        ONLINE_VIDEO = 8,
        
    };
    enum zEakonModel{
        UNKNOWN_MODEL = 0,
        IE_MODEL,
        DEKAKE_MODEL
    };

    typedef struct tag_resolution
    {
        int width;
        int height;

        tag_resolution()
            :width(0), height(0)
        {
        }
    }resolution_t;

    typedef struct tagQueryParam
    {
        std::int32_t id;    // id值
        std::int32_t top;   // 查询个数
        std::int32_t skip;  // 跳过个数
        std::string render;
        std::string type;
    }QueryParam;

    typedef struct tagQueryParamBaseOnTime
    {
        RW::file_date bgn_date;
        RW::file_date end_date;
        std::string type;
        std::int32_t max_count = 0;
        std::string render;
    }QueryParamBaseOnTime;

    typedef struct tagDownloadParam
    {
        std::string filename;
        std::string renderPath;
        std::string render;
    }DownloadParam;

    typedef struct tagMsg
    {
        std::uint32_t msg;
        time_t time;
        void* pparam;
        int nparam;
        tagMsg()
            : msg(ZM_UNKNOWN)
            , pparam(NULL)
            , nparam(-1)
        {
            std::chrono::time_point<std::chrono::system_clock> cur = (std::chrono::time_point<std::chrono::system_clock>::max)();
            time = std::chrono::system_clock::to_time_t(cur);
        }
    }zMsg;

    typedef struct tagRect
    {
        int left;
        int top;
        int right;
        int bottom;
    }zRect;
    
    typedef struct tagPoint2
    {
        int cx;
        int cy;
    }zPoint2;

    typedef struct tagPoint3
    {
        int cx;
        int cy;
        int cz;
    }zPoint3;

    typedef struct tagHeaderInfo
    {
        int headLable ;
        int headLeft ;
        int headTop ;
        int headRight ;
        int headBottom ;
        int headType ;
        double headProb ;
    }HeadInfo, *pHeadInfo;

    typedef struct tagUnndorInfo
    {
        char name[MAX_NAME];
        long timeStamp;
        int udLighting;
        int udCnt;
        int udLable;
        int udLeft;
        int udTop;
        int udRight;
        int udBottom;
    }UnndouInfo, *pUnndouInfo;

    typedef struct tagHeadersInfo
    {
        char name[MAX_NAME] ;
        long timeStamp ;
        double headLighting;
        int headCnt ;

        HeadInfo heads[MAX_HEAD_CNT] ;

    }HeadersInfo, *pHeadersInfo;

    typedef struct tagKao
    {
        char name[MAX_NAME];
        long timeStamp;
        int kaoLighting;
        int kaoCnt;
        int kaoLable;
        int kaoLeft;
        int kaoTop;
        int kaoRight;
        int kaoBottom;
        int kaoType;
        int kaoPid;
        int kaoTid;
    }KaoInfo, *pKaoInfo;

    typedef struct tagTemane
    {
        char name[MAX_NAME];
        long timeStamp;
        int teCnt;
        double teLighting;
        int teLable;
        int teLeft;
        int teTop;
        int teRight;
        int teBottom;
        int teType;
        double teProb;
        double teVelx;
        double teVely;
        double teVellogr;
        double teMouseAbsx;
        double teMouseAbsy;
        int teMouseEvent;
    }TemaneInfo, *pTemaneInfo;

    typedef struct tagDa
    {
        int d;
        int a;
    }Da;

    enum deviceCmd
    {
        OPEN_DOOR = 0 
    };
    //////////////////////////////////////////////////////////////////////////
    // for v2
    typedef struct tagv2Hand
    {
        int label_;
        int left_;
        int top_;
        int right_;
        int bottom_;
        int type_;
        double prob_;
        int stable_;
        double liveness_;
        tagv2Hand()
        {
            label_ = type_ = prob_ = stable_ = liveness_ = -1;
            left_ = top_ = right_ = bottom_ = 0;
        }
    } v2Hand;
    
    typedef struct tagv2Head
    {
        int label_;
        int left_;
        int top_;
        int right_;
        int bottom_;
        int type_;
        double prob_;
        int stable_;
        double liveness_;
        tagv2Head()
        {
            label_ = type_ = prob_ = stable_ = liveness_ = -1;
            left_ = top_ = right_ = bottom_ = 0;
        }
    } v2Head;

    typedef struct tagv2Rect
    {
        int label_;
        int left_;
        int top_;
        int right_;
        int bottom_;
        int type_;
        double prob_;
        int stable_;
        double liveness_;
        tagv2Rect()
        {
            label_ = type_ = prob_ = stable_ = liveness_ = -1;
            left_ = top_ = right_ = bottom_ = 0;
        }
    } v2Rect;

    typedef struct tagv2ImgResult
    {
        char name_[MAX_NAME];
        double lighting_;
        std::int64_t timeStamp_;
        int nHand_;
        int nHead_;
        int nRect_;
        v2Hand* listHand_;
        v2Head* listHead_;
        v2Rect* listRect_;

        tagv2ImgResult()
        {
            lighting_ = -1;
            timeStamp_ = nHand_ = nHead_ = nRect_ = 0;
            listHand_ = NULL;
            listHead_ = NULL;
            listRect_ = NULL;
            memset(name_, 0, sizeof(char) * MAX_NAME);
        }

        ~tagv2ImgResult()
        {
            if (NULL != listHand_){ delete[] listHand_; listHand_ = NULL; }
            if (NULL != listHead_){ delete[] listHead_; listHead_ = NULL; }
            if (NULL != listRect_){ delete[] listRect_; listRect_ = NULL; }
        }
    } v2ImgResult;

    //////////////////////////////////////////////////////////////////////////
    std::string& trim(std::string &str);
    int separateMura(int* arr, int nLen, int gap);
    int getmg(const std::vector<Da>& da, int lb, int rb, int& lA, int& rA, int& dis);
    int getHeadersInfo(HeadersInfo& hdInfo, const char* szInfo);    // return head cnt?
    int getUnndouInfo(UnndouInfo& udInfo, const char* szInfo);
    int getDistance(int faceType, int camer_angle, int face_width); // 单位: 厘米
    int getKaoInfo(KaoInfo& kaoInfo, const char* szInfo);
    int getTemaneInfo(TemaneInfo& temane, const char* szInfo);
    int getAngle(const HeadInfo& hdInfo, int camera_angle, int img_width) ;
    int getAngleV2(const v2Head& head, int camera_angle, int img_width, int lb);
    int getAngleV3(int camera_angle, int img_width, int lb, int xpos);

    int startMCUEvent();
    int endMCUEvent();
    int getMCUEvent();

    int get_mountpoint(char const *label, char *mountpoint);
    unsigned char connect_wan();
    unsigned char connect_router();
    
    std::int64_t getMicroSecond(void);
    std::int64_t getMilliSecond(void);
    std::int64_t getSecond(void);

    //////////////////////////////////////////////////////////////////////////
    // for v2
    int analyzeV2String(const std::string& str, v2ImgResult& result);
    std::uint32_t getThreadId();
    //////////////////////////////////////////////////////////////////////////
    //int wifiCheck();
    int videoCheck();
    //int chipCheck();
    
    int writeBuf2Device(ZDEVICE dev,const char* szBuf, int nLen);
    unsigned char transEakonErr(int nErr, int type);
    unsigned char transEakonErrV2(int nErr, int type);

    //////////////////////////////////////////////////////////////////////////
    // system
    std::string getTempDir();
    
    int write_deviceid_file(std::string &deviceid);
    int write_status_file();
    std::int64_t get_up_time();
    int get_temperature();

    int RW_System(const char *cmdstring);
    int RW_Echo(const void *buf, int size, const char *filename);

    bool ntp_sync_time();
    time_t ntp_time();

    int write_sync_time(time_t& time);
    int read_sync_time(time_t& time);
    void sync_system_time(time_t& time);
    int has_synced_time_flag_file();
    int create_synced_time_flag_file();

    int mcu_reset();
    int get_gateway_addr(std::string& addr);
    int get_device_test_ipaddr(std::string& addr, bool bGateWay);
    int property_get(const char *key, char *value, const char *default_value);

    void program_begin();
    std::int64_t program_duration();
    bool reboot_program();
    
    template < typename T > 
    std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }

    std::string to_string(bool b);
    int to_int(std::string &s);
    bool to_bool(const std::string &s);
    double to_double(std::string &s);

    void file_sync(const std::string &full_filename);

    int randint(int min, int max);
    int randint(unsigned int seed, int min, int max);
    
	bool crc_checksum_only(const uint8_t *buf, int len);
	bool crc_checksum_with_endcode(const uint8_t *buf, int len);
}

#endif
