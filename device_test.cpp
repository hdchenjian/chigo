/***********************************************************************************
 * 文 件 名   : device_test.cpp
 * 负 责 人   : 刘春龙
 * 创建日期   : 2015年10月21日
 * 文件描述   : 工装测试(Device Test (DT))
 * 版权说明   : Copyright (c) 2008-2015   xx xx xx xx 技术有限公司
 * 其    他   : 
 * 修改日志   : 
***********************************************************************************/


/*
 * License Server实现。
 * 不用多线程，直接用select()实现。
 *
 * Created by Cyberman Wu on Oct xxth, 2013.
 */

#include <cassert>
#include <cstdio>
#include <cstring>
#include <list>
#include <algorithm>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>
#include <fcntl.h>

#include "zVideoThread.h"
#include "zAlgorithmManager.h"
#include "zAlgorithmDetectThread.h"
#include "zEakonThread.h"
#include "zUtil.h"
#include "zLog.h"

#include "device_test.h"
#include "zImage.h"

// License的root目录，缺省是当前目录。
#define RET_OK       0
#define RET_ERR     -1

#define DEAD_END    0x0d0e0a0d
#define HEARTBEAT_GAP 3000 // 单位: 毫秒
#define HEARTBEAT_RESPONSE_GAP 10000 // 单位: 毫秒

DtThread* DtThread::m_instance = NULL;

#if USE_GATEWAY
static std::string server_ip_addr = "";
#else
static std::string server_ip_addr = "192.168.96.1";
#endif

static const unsigned short server_ip_port = 10086;

/*********************************************wired process**********************************/
#define MAX_DURATION 3600
#define MAX_TIMEOUT 15 // 超过MAX_TIMEOUT秒，进入HOST模式
#define WIRED_SOCKET_PORT 10086 // 有线工装端口

std::int64_t DtThread::m_wired_start_time = 0;
volatile otg_mode_t DtThread::m_otg_mode = OTG_MODE_AUTO;

bool DtThread::m_wired_timeout = true;
/*********************************************wired process**********************************/


/*****************************************************************************
 * 函 数 名  : DtThread.heartbeat_update
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月23日
 * 函数功能  : 更新心跳标志和时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::heartbeat_update()
{
    m_heartbeat_time = zBaseUtil::getMilliSecond();
    m_received_heartbeat_flag = true;
    LOG_D("\n");
    return;
}

/*****************************************************************************
 * 函 数 名  : DtThread.heartbeat_send
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月23日
 * 函数功能  : 发送心跳数据
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::heartbeat_send()
{
    if (0 == dt_send_packet(tcpSock, HEARTBEAT, "ok"))
    {
        m_heartbeat_time = zBaseUtil::getMilliSecond();
        LOG_D("\n");
    }

    m_received_heartbeat_flag = false;
    return;
}

/*****************************************************************************
 * 函 数 名  : dt_set_otg_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月26日
 * 函数功能  : 设置OTG状态
 * 输入参数  : mode: OTG模式
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::dt_set_otg_mode(int mode)
{
    LOG_I("dt: dt_set_otg_mode: mode = %d, m_otg_mode=%d \n", mode, m_otg_mode);
    if (mode == m_otg_mode)
    {
        return;
    }

    switch(mode)
    {
        case OTG_MODE_AUTO:
            (void)zBaseUtil::RW_System("echo null > /proc/otg_state");
            m_otg_mode = OTG_MODE_AUTO;
            break;
        case OTG_MODE_DEVICE:
            (void)zBaseUtil::RW_System("echo device > /proc/otg_state");
            m_otg_mode = OTG_MODE_DEVICE;
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : DtThread.dt_get_otg_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月5日
 * 函数功能  : 获取OTG状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_get_otg_mode()
{
    return m_otg_mode;
}

/*****************************************************************************
 * 函 数 名  : DtThread.is_check_enable
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月18日
 * 函数功能  : 工装测试项是否使能
 * 输入参数  : int type  类型
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool DtThread::is_check_enable(int type)
{
    if (m_wireless_mode_enable) // 无线工装模式
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        return dt_message_enable(type);
    }
    else
    {
        std::unique_lock<std::mutex> lock(m_wired_sock_mutex);
        return dt_message_enable(type);
    }
}

/*****************************************************************************
 * 函 数 名  : DtThread.dt_message_enable
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 判断工装是否启动，并设置了检测开关。
               注意: 函数用来快速判断，是否真正打开了，需使用有锁版本is_check_enable
 * 输入参数  : int type  检测消息类型
 * 输出参数  : 无
 * 返 回 值  : bool: true: 需要检测， false: 不需要检测
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool DtThread::dt_message_enable(int type)
{
    bool msg_enable = false;
    if (m_wireless_mode_enable) // 无线工装模式
    {
        if (m_initialize_flag)
        {
            if (type < MSG_BUTT)
            {
                msg_enable = (0 != m_rcv_buf.switchs[type]);
            }
        }
    }
    else // 有线工装模式
    {
        if (    m_wired_initialize_flag
            &&  (0 != m_wired_rcv_buf_size))
        {
            if (type < MSG_BUTT)
            {
                msg_enable = (0 != m_wired_rcv_buf.rcv_buf.switchs[type]);
            }
        }
    }
    return msg_enable;
}

/*****************************************************************************
 * 函 数 名  : DtThread.dt_send_packet
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月23日
 * 函数功能  : 供其它线程打印消息的接口
 * 输入参数  : SOCKET client     1
               int type          1
               const char *data  1
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_send_message(int type, const char *data)
{
    //LOG_D("dt: dt_send_message: enter.\n");
    if (m_wireless_mode_enable) // 无线工装模式
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        if ((type < MSG_BUTT) && (0 != m_rcv_buf.switchs[type]))
        {
            (void)dt_send_packet(m_rcv_buf.s, MESSAGE, data);
        }
    }
    else
    {
        std::unique_lock<std::mutex> lock(m_wired_sock_mutex);
        if (    (0 != m_wired_rcv_buf_size)
            &&  (type < MSG_BUTT)
            &&  (0 != m_wired_rcv_buf.rcv_buf.switchs[type]))
        {
            (void)dt_send_packet(m_wired_rcv_buf.rcv_buf.s, MESSAGE, data);
        }
    }
    //LOG_D("dt: dt_send_message: exit.\n");
    return 0;
}

/*****************************************************************************
 * 函 数 名  : DtThread.dt_send_image
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月23日
 * 函数功能  : 发送图像
 * 输入参数  : SOCKET client     socket
               int type          类型
               S_VCBuf * pFrame  图像
 * 输出参数  : 无
 * 返 回 值  : int: 0: ok, -1: fail.
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_send_image(SOCKET client, int type, S_VCBuf * pFrame)
{   
    int data_size = (NULL == pFrame) ? 0 : pFrame->len;
    int tlen = 0;

    int ntype = htonl(type);
    tlen = send(client, (char *)(&ntype), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    int nsize = htonl(data_size);
    tlen = send(client, (char *)(&nsize), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    if (0 != data_size)
    {
        LOG_D("img send: begin.\n");
        tlen = send(client, pFrame->buf, data_size, 0);
        LOG_D("img send: end.\n");
        if (tlen != data_size)
        {
            LOG_E("dt: send failed. tlen=%d, data_size=%d\n", tlen, data_size);
            return -1;
        }
    }

    int ndead = htonl(DEAD_END);
    tlen = send(client, (char *)(&ndead), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : DtThread.dt_send_jpg_image
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月4日
 * 函数功能  : 发送图片
 * 输入参数  : SOCKET client   socket客户端
               int type        数据类型
               char *jpg_data  数据内存
               int jpg_size    数据大小
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_send_jpg_image(
    SOCKET client, int type, unsigned char *jpg_data, unsigned int jpg_size)
{   
    int data_size = (NULL == jpg_data) ? 0 : jpg_size;
    int tlen = 0;

    int ntype = htonl(type);
    tlen = send(client, (char *)(&ntype), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    int nsize = htonl(data_size);
    tlen = send(client, (char *)(&nsize), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    if (0 != data_size)
    {
        LOG_D("img send: begin.\n");
        tlen = send(client, jpg_data, data_size, 0);
        LOG_D("img send: end.\n");
        if (tlen != data_size)
        {
            LOG_E("dt: send failed. tlen=%d, data_size=%d\n", tlen, data_size);
            return -1;
        }
    }

    int ndead = htonl(DEAD_END);
    tlen = send(client, (char *)(&ndead), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed.\n");
        return -1;
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : dt_send_packet
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月22日
 * 函数功能  : 打包发送
 * 输入参数  : SOCKET client  socke
               int type       类型
               char *data     消息
 * 输出参数  : 无
 * 返 回 值  : char 0: ok, -1: fail
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_send_packet(SOCKET client, int type, const char *data)
{
    int data_size = (NULL == data) ? 0 : strlen(data);
    int tlen = 0;

    //LOG_D("dt: sizeof(int) = %d \n", sizeof(int));
    int ntype = htonl(type);
    tlen = send(client, (char *)(&ntype), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed. type=%d\n", type);
        return -1;
    }

    int nsize = htonl(data_size);
    tlen = send(client, (char *)(&nsize), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed. data_size=%d\n", data_size);
        return -1;
    }

    if (0 != data_size)
    {
        tlen = send(client, data, data_size, 0);
        if (tlen != data_size)
        {
            LOG_E("dt: send failed. tlen=%d, data_size=%d\n", tlen, data_size);
            return -1;
        }
    }

    int ndead = htonl(DEAD_END);
    tlen = send(client, (char *)(&ndead), sizeof(int), 0);
    if (tlen != sizeof(int))
    {
        LOG_E("dt: send failed. DEAD_END \n");
        return -1;
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : dt_proc_cmd
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月22日
 * 函数功能  : 命令处理
 * 输入参数  : SOCKET client  socket
               int type       类型
               char * para    参数
               int len        参数长度
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_proc_cmd(rcv_buffer_t &rSock, int type, char * para, int len)
{
    LOG_D("dt: type=%d \n", type);

    SOCKET client = rSock.s;
    (void)heartbeat_update();

    switch (type)
    {
        case VERSION_CHECK:
        {
            std::string ver = zVideoThread::getInstance()->version();
            // 输出device id号。
            zEakonThread * pEakon = zVideoThread::getInstance()->getAlgManager()->getEakon();
            if (NULL != pEakon)
            {
                std::string devid = pEakon->GetDevID();
                if (!devid.empty())
                {
                    ver += ", DEVID: " + pEakon->GetDevID();
                }
            }
            (void)dt_send_packet(client, VERSION_OK, ver.c_str());
            break;
        }

        case COMMUNICATE_CHECK:
        {
            int state = zEakonThread::getLastErr();
            if (state & EAKON_COMMU_ERR)
            {
                std::string out = "FAIL";
                (void)dt_send_packet(client, COMMUNICATE_FAILED, out.c_str());
            }
            else
            {
                std::string out = "OK";
                (void)dt_send_packet(client, COMMUNICATE_OK, out.c_str());
            }

            break;
        }

        case CHIPSET_CHECK:
        {
            int state = zEakonThread::getLastErr();
            if (state & EAKON_CHIP_ERR)
            {
                std::string out = std::string("加密芯片校验结果:") + "FAIL";
                (void)dt_send_packet(client, CHIPSET_FAILED, out.c_str());
            }
            else
            {
                std::string out = std::string ("加密芯片校验结果:") + "OK";
                (void)dt_send_packet(client, CHIPSET_OK, out.c_str());
            }

            break;
        }
        
        case WIFI_CHECK:
        {
            zEakonThread * pEakon = zVideoThread::getInstance()->getAlgManager()->getEakon();
            if (NULL != pEakon)
            {
                int state = pEakon->getLastErr();
                LOG_D("state=%d.\n", state);

                std::string ssid;
                std::string passwd;
                pEakon->GetWifiInfo(ssid, passwd);

                std::string out = "SSID:" + ssid + "\n";
                out += "密码:" + passwd + "\n";
                out += "连接状态:";
                
                if (state & EAKON_WIFI_ERR)
                {
                    out += "未连接";
                }
                else
                {
                    out += "已连接";
                }
                (void)dt_send_packet(client, WIFI_OK, out.c_str());
            }
            else
            {
                (void)dt_send_packet(client, WIFI_FAILED, NULL);
            }
            break;
        }

        case CAMERA_CHECK:
        {
            static S_VCBuf frame = { 0 };
            zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();

            // 不考虑刷新标志
            int width, height;
            bool ret = pManage->getFrame2(frame, width, height, zAlgManager::Threads::THREADANONYMOUS);
            if (ret && (0 != frame.len))
            {
                // 无线工装，发送JPG图像。有线工装，发送YUV图像。
                if (m_wireless_mode_enable)
                {
                    tjhandle jpg_handle = zImageUtil::zImage::init_jpg_handle();
                    if (NULL != jpg_handle)
                    {
                        unsigned char *p_jpeg_buffer = NULL;
                        unsigned long jpeg_size = 0;
                        bool ret_compress = zImageUtil::zImage::yuv_to_jpg_buffer(
                            jpg_handle, frame.buf, frame.BytesUsed, width, height, 2, 
                            &p_jpeg_buffer, &jpeg_size);
                        if (ret_compress)
                        {
                            (void)dt_send_jpg_image(client, UPLOAD_IMAGE, p_jpeg_buffer, jpeg_size);
                            (void)dt_send_packet(client, CAMERA_OK, "");

                            zImageUtil::zImage::jpg_buffer_free(&p_jpeg_buffer);
                        }
                        else
                        {
                             LOG_E("Camera check failed for yuv_to_jpg_buffer.\n");
                             dt_send_packet(client, CAMERA_FAILED, "");
                        }
                        
                        zImageUtil::zImage::destroy_jpg_handle(jpg_handle);
                    }
                    else
                    {
                        LOG_E("Camera check failed for init_jpg_handle.\n");
                        dt_send_packet(client, CAMERA_FAILED, "");
                    }
                }
                else
                {
                    dt_send_image(client, UPLOAD_IMAGE, &frame);
                    dt_send_packet(client, CAMERA_OK, "");
                }
            }
            else 
            {
                LOG_E("Camera check failed for getFrame.\n");
                dt_send_packet(client, CAMERA_FAILED, "");
            }
            break;
        }

        case GESUTER_CHECK:
        {
            zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();
            pManage->setEakonModel(zBaseUtil::zEakonModel::IE_MODEL);

            //LOG_D("dt: GESUTER_CHECK: GESUTER switch: %d \n", rSock.switchs[GESTURE_MSG]);
            if (0 == rSock.switchs[GESTURE_MSG])
            {
                rSock.switchs[GESTURE_MSG] = 1;
            }
            //LOG_D("dt: GESUTER_CHECK: GESUTER switch: %d \n", rSock.switchs[GESTURE_MSG]);
            break;
        }

        case GESTURE_CHECK_STOP:
        {
            //LOG_D("dt: GESTURE_CHECK_STOP:GESUTER switch: %d \n", rSock.switchs[GESTURE_MSG]);
            if (0 != rSock.switchs[GESTURE_MSG])
            {
                rSock.switchs[GESTURE_MSG] = 0;
            }
            //LOG_D("dt: GESTURE_CHECK_STOP:GESUTER switch: %d \n", rSock.switchs[GESTURE_MSG]);
            break;
        }

        case WIND_CHECK:
        {
            zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();
            pManage->setEakonModel(zBaseUtil::zEakonModel::IE_MODEL);

            if (!(zAlgManager::EakonState::OPENED_EAKON & (pManage->getEakonState())))
            {
                pManage->eakonCtrl(true);
            }

            //LOG_D("dt: WIND_CHECK: wind switch: %d \n", rSock.switchs[WIND_MSG]);
            if (0 == rSock.switchs[WIND_MSG])
            {
                rSock.switchs[WIND_MSG] = 1;
            }
            //LOG_D("dt: WIND_CHECK: wind switch: %d \n", rSock.switchs[WIND_MSG]);
            break;
        }

        case WIND_CHECK_STOP:
        {
            //LOG_D("dt: WIND_CHECK_STOP: wind switch: %d \n", rSock.switchs[WIND_MSG]);
            if (0 != rSock.switchs[WIND_MSG])
            {
                rSock.switchs[WIND_MSG] = 0;
            }
            //LOG_D("dt: WIND_CHECK_STOP: wind switch: %d \n", rSock.switchs[WIND_MSG]);
            break;
        }

        case SECURITY_CHECK:
        {
            zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();
            pManage->setEakonModel(zBaseUtil::zEakonModel::DEKAKE_MODEL);
            
            if (0 == rSock.switchs[SECURITY_MSG])
            {
                rSock.switchs[SECURITY_MSG] = 1;
            }
            break;
        }

        case SECURITY_CHECK_STOP:
        {
            zAlgManager* pManage = zVideoThread::getInstance()->getAlgManager();
            pManage->setEakonModel(zBaseUtil::zEakonModel::IE_MODEL);

            if (0 != rSock.switchs[SECURITY_MSG])
            {
                rSock.switchs[SECURITY_MSG] = 0;
            }
            break;
        }   
        
        case LIGHT_CHECK:
        {
            zAlgorithmDetectThread* pDetect = zVideoThread::getInstance()->getAlgManager()->getAlgorithmDetect();
            if (NULL != pDetect)
            {
                bAutoLight = pDetect->autoLighting(false);
                pDetect->chgLighting(true);
                dt_send_packet(client, MESSAGE, "手动强制打开");
            }
            
            if (0 == rSock.switchs[LIGHT_MSG])
            {
                rSock.switchs[LIGHT_MSG] = 1;
            }
            break;
        }

        case LIGHT_CHECK_STOP:
        {
            zAlgorithmDetectThread* pDetect = zVideoThread::getInstance()->getAlgManager()->getAlgorithmDetect();
            if (NULL != pDetect)
            {
                pDetect->chgLighting(false);
                (void)pDetect->autoLighting(bAutoLight);
                dt_send_packet(client, MESSAGE, "恢复自动模式");
            }

            if (0 != rSock.switchs[LIGHT_MSG])
            {
                rSock.switchs[LIGHT_MSG] = 0;
            }
            break;
        }

        case HEARTBEAT:
        {
            // todo: nothing.
            break;
        }

        default:
            LOG_E("dt: type = %d\n", type);
            break;
    }
    
    return 0;
}

/*****************************************************************************
 * 函 数 名  : dt_parse_cmd
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月22日
 * 函数功能  : 解析出命令类型，命令参数
 * 输入参数  : char * buf  必须以命令字开头
               int len     长度
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_parse_cmd(rcv_buffer_t &rSock, int *read)
{   
    if ((NULL == read))
    {
        LOG_E("dt: parameter err: %p\n", read);
        return RET_OK;
    }

    char * buf = rSock.buf;
    const int len = rSock.pos;

    *read = 0;
    
    char *cmd_buf = buf;
    int cmd_len = len;
    for(;;)
    {
        if ((MIN_CMD_LEN > cmd_len))
        {
            break;
        }

        int type = ntohl(*(int *)cmd_buf);
        int data_len = ntohl(*(int *)(cmd_buf + 4));
        LOG_I("dt: data_len=%d \n", data_len);
        
        if (    (type <= CMD_TYPE_NULL)
            ||  (type >= CMD_TYPE_BUTT)
            ||  (data_len > MAX_DATA_LEN))
        {
            LOG_E("dt: parse cmd err: %u, %u\n", type, data_len);
            return RET_ERR;
        }

        if ((data_len + MIN_CMD_LEN) > cmd_len)
        {
            return RET_OK;
        }

        int cmd_end = ntohl(*(int*)(cmd_buf + data_len + 8));
        if (DEAD_END != cmd_end)
        {
            LOG_E("dt: cmd end err: 0x%x\n", cmd_end);
            return RET_ERR;
        }

        (void)dt_proc_cmd(rSock, type, cmd_buf + data_len, data_len);

        // 移动到下个命令起始位置
        cmd_buf += data_len + MIN_CMD_LEN;
        cmd_len -= data_len + MIN_CMD_LEN;

        *read = cmd_buf - buf;
    }
        
    return RET_OK;
}

/*****************************************************************************
 * 函 数 名  : dt_handle_cmds
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月22日
 * 函数功能  : 命令处理
 * 输入参数  : LicenseCheckSock &rSock  sockt
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::dt_handle_cmds(rcv_buffer_t &rSock)
{
    SOCKET s = rSock.s;
    struct sockaddr_in CliAddr;
    // 在Linux下用int后面编译会失败。
    /*int */socklen_t NameLen = sizeof(CliAddr);
    // getsockname(*itr, (struct sockaddr *)&CliAddr, &NameLen);
    getpeername(s, (struct sockaddr *)&CliAddr, &NameLen);

    char *buf = (char *)rSock.buf;
    int iCnt = recv(s, &buf[rSock.pos], MAX_CMD_LEN - rSock.pos, 0);
    if (iCnt < 0)
    {
        LOG_D("Connection with %u.%u.%u.%u:%u failed!.\n", 
            IPoct1(CliAddr), IPoct2(CliAddr), 
            IPoct3(CliAddr), IPoct4(CliAddr), 
            ntohs(CliAddr.sin_port));
        return -101;
    }

    if (iCnt == 0)
    {
        LOG_D("Connection with %u.%u.%u.%u:%u closed unexpectedly!\n", 
            IPoct1(CliAddr), IPoct2(CliAddr), 
            IPoct3(CliAddr), IPoct4(CliAddr), 
                ntohs(CliAddr.sin_port));
        return -102;
    }
    
    //LOG_D("dt: rSock.pos = %d, iCnt = %d\n", rSock.pos, iCnt);
    rSock.pos += iCnt;

#if 0
    int i =0;    
    for (i=0; i<rSock.pos; i++)
    {
        LOG_D("dt: i=%d, value=%u \n", i, rSock.buf[i]);
    }
#endif

    int read = 0;
    int ret = dt_parse_cmd(rSock, &read);
    if (RET_OK == ret)
    {
        int left = rSock.pos - read;
        if (0 != left)
        {
            (void)memmove(rSock.buf, rSock.buf + read, left);
        }
        rSock.pos = left;

        //LOG_D("dt: rSock.pos = %d, read = %d\n", rSock.pos, read);
    }
    else
    {
        rSock.pos = 0;
        //LOG_D("dt: rSock.pos = %d, read = %d\n", rSock.pos, read);
    }
    
    return 0;
}

DtThread* DtThread::getInstance()
{
    if (NULL == m_instance)
    {
        m_instance = new DtThread();
    }
    return m_instance;
}

void DtThread::destroyInstance()
{
    if (NULL != m_instance)
    {
        delete m_instance;
        m_instance = NULL;
    }
}

DtThread::DtThread()
    : m_wireless_mode_enable(false)
    , m_use_gateway(false)
    , tcpSock(INVALID_SOCKET)
    , bAutoLight(true)
    , m_heartbeat_time(0)
    , m_received_heartbeat_flag(true)
    , m_initialize_flag(false)
    , m_wired_tcpSock(INVALID_SOCKET)
    , m_wired_rcv_buf_size(0)
    , m_wired_initialize_flag(false)
    , m_wired_connected_flag(false)
{
    m_instance = this;
    (void)memset(&m_rcv_buf, 0, sizeof(rcv_buffer_t));
    m_rcv_buf.s = INVALID_SOCKET;

    /* SOCKET 服务器断开导致客户端SEND崩溃问题解决办法: 管道破裂导致程序崩溃，屏蔽一下 SIGPIPE 信号就好了。*/
    signal(SIGPIPE, SIG_IGN);

    // 有线初始化
    (void)memset(&m_wired_rcv_buf, 0, sizeof(wired_rcv_buffer_t));
    m_wired_rcv_buf.rcv_buf.s = INVALID_SOCKET;
    /* add by 刘春龙, 2016-04-08, 原因: 启动阶段初始化，否则，如果在执行wifi_connect,可能会失败 */
    if (0 == wired_init(NULL, WIRED_SOCKET_PORT))
    {
        m_wired_initialize_flag = true;
    }

    return;
}

DtThread::~DtThread()
{
    LOG_I("DtThread::~DtThread() begin \n");
    if (INVALID_SOCKET != tcpSock)
    {
        closesocket(tcpSock);
    }

    if (INVALID_SOCKET != m_wired_tcpSock)
    {
        closesocket(m_wired_tcpSock);
    }
    LOG_I("DtThread::~DtThread() end \n");
}

/*****************************************************************************
 * 函 数 名  : DtThread.wait_connect_ready
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月2日
 * 函数功能  : 非阻塞connect时，判断是否已连接
 * 输入参数  : const struct timeval *timeout  超时时间
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::wait_connect_ready(const struct timeval *timeout)
{
    struct timeval to;
    struct timeval *toptr = NULL;
    fd_set wfd;
    int err;
    socklen_t errlen;

    /* Only use timeout when not NULL. */
    if (timeout != NULL) {
        to = *timeout;
        toptr = &to;
    }

    FD_ZERO(&wfd);
    FD_SET(tcpSock, &wfd);

    if (select(tcpSock + 1, NULL, &wfd, NULL, toptr) == -1)
    {
        //LOG_E("fail 1.\n");
        return -1;
    }

    if (!FD_ISSET(tcpSock, &wfd))
    {
        errno = ETIMEDOUT;
        //LOG_E("fail 2.\n");
        return -1;
    }

    err = 0;
    errlen = sizeof(err);
    if (getsockopt(tcpSock, SOL_SOCKET, SO_ERROR, &err, &errlen) == -1)
    {
        //LOG_E("fail 3.\n");
        return -1;
    }

    if (err)
    {
        errno = err;
        //LOG_E("fail 4.\n");
        return -1;
    }

    return 0;
}


/*****************************************************************************
 * 函 数 名  : DtThread.socket_connect
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月1日
 * 函数功能  : 建立连接
 * 输入参数  : const char *ip        ip 地址
               unsigned short wPort  端口
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::socket_connect(const std::string &ip, unsigned short wPort)
{
    /////////////////////////////////////////////////////////////////////////////////
    // TCP的侦听socket
    /////////////////////////////////////////////////////////////////////////////////
    if (m_initialize_flag)
    {
        return 0;
    }

    // ip: 入参判断
    if (ip.empty())
    {
        return -1;
    }

    tcpSock = socket(AF_INET, SOCK_STREAM, 0);
    if (INVALID_SOCKET == tcpSock)
    {
        LOG_E("dt: Create socket failed! %d:%s\n", errno, strerror(errno));
        return -1;
    }

    int opt = 1;
    setsockopt(tcpSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    const int flags = fcntl(tcpSock, F_GETFL, 0);
    (void)fcntl(tcpSock, F_SETFL, flags|O_NONBLOCK);  //设置为非阻塞模式

    struct sockaddr_in SrvAddr;
    SrvAddr.sin_family = AF_INET;
    SrvAddr.sin_addr.s_addr = (!ip.empty()) ? inet_addr(ip.c_str()) : htonl(INADDR_ANY);
    SrvAddr.sin_port = htons(wPort);

    int ret_connect = connect(tcpSock, (struct sockaddr *)&SrvAddr, sizeof(SrvAddr));
    if (ret_connect < 0)
    {
        if (EINPROGRESS == errno)
        {
            struct timeval tm;
            tm.tv_sec = 2;
            tm.tv_usec = 0;

            if (0 != wait_connect_ready(&tm))
            {
                closesocket(tcpSock);
                //LOG_E("fail 1: connect socket failed!");
                return -1;
            }
            else
            {
                (void)fcntl(tcpSock, F_SETFL, flags);  // 恢复阻塞模式

                struct timeval timeout={8,0}; // 8s
                (void)setsockopt(tcpSock,SOL_SOCKET,SO_SNDTIMEO,(char *)&timeout,sizeof(struct timeval));

                int sendbuf = 32*1024; // 32K
                int len = sizeof(sendbuf);
                
                //设置发送缓冲区大小  
                setsockopt(tcpSock, SOL_SOCKET, SO_SNDBUF, &sendbuf, sizeof(sendbuf));  

                //获取系统修改后的大小
                getsockopt(tcpSock, SOL_SOCKET, SO_SNDBUF, &sendbuf, (socklen_t*)&len);  
                LOG_D("new. sendbuf=%d\n", sendbuf);
            }
        }
        else
        {
            closesocket(tcpSock);
            //LOG_E("fail 2: connect socket failed!");
            return -1;
        }
    }
    LOG_I("Server ready for connection on TCP port %u ...\n", wPort);

    FD_ZERO(&sockSet);
    FD_SET(tcpSock, &sockSet);
    maxSockID = tcpSock;

    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        (void)memset(&m_rcv_buf, 0, sizeof(rcv_buffer_t));
        m_rcv_buf.s = tcpSock;
    }

    clear_receive_data_buffer();
    heartbeat_update();
    m_initialize_flag = true;
    return 0;
}

/*****************************************************************************
 * 函 数 名  : DtThread.socket_reconnect
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月1日
 * 函数功能  : 重新连接
 * 输入参数  : const char *ip        ip 
               unsigned short wPort  port
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::socket_reconnect(const std::string &ip, unsigned short wPort)
{
    closesocket(tcpSock);
    m_initialize_flag = false;

    LOG_D("reconnect.\n");
    return socket_connect(ip, wPort);
}

/*****************************************************************************
 * 函 数 名  : DtThread.clear_receive_data_buffer
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月1日
 * 函数功能  : 清理接收缓存
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::clear_receive_data_buffer()
{
    // 循环获取数据次数
    #define RCV_LOOP_COUNT 4
    #define MAX_RCV_BUF_LEN 1024
    
    char rcv_buf[MAX_RCV_BUF_LEN];
    
    for (int i=0; i<RCV_LOOP_COUNT; i++)
    {
        struct timeval tm;
        tm.tv_sec = 0;
        tm.tv_usec = 0;

        fd_set rSet = sockSet;
        int iRet = select(maxSockID + 1, &rSet, NULL, NULL, &tm);
        if (iRet < 0)
        {
            continue;
        }

        if (FD_ISSET(tcpSock, &rSet))
        {
            int len = recv(tcpSock, rcv_buf, MAX_RCV_BUF_LEN, 0);
            if (len > 0)
            {
                LOG_D("i=%d, len=%d\n", i, len);
            }
        }
    }
    return;
}

void DtThread::Wait()
{
    zThread::Wait();

    // 无线清理
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        (void)memset(&m_rcv_buf, 0, sizeof(rcv_buffer_t));

        closesocket(tcpSock);
        m_initialize_flag = false;
    }

    // 有线清理
    if (0 != m_wired_rcv_buf_size)
    {
        std::unique_lock<std::mutex> lock(m_wired_sock_mutex);
        wired_client_clear();
    }

    return;
}

/*****************************************************************************
 * 函 数 名  : DtThread.WakeUp
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月7日
 * 函数功能  : 唤醒工装进程
 * 输入参数  : bool wireless  true：无线模式， false：有线模式
               bool bGateWay  true: 网关IP,    falee: .2网段
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::WakeUp(bool wireless, bool bGateWay)
{
    m_wireless_mode_enable = wireless;
    m_use_gateway = bGateWay;
    m_received_heartbeat_flag = true;

    zThread::WakeUp();
    return;   
}

/*****************************************************************************
 * 函 数 名  : DtThread.wireless_process
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月7日
 * 函数功能  : 无线工装处理
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::wireless_process()
{
    if (!m_initialize_flag)
    {
        if (0 == zBaseUtil::get_device_test_ipaddr(server_ip_addr, m_use_gateway))
        {
            LOG_D("server_ip_addr=%s\n", server_ip_addr.c_str());
        }

        if (0 != socket_connect(server_ip_addr, server_ip_port))
        {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            LOG_D("socket_connect failed. ip=%s, port=%d\n", server_ip_addr.c_str(), server_ip_port);
            return;
        }
    }

    // 检测网关地址是否变化，如果有变化重新连接。
    std::string gateway_addr = "";
    if (0 == zBaseUtil::get_device_test_ipaddr(gateway_addr, m_use_gateway))
    {
        if (gateway_addr != server_ip_addr)
        {
            LOG_D("socket_reconnect. server_ip_addr=%s, gateway_addr=%s\n", 
                server_ip_addr.c_str(), gateway_addr.c_str());

            server_ip_addr = gateway_addr;
            (void)socket_reconnect(server_ip_addr, server_ip_port);
            return;
        }
    }

    // 心跳处理
    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    if (m_received_heartbeat_flag)
    {
        if ((curr_time - m_heartbeat_time) >= HEARTBEAT_GAP)
        {
            heartbeat_send();
        }
    }
    else
    {
        if ((curr_time - m_heartbeat_time) >= HEARTBEAT_RESPONSE_GAP)
        {
            LOG_D("socket_reconnect. curr_time=%lld, m_heartbeat_time=%lld\n", 
                curr_time, m_heartbeat_time);

            (void)zBaseUtil::get_device_test_ipaddr(server_ip_addr, m_use_gateway);
            (void)socket_reconnect(server_ip_addr, server_ip_port);
            return;
        }
    }

    // 每次要重新赋值，各系统实现不同，Linux往往会修改它。
    struct timeval tm;
    tm.tv_sec = 5;
    tm.tv_usec = 0;

    fd_set rSet = sockSet;
    // Windows上第一个参数没有意义。
    int iRet = select(maxSockID + 1/*0*/, &rSet, NULL, NULL, &tm);
    if (iRet < 0)
    {
        LOG_D("dt: select return errror, code#%d\n", socket_error);
        return;
    }

    if (FD_ISSET(tcpSock, &rSet))
    {
        (void)dt_handle_cmds(m_rcv_buf);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return;
}

/*********************************************wired process**********************************/
/*****************************************************************************
 * 函 数 名  : DtThread.dt_set_start_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月26日
 * 函数功能  : 设置启动时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::dt_set_start_time()
{
    m_wired_start_time = zBaseUtil::getSecond();
    LOG_I("start time=%lld \n", m_wired_start_time);

    m_wired_timeout = false;
    return;
}

/*****************************************************************************
 * 函 数 名  : DtThread.wired_client_clear
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月8日
 * 函数功能  : 清理有线工装时，所有连接的客户端
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::wired_client_clear()
{
    if (0 != m_wired_rcv_buf_size)
    {
        if (INVALID_SOCKET != m_wired_rcv_buf.rcv_buf.s)
        {
            closesocket(m_wired_rcv_buf.rcv_buf.s);
        }

        if (INVALID_SOCKET != m_wired_rcv_buf.rcv_buf.s)
        {
            FD_CLR(m_wired_rcv_buf.rcv_buf.s, &m_wired_sockSet);
        }

        (void)memset(&m_wired_rcv_buf, 0, sizeof(wired_rcv_buffer_t));
        m_wired_rcv_buf.rcv_buf.s = INVALID_SOCKET;

        m_wired_rcv_buf_size = 0;
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : DtThread.init
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月7日
 * 函数功能  : 有线工装初始化
 * 输入参数  : const char *ip        ip地址
               unsigned short wPort  端口
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int DtThread::wired_init(const char *ip, unsigned short wPort)
{
    /////////////////////////////////////////////////////////////////////////////////
    // TCP的侦听socket
    /////////////////////////////////////////////////////////////////////////////////

    m_wired_tcpSock = socket(AF_INET, SOCK_STREAM, 0);
    if (INVALID_SOCKET == m_wired_tcpSock)
    {
        LOG_E("Create socket failed! %d:%s\n", errno, strerror(errno));
        return -1;
    }

    int opt = 1;
    setsockopt(m_wired_tcpSock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    struct sockaddr_in SrvAddr;
    SrvAddr.sin_family = AF_INET;
    SrvAddr.sin_addr.s_addr = ip? inet_addr(ip) : htonl(INADDR_ANY);
    SrvAddr.sin_port = htons(wPort);
    if (bind(m_wired_tcpSock, (struct sockaddr *)&SrvAddr, sizeof(SrvAddr)) == SOCKET_ERROR)
    {
        closesocket(m_wired_tcpSock);
        LOG_E("Bind socket failed! %d:%s\n", errno, strerror(errno));
        return -1;
    }

    if (listen(m_wired_tcpSock, MAX_CONNECTIONS) == SOCKET_ERROR)
    {
        closesocket(m_wired_tcpSock);
        LOG_E("Listen on socket failed! %d:%s\n", errno, strerror(errno));
        return -1;
    }
    LOG_I("Server ready for connection on TCP port %u ...\n", wPort);

    FD_ZERO(&m_wired_sockSet);
    FD_SET(m_wired_tcpSock, &m_wired_sockSet);
    m_wired_maxSockID = m_wired_tcpSock;
    LOG_D("success.\n");

    return 0;
}

/*****************************************************************************
 * 函 数 名  : DtThread.wired_process
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月7日
 * 函数功能  : 有线工装，处理
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::wired_process()
{
    // 有线工装测试时，工作在OTG DEVICE模式
    if (OTG_MODE_DEVICE != m_otg_mode)
    {
        //std::this_thread::sleep_for(std::chrono::seconds(10));
        LOG_D("wait enter.\n");
        Wait();
        return;
    }

    LOG_D("m_wired_timeout=%d, m_otg_mode=%d, m_wired_rcv_buf_size=%d\n", 
        m_wired_timeout, m_otg_mode, m_wired_rcv_buf_size);

    if (!m_wired_timeout)
    {
        int time_now = zBaseUtil::getSecond();
        if ((m_wired_start_time + MAX_TIMEOUT) < time_now)
        {
            m_wired_timeout = true;

            // 超时未连接工装客户端，切换到OTG HOST模式
            if (0 == m_wired_rcv_buf_size)
            {
                dt_set_otg_mode(OTG_MODE_AUTO);
                LOG_I("dt: proc: timeout=%d, m_otg_mode=%d\n", time_now, m_otg_mode);
                return;
            }
        }
    }

    // 每次要重新赋值，各系统实现不同，Linux往往会修改它。
    struct timeval tm;
    tm.tv_sec = 5;     // 用于调试, tm.tv_sec = 50000;
    tm.tv_usec = 0;

    fd_set rSet = m_wired_sockSet;
    int iRet = select(m_wired_maxSockID + 1, &rSet, NULL, NULL, &tm);
    if (iRet < 0)
    {
        LOG_D("select errror, code= %d\n", socket_error);
        return;
    }

    if (FD_ISSET(m_wired_tcpSock, &rSet))
    {
        struct sockaddr_in accept_sin;
        int accept_sin_len = sizeof(accept_sin);
        memset(&accept_sin, 0, accept_sin_len);
        SOCKET ClientSock = accept(
            m_wired_tcpSock, (struct sockaddr *)&accept_sin, (socklen_t *)&accept_sin_len);
        if (INVALID_SOCKET != ClientSock)
        {
            if (0 != m_wired_rcv_buf_size)
            {
                std::unique_lock<std::mutex> lock(m_wired_sock_mutex);
                wired_client_clear();
            }

            FD_SET(ClientSock, &m_wired_sockSet);
            if (m_wired_maxSockID < ClientSock)
            {
                m_wired_maxSockID = ClientSock;
            }

            {
                std::unique_lock<std::mutex> lock(m_wired_sock_mutex);
                rcv_buffer_t &rcv_buf = m_wired_rcv_buf.rcv_buf;
                rcv_buf.s = ClientSock;
                rcv_buf.pos = 0;
                m_wired_rcv_buf.last_time = zBaseUtil::getSecond();
                (void)memset(rcv_buf.switchs, 0, sizeof(rcv_buf.switchs));

                m_wired_rcv_buf_size++;
            }
            
            LOG_I("Accept connection from %u.%u.%u.%u:%u\n", 
                  IPoct1(accept_sin), IPoct2(accept_sin), 
                  IPoct3(accept_sin), IPoct4(accept_sin), 
                  ntohs(accept_sin.sin_port));

            // 设置有线工装时，客户端连接标志
            if (!m_wired_connected_flag)
            {
                m_wired_connected_flag = true;
                LOG_D("m_wired_connected_flag=%d\n", m_wired_connected_flag);
            }
        }
        else
        {
            LOG_E("Accept on socket failed!\n");
        }
    }

    if (0 != m_wired_rcv_buf_size)
    {
        if (FD_ISSET(m_wired_rcv_buf.rcv_buf.s, &rSet))
        {
            (void)dt_handle_cmds(m_wired_rcv_buf.rcv_buf);
            m_wired_rcv_buf.last_time = zBaseUtil::getSecond();
        }
        // 1个小时无操作，自动退出
        else if ((m_wired_rcv_buf.last_time + MAX_DURATION) < zBaseUtil::getSecond()) 
        {
            struct sockaddr_in CliAddr;
            socklen_t NameLen = sizeof(CliAddr);
            getsockname(m_wired_rcv_buf.rcv_buf.s, (struct sockaddr *)&CliAddr, &NameLen);
            unsigned char *ip = (unsigned char *)&CliAddr.sin_addr.s_addr;
            LOG_I("Connection with %d.%d.%d.%d timeout!\n", ip[0], ip[1], ip[2], ip[3]);

            wired_client_clear();
        }
    }

    return;
}
/*********************************************wired process**********************************/

/*****************************************************************************
 * 函 数 名  : dt_is_started
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月22日
 * 函数功能  : 判断工装是否启动
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : true: 当无线标志为真，或有线已连接时， false: 其他
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool DtThread::dt_is_started()
{
    return (m_wireless_mode_enable || m_wired_connected_flag);
}

/*****************************************************************************
 * 函 数 名  : DtThread.proc
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月7日
 * 函数功能  : 工装线程处理函数
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void DtThread::proc()
{
    if (m_wireless_mode_enable)
    {
        wireless_process();
    }
    else 
    {
        if (m_wired_initialize_flag)
        {
            wired_process();
        }
        else if (0 == wired_init(NULL, WIRED_SOCKET_PORT))
        {
            m_wired_initialize_flag = true;
            wired_process();
        }
    }
    return;
}

