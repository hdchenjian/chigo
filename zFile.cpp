#include <list>
#include <algorithm>
#include <cstdint>
#include <chrono>
#include "zFile.h"
#include "zLog.h"
#include "zUtil.h"

#ifdef _WINDOWS
#include <windows.h>
#include <stdio.h>
#include <Shlobj.h>
#else
#include <dirent.h> 
#include <sys/statfs.h>  
#include <sys/stat.h> 
#include <unistd.h> 
#include<sys/types.h>
#include <errno.h>
#endif

#define MAX_FILE_SIZE (128 * 1024 * 1024)           //
#define MIN_FREE_SIZE (64)                          // M

//////////////////////////////////////////////////////////////////////////
namespace zFileUtil
{
    std::string getTimeString()
    {
        time_t tt = zBaseUtil::ntp_time();
        struct tm* ptm = localtime(&tt);
        char date[60] = { 0 };
        sprintf(date, "%d-%02d-%02d_%02d-%02d-%02d",
            (int)ptm->tm_year + 1900, (int)ptm->tm_mon + 1, (int)ptm->tm_mday,
            (int)ptm->tm_hour, (int)ptm->tm_min, (int)ptm->tm_sec);
        return std::string(date);
    }

    static void StringReplace(std::string &strBase, std::string strSrc, std::string strDes)
    {
        std::string::size_type pos = 0;
        std::string::size_type srcLen = strSrc.size();
        std::string::size_type desLen = strDes.size();
        pos = strBase.find(strSrc, pos);
        while ((pos != std::string::npos))
        {
            strBase.replace(pos, srcLen, strDes);
            pos = strBase.find(strSrc, (pos + desLen));
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // zFileManager
    zFileManager* zFileManager::instance_ = NULL;
    zFileManager* zFileManager::getInstance()
    {
        if (NULL == instance_)
        {
            instance_ = new zFileManager();
        }
        return instance_;
    }

    void zFileManager::destroyInstance()
    {
        if (NULL != instance_)
        {
            delete instance_;
            instance_ = NULL;
        }
    }

    zFileManager::zFileManager()
    {
        bSyncTime_ = false;
    }

    zFileManager::~zFileManager()
    {
    }

    void zFileManager::setDefaultRootPath(const std::string& path)
    {
        rootPath_ = path;
    }

    void zFileManager::normalizePath(std::string& str)
    {
        if ('/' == str.back()) str.pop_back();
        StringReplace(str, "\\", "/");
    }

    void zFileManager::normalizeDir(std::string& str)
    {
        if ('/' != str.back()) str.push_back('/');
        StringReplace(str, "\\", "/");
    }

    bool zFileManager::isFileExist(const std::string& filename)
    {
#ifdef _WINDOWS
        if (0 == filename.length())
        {
            return false;
        }

        std::string strPath = filename;
        if (!isAbsolutePath(strPath))
        { // Not absolute path, add the default root path at the beginning.
            strPath.insert(0, rootPath_+"\\");
        }

        WCHAR utf16Buf[MAX_PATH] = { 0 };
        MultiByteToWideChar(CP_UTF8, 0, strPath.c_str(), -1, utf16Buf, sizeof(utf16Buf) / sizeof(utf16Buf[0]));

        DWORD attr = GetFileAttributesW(utf16Buf);
        if (attr == INVALID_FILE_ATTRIBUTES || (attr & FILE_ATTRIBUTE_DIRECTORY))
            return false;   //  not a file
        return true;
#else
        if (filename.empty())
        {
            return false;
        }

        std::string strPath = filename;
        if (!isAbsolutePath(strPath))
        { // Not absolute path, add the default root path at the beginning.
            strPath.insert(0, rootPath_+"/");
        }

        struct stat sts;
        return (stat(strPath.c_str(), &sts) != -1) ? true : false;
#endif
    }

    bool zFileManager::isAbsolutePath(const std::string& path) const 
    {
        return (path[0] == '/');
    }
    
    //bool zFileManager::isDirectory(const std::string& path)
    //{
    //  struct stat S_stat;

    //  //取得文件状态
    //  if (stat(path.c_str(), &S_stat) < 0)
    //  {
    //      return false;
    //  }

    //  if (S_stat.st_mode & _S_IFDIR)
    //  {
    //      return true;
    //  }
    //  else
    //  {
    //      return false;
    //  }
    //}


    bool zFileManager::isDirectoryExist(const std::string& dirPath) const
    {
#if defined(_WINRT)
        WIN32_FILE_ATTRIBUTE_DATA wfad;
        std::wstring wdirPath(dirPath.begin(), dirPath.end());
        if (GetFileAttributesEx(wdirPath.c_str(), GetFileExInfoStandard, &wfad))
        {
            return true;
        }
        return false;
#elif defined(_WINDOWS)
        unsigned long fAttrib = GetFileAttributesA(dirPath.c_str());
        if (fAttrib != INVALID_FILE_ATTRIBUTES &&
            (fAttrib & FILE_ATTRIBUTE_DIRECTORY))
        {
            return true;
        }
        return false;
#else
        struct stat st;
        if (stat(dirPath.c_str(), &st) == 0)
        {
            return S_ISDIR(st.st_mode);
        }
        return false;
#endif
    }

    bool zFileManager::createDirectory(const std::string& path)
    {
        if (isDirectoryExist(path))
            return true;

        // Split the path
        size_t start = 0;
        size_t found = path.find_first_of("/\\", start);
        std::string subpath;
        std::vector<std::string> dirs;

        if (found != std::string::npos)
        {
            while (true)
            {
                subpath = path.substr(start, found - start + 1);
                if (!subpath.empty())
                    dirs.push_back(subpath);
                start = found + 1;
                found = path.find_first_of("/\\", start);
                if (found == std::string::npos)
                {
                    if (start < path.length())
                    {
                        dirs.push_back(path.substr(start));
                    }
                    break;
                }
            }
        }


#if defined(_WINRT)
        WIN32_FILE_ATTRIBUTE_DATA wfad;
        std::wstring wpath(path.begin(), path.end());
        if (!(GetFileAttributesEx(wpath.c_str(), GetFileExInfoStandard, &wfad)))
        {
            subpath = "";
            for (unsigned int i = 0; i < dirs.size(); ++i)
            {
                subpath += dirs[i];
                if (i > 0 && !isDirectoryExist(subpath))
                {
                    std::wstring wsubpath(subpath.begin(), subpath.end());
                    BOOL ret = CreateDirectory(wsubpath.c_str(), NULL);
                    if (!ret && ERROR_ALREADY_EXISTS != GetLastError())
                    {
                        return false;
                    }
                }
            }
        }
        return true;
#elif defined(_WINDOWS)
        if ((GetFileAttributesA(path.c_str())) == INVALID_FILE_ATTRIBUTES)
        {
            subpath = "";
            for (unsigned int i = 0; i < dirs.size(); ++i)
            {
                subpath += dirs[i];
                if (!isDirectoryExist(subpath))
                {
                    BOOL ret = CreateDirectoryA(subpath.c_str(), NULL);
                    if (!ret && ERROR_ALREADY_EXISTS != GetLastError())
                    {
                        return false;
                    }
                }
            }
        }
        return true;
#else
        DIR *dir = NULL;

        // Create path recursively
        subpath = "";
        for (uint32_t i = 0; i < dirs.size(); ++i)
        {
            subpath += dirs[i];
            dir = opendir(subpath.c_str());

            if (!dir)
            {
                // directory doesn't exist, should create a new one

                int ret = mkdir(subpath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
                if (ret != 0 && (errno != EEXIST))
                {
                    // current directory can not be created, sub directories can not be created too
                    // should return
                    return false;
                }
            }
            else
            {
                // directory exists, should close opened dir
                closedir(dir);
            }
        }
        return true;
#endif
    }

    bool zFileManager::removeDirectory(const std::string& path)
    {
        if (path.size() > 0 && path[path.size() - 1] != '/')
        {
            LOG_E("Fail to remove directory, path must termniate with '/': %s", path.c_str());
            return false;
        }

        // Remove downloaded files

#if defined(_WINRT)
        std::wstring wpath = std::wstring(path.begin(), path.end());
        std::wstring files = wpath + L"*.*";
        WIN32_FIND_DATA wfd;
        HANDLE  search = FindFirstFileEx(files.c_str(), FindExInfoStandard, &wfd, FindExSearchNameMatch, NULL, 0);
        bool ret = true;
        if (search != INVALID_HANDLE_VALUE)
        {
            BOOL find = true;
            while (find)
            {
                //. ..
                if (wfd.cFileName[0] != '.')
                {
                    std::wstring temp = wpath + wfd.cFileName;
                    if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                        temp += '/';
                        ret = ret && this->removeDirectory(std::string(temp.begin(), temp.end()));
                    }
                    else
                    {
                        SetFileAttributes(temp.c_str(), FILE_ATTRIBUTE_NORMAL);
                        ret = ret && DeleteFile(temp.c_str());
                    }
                }
                find = FindNextFile(search, &wfd);
            }
            FindClose(search);
        }
        if (ret && RemoveDirectory(wpath.c_str()))
        {
            return true;
        }
        return false;
#elif defined(_WINDOWS)
        std::string command = "cmd /c rd /s /q ";
        // Path may include space.
        command += "\"" + path + "\"";

        if (WinExec(command.c_str(), SW_HIDE) > 31)
            return true;
        else
            return false;
#else
        std::string command = "rm -r ";
        // Path may include space.
        command += "\"" + path + "\"";
        if (zBaseUtil::RW_System(command.c_str()) >= 0)
            return true;
        else
            return false;
#endif
    }

    bool zFileManager::appendDirectory(std::string& dirPath, const std::string& subDir)
    {
        LOG_D("zFileManager::appendDirectory dirPath == %s, subDir = %s \n", dirPath.c_str(), subDir.c_str());
        std::string tmpDir = dirPath;
        normalizeDir(tmpDir);
        if (!isDirectoryExist(tmpDir)) return false;

        tmpDir += subDir;
        normalizeDir(tmpDir);
        if ( !isDirectoryExist(tmpDir) )
        { 
            if (!createDirectory(tmpDir)) return false;
        }

        dirPath = tmpDir;
        return true;
    }

    bool zFileManager::removeFile(const std::string &path)
    {
        // Remove downloaded file

#if defined(_WINRT)
        std::wstring wpath(path.begin(), path.end());
        if (DeleteFile(wpath.c_str()))
        {
            return true;
        }
        return false;
#elif defined(_WINDOWS)
        std::string command = "cmd /c del /q ";
        std::string win32path = path;
        int len = win32path.length();
        for (int i = 0; i < len; ++i)
        {
            if (win32path[i] == '/')
            {
                win32path[i] = '\\';
            }
        }
        command += win32path;

        if (WinExec(command.c_str(), SW_HIDE) > 31)
            return true;
        else
            return false;
#else
        if (remove(path.c_str())) {
            LOG_E("remove fail. path=%s\n", path.c_str());
            return false;
        }
        else {
            return true;
        }
#endif
    }

    bool zFileManager::renameFile(const std::string &path, const std::string &oldname, const std::string &name)
    {
        std::string oldPath = path + oldname;
        std::string newPath = path + name;

        // Rename a file
#if defined(_WINRT)
        std::regex pat("\\/");
        std::string _old = std::regex_replace(oldPath, pat, "\\");
        std::string _new = std::regex_replace(newPath, pat, "\\");
        if (MoveFileEx(std::wstring(_old.begin(), _old.end()).c_str(),
            std::wstring(_new.begin(), _new.end()).c_str(),
            MOVEFILE_REPLACE_EXISTING & MOVEFILE_WRITE_THROUGH))
        {
            return true;
        }
        return false;
#elif defined(_WINDOWS)
        std::regex pat("\\/");
        std::string _old = std::regex_replace(oldPath, pat, "\\");
        std::string _new = std::regex_replace(newPath, pat, "\\");

        if (zFileManager::getInstance()->isFileExist(_new))
        {
            if (!DeleteFileA(_new.c_str()))
            {
                LOG_E("Fail to delete file %s !Error code is 0x%x", newPath.c_str(), GetLastError());
            }
        }

        if (MoveFileA(_old.c_str(), _new.c_str()))
        {
            return true;
        }
        else
        {
            LOG_E("Fail to rename file %s to %s !Error code is 0x%x", oldPath.c_str(), newPath.c_str(), GetLastError());
            return false;
        }
#else
        int errorCode = rename(oldPath.c_str(), newPath.c_str());

        if (0 != errorCode)
        {
            LOG_E("Fail to rename file %s to %s !Error code is %d", oldPath.c_str(), newPath.c_str(), errorCode);
            return false;
        }
        return true;
#endif
    }

    std::string zFileManager::fullPathForFilename(const std::string &filename) const
    {
        if (filename.empty())
        {
            return "";
        }

        if (isAbsolutePath(filename))
        {
            return filename;
        }

        // Already Cached ?

        // The file wasn't found, return empty string.
        return rootPath_+"\\"+ filename ;
    }

    // %d-%02d-%02d_%02d-%02d-%02d_(T OR R)
    // filename == dir + prefix+"_" + time + "_T" or "_F" + "." + suffix
    std::string zFileManager::getNewFilename(const std::string& rootDir /* = "" */
        , std::string prefix /* = "0" */, std::string suffix /* = "" */ )
    {
        std::string dir = rootDir;
        if (dir.empty())
        {
            dir = path2Dir(rootPath_);
        }
        if (!dir.empty() && !isDirectoryExist(dir)) return "";
        std::string time = getTimeString();
        std::string filename;

        if (!prefix.empty()) prefix += "_";
        filename = prefix  + time;
        filename += bSyncTime_ ? "_T" : "_F";

        if (!suffix.empty())
        {
            filename = dir + filename + "." + suffix;
        }
        else
        {
            filename = dir + filename;
        }
        if (!isFileExist(filename))
            return filename;
        LOG_E("getNewFilename Failed! %s exist! \n", filename.c_str());
        return "";
    }


    std::string zFileManager::getNewFilenameByTime(const std::string& suffix /* = "" */)
    {
        std::string time = getTimeString();

        return time + suffix;
    }

    long zFileManager::getFileSize(const std::string& filePath)
    {
        if (filePath.empty())
        {
            LOG_E("Invalid file path!");
            return -1;
        }
        std::string fullpath = filePath;
        if (!isAbsolutePath(filePath))
        {
            fullpath = fullPathForFilename(filePath);
            if (fullpath.empty())
                return 0;
        }

        struct stat info;
        // Get data associated with "crt_stat.c":
        int result = stat(fullpath.c_str(), &info);

        // Check if statistics are valid:
        if (result != 0)
        {
            // Failed
            return -1;
        }
        else
        {
            return (long)(info.st_size);
        }
    }

    /*****************************************************************************
     * 函 数 名  : zFileUtil.zFileManager.getDirectorySize
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年3月21日
     * 函数功能  : 获取指定文件夹的大小, 单位: M
     * 输入参数  : const std::string& dir  路径
     * 输出参数  : 无
     * 返 回 值  : int，-1: 可能文件夹不存在
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int zFileManager::getDirectorySize(const std::string& dir)
    {
        int size = -1;
        std::string cmd;
        cmd += "busybox du -sm " + dir;
    
        FILE* pipe = popen(cmd.c_str(), "r");
        if (NULL != pipe)
        {
            char line[64];
            while (  fgets(line, 64, pipe) )
            {
                //LOG_D("line=%s\n", line);
                size = atoi(line);
            }
            pclose(pipe);
        }
    
        return size;
    }

    std::uint64_t zFileManager::getDiskFreeSize(const std::string& path, std::uint64_t *total_size)
    {
        std::string diskName = path;
        std::uint64_t result = 0;
#ifdef _WINDOWS

        if (diskName.empty())
        { 
            char szBuf[MAX_PATH] = { 0 };
            GetModuleFileName(NULL, szBuf, MAX_PATH);
            diskName = szBuf;
            diskName = diskName.substr(0, 3);
        }
        ULARGE_INTEGER lpuse;
        ULARGE_INTEGER lptotal;
        ULARGE_INTEGER lpfree;
        GetDiskFreeSpaceExA(diskName.c_str(), &lpuse, &lptotal, &lpfree);
        result = lpfree.QuadPart;
#else
        diskName = path;
        struct statfs diskInfo;
        statfs(diskName.c_str(), &diskInfo);
        unsigned long long block_size = diskInfo.f_bsize;
        unsigned long long freeDisk = diskInfo.f_bfree * block_size;

        /*
            LOG_D("f_blocks=%lld, f_bfree=%lld, f_bavail=%lld, f_bsize=%lld\n", 
                diskInfo.f_blocks, diskInfo.f_bfree, diskInfo.f_bavail, diskInfo.f_bsize);
         */

        if (NULL != total_size)
        {
            *total_size = diskInfo.f_blocks * block_size;
        }
        
        result = freeDisk;
        LOG_D("%s block_size == %lld, diskInfo.f_bfree == %lld, freeDisk == %lld \n", 
            diskName.c_str(), block_size, diskInfo.f_bfree, freeDisk);
#endif // _WINDOWS

        return result;
    }

    int zFileManager::EnumDirFiles(const std::string& rootPath,
        std::list<std::string>& list, std::string suffix, int limit_size)
    {
        std::string strFind;
#ifdef _WINDOWS
        std::string tmpDir = rootPath + "\\*.*";
        char file[MAX_PATH];
        //lstrcpy(file, Path);
        //lstrcat(file, "\\*.*");
        lstrcpy(file, tmpDir.c_str());
        WIN32_FIND_DATA wfd;
        HANDLE Find = FindFirstFile(file, &wfd);
        if (Find == INVALID_HANDLE_VALUE)
            return;
        while (FindNextFile(Find, &wfd))
        {
            if (wfd.cFileName[0] == '.')
            {
                continue;
            }
            if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
                // do nothing
                //EnumDirFiles(rootPath + "\\" + wfd.cFileName, list, suffix);
            }
            else if (suffix.empty()
                || (strFind.length() - suffix.length()) == strFind.rfind(suffix.c_str()))
            {
                //list.push_back(destNewPath);
                list.push_back(wfd.cFileName);

            }
            else
            {/* do something */
                //printf("name is: %s \n desNewPath: %s \n", szTmpPath, destNewPath.c_str());
            }
            list.push_back(rootPath + "\\" + wfd.cFileName);
        }
        FindClose(Find);
#else
        DIR *pDir;
        struct dirent *pDirent;
        pDir = opendir(rootPath.c_str());
        if (NULL == pDir)
        {
            return -1;
        }

        while ((pDirent = readdir(pDir)) != NULL)
        {
            // if ((pDirent->d_name == '.') || pDirent->d_name[0] == '..')
            if (strcmp(pDirent->d_name, ".") == 0 || strcmp(pDirent->d_name, "..") == 0)
            {
                continue;
            }
            char szTmpPath[1024] = { 0 };
            sprintf(szTmpPath, "%s/%s", rootPath.c_str(), pDirent->d_name);
            strFind = /*rootPath + "/" +*/ pDirent->d_name;
            //LOG_D("szTmpPath=%s, pDirent->d_name= %s\n", szTmpPath, pDirent->d_name);

            /* delete by 刘春龙, 2016-03-28, 原因: 
                目录下存在大量文件时(如10000), 导致函数执行慢，进而导致程序启动慢 */
            #if 0
            if (isDirectoryExist(szTmpPath))
            {
                //如果是文件夹则进行递归
                //int iRet = EnumDirFiles(szTmpPath, list, suffix);
                //if (iRet < 0)
                //{
                //  break;
                //}
            }
            else 
            #endif
            {
                if (    suffix.empty()
                    || ((strFind.length() - suffix.length()) == strFind.rfind(suffix.c_str())))
                {
                    if (0 == limit_size)
                    {
                        list.push_back(pDirent->d_name);
                    }
                    else
                    {
                        int file_size = getFileSize(szTmpPath);
                        if (file_size >= limit_size)
                        {
                            list.push_back(pDirent->d_name);
                        }
                        else
                        {
                            (void)removeFile(szTmpPath);
                        }
                    }
                }
            }
        }

        closedir(pDir);

#endif
        return list.size();
    }

    int zFileManager::EnumDirFilesWithFullPath(const std::string& rootPath, std::list<std::string>& list
        , std::string suffix /* = "" */, bool brev /* = true */)
    {
        std::string strFind;
#ifdef _WINDOWS
        std::string tmpDir = rootPath + "\\*.*";
        char file[MAX_PATH];
        //lstrcpy(file, Path);
        //lstrcat(file, "\\*.*");
        lstrcpy(file, tmpDir.c_str());
        WIN32_FIND_DATA wfd;
        HANDLE Find = FindFirstFile(file, &wfd);
        if (Find == INVALID_HANDLE_VALUE)
            return;
        while (FindNextFile(Find, &wfd))
        {
            if (wfd.cFileName[0] == '.')
            {
                continue;
            }
            strFind = rootPath + "\\" + wfd.cFileName
            if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY
                && brev)
            {
                EnumDirFilesWithFullPath(rootPath + "\\" + wfd.cFileName, list, suffix, brev);
            }
            else if (suffix.empty()
                || (strFind.length() - suffix.length()) == strFind.rfind(suffix.c_str()))
            {
                //list.push_back(destNewPath);
                list.push_back(rootPath + "\\" + wfd.cFileName);

            }
            else
            {/* do something */
                //printf("name is: %s \n desNewPath: %s \n", szTmpPath, destNewPath.c_str());
            }
            list.push_back(rootPath + "\\" + wfd.cFileName);
            //DeleteFile(FilePath);
            //这里写上你要执行的操作
        }
        FindClose(Find);
#else
        int iRet = 0;
        DIR *pDir;
        struct dirent *pDirent;
        pDir = opendir(rootPath.c_str());
        if (pDir == NULL)
        {
            return -1;
        }
        while ((pDirent = readdir(pDir)) != NULL)
        {
            // if ((pDirent->d_name == '.') || pDirent->d_name[0] == '..')
            if (strcmp(pDirent->d_name, ".") == 0 || strcmp(pDirent->d_name, "..") == 0)
            {
                continue;
            }
            char szTmpPath[1024] = { 0 };
            sprintf(szTmpPath, "%s/%s", rootPath.c_str(), pDirent->d_name);
            strFind = rootPath + "/" + pDirent->d_name;

            //printf("name is: %s \n desNewPath: %s \n", szTmpPath, destNewPath.c_str());
            if (isDirectoryExist(szTmpPath)
                && brev)
            {
                //如果是文件夹则进行递归
                iRet = EnumDirFilesWithFullPath(szTmpPath, list, suffix, brev);
                if (iRet < 0)
                {
                    break;
                }
            }
            //else
            //{

            //  list.push_back(szTmpPath);
            //}
            else if ( suffix.empty()
                || (strFind.length() - suffix.length()) == strFind.rfind(suffix.c_str()))
            {
                list.push_back(strFind);

            }
            else
            {/* do something */
                //printf("name is: %s \n desNewPath: %s \n", szTmpPath, destNewPath.c_str());
            }
        }

        closedir(pDir);

#endif
        return list.size();
    }

    std::string zFileManager::path2Dir(const std::string& path)
    {
        std::string result = path;
        normalizeDir(result);
        return result;
    }

    std::string zFileManager::dir2Path(const std::string& dir)
    {
        std::string result = dir;
        normalizePath(result);
        return result;
    }

    void zFileManager::timeSync()
    {
        bSyncTime_ = true;
    }

    /*****************************************************************************
     * 函 数 名  : zFileUtil.zFileManager.get_time_sync
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年1月17日
     * 函数功能  : 获取时间同步标志
     * 输入参数  : 无
     * 输出参数  : 无
     * 返 回 值  : bool
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    bool zFileManager::get_time_sync()
    {
        return bSyncTime_;
    }

    /*****************************************************************************
     * 函 数 名  : zFileUtil.zFileManager.init_sync_time
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年1月24日
     * 函数功能  : 时间同步- 初始设置
     * 输入参数  : 无
     * 输出参数  : 无
     * 返 回 值  : void
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    void zFileManager::init_sync_time()
    {
        bSyncTime_ = zBaseUtil::has_synced_time_flag_file();
        LOG_D("bSyncTime_=%d\n", bSyncTime_);

        /* add by 刘春龙, 2016-01-24, 原因: 录像文件名更新 */
        time_t sync_time;
        time_t curr_time = zBaseUtil::ntp_time();
        if (   (0 == zBaseUtil::read_sync_time(sync_time))
            &&  (sync_time > curr_time) )
        {
            zBaseUtil::sync_system_time(sync_time);
        }
        else
        {
            (void)zBaseUtil::write_sync_time(curr_time);
        }

        LOG_D("time=%s \n", getTimeString().c_str());
        return;
    }

}

