#ifndef __CONFIG_MGR_H__
#define __CONFIG_MGR_H__

#include <string>
#include <map> 

namespace zConfigManager
{
    class zTxtConfigMgr
    {
    public:
        // key = val must kankaku with " "
        zTxtConfigMgr(std::string file, std::string yarikada);
        ~zTxtConfigMgr();

        bool HasKey(std::string key);
        int setValue(std::string key, std::string val);
        std::string getValue(const std::string &key);
        bool SaveConfig(std::string file = std::string());
    private:
        std::string m_fileName;
        std::map<std::string, std::string> m_valMap;
    };
}
#endif