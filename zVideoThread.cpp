#include <vector>
#include <algorithm>

#include "3rdParty/V4L2ImageCap/jni/cap_types.h"
#include "3rdParty/V4L2ImageCap/jni/capture.h"

#include "config_mgr.h"
#include "p2pThread.h"
#include "zUtil.h"
#include "zLog.h"
#include "zRokugaThread.h"
#include "zVideoThread.h"
#include "zAlgorithmManager.h"
#include "zEakonThread.h"

#define GET_FRAME_GAP (30) // 单位: 毫秒
#define DEFAULT_VIDEO_WIDTH (1280)
#define DEFAULT_VIDEO_HEIGHT (720)

#define GETFRAME_FAILED_THRESHOLD 6 // 获取帧失败阈值

#define MAX_P2P2MSG (256)
#define FRAME_ANALYZEGAP (5)
#define FRAME_RATE (50)         // 500ms a frame
// #define H264_ENC 0

static const char gDevName[] = "/dev/mcu_dev";

zVideoThread* zVideoThread::m_instance = NULL;

zVideoThread* zVideoThread::getInstance()
{
    //LOG_D("zVideoThread::getInstance start\n");
    if (NULL == m_instance)
    {
        m_instance = new zVideoThread();
        if (NULL == m_instance)
        {
            LOG_E("failed to creat video thread.\n");
        }
    }
    //LOG_D("zVideoThread::getInstance end\n");
    return m_instance;
}

void zVideoThread::destroyInstance()
{
    LOG_I("zVideoThread::destroyInstance start\n");
    if (NULL != m_instance)
    {
        delete m_instance;
        m_instance = NULL;
    }
    LOG_I("zVideoThread::destroyInstance end\n");
    return;
}

zVideoThread::zVideoThread( )
    : m_nVideoState(zBaseUtil::zVideoState::ONLINE_VIDEO)
    , m_pRokugaThread(NULL)
    , m_getframe_fail_continuous_count(0)
    , m_prev_frame_time(0)
    , m_need_gamma_correct(false)
    , m_gamma_correct_state(false)
{
    m_pManager = NULL;
    m_pRokugaThread = NULL;
    
    m_DtPower = NULL;
    m_poweron_image = false;

    m_nWidth = DEFAULT_VIDEO_WIDTH;
    m_nHeight = DEFAULT_VIDEO_HEIGHT;

    m_instance = this;
    {
        zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
        std::string val = conf.getValue("version");
        if (val.empty()) val = "default_version";
        m_strVersion = val;
        LOG_I("m_strVersion=%s\n", m_strVersion.c_str());

        std::string poweron_image = conf.getValue("poweron_image");
        m_poweron_image = ("on" == poweron_image);
    }

    m_video_initialize_fail_flag = false;
    m_h264_enc_fail_flag = false;
    m_h264_enc_frm_cnt = 0;
    m_frame_threshold = 20; // 20帧

    m_fp = NULL;
    m_total_frame = 0;
}

zVideoThread::~zVideoThread()
{
    LOG_I("zVideoThread::~zVideoThread() begin \n");
    if (NULL != m_pManager)
    {
        delete m_pManager;
        m_pManager = NULL;
    }
    if (NULL != m_pRokugaThread)
    {
        delete m_pRokugaThread;
        m_pRokugaThread = NULL;
    }
    if (NULL != m_DtPower)
    {
        delete m_DtPower;
        m_DtPower = NULL;
    }
    UninitH264Enc();
    VideoCapture_Stop();
    VideoCapture_Finalize();

    LOG_I("zVideoThread::~zVideoThread() end \n");
}

void zVideoThread::init(int dev_id /* = 0 */, int width /* = 1280 */, int height /* = 720 */
    , int pixel_fmt /* = VIDEO_CAP_FMT_NV21 */, int frame_rate /* = 0 */, int reverse /* = 0 */)
{
    /* add by 刘春龙, 2016-03-31, 原因: 无摄像头，程序继续向下执行 */
    if (0 != zBaseUtil::videoCheck())
    {
        LOG_E("video device not found!\n");
    }

    LOG_D("dev_id = %d, width = %d, height = %d, pixel_fmt = %d, frame_rate = %d, reverse = %d\n",
        dev_id, m_nWidth, m_nHeight, pixel_fmt, frame_rate, reverse);
    int nResult = VideoCapture_Initialize(dev_id, m_nWidth, m_nHeight, pixel_fmt, frame_rate, reverse);
    if (nResult < 0)
    {
        LOG_E("VideoCapture_Initialize failed %d\n", nResult);
        int nErr = zEakonThread::getLastErr();
        nErr |= EAKON_VIDEO_ERR;
        zEakonThread::setLastErr(nErr);
        m_video_initialize_fail_flag = true;
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        nErr &= ~EAKON_VIDEO_ERR;
        zEakonThread::setLastErr(nErr);
        m_video_initialize_fail_flag = false;

        //m_pManager = new zAlgManager(m_nWidth, m_nHeight);
        //m_pRokugaThread = new zRokugaThread(m_pManager);
    }
    m_pManager = new zAlgManager(m_nWidth, m_nHeight);
    m_pRokugaThread = new zRokugaThread(m_pManager, m_nWidth, m_nHeight);

    if (m_poweron_image)
    {
        m_DtPower = new DtPower_OnAndOff(m_nWidth,m_nHeight);
    }
}

void zVideoThread::switchEEKonState(int step /* = 1 */)
{
    // each time switch eakon state to next
    return;
}

void zVideoThread::DealFrame(S_VCBuf& buf)
{
    // algorithm deal

    if (NULL != m_pManager)
    {
        m_pManager->addFrame(buf);
    }

}

void zVideoThread::DealEncFrame(S_H264EncBuf& buf, std::int64_t TimeStamp)
{
    if (NULL == m_pManager)
    {
        LOG_D("m_pManager is: %p\n", m_pManager);
        return;
    }

    (void)m_pManager->addEncFrame(buf, TimeStamp);

    if ( m_pManager->get_live_video_state())
    {
        if (    (m_nVideoState == zBaseUtil::zVideoState::ONLINE_VIDEO)
                &&  ("" != zP2PThread::m_deviceId))
        {
            //LOG_D("sendFrameToWebrtc buf.BytesUsed == %u, m_nVideoState:%d \n", buf.BytesUsed, m_nVideoState);
            sendFrameToWebrtc((unsigned char*)buf.buf, buf.BytesUsed, m_nWidth, m_nHeight, !!buf.IsKeyFrame);
        }
    }
    return;
}

void zVideoThread::Start()
{
    // for test
    LOG_I("zVideoThread::Start() \n");
    if (!m_video_initialize_fail_flag)
    {
        int nResult = VideoCapture_Start();

        if (0 == nResult)
        {
            LOG_I("zVideoThread::Start: VideoCapture_Start ok.\n");
            (void)InitH264Enc(DEFAULT_H264_W, DEFAULT_H264_H);
        }
        else if (nResult < 0)
        {
            LOG_E("VideoCapture_Start %d\n", nResult);

            /* add by 刘春龙, 2015-11-09, 原因: 摄像头未安装或故障时, 继续执行，不退出程序。*/
            //m_threadState = zThread::ThreadState::DEAD_THREAD;
            //return;

            int nErr = zEakonThread::getLastErr();
            if (!(nErr & EAKON_VIDEO_ERR))
            {
                nErr |= EAKON_VIDEO_ERR;
                zEakonThread::setLastErr(nErr);
                m_video_initialize_fail_flag = true;
            }
        }
    }

    zThread::Start();
    if (NULL != m_pManager)
    {
        int nReturn = m_pManager->StartThreads();
        if (0 != nReturn)
        {
            delete m_pManager;
            m_pManager = NULL;
        }
    }

    if (NULL != m_pRokugaThread)
    {
        LOG_D("rokugaThread start \n");
        m_pRokugaThread->set_record_resolution(m_h264_encode_w, m_h264_encode_h);
        m_pRokugaThread->Start();
        LOG_D("rokugaThread end \n");
    }

    if (NULL != m_DtPower)
    {
        LOG_D("DtPower_OnAndOff start \n");
        m_DtPower->Start();
        LOG_D("DtPower_OnAndOff end \n");
    }
    LOG_D("zVideoThread::Start() end \n");
}

std::string zVideoThread::version()
{
    return m_strVersion;
}

void zVideoThread::chgModel(zBaseUtil::zEakonModel model)
{
    if (m_pManager) m_pManager->setEakonModel(model);
    return;
}

zBaseUtil::zEakonModel zVideoThread::getEAKonModel()
{
    //LOG_D(" zVideoThread::getEAKonModel \n");
    zBaseUtil::zEakonModel eakon = zBaseUtil::zEakonModel::UNKNOWN_MODEL;
    if (m_pManager) eakon = m_pManager->getEakonModel();
    //LOG_D(" zVideoThread::getEAKonModel end \n");
    return eakon;
}

void zVideoThread::setEakonDev(ZDEVICE device)
{
    //m_eakonDev = device;
    if (m_pManager) setEakonDev(device);
    return;
}

void zVideoThread::setVideoState(zBaseUtil::zVideoState state)
{
    {
        //std::unique_lock<std::mutex> lock(m_ctrlMutex);
        m_nVideoState = state;
    	LOG_D("webrtc: setVideoState m_nVideoState:%d \n", m_nVideoState);
    }
    //if (NULL != m_pManager)
    //  m_pManager->setVideoState(state);
    return;
}

zBaseUtil::zVideoState zVideoThread::getVideoState()
{
    {
        //std::unique_lock<std::mutex> lock(m_ctrlMutex);
        return m_nVideoState;
    }
    //return m_pManager ? zBaseUtil::zVideoState::UNKNOWN_VIDEO
    //                          : m_pManager->getVideoState();
}

void zVideoThread::setEAKonState(int state)
{
    if (m_pManager) 
        m_pManager->setEakonState(state);
    return;
}

int zVideoThread::getEAKonState()
{
    return m_pManager ? (int)zAlgManager::EakonState::UNKNOWN_EAKON
        : m_pManager->getEakonState();
}

int zVideoThread::getRokuState()
{
    int ret = -1;
    if (NULL != m_pRokugaThread)
    {
        ret = m_pRokugaThread->rokuState();
    }
    return ret;
}

void zVideoThread::SetNeedEnc(bool need)
{
    //do{
    //  {
    //      std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //      if (need == m_bNeedUpload) break;
    //      need ? InitH264Enc() : UninitH264Enc();
    //      m_bNeedUpload = need;
    //  }
    //} while (false);
    return;
}

bool zVideoThread::NeedEnc()
{
    //std::unique_lock<std::mutex> lock(m_ctrlMutex);
    ////return m_bNeedUpload;
    //return m_nVideoState != zBaseUtil::zVideoState::CLOSED_VIDEO;
    return true;
}

int zVideoThread::onMsg(const zBaseUtil::zMsg& msg)
{
    if (NULL != m_pRokugaThread)
    {
        m_pRokugaThread->onMsg(msg);
    }
    return 0;
}

int zVideoThread::onEvent(int nEvent)
{
    int nResult = -1;
    if (NULL != m_pManager)
    {
        LOG_I("zVideoThread::onEvent: nEvent=%d\n", nEvent);
        m_pManager->onEvent(nEvent);
        nResult = 0;
    }
    {
        //std::unique_lock<std::mutex> lock(m_ctrlMutex);
        if (ZM_ROKUUP == nEvent)
        {
            setVideoState(zBaseUtil::zVideoState::UPROKU_VIDEO);
            //m_bOnline = false;
        }
        else if (ZM_ONLINE == nEvent)
        {
            setVideoState(zBaseUtil::zVideoState::ONLINE_VIDEO);
            //m_bOnline = true;
        }
        else if (ZM_ROKUUPSTOP == nEvent)
        {
            setVideoState(zBaseUtil::zVideoState::UNKNOWN_VIDEO);
        }
    }
    //
    if (nEvent == ZM_ROKU)
    {
        m_pRokugaThread->onEvent(ZM_ROKU);
    }
    else if (nEvent == ZM_BEGIN_ROKUSTRANGER)
    {
        m_pRokugaThread->onEvent(ZM_BEGIN_ROKUSTRANGER);
    }
    else if (nEvent == ZM_END_ROKUSTRANGER)
    {
        m_pRokugaThread->onEvent(ZM_END_ROKUSTRANGER);
    }
    else if (nEvent == ZM_ROKUSTOP)
    {
        m_pRokugaThread->onEvent(ZM_ROKUSTOP);
    }
    else if (ZM_TIMESYNC == nEvent)
    {
        //时间同步了
        m_pRokugaThread->onEvent(ZM_TIMESYNC);
    }
    return nResult;
}

//////////////////////////////////////////////////////////////////////////
bool zVideoThread::findRokuFile(const std::string& fileName, std::string& fullPath)
{
    return m_pRokugaThread->findFile(fileName, fullPath);
}


bool zVideoThread::downloadFile(const zBaseUtil::DownloadParam& param)
{
    return m_pRokugaThread->downloadFile(param);
}


bool zVideoThread::removeFile(const std::string& fileName)
{
    bool result = m_pRokugaThread->removeFile(fileName);

    return result;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.change_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 改变直播和录像的分辨率
 * 输入参数  : zBaseUtil::resolution_t& resolution  分辨率
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::change_resolution(zBaseUtil::resolution_t& resolution)
{
    int err_ret = -1;
    LOG_I("resolution: %d, %d\n", resolution.width, resolution.height);
    if (!zP2PThread::is_support_resolution(resolution.width, resolution.height))
    {
        
return err_ret;
    }

    //LOG_E("line=%d\n", __LINE__);
    // 需要获取互斥锁
    std::unique_lock<std::mutex> video_lock(zBaseUtil::g_resolution_video_mutex);
    std::unique_lock<std::mutex> record_lock(zBaseUtil::g_resolution_record_mutex);
    //LOG_E("line=%d\n", __LINE__);

    // 如果正在录像，禁止切换分辨率
    if (NULL != m_pRokugaThread)
    {
        int state = m_pRokugaThread->rokuState();
        if ((1 == state) || (2 == state))
        {
            return err_ret;
        }
    }

    if (    (m_h264_encode_w == resolution.width)
        &&  (m_h264_encode_h == resolution.height))
    {
        LOG_D("no change. %d, %d \n", m_h264_encode_w, m_h264_encode_h);
        return 0;
    }

    //LOG_E("line=%d\n", __LINE__);

    // 重新初始化H264, 硬编码参数
    if (0 != UninitH264Enc())
    {
        return err_ret;
    }
    //LOG_E("line=%d\n", __LINE__);

    if (0 != InitH264Enc(resolution.width, resolution.height))
    {
        return err_ret;
    }
    //LOG_E("line=%d\n", __LINE__);

    // 设置录像分辨率
    if (NULL != m_pRokugaThread)
    {
        m_pRokugaThread->set_record_resolution(m_h264_encode_w, m_h264_encode_h);
    }

    // 清理编码帧缓存队列
    if (NULL != m_pManager)
    {
        m_pManager->clrEncFrames();
    }

    // 修改P2P 视频传输参数
    if ("" != zP2PThread::m_deviceId)
    {
        LOG_D("set_video_mode enter. m_h264_encode_w=%d, m_h264_encode_h=%d\n",
            m_h264_encode_w, m_h264_encode_h);
        //zP2PThread::set_video_mode(m_h264_encode_w, m_h264_encode_h);
    }
    //LOG_E("line=%d\n", __LINE__);

    //reset_raw_video(); // 调试
    //std::this_thread::sleep_for(std::chrono::seconds(2));

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.get_encode_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 获取编码分辨率
 * 输入参数  : int& width   返回宽度
               int& height  返回高度
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::get_encode_resolution(int& width, int& height)
{
    // 需要获取互斥锁
    std::unique_lock<std::mutex> lock(m_encode_mutex);
    {
        width = m_h264_encode_w;
        height = m_h264_encode_h;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////

/*****************************************************************************
 * 函 数 名  : zVideoThread.InitH264Enc
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 初始化v4l2的h264硬编码器
 * 输入参数  : int width   图像宽度
               int height  图像高度
 * 输出参数  : 无
 * 返 回 值  : int: 0: 初始化成功，其他: 错误码
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::InitH264Enc(int width, int height)
{
    int ret = VideoCapture_H264EncStart(
        width, height, H264_PROFILE_HIGH, 512 * 1024, NULL);
    if (0 != ret)
    {
        LOG_E("VideoCapture_H264EncStart() failed with %d!\n", ret);
        return ret;
    }

    /* add by 刘春龙, 2015-11-10, 原因: 分辨率切换 */
    std::unique_lock<std::mutex> lock(m_encode_mutex);
    {
        m_h264_encode_w = width;
        m_h264_encode_h = height;
        m_encode_inited_flag = true;
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.UninitH264Enc
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 去初始化v4l2的h264硬编码器
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int: 0: 去初始化成功，其他: 错误码
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::UninitH264Enc()
{
    int ret = VideoCapture_H264EncStop();
    if (0 != ret)
    {
        LOG_E("VideoCapture_H264EncStop() failed with %d!\n", ret);
        return ret;
    }

    /* add by 刘春龙, 2015-11-10, 原因: 分辨率切换 */
    std::unique_lock<std::mutex> lock(m_encode_mutex);
    {
        m_h264_encode_w = 0;
        m_h264_encode_h = 0;
        m_encode_inited_flag = false;
    }
    return ret;
}

void TestGetFrame(S_VCBuf& frameBuf)
{
    unsigned int uDataSize = 0;
    unsigned char*  ucData = (unsigned char*)malloc(16384);
    memset(ucData, 0, 16384);

    int ifrmSize = -1;

    FILE* pFile = fopen("one_h264_frm.frm", "rb");
    if (pFile != NULL) {
        fseek(pFile, 0, SEEK_END);
        ifrmSize = ftell(pFile);
    }
    // Get H.264 video input frame data from codec.
    if (pFile != NULL) {
        fseek(pFile, 0, SEEK_SET);
        int iRead = fread(ucData, ifrmSize, 1, pFile);
        if (iRead > 0 || feof(pFile)) {
            uDataSize = ifrmSize; //
        }
        else {
            uDataSize = 1024; // assume the H.264 frame size.
        }
    }
    else {
        uDataSize = 1024; // assume the H.264 frame size.
    }

    if (pFile != NULL){
        fclose(pFile);
    }
    frameBuf.buf = ucData;
    frameBuf.len = uDataSize;
    LOG_D("TestGetFrame get buf size == %ud\n", uDataSize);
    return;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.proc_h264_enc_result
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月29日
 * 函数功能  : 处理H264编码结果，设置状态
 * 输入参数  : int result  : H264编码返回值
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zVideoThread::proc_h264_enc_result(int result)
{
    // 初始化失败标志为真，直接返回
    if (m_video_initialize_fail_flag)
    {
        LOG_E("proc_h264_enc_result: m_video_initialize_fail_flag true.\n");
        return;
    }

    if (0 == result)
    {
        // 故障恢复
        if (true == m_h264_enc_fail_flag)
        {
            if (++m_h264_enc_frm_cnt >= m_frame_threshold)
            {
                m_h264_enc_fail_flag = false;
                m_h264_enc_frm_cnt = 0;

                int nErr = zEakonThread::getLastErr();
                if (nErr & EAKON_VIDEO_ERR)
                {
                    nErr &= ~EAKON_VIDEO_ERR;
                    zEakonThread::setLastErr(nErr);
                    LOG_I("proc_h264_enc_result: video state restore.\n");
                }
            }
        }
        // 连续故障数大于0，但小于阈值
        else if (m_h264_enc_frm_cnt > 0)
        {
            m_h264_enc_frm_cnt = 0;
        }
    }
    else
    {
        // 故障处理
        if (false == m_h264_enc_fail_flag)
        {
            if (++m_h264_enc_frm_cnt >= m_frame_threshold)
            {
                m_h264_enc_fail_flag = true;
                m_h264_enc_frm_cnt = 0;
                
                int nErr = zEakonThread::getLastErr();
                if (!(nErr & EAKON_VIDEO_ERR))
                {
                    nErr |= EAKON_VIDEO_ERR;
                    zEakonThread::setLastErr(nErr);
                    LOG_I("proc_h264_enc_result: video state error.\n");
                }
            }
        }
        // 连续无故障数大于0，但小于阈值
        else if (m_h264_enc_frm_cnt > 0)
        {
            m_h264_enc_frm_cnt = 0;
        }
    }
    return;
}

int zVideoThread::gamma_correct(bool bOpen)
{
    if (m_need_gamma_correct != bOpen)
    {
        m_need_gamma_correct = bOpen;
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.gamma_correct
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月12日
 * 函数功能  : gamma校正
 * 输入参数  : bool bOpen  true：打开gamma校正，false：关闭gamma校正
 * 输出参数  : 无
 * 返 回 值  : int 0: ok
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::gamma_correct_execute(bool bOpen)
{
    //std::unique_lock<std::mutex> lock(m_ctrlMutex);
    
    int ret = 0;
    if (bOpen)
    {
        #define GAMMA 2.0
        ret = VideoCapture_GammaCorrectionEnable(GAMMA);
        LOG_D("ret=%d, bOpen=%d\n", ret, bOpen);
    }
    else
    {
        ret = VideoCapture_GammaCorrectionDisable();
        LOG_D("ret=%d, bOpen=%d\n", ret, bOpen);
    }
    
    return ret;
}

/*****************************************************************************
 * 函 数 名  : redo_init
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月7日
 * 函数功能  : 重新初始化视频捕获
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zVideoThread::redo_init()
{
    static int redo_init_cnt = 0;
    LOG_I("redo_init_cnt=%d\n", ++redo_init_cnt);

    int nResult = 0;
    int prev_m_h264_encode_w = m_h264_encode_w;
    int prev_m_h264_encode_h = m_h264_encode_h;

    nResult = UninitH264Enc();
    if (0 != nResult)
    {
        LOG_E("UninitH264Enc failed. nResult=%d\n", nResult);
    }

    nResult = VideoCapture_Finalize();
    if (nResult < 0)
    {
        LOG_E("VideoCapture_Finalize failed. nResult=%d\n", nResult);
    }

    nResult = VideoCapture_Initialize(
        0, DEFAULT_VIDEO_WIDTH, DEFAULT_VIDEO_HEIGHT, VIDEO_CAP_FMT_NV21, 0, 0);
    if (0 == nResult)
    {
        nResult = VideoCapture_Start();
        if (0 == nResult)
        {
            nResult = InitH264Enc(prev_m_h264_encode_w, prev_m_h264_encode_h);
            if (0 != nResult)
            {
                LOG_E("InitH264Enc failed. nResult=%d\n", nResult);
            }
        }
        else
        {
            LOG_E("VideoCapture_Start failed. nResult=%d\n", nResult);
        }
    }
    else
    {
        LOG_E("VideoCapture_Initialize failed. nResult=%d\n", nResult);
    }

    return;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.do_video_capture
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月21日
 * 函数功能  : 获取图像
 * 输入参数  : S_VCBuf& frameBuf  帧
               int& encode        是否编码
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::do_video_capture(S_VCBuf& frameBuf, int need_encode)
{
    int nResult = VideoCapture_GetFrame(&frameBuf, need_encode);
    if (0 == nResult)
    {
        // may just alloc 2 buffer and just swap(pCurBuff, pNewBuff);
        LOG_D("VideoCapture_GetFrame ok. nResult=%d \n", nResult);
        DealFrame(frameBuf);

        if (0 != m_getframe_fail_continuous_count)
        {
            m_getframe_fail_continuous_count = 0;
        }
    }
    else
    {
        LOG_E("VideoCapture_GetFrame error. nResult=%d\n", nResult);
        if (++m_getframe_fail_continuous_count >= GETFRAME_FAILED_THRESHOLD)
        {
            // 连续抓取帧失败，重新初始化
            redo_init();            
            m_getframe_fail_continuous_count = 0;
        }
    }
    return nResult;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.do_video_encode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月21日
 * 函数功能  : 帧编码
 * 输入参数  : const int bufLen  最大编码长度
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zVideoThread::do_video_encode(int bufLen)
{
    static S_H264EncBuf h264_vbuf = { 0 };
    
    if ( (0 == h264_vbuf.len) || (NULL == h264_vbuf.buf) )
    {
        uint8_t *h264_buf = (uint8_t *)malloc(bufLen);
        if (!h264_buf)
        {
            LOG_E("Allocate h264 buffer failed!\n");
            return -1;
        }
        
        h264_vbuf.BytesUsed = 0;
        h264_vbuf.buf = h264_buf;
        h264_vbuf.len = bufLen;
    }
    
    int nResult = VideoCapture_H264GetEncFrame(&h264_vbuf);
    proc_h264_enc_result(nResult);

    if (!nResult)
    {
        // may just alloc 2 buffer and just swap(pCurBuff, pNewBuff) ;
        LOG_D("m_h264_encode_w=%d, m_h264_encode_h=%d, IsKeyFrame=%d\n",
            m_h264_encode_w, m_h264_encode_h, h264_vbuf.IsKeyFrame);
        //save_raw_video(h264_vbuf); // 用于调试

        DealEncFrame(h264_vbuf, zBaseUtil::getMilliSecond());
    }
    else
    {
        LOG_E("VideoCapture_H264GetEncFrame() failed with %d!\n", nResult);
    }
    
    return nResult;
}

void zVideoThread::proc()
{
    static S_VCBuf frameBuf = { 0 };
    static const int bufLen = (sizeof(uint8_t) * m_nWidth * m_nHeight * 3) >> 1;
    static int64_t initTime = zBaseUtil::getMilliSecond();
    static int64_t preFrame = -1;

    if (m_video_initialize_fail_flag)
    {
        LOG_D("zVideoThread::proc: m_video_initialize_fail_flag =%d\n", m_video_initialize_fail_flag);
        std::this_thread::sleep_for(std::chrono::seconds(10));
        return;
    }

    if (0 == frameBuf.len || NULL == frameBuf.buf)
    {
        frameBuf.len = bufLen;

        uint8_t* pBuf = (uint8_t*)malloc(frameBuf.len);
        if (NULL == pBuf)
        {
            LOG_E("(uint8_t*)malloc( %d ) failed", frameBuf.len);
            return;
        }
        frameBuf.buf = pBuf;
    }

    // gamma 切换
    if (m_need_gamma_correct != m_gamma_correct_state)
    {
        if (0 == gamma_correct_execute(m_need_gamma_correct))
        {
            m_gamma_correct_state = m_need_gamma_correct;
        }
    }

    int encode = 0;
    int64_t curTime = zBaseUtil::getMilliSecond();
    int64_t curFrame = (curTime-initTime) / ROKUGAG_FRAME_GAP;
    if (curFrame != preFrame)
    {
        preFrame = curFrame;
        encode = 1;
    }

    if (encode)
    {
        // 睡眠10毫秒，防止带着锁进入
        std::this_thread::sleep_for(std::chrono::milliseconds(10));

        // 使用try_lock，防止在切换分辨率时，进入长时间等待
        std::unique_lock<std::mutex> lck(zBaseUtil::g_resolution_video_mutex, std::defer_lock);
        if (lck.try_lock())
        {
            int ret = do_video_capture(frameBuf, true);
            if (0 == ret)
            {
                (void)do_video_encode(bufLen);
            }
        }
        else
        {
            (void)do_video_capture(frameBuf, false);
        }
    }
    else
    {
        (void)do_video_capture(frameBuf, false);
    }

    // 控制帧速
    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    if (0 != m_prev_frame_time)
    {
        std::int64_t elapse = curr_time - m_prev_frame_time;
        if (elapse < GET_FRAME_GAP)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(GET_FRAME_GAP - elapse));
            curr_time = zBaseUtil::getMilliSecond();
            LOG_D("elapse=%lld, curr_time=%lld\n", elapse, curr_time);
        }
    }
    m_prev_frame_time = curr_time;

    return;
}

zAlgManager* zVideoThread::getAlgManager()
{
    return m_pManager;
}

zRokugaThread* zVideoThread::getRokugaThread()
{
    return m_pRokugaThread;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.save_raw_video
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 保存 编码的裸H264码流 到文件，用于调试
 * 输入参数  : S_H264EncBuf& h264_vbuf : 编码帧
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zVideoThread::save_raw_video(const S_H264EncBuf& h264_vbuf)
{
    static int h264_count = 0;
    if (NULL == m_fp)
    {
        char h264_filename[128];
        sprintf(h264_filename, "/sdcard/test/raw_%d_%d_%d.h264", 
            m_h264_encode_w, m_h264_encode_h, h264_count++);

        m_fp = (FILE *)fopen(h264_filename, "wb");
        if (NULL == m_fp)
        {
            return;
        }
    }

    if (m_total_frame < 10000)
    {
        int size = fwrite(h264_vbuf.buf, 1, h264_vbuf.BytesUsed, m_fp);
        if (size != static_cast<int>(h264_vbuf.BytesUsed))
        {
            LOG_D("h264_vbuf.BytesUsed=%d, size=%d\n", h264_vbuf.BytesUsed, size);
        }
        
        m_total_frame++;
        (void)fflush(m_fp);
    }
    
    return;
}

/*****************************************************************************
 * 函 数 名  : zVideoThread.reset_raw_video
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 复位
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zVideoThread::reset_raw_video()
{
    if (NULL != m_fp)
    {
        (void)fclose(m_fp);
        m_fp = NULL;
    }
    m_total_frame = 0;
    return;
}

