#ifndef __ZDATABUF_H__
#define __ZDATABUF_H__

#define DATABUF_DEFAUT_SIZE (32)
#define MAX_ANGLE_SIZE 8 // 用于求角度均值

#include <stdlib.h>
#include "zLog.h"

class zDataBuf
{
public:
    zDataBuf()
        : m_index(0), m_curCnt(0)
    {
        memset(m_dis, 0, sizeof(m_dis));
        memset(m_min, 0, sizeof(m_min));
        memset(m_max, 0, sizeof(m_max));
    }

    int addInfo(int dis, int min, int max)
    {
        int curDis, curMin, curMax;
        getAverage(curDis, curMin, curMax);
        m_dis[DATABUF_DEFAUT_SIZE] = curDis;
        m_min[DATABUF_DEFAUT_SIZE] = curMin;
        m_max[DATABUF_DEFAUT_SIZE] = curMax;

        m_dis[m_index] = dis;
        m_min[m_index] = min;
        m_max[m_index] = max;

        m_index++;
        if (m_index >= DATABUF_DEFAUT_SIZE)
        {
            m_index = 0;
        }

        if (m_curCnt < DATABUF_DEFAUT_SIZE)
        {
            m_curCnt++;
        }
        return m_curCnt;
    }

    /*****************************************************************************
     * 函 数 名  : zDataBuf.get_average_distance
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年12月24日
     * 函数功能  : 获取平均距离，去掉一个最大值，去掉一个最小值
     * 输入参数  : int& dis : 输出平均距离
     * 输出参数  : 无
     * 返 回 值  : void
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    void get_average_distance(int& dis)
    {
        if (0 == m_curCnt)
        {
            dis = 0;
            return;
        }

        int min_dis = m_dis[0];
        int max_dis = m_dis[0];
        int sum_dis = m_dis[0];
        for (int i = 1; i < m_curCnt; i++)
        {
            if (m_dis[i] < min_dis)
            {
                min_dis = m_dis[i];
            }
            else if (m_dis[i] > max_dis)
            {
                max_dis = m_dis[i];
            }
            sum_dis += m_dis[i];
        }

        int dis_cnt = m_curCnt;
        if (m_curCnt >= 3)
        {
            sum_dis -= min_dis + max_dis;
            dis_cnt -= 2;
            //LOG_D("min_dis=%d, max_dis=%d, m_curCnt=%d\n", min_dis, max_dis, m_curCnt);
        }

        // 四舍五入
        dis = (sum_dis + dis_cnt /2) / dis_cnt ;
        return;
    }

    /*****************************************************************************
     * 函 数 名  : zDataBuf.get_average_angle
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年12月24日
     * 函数功能  : 获取平均角度
     * 输入参数  : int& min  输出左角度均值
                   int& max  输出右角度均值
     * 输出参数  : 无
     * 返 回 值  : void
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    void get_average_angle(int& min, int& max)
    {
        if (0 == m_curCnt)
        {
            // 特殊情况处理
            min = 0;    
            max = 0;
            return;
        }

        int angle_cnt = m_curCnt; 
        if (angle_cnt > MAX_ANGLE_SIZE)
        {
            angle_cnt = MAX_ANGLE_SIZE;
        }

        int minSum = 0;
        int maxSum = 0;
        int index = m_index;
        // 从后向前遍历，求角度均值
        for (int i = 0; i < angle_cnt; i++)
        {
            if ((--index) < 0)
            {
                index = DATABUF_DEFAUT_SIZE - 1;
            }
            minSum += m_min[index];
            maxSum += m_max[index];
        }

        // 四舍五入 
        min = (minSum + angle_cnt/2) / angle_cnt;
        max = (maxSum + angle_cnt/2) / angle_cnt;
        return;
    }

    /*****************************************************************************
     * 函 数 名  : getAverage
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年12月22日
     * 函数功能  : 求平均值
     * 输入参数  : 无
     * 输出参数  : 无
     * 返 回 值  : 
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int getAverage(int& dis, int& min, int& max)
    {
        get_average_distance(dis);
        get_average_angle(min, max);
        return 0;
    }

    int diff(int& dis, int& min, int& max)
    {
        int curDis, curMin, curMax;
        getAverage(curDis, curMin, curMax);
        dis = curDis - m_dis[DATABUF_DEFAUT_SIZE];
        min = curMin - m_min[DATABUF_DEFAUT_SIZE];
        max = curMax - m_max[DATABUF_DEFAUT_SIZE];
        return 0;
    }
    
    void Clear()
    {
        //LOG_D("zFaceBodyInfoBuf Clear()\n");
        m_curCnt = 0;
        m_index = 0;
        memset(m_dis, 0, sizeof(m_dis));
        memset(m_min, 0, sizeof(m_min));
        memset(m_max, 0, sizeof(m_max));
        //LOG_D("zFaceBodyInfoBuf Clear() end\n");
        return;
    }
    
    bool empty()
    {
        return (0 == m_curCnt);
    }
    
private:
    int m_dis[DATABUF_DEFAUT_SIZE + 1];
    int m_min[DATABUF_DEFAUT_SIZE + 1];
    int m_max[DATABUF_DEFAUT_SIZE + 1];
    int m_index;
    int m_curCnt;
};
#endif