#ifndef __ZIMAGE_H__
#define __ZIMAGE_H__

#include <string>
#include "turbojpeg.h"

#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
#define ZZ_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
#elif _MSC_VER >= 1400 //vs 2005 or higher
#define ZZ_DEPRECATED_ATTRIBUTE __declspec(deprecated) 
#else
#define ZZ_DEPRECATED_ATTRIBUTE
#endif 

namespace zImageUtil
{
    class zImage
    {
    public:
        enum class Format
        {
            UNKNOWN,
            JPG, 
            PNG, 
            RAW_DATA,
        };
        enum class PixelFormat
        {
            //! auto detect the type
            AUTO,
            //! 32-bit texture: BGRA8888
            BGRA8888,
            //! 32-bit texture: RGBA8888
            RGBA8888,
            //! 24-bit texture: RGBA888
            RGB888,
            //! 16-bit texture without Alpha channel
            RGB565,
            //! 16-bit textures: RGBA4444
            RGBA4444,
            //! 16-bit textures: RGB5A1
            RGB5A1,
            //! Default texture format: AUTO
            DEFAULT = AUTO,

            NONE = -1
        };
    public:
        zImage();
        ~zImage();

        // Getters
        unsigned char*  getData()                   { return data_; }
        size_t          getDataLen()                { return dataLen_; } 
        Format          getFileType()               { return fileType_; } 
        int             getWidth()                  { return width_; }
        int             getHeight()                 { return height_; } 
        //bool              hasPremultipliedAlpha()     { return hasPremultipliedAlpha_; } 
        //ZZ_DEPRECATED_ATTRIBUTE bool isPremultipliedAlpha()   { return hasPremultipliedAlpha_; } 


        bool save2File(const std::string& filename, bool is2RGB = true);
        bool initWithFile(const std::string& fullPath);
        bool initWithMem(const unsigned char* data, size_t dataLen);
        bool initWithNV12Data(const unsigned char* data, size_t dataLen, int width, int height, int bitsPerComponent, bool preMulti);
        bool initWithRawData(const unsigned char* data, size_t dataLen, int width, int height, int bitsPerComponent, bool preMulti);

        static bool yuv_to_jpg_file(const std::string& output,
            unsigned char* yuv_buffer, int yuv_size, int width, int height, int subsample);

        static tjhandle init_jpg_handle();
        static void destroy_jpg_handle(tjhandle handle);
        static bool yuv_to_jpg_buffer(tjhandle handle, 
            unsigned char* yuv_buffer, int yuv_size, int width, int height, int subsample,
            unsigned char **pp_jpeg_buffer, unsigned long *p_jpeg_size);
        static void jpg_buffer_free(unsigned char **pp_jpeg_buffer);

    protected:

        bool saveImageToPNG(const std::string& filePath, bool is2RGB = true);
        bool saveImageToBMP(const std::string& filePath);

        //////////////////////////////////////////////////////////////////////////
        zImage(const zImage& rhs);
        zImage& operator=(const zImage& src);

        bool initWithImageFileThreadSafe(const std::string& fullPath);

        Format detectFormat(const unsigned char* data, size_t dataLen);
        bool isPng(const unsigned char* data, size_t dataLen);
        bool isJpg(const unsigned char* data, size_t dataLen);

    protected:
        unsigned char* data_;
        size_t dataLen_;
        int width_;
        int height_;
        int comp_ {4};
        bool unpack_;
        Format fileType_;
        //PixelFormat renderFormat_;
        //bool hasPremultipliedAlpha_;
        std::string filePath_;
    };
}
#endif
