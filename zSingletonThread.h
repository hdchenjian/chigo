#ifndef __ZSINGLETONTHREAD_H__
#define __ZSINGLETONTHREAD_H__

#include <thread>
#include <mutex>

#include "zUtil.h"

class zSingletonThread
{
public:
    //static zSingletonThread* getInstance();
    //static void destroyInstance();

    void setThreadGap(std::uint64_t nGap);      //milli
    std::uint64_t threadGap();

    bool busy();
    virtual bool proc();

    virtual int onMsg(const zBaseUtil::zMsg& msg);
    bool startThread();
    static bool ThreadPro(void* param);
protected:
    zSingletonThread();
    virtual ~zSingletonThread();
    void begin();
    void end();
private:
    //static zSingletonThread* instance_;
    bool busy_;
    int nGap_;
    std::thread* m_pThread;
    std::mutex mutex_;
};

#endif // !__ZSINGLETONTHREAD_H__
