#include <iostream>
#include <cstdint>
#include <array>
#include <vector>
#include <utility>
#include <algorithm>
#include <cctype>

#include "zVideoThread.h"
#include <./3rdParty/V4L2ImageCap/jni/cap_types.h>

#include <memory>
#include "zUtil.h"
#include "zLog.h"
#include "config_mgr.h"

//#include "zAlgorithmManager.h"
#include "zSingletonThread.h"
#include "p2pThread.h"
#include "zEakonThread.h"
#include "zRokugaThread.h"
#include "zRokuUpThread.h"
#include "zFile.h"
#include "cJSON.h"
#include "md5_proc.h"
#include "process_start.hpp"

#define P2P_CMD_GAP 50 // 单位: 毫秒
#define MAX_FILE_QUERY_SIZE (100) // 一次录像查询最大返回数量
#define P2P_CONFIG_ENABLE 0 // 使能P2P配置功能标志位，否则从主控获取

// #define TRANSSIZE (3)
//int zP2PThread::m_gDevInstID = 0;
//unsigned int zP2PThread::m_uRunning = 0;
zP2PThread* zP2PThread::m_instance = NULL;
std::string zP2PThread::m_deviceId = "";

typedef struct p2p_resolution_s
{
    int width;
    int height;
    int mode;
}p2p_resolution_t;

const char* gModel[] = { "unknown state", "no_ouside_mode", "ouside_mode" };

// 支持的分辨率列表
#define RESOLUTION_CNT 3 // 数量
const p2p_resolution_t gResoTab[RESOLUTION_CNT] = 
{
    { 320,  240 ,   2 },
    { 640,  480 ,   3 },
    {1280,  720 ,   10}
};

namespace AcKeysIdx {
    static constexpr uint32_t idx_act                   = 0;
    static constexpr uint32_t idx_version_id            = 1;
    static constexpr uint32_t idx_send_debug            = 2;
    static constexpr uint32_t idx_outside               = 3;
    static constexpr uint32_t idx_video_live            = 4;
    static constexpr uint32_t idx_p2p_enable            = 5;
    static constexpr uint32_t idx_video_replay          = 6;
    static constexpr uint32_t idx_video_record          = 7;
    static constexpr uint32_t idx_wind_blow_strategy    = 8;
    static constexpr uint32_t idx_resolution            = 9;
    static constexpr uint32_t idx_lighting_mode         = 10;
    static constexpr uint32_t idx_ac_opened             = 11;
    static constexpr uint32_t idx_gesture_control       = 12;
    static constexpr uint32_t idx_intelligent_enable    = 13;
    static constexpr uint32_t idx_heat_source_enable    = 14;
    static constexpr uint32_t idx_butt                  = 15;
};

static std::array<std::string, AcKeysIdx::idx_butt> ac_keys {
    "act",                  // 空调运行状态() 或 msgType
    "version_id",           // 版本号
    "send_debug",           // 调试开关，true发送debug信息，false不发送debug信息。
    "outside",              // true:离家模式开启, false:离家模式关闭
    "video_live",           // true:视频直播, false:直播关闭
    "p2p_enable",           //true: 允许查看远程实时视频，否则false
    "video_replay",         // 视频回放文件名称
    "video_record",         // true:正在录制视频，否则false
    "wind_blow_strategy",   // 1：风吹人，2:风避人, 3：禁用（自动扫风）
    "resolution",           // 编码分辨率
    "lighting_mode",        // auto: 自动补光 open: 强制开补光 close：强制关补光
    "ac_opened",            // true: 空调已开机 false：空调已关机
    "gesture_control",      // 0: 手势关闭 1: 手势开关机， 2：手势定向风（风吹手） 3：手势定向风（智能风或自动扫风）
    "intelligent_enable",   // true: 智能总开关打开， false：智能总开关关闭
    "heat_source_enable",   // true: 热源告警开关打开， false：热源告警开关关闭
};

/*****************************************************************************
 * 函 数 名  : split_with_delimiter
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月27日
 * 函数功能  : 分割字符串
 * 输入参数  : const std::string &str           源字符串
               char delimiter                   分隔符
               std::vector<std::string> &elems  分割向量
 * 输出参数  : 无
 * 返 回 值  : static std::vector<std::string> &
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::vector<std::string> & zP2PThread::split_with_delimiter(
    const std::string &str, char delimiter, std::vector<std::string> &elems)
{
    if (str.empty())
    {
        return elems;
    }

    size_t bgn_pos = 0;
    size_t end_pos = str.find(delimiter, bgn_pos);
    while(end_pos != std::string::npos)
    {
        elems.push_back(str.substr(bgn_pos, end_pos - bgn_pos));

        bgn_pos = end_pos + 1;
        end_pos = str.find(delimiter, end_pos + 1);
    }

    elems.push_back(str.substr(bgn_pos));
    return elems;
}

/*****************************************************************************
 * 函 数 名  : parse_option
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月27日
 * 函数功能  : 解析命令参数, 字符串结构: opt= val
 * 输入参数  : zP2PThread::p2pMsg& msg  命令参数
               std::string &opt         命令参数字符串
               char delimiter           分割符
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static void parse_option(zP2PThread::p2pMsg& msg, std::string &opt, char delimiter)
{
    if (opt.empty())
    {
        return; // do nothing
    }

    size_t pos = opt.find(delimiter);
    if (pos == std::string::npos) // no equal delimiter.
    {
        msg.msgParam.insert(std::make_pair(opt, ""));
    }
    else
    {
        msg.msgParam.insert(std::make_pair(opt.substr(0, pos), opt.substr(pos + 1)));
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : parasP2pMsg
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月27日
 * 函数功能  : 解析P2P命令
 * 输入参数  : zP2PThread::p2pMsg& msg  命令参数
               std::string &cmd_str     命令字符串
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static bool parasP2pMsg(zP2PThread::p2pMsg& msg, std::string &cmd_str)
{
    if (cmd_str.empty())
    {
        return false;
    }

    // remove whitespace.
    cmd_str.erase( std::remove_if( cmd_str.begin(), cmd_str.end(), ::isspace ), cmd_str.end());

    size_t nstart = cmd_str.find_first_of('?');
    if (nstart == std::string::npos)
    {
        msg.msgType = cmd_str;
        return true;
    }

    msg.msgType = cmd_str.substr(0, nstart);
    std::string opts_str = cmd_str.substr(nstart + 1);

    std::vector<std::string> opts;
    if (! zP2PThread::split_with_delimiter(opts_str, '&', opts).empty())
    {
        for (auto i: opts)
        {
            parse_option(msg, i, '=');
        }
    }
    return true;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.get_state_impl
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 获取状态值
 * 输入参数  : uint32_t index  状态索引
 * 输出参数  : 无
 * 返 回 值  : std::string
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::string zP2PThread::get_state_impl(uint32_t index)
{
    if (AcKeysIdx::idx_act == index)
    {
        return std::string("state");
    }
    else if (AcKeysIdx::idx_version_id == index)
    {
        return zVideoThread::getInstance()->version();
    }
    else if (AcKeysIdx::idx_send_debug == index)
    {
        return zBaseUtil::to_string(bDebug());
    }
    else if (AcKeysIdx::idx_outside == index)
    {
        return zBaseUtil::to_string(
                zBaseUtil::zEakonModel::DEKAKE_MODEL == 
                zVideoThread::getInstance()->getEAKonModel());
    }
    else if (AcKeysIdx::idx_video_live == index)
    {
        zBaseUtil::zVideoState videoState = zVideoThread::getInstance()->getVideoState();
        return zBaseUtil::to_string( zBaseUtil::zVideoState::ONLINE_VIDEO == videoState);
    }
    else if (AcKeysIdx::idx_p2p_enable == index)
    {
        return zBaseUtil::to_string(m_pEakonThread->get_live_video_state());
    }
    else if (AcKeysIdx::idx_video_replay == index)
    {
        return zRokuUpThread::getInstance()->filename();
    }
    else if (AcKeysIdx::idx_video_record == index)
    {
        return zBaseUtil::to_string(zVideoThread::getInstance()->getRokuState() > 0);
    }
    else if (AcKeysIdx::idx_wind_blow_strategy == index)
    {
        return to_string_muki();
    }
    else if (AcKeysIdx::idx_resolution == index)
    {
        int width, height;
        zVideoThread::getInstance()->get_encode_resolution(width, height);
        return zBaseUtil::to_string(width) + "X" + zBaseUtil::to_string(height);
    }
    else if (AcKeysIdx::idx_lighting_mode == index)
    {
        bool auto_mode, day;
        m_pEakonThread->get_lighting_mode(auto_mode, day);

        std::string lighting_mode;
        if (auto_mode)
        {
            lighting_mode = "auto";
        }
        else if (!day)
        {
            lighting_mode = "open";
        }
        else
        {
            lighting_mode = "close";
        }
        return lighting_mode;
    }
    else if (AcKeysIdx::idx_ac_opened == index)
    {
        return zBaseUtil::to_string( m_pEakonThread->get_eakon_opened());
    }
    else if (AcKeysIdx::idx_gesture_control == index)
    {
        return zBaseUtil::to_string( m_pEakonThread->get_gesture_state());
    }
    else if (AcKeysIdx::idx_intelligent_enable == index)
    {
        return zBaseUtil::to_string( m_pEakonThread->get_intelligent_enable_flag());
    }

    return std::string("");
}

/*****************************************************************************
 * 函 数 名  : make_json_response
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 生成json响应串
 * 输入参数  : std::vector<std::pair<string, string>> &tuple {名称：值} 队列
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static std::string make_json_response(
    std::vector<std::pair<std::string, std::string>> &tuple)
{
    std::string json_str;
    cJSON *root = cJSON_CreateObject();
    if (NULL != root)
    {
        for (auto i: tuple)
        {
            cJSON_AddStringToObject(root, i.first.c_str(), i.second.c_str());
        }

        json_str = cJSON_PrintUnformatted(root);
        cJSON_Delete(root);
    }
    return json_str;
}

void LogOutput(unsigned int uLevel, const char* lpszOut)
{
    #if 0
    if (NULL != lpszOut)
    {
        LOG_D("pptun: %s\n", lpszOut);
    }
    #endif
}

zP2PThread::zP2PThread(zEakonThread* pParent, const char* szDevID)
    : m_bDebug(false)
    , m_pEakonThread(pParent)
      //, m_pgLiveForwardAlloced(false)
    , m_p2p_enable(true)
{
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
    
    #if 1
    strSrvAddr = conf.getValue("srvAddr_pptun");
    LOG_D("strSrvAddr = %s\n", strSrvAddr.c_str());
    #endif

    #if 0
    std::string p2p_lib = conf.getValue("p2p_library");
    HR_ENGINE_TYPE engineType = ENGINE_PPTUN;
    std::string strSrvAddr = "p1.ansiblewiring.com:7781";

    if ("WEBRTC" == p2p_lib)
    {
        engineType = ENGINE_RTC;
        strSrvAddr = conf.getValue("srvAddr_rtc");

        if (strSrvAddr.empty())
        {
            strSrvAddr = "p2.ansiblewiring.com:8099";
        }
    }
    else if ("PPTUN" == p2p_lib)
    {
        engineType = ENGINE_PPTUN;
        strSrvAddr = conf.getValue("srvAddr_pptun");

        if (strSrvAddr.empty())
        {
            strSrvAddr = "p1.ansiblewiring.com:7781";
        }
    }
    LOG_I("szDevID=%s, szSvrAddr=%s, engineType=%d\n", szDevID, strSrvAddr.c_str(), engineType);
    #endif
    
    m_instance = this;

    // 获取视频分辨率，转换为P2P的 视频模式
    int mode = video_mode_from_resolution(DEFAULT_H264_W, DEFAULT_H264_H);
    zVideoThread* vthread = zVideoThread::getInstance();
    if (NULL != vthread)
    {
        int width =0, height = 0;
        if (0 == vthread->get_encode_resolution(width, height))
        {
            mode = video_mode_from_resolution(width, height);
            LOG_I("width=%d, height=%d, mode=%d \n", width, height, mode);
        }
    }

    LOG_D("zP2PThread::initializeWebrtc befor webrtc");
    m_deviceName = szDevID;
    if (initializeWebrtc(m_deviceName, strSrvAddr, zP2PThread::m_deviceId) != WEBRTC_SUCCESS)
    {
        LOG_E("Init initializeWebrtc failed. webrtc\n");
    }

    LOG_D("zP2PThread::initializeWebrtc end webrtc\n");
    //m_gDevInstID = m_guInstID;
}

zP2PThread::~zP2PThread()
{
    m_instance = NULL;
    cleanupWebrtc();
    m_client_list.clear();
    LOG_I("zP2PThread::~zP2PThread() end\n");
}

zP2PThread* zP2PThread::getInstance()
{
    // 
    return m_instance;
}

void zP2PThread::DestroyInstance()
{
    // todo
}

void zP2PThread::Close()
{
    zThread::Close();
    return;
}

// 返回响应JSON串
std::string zP2PThread::mkP2pMsg(const std::string& strAct, const std::string& strModel
    , const std::string& videoModel, const std::string& etc, const std::string& strDebug
    , const std::string& strRoku, const std::string& strVersion, const std::string& strWind
    , const std::string& strResolution)
{
    std::string strP2pEnable = get_state_impl(AcKeysIdx::idx_p2p_enable);
    std::string lighting_mode = get_state_impl(AcKeysIdx::idx_lighting_mode); 
    std::string ac_state = zBaseUtil::to_string( m_pEakonThread->get_eakon_opened());
    std::string gesture_control = zBaseUtil::to_string( m_pEakonThread->get_gesture_state());
    std::string intelligent_enable = zBaseUtil::to_string( m_pEakonThread->get_intelligent_enable_flag());
    std::string heat_source_enable = zBaseUtil::to_string( m_pEakonThread->get_heat_source_enable_flag());

    std::vector<std::pair<std::string, std::string>> tuple_KV;
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_act], strAct));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_version_id], strVersion));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_send_debug], strDebug));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_outside], strModel));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_video_live], videoModel));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_p2p_enable], strP2pEnable));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_video_replay], etc));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_video_record], strRoku));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_wind_blow_strategy], strWind));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_resolution], strResolution));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_lighting_mode], lighting_mode));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_ac_opened], ac_state));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_gesture_control], gesture_control));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_intelligent_enable], intelligent_enable));
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_heat_source_enable], heat_source_enable));

    //return make_json_response(tuple_KV);
    std::string ret = std::move(make_json_response(tuple_KV));
    LOG_D("json : %s\n", ret.c_str());

    return ret;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.to_string_muki
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月25日
 * 函数功能  : 转换智能风标志 -- int -> string
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std::string
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::string zP2PThread::to_string_muki()
{
    zEakonThread::kazeMuki muki = m_pEakonThread->getKazeMuki();

    int int_muki = 3; // zEakonThread::k_unknown
    if (zEakonThread::k_focuse == muki)
    {
        int_muki = 1;
    }
    else if (zEakonThread::k_unfocuse == muki)
    {
        int_muki = 2;
    }
    return zBaseUtil::to_string(int_muki);
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.notify_response_impl
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 发送json串响应
 * 输入参数  : const std::string& response  json串
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zP2PThread::notify_response_impl(const std::string& response)
{
    notifyWebrtc(response);
    LOG_I("notifyWebrtc str.c_str() == %s\n", response.c_str());
    return;
}

void zP2PThread::notifyP2P(const std::string& strAct)
{
    zBaseUtil::zEakonModel eakonModel = zVideoThread::getInstance()->getEAKonModel();
    std::string strModel = zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel ? "true" : "false";
    //LOG_D("notifyP2P strModel == %s \n", strModel.c_str());

    zBaseUtil::zVideoState videoState = zVideoThread::getInstance()->getVideoState();
    std::string strVideo = zBaseUtil::zVideoState::ONLINE_VIDEO == videoState ? "true" : "false";
    //LOG_D("notifyP2P strVideo == %s \n", strVideo.c_str());
    
    std::string filename;
    zRokuUpThread* pru = zRokuUpThread::getInstance();
    filename = pru->filename();
    //LOG_D("file name == %s \n", filename.c_str());

    // add info 
    std::string strDebug = bDebug()? "true" : "false";

    int nRokuState = zVideoThread::getInstance()->getRokuState();
    std::string strRecord = nRokuState > 0 ? "true" : "false";

    std::string strVer = zVideoThread::getInstance()->version();

    std::string strWind = to_string_muki();

    char strResolution[32] = {0};
    int width, height;
    zVideoThread::getInstance()->get_encode_resolution(width, height);
    (void)sprintf(strResolution, "%dX%d", width, height);

    std::string str = mkP2pMsg("state", 
        strModel, strVideo, filename,strDebug, strRecord, strVer, strWind, strResolution);
    notifyWebrtc(str);
    LOG_I("notifyWebrtc str.c_str() == %s\n", str.c_str());

    return;
}

void zP2PThread::notifyP2P_change_resolution(
    const std::string& strAct, int roku_state)
{
    zBaseUtil::zEakonModel eakonModel = zVideoThread::getInstance()->getEAKonModel();
    std::string strModel = zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel ? "true" : "false";
    //LOG_D("notifyP2P strModel == %s \n", strModel.c_str());

    zBaseUtil::zVideoState videoState = zVideoThread::getInstance()->getVideoState();
    std::string strVideo = zBaseUtil::zVideoState::ONLINE_VIDEO == videoState ? "true" : "false";
    //LOG_D("notifyP2P strVideo == %s \n", strVideo.c_str());
    
    std::string filename;
    zRokuUpThread* pru = zRokuUpThread::getInstance();
    filename = pru->filename();
    //LOG_D("file name == %s \n", filename.c_str());

    // add info 
    std::string strDebug = bDebug()? "true" : "false";
    std::string strRecord = roku_state > 0 ? "true" : "false";

    std::string strVer = zVideoThread::getInstance()->version();

    std::string strWind = to_string_muki();

    char strResolution[32] = {0};
    int width, height;
    zVideoThread::getInstance()->get_encode_resolution(width, height);
    (void)sprintf(strResolution, "%dX%d", width, height);

    std::string str = mkP2pMsg("state", 
        strModel, strVideo, filename,strDebug, strRecord, strVer, strWind, strResolution);
    notifyWebrtc(str);
    LOG_I("notifyWebrtc str.c_str() == %s\n", str.c_str());
    
    return;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.stop_video_live_response
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 停止直播响应
 * 输入参数  : const std::string& strAct  消息名字
               const std::string& render  render名字
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zP2PThread::stop_video_live_response(
    const std::string& strAct, const std::string& render)
{
    zBaseUtil::zEakonModel eakonModel = zVideoThread::getInstance()->getEAKonModel();
    std::string strModel = zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel ? "true" : "false";
    //LOG_D("notifyP2P strModel == %s \n", strModel.c_str());

    //zBaseUtil::zVideoState videoState = zVideoThread::getInstance()->getVideoState();
    //std::string strVideo = zBaseUtil::zVideoState::ONLINE_VIDEO == videoState ? "true" : "false";
    std::string strVideo = "false"; // 非直播态 

    std::string filename;
    zRokuUpThread* pru = zRokuUpThread::getInstance();
    filename = pru->filename();

    // add info 
    std::string strDebug = bDebug()? "true" : "false" ;

    int nRokuState = zVideoThread::getInstance()->getRokuState();
    std::string strRecord = nRokuState > 0 ? "true" : "false";

    std::string strVer = zVideoThread::getInstance()->version();

    std::string strWind = to_string_muki();

    char strResolution[32] = {0};
    int width, height;
    zVideoThread::getInstance()->get_encode_resolution(width, height);
    (void)sprintf(strResolution, "%dX%d", width, height);

    std::string str = mkP2pMsg("state", 
        strModel, strVideo, filename,strDebug, strRecord, strVer, strWind, strResolution);
    sendMessageWebrtc(render, str);

    LOG_I("webrtc response: %s\n", str.c_str());
    return;
}

void zP2PThread::state_changed(bool &changed)
{
    changed = true;
    return;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.OnRenderMSG_impl
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年8月11日
 * 函数功能  : 执行命令
 * 输入参数  : std::string strRender  p2p render
               std::string strMsg     命令信息
               bool &changed          命令执行，是否有状态变更:　置为true，否则，不变.
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zP2PThread::OnRenderMSG_impl(std::string strRender, std::string strMsg, bool &changed)
{
    // 解析命令字符串
    p2pMsg p2pmsg;
    if (!parasP2pMsg(p2pmsg, strMsg))
    {
        return;
    }

    LOG_I("webrtc msgType=%s \n", p2pmsg.msgType.c_str());
    zRokuUpThread* pru = zRokuUpThread::getInstance();
    if ("start_video_live" == p2pmsg.msgType)
    {
    	LOG_I("webrtc");
        if (!(pru->busy()))
        {
            zVideoThread::getInstance()->onEvent(ZM_ONLINE);    // 根据优先级调整策略 如果在上传则无视此消息
            LOG_I("webrtc");
            startVideoWebrtc(strRender);
            state_changed(changed);
        }
    }
    else if ("stop_video_live" == p2pmsg.msgType)
    {
        #if 0
        if (zBaseUtil::zVideoState::UPROKU_VIDEO != zVideoThread::getInstance()->getVideoState())
        {
            zVideoThread::getInstance()->setVideoState(zBaseUtil::zVideoState::CLOSED_VIDEO);
        }
        notifyP2P(p2pmsg.msgType);
        #endif
        stopVideoWebrtc(strRender);
        stop_video_live_response(p2pmsg.msgType, strRender);
    }
    else if ("start_outside_mode" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        //LOG_D("outside_mode enter.\n");
        m_pEakonThread->set_home_leave_mode(false);
        zVideoThread::getInstance()->chgModel(zBaseUtil::zEakonModel::DEKAKE_MODEL);
        state_changed(changed);
        #endif
    }
    else if ("stop_outside_mode" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        //LOG_D("stop_outside_mode enter.\n");
        m_pEakonThread->set_home_leave_mode(true);
        zVideoThread::getInstance()->chgModel(zBaseUtil::zEakonModel::IE_MODEL);
        state_changed(changed);
        #endif
    }
    else if ("file_query" == p2pmsg.msgType)
    {
        zBaseUtil::QueryParam param ;
        std::string topval = p2pmsg.msgParam["top"];
        std::string skipval = p2pmsg.msgParam["skip"];
        std::string queryType = p2pmsg.msgParam["type"];
        std::string id = p2pmsg.msgParam["id"];

        param.id = id.empty() ? -1 : zBaseUtil::to_int(id);
        param.top = zBaseUtil::to_int(topval);
        if (param.top > MAX_FILE_QUERY_SIZE)
        {
            param.top = MAX_FILE_QUERY_SIZE; // 限制一次录像查询数量
        }
        param.skip = zBaseUtil::to_int(skipval);
        param.type = queryType;
        param.render = strRender;

        zBaseUtil::zMsg msg;
        msg.msg = ZM_QUERYVIDEO;
        msg.pparam = &param;
        zVideoThread::getInstance()->onMsg(msg);
    }
    else if ("file_query_base_on_time" == p2pmsg.msgType)
    {
        zBaseUtil::QueryParamBaseOnTime param;
        std::string bgn_time = p2pmsg.msgParam["bgn_time"];
        std::string end_time = p2pmsg.msgParam["end_time"];
        std::string type = p2pmsg.msgParam["type"];
        std::string max_count = p2pmsg.msgParam["max_count"];

        param.bgn_date = RW::file_date(bgn_time);
        param.end_date = RW::file_date(end_time);
        param.type = type;
        param.max_count = max_count.empty() ? -1 : zBaseUtil::to_int(max_count);
        if (param.max_count < 0 || param.max_count > MAX_FILE_QUERY_SIZE)
        {
            param.max_count = MAX_FILE_QUERY_SIZE; // 限制一次录像查询数量
        }
        param.render = strRender;

        zBaseUtil::zMsg msg;
        msg.msg = ZM_QUERYVIDEO_BASEONTIME;
        msg.pparam = &param;
        zVideoThread::getInstance()->onMsg(msg);
    }
    else if ("video_replay" == p2pmsg.msgType)
    {
        zBaseUtil::zMsg msg;
        msg.msg = ZM_ROKUUP;
        std::string filename = p2pmsg.msgParam["video"];
        if (filename.empty())
        {
            LOG_E(" video_repaly with err file name!");
            return;
        }
        //////////////////////////////////////////////////////////////////////////
        std::string fullPath;
        bool bExist = zVideoThread::getInstance()->findRokuFile(filename, fullPath);
        if (!bExist)
        {
        	sendMessageWebrtc(strRender, "{\"act\":\"video_replay\",\"ret\":\"-1\"}");
            LOG_E("video_replay cannot find file %s \n", filename.c_str());
        }

        if (!(pru->busy()))
        {
            zBaseUtil::zMsg msg2 ;
            std::string strFilename = fullPath;                 // 这里得传指定文件名的、、、、
            if (!strFilename.empty())
            {
            	LOG_I("webrtc");
            	startVideoWebrtc(strRender);
                msg2.msg = ZM_ROKUUP;
                msg2.pparam = &fullPath;
                pru->onMsg(msg2);               // 开始上传
                zVideoThread::getInstance()->onEvent(ZM_ROKUUP);        // 停止直播
                m_video_replay_peer_name = strRender;
                state_changed(changed);
            }
        }
        else
        {
            LOG_E("cannot replay %s  busy! \n", filename.c_str());
            sendMessageWebrtc(strRender, "{\"act\":\"video_replay\",\"ret\":\"-2\"}");
        }
    }
    else if ("stop_video_replay" == p2pmsg.msgType)
    {
        zBaseUtil::zMsg msg;
        msg.msg = ZM_ROKUUPSTOP;
        std::string filename ;
        if (pru->busy())
        {
            filename = pru->filename();
        }

        pru->onMsg(msg); 
        zVideoThread::getInstance()->onEvent(ZM_ROKUUPSTOP);

        //zVideoThread::getInstance()->onEvent(ZM_ONLINE);  // 开始直播（也可以在后期加入参数、、）
        // 阻塞？
        //LOG_D("******************************************** begin stop_video_replay ********************************************\n");
        //notifyP2P(p2pmsg.msgType);
        state_changed(changed);
        //LOG_D("******************************************** end stop_video_replay ********************************************\n");
        stopVideoWebrtc(strRender);
    }
    else if ("start_record" == p2pmsg.msgType)
    {
        zVideoThread::getInstance()->onEvent(ZM_ROKU);
        state_changed(changed);
    }
    else if ("stop_record" == p2pmsg.msgType)
    {
        zVideoThread::getInstance()->onEvent(ZM_ROKUSTOP);
        state_changed(changed);
    }
    else if ("enable_send_debug" == p2pmsg.msgType)
    {
        setDebug(true);
        notify_process_start_info(p2pmsg.msgType);
        state_changed(changed);
    }
    else if ("disable_send_debug" == p2pmsg.msgType)
    {
        setDebug(false);
        state_changed(changed);
    }
    else if ("wind_blow_person" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        m_pEakonThread->setKazeMuki(zEakonThread::k_focuse);
        state_changed(changed);
        #endif
    }
    else if ("wind_avoid_person" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        m_pEakonThread->setKazeMuki(zEakonThread::k_unfocuse);
        state_changed(changed);
        #endif
    }
    else if ("wind_blow_disable" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        m_pEakonThread->setKazeMuki(zEakonThread::k_unknown);
        state_changed(changed);
        #endif
    }
    else if ("infrared_light_auto" == p2pmsg.msgType)
    {
        m_pEakonThread->autoLighting(true);
        state_changed(changed);
    }
    else if ("infrared_light_manually" == p2pmsg.msgType)
    {
        m_pEakonThread->autoLighting(false);
        state_changed(changed);
    }
    else if ("infrared_light_open" == p2pmsg.msgType) 
    {
        m_pEakonThread->chgLighting(true);
        state_changed(changed);
    }
    else if ("infrared_light_close" == p2pmsg.msgType)
    {
        m_pEakonThread->chgLighting(false);
        state_changed(changed);
    }
    else if ("delete_file" == p2pmsg.msgType)
    {
        std::string filename;
        filename = p2pmsg.msgParam["filename"];
        zVideoThread::getInstance()->removeFile(filename);
        
        #if 0 // 测试分辨率切换用
        zVideoThread* vthread = zVideoThread::getInstance();
        if (NULL != vthread)
        {
            int width, height;
            if (0 == vthread->get_encode_resolution(width, height))
            {
                LOG_D("width:%d, height=%d\n", width, height);
                
                zBaseUtil::resolution_t resolution;
                resolution.width = DEFAULT_H264_W;
                resolution.height = DEFAULT_H264_H;

                if (640 == width)
                {
                    resolution.width = 1280;
                    resolution.height = 720;
                }
                else if (1280 == width)
                {
                    resolution.width = 320;
                    resolution.height = 240;
                }
                
                (void)zVideoThread::getInstance()->change_resolution(resolution);
            }
        }
        #endif
    }
    else if ("change_resolution" == p2pmsg.msgType)
    {
        std::string wid_s = p2pmsg.msgParam["width"];
        std::string hei_s = p2pmsg.msgParam["height"];
        zBaseUtil::resolution_t resolution;
        resolution.width = atoi(wid_s.c_str());
        resolution.height = atoi(hei_s.c_str());

        zVideoThread* vthread = zVideoThread::getInstance();
        if (NULL != vthread)
        {
            int state = vthread->getRokuState();
            if ((1 == state) || (2 == state))
            {
                // 录像状态，禁止切换分辨率
                LOG_D("forbiden change resolution. state=%d\n", state);
                notifyP2P_change_resolution(p2pmsg.msgType, state);
            }
            else
            {
                (void)vthread->change_resolution(resolution);
                state_changed(changed);
            }
        }
        else
        {
            LOG_E("vthread NULL. %p\n", vthread);
            state_changed(changed);
        }
    }
    else if ("gesture_control" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        uint32_t uint_opt = (uint32_t)zBaseUtil::to_int(p2pmsg.msgParam["opt"]);
        m_pEakonThread->set_gesture_flag(uint_opt);
        state_changed(changed);
        #endif
    }
    else if ("intelligent_enable" == p2pmsg.msgType)
    {
        #if P2P_CONFIG_ENABLE
        bool opt = zBaseUtil::to_bool(p2pmsg.msgParam["opt"]);
        m_pEakonThread->set_intelligent_enable_flag(opt);
        state_changed(changed);
        #endif
    }
    else if ("state_query" == p2pmsg.msgType)
    {
        if ((0 == p2pmsg.msgParam.size()) ||
        	((1 == p2pmsg.msgParam.size()) && ("all" == p2pmsg.msgParam.begin()->first)))
        {
            // 无参数，发送所有状态。
            state_changed(changed);
        }
        else
        {
            std::vector<std::pair<std::string, std::string>> tuple_KV;
            tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_act], p2pmsg.msgType));

            for(auto i: p2pmsg.msgParam)
            {
                int idx = 0;
                for(auto j: ac_keys)
                {
                    if (i.first == j)
                    {
                        tuple_KV.push_back(std::make_pair(i.first, get_state_impl(idx)));
                        break;
                    }
                    ++idx;
                }
            }

            notify_response_impl(make_json_response(tuple_KV));
        }
    }
    else if ("heat_source_enable" == p2pmsg.msgType)
    {
        bool opt = zBaseUtil::to_bool(p2pmsg.msgParam["opt"]);
        m_pEakonThread->set_heat_source_enable_flag(opt);
        state_changed(changed);
    }

    return;
}

void zP2PThread::OnRenderMSG(std::string strRender, std::string strMsg)
{
    LOG_I("webrtc On OnRenderMSG render:%s cmd: %s\n", strRender.c_str(), strMsg.c_str());
    if (strMsg.empty()) 
    {
        return;
    }

    // process multi commands.
    std::vector<std::string> cmds;
    if (!split_with_delimiter(strMsg, '|', cmds).empty())
    {
        bool changed = false;
        for (auto i : cmds)
        {
            OnRenderMSG_impl(strRender, i, changed);
        }

        if (changed)  // 上报状态
        {
            notifyP2P("state"); 
        }
    }
    return;
}

void zP2PThread::proc()
{
    bool p2p_enable = true;
    if (NULL != m_pEakonThread)
    {
        p2p_enable = m_pEakonThread->get_live_video_state();
        if (m_p2p_enable != p2p_enable)
        {
            m_p2p_enable = p2p_enable;
            notifyP2P("");
        }
    }

    do
    {
    	if(m_server_shutdown_retry) {
    	    LOG_D("zP2PThread::initializeWebrtc befor webrtc");
    	    if (initializeWebrtc(m_deviceName, strSrvAddr, zP2PThread::m_deviceId) != WEBRTC_SUCCESS)
    	    {
    	        LOG_E("Init initializeWebrtc failed. webrtc\n");
    	    }
    	    std::this_thread::sleep_for(std::chrono::milliseconds(20000));
    	    LOG_D("zP2PThread::initializeWebrtc end webrtc\n");
    	}

        std::string szData;
        std::string szRender;
        //unsigned int uParam = 0;
        int uEventNow = WEBRTC_EVENT_NULL;

        if ("" == zP2PThread::m_deviceId)
        {
            LOG_I("zP2PThread::proc: m_guInstID=%s start.\n", (zP2PThread::m_deviceId).c_str());
            break;
        }

        int iErr = getEventWebrtc(uEventNow, szRender, szData, 20);
        if (iErr != WEBRTC_SUCCESS)
        {
            if (iErr != WEBRTC_TIMEOUT)
            {
                LOG_E("pgLiveEvent: iErr=%d\n", iErr);
            }
            break;
        }
        LOG_I("getEventWebrtc, Event: %d, from: %s, data: %s \n", uEventNow, szRender.c_str(), szData.c_str());

        LOG_I("webrtc%d", (uEventNow == WEBRTC_EVENT_MESSAGE));
        if (uEventNow == WEBRTC_EVENT_MESSAGE)
        {
        	LOG_I("webrtc");
            if (p2p_enable)
            {
            	LOG_I("webrtc");
                OnRenderMSG(szRender,szData);
            }
            else
            {
                notifyP2P("");
            }
        }
        else if (uEventNow == WEBRTC_EVENT_RENDER_JOIN)
        {
			std::string render = szRender;
			if (!client_is_logined(render))
			{
                            client_info_t client;
                            client.render = szRender;
                            client.login_time = zBaseUtil::getMilliSecond();
                            m_client_list.push_back(client);
                            LOG_I("RENDER_JOIN: unlogined: szRender=%s, size=%d\n",
                                  szRender.c_str(), m_client_list.size());
			}
			else
			{
                            LOG_I("RENDER_JOIN: logined: szRender=%s, size=%d\n",
                                  szRender.c_str(), m_client_list.size());
			}
            
            if (NULL != zVideoThread::getInstance())
            {
                notifyP2P("state");
            }

            /* add by 刘春龙, 2015-11-07, 原因: */
            if(zVideoThread::getInstance()->getVideoState() != zBaseUtil::zVideoState::UPROKU_VIDEO) {
            	zVideoThread::getInstance()->onEvent(ZM_ONLINE);    // 根据优先级调整策略 如果在上传则无视此消息
            }
        }
        else if (uEventNow == WEBRTC_EVENT_RENDER_LEAVE)
        {
            LOG_I("RENDER_LEAVE: unerased: szRender=%s, size=%d\n",
                  szRender.c_str(), m_client_list.size());
            std::string render = szRender;
            (void)client_erase(render);
            LOG_I("RENDER_LEAVE: erased: szRender=%s, size=%d\n", szRender.c_str(), m_client_list.size());

            stopVideoWebrtc(szRender);
            fileCancelWebrtc(szRender);
            /* add by 刘春龙, 2015-11-07, 原因: 解决多个客户端播放时，关闭其中一个，其他的播放也停止的问题 */
            if (m_client_list.empty())
            {
                 zVideoThread::getInstance()->setVideoState(zBaseUtil::zVideoState::CLOSED_VIDEO);
            }
        }
        else if (uEventNow == WEBRTC_EVENT_SERVER_LOGIN)
        {
        	m_server_shutdown_retry = false;
        }
        else if (uEventNow == WEBRTC_EVENT_SERVER_LOGOUT)
        {
            /* add by 刘春龙, 2015-11-07, 原因: 和start对应, 这样比较好 */
            //20150914 不调用
        }
        else if (uEventNow == WEBRTC_EVENT_SERVER_KICK_OUT)
        {
            //LOG_I("SVR_REPLY: szData=%s\n", szData);
        }
        //////////////////////////////////////////////////////////////////////////
        else if (uEventNow == WEBRTC_EVENT_FILE_PUSH_REQUEST)
        {
            //LOG_I(" PG_LIVE_EVENT_FILE_PUT_REQUEST szData %s \n", szData);
        }
        else if (uEventNow == WEBRTC_EVENT_FILE_PULL_REQUEST)
        {
            if (p2p_enable)
            {
            	LOG_I("webrtc");
                zVideoThread* pThread = zVideoThread::getInstance();
                std::string peerPath = szData;
                zBaseUtil::DownloadParam param;
                param.render = szRender;
                size_t nPos = peerPath.find('=');
                if (nPos != std::string::npos)
                {
                    std::string filename = peerPath.substr(nPos+1);
                	LOG_I("webrtc:%s", filename.c_str());
                    if ("facedis.log" == filename) // 日志
                    {
                    	LOG_I("webrtc:%s", filename.c_str());
                        (void)zLog::upload_logfile(szRender);
                    }
                    else
                    {
                        param.filename = filename;
                        pThread->downloadFile(param);
                    }
                }
            }
            else
            {
            	LOG_I("webrtc");
                notifyP2P("");
            }
        }
        else if (uEventNow == WEBRTC_EVENT_FILE_PROGRESS)
        {
            //LOG_I(" PG_LIVE_EVENT_FILE_PROGRESS szData %s \n", szData);
        }
        else if (uEventNow == WEBRTC_EVENT_FILE_FINISH)
        {
            //LOG_I(" PG_LIVE_EVENT_FILE_FINISH szData %s \n", szData);
        }
        else if (uEventNow == WEBRTC_EVENT_FILE_ABORT)
        {
            //LOG_I(" PG_LIVE_EVENT_FILE_ABORT szData %s \n", szData);
        	fileCancelWebrtc(szRender);
        }
        else if (uEventNow == WEBRTC_EVENT_CONNECT_CLOSED)
        {
        	cleanupWebrtc();
        	zVideoThread::getInstance()->setVideoState(zBaseUtil::zVideoState::CLOSED_VIDEO);
        	m_server_shutdown_retry = true;
        }
    } while (false);

    //LOG_D("zP2PThread::proc: m_guInstID=%d end.\n", m_guInstID);
    std::this_thread::sleep_for(std::chrono::milliseconds(P2P_CMD_GAP));
    return;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.client_is_logined
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月21日
 * 函数功能  : 判断是否已登录
 * 输入参数  : std::string& render  render ID
 * 输出参数  : 无
 * 返 回 值  : bool:   true:已登录， false: 未登录
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zP2PThread::client_is_logined(std::string& render)
{
    std::list<client_info_t>::iterator it = m_client_list.begin();
    while (it != m_client_list.end())
    {
        if (render == it->render)
        {
            return true;
        }
        it++;
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.client_remove
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月21日
 * 函数功能  : 用户登出，移除登录列表
 * 输入参数  : std::string& render  render ID
 * 输出参数  : 无
 * 返 回 值  : bool:  true: 异常成功， false: 失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zP2PThread::client_erase(std::string& render)
{
    std::list<client_info_t>::iterator it = m_client_list.begin();
    while (it != m_client_list.end())
    {
        if (render == it->render)
        {
            m_client_list.erase(it);
            return true;
        }
        it++;
    }
    return false;
}

bool zP2PThread::bDebug()
{
    return m_bDebug;
}

void zP2PThread::setDebug(bool bd)
{
    if (bd)
    {
        // 写日志到日志文件
        zLog::zLog2File_begin();
    }
    else
    {
        zLog::zLog2File_end();
    }
    
    m_bDebug = bd;
}

void zP2PThread::NotifyRokuFileEnd(const std::string& filename)
{
    if (NULL == this) return;

    zBaseUtil::zEakonModel eakonModel = zVideoThread::getInstance()->getEAKonModel();
    std::string strModel = zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel ? "true" : "false";
    //LOG_D("notifyP2P strModel == %s \n", strModel.c_str());

    zBaseUtil::zVideoState videoState = zVideoThread::getInstance()->getVideoState();
    std::string strVideo = zBaseUtil::zVideoState::ONLINE_VIDEO == videoState ? "true" : "false";
    //LOG_D("notifyP2P strVideo == %s \n", strVideo.c_str());

    //LOG_D("file name == %s \n", filename.c_str());

    // add info 
    std::string strDebug = bDebug() ? "true" : "false";

    int nRokuState = zVideoThread::getInstance()->getRokuState();
    std::string strRecord = nRokuState > 0 ? "true" : "false";

    std::string strVer = zVideoThread::getInstance()->version();

    std::string strWind = to_string_muki();

    char strResolution[32] = {0};
    int width, height;
    zVideoThread::getInstance()->get_encode_resolution(width, height);
    (void)sprintf(strResolution, "%dX%d", width, height);

    std::string str = mkP2pMsg("state", 
        strModel, strVideo, filename, strDebug, strRecord, strVer, strWind, strResolution);
    notifyWebrtc(str);
    LOG_D("notifyWebrtc str.c_str() == %s\n", str.c_str());

    return;
}

void zP2PThread::NotifyDebugInfo(const std::string& debugInfo)
{
    //{
    //“act”:”debug”
    //“msg” : ””XXXXXXXX”
    //}
    if (NULL == this || !bDebug()) return;

    std::string str = "{ \"act\" : \"debug\", \"msg\" : \"" + debugInfo + "\"}";
    notifyWebrtc(str);
    LOG_D("notifyWebrtc str.c_str() == %s\n", str.c_str());
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.is_support_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 是否为支持的分辨率
 * 输入参数  : int width   宽度
               int height  高度
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zP2PThread::is_support_resolution(int width, int height)
{
    bool is_support = false;
    for (int i=0; i < RESOLUTION_CNT; i++)
    {
        if (    (width == gResoTab[i].width)
            &&  (height == gResoTab[i].height))
        {
            is_support = true;
            break;
        }
    }
    return is_support;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.video_mode_from_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 分辨率和 p2p模式映射关系
 * 输入参数  : int width   宽度
               int height  高度
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zP2PThread::video_mode_from_resolution(int width, int height)
{
    int mode = -1;
    for (int i=0; i < RESOLUTION_CNT; i++)
    {
        if (    (width == gResoTab[i].width)
            &&  (height == gResoTab[i].height))
        {
            mode = gResoTab[i].mode;
            break;
        }
    }
    return mode;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.video_mode_to_resolution
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 分辨率和 p2p模式映射关系
 * 输入参数  : int mode     模式
               int& width   返回宽度
               int& height  返回高度
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zP2PThread::video_mode_to_resolution(int mode, int& width, int& height)
{
    int ret = -1;
    for (int i=0; i < RESOLUTION_CNT; i++)
    {
        if (mode == gResoTab[i].mode)
        {
            width = gResoTab[i].width;
            height = gResoTab[i].height;
            ret = 0;
            break;
        }
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.set_video_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月10日
 * 函数功能  : 设置视频传输模式
 * 输入参数  : int width   宽度
               int height  高度
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
/*int zP2PThread::set_video_mode(int width, int height)
{
    PG_LIVE_VIDEO_S stVideo = { 0 };
    stVideo.uCode = 3;
    stVideo.uMode = zP2PThread::video_mode_from_resolution(width, height);
    stVideo.uRate = 80;
    stVideo.uBitRate = 512;
    stVideo.uMaxVideoStream = 2;

    LOG_I("width=%d, height=%d, mode=%d\n", width, height, stVideo.uMode);
    int ret = pgLiveVideoParam(zP2PThread::m_guInstID, &stVideo);
    if (0 != ret)
    {
        LOG_E("pgLiveVideoParam failed. ret=%d\n", ret);
    }
    
    return ret;
} */

/*****************************************************************************
 * 函 数 名  : zP2PThread.print_msg
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 打印消息到P2P客户端。
 * 输入参数  : const std::string &msg  消息内容
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zP2PThread::print_msg(const std::string &msg)
{
    zP2PThread * p2p_thread = zP2PThread::getInstance();
    if ((NULL != p2p_thread) && p2p_thread->bDebug())
    {
        p2p_thread->NotifyDebugInfo(msg);
    }
}

/*****************************************************************************
 * 函 数 名  : zP2PThread.get_client_count
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 获取P2P客户端数目
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zP2PThread::get_client_count()
{
    if (NULL == this) 
    {
        return 0;
    }

    return m_client_list.size();
}

void zP2PThread::notify_process_start_info(const std::string& strAct)
{
    std::string app_count = zBaseUtil::to_string( ProcessStartCount::get_instance()->count());

    std::int64_t sys_time = zBaseUtil::getMicroSecond();
    std::int64_t app_time = sys_time - ProcessStartCount::get_instance()->begin_time();
    std::string app_duration = zBaseUtil::to_string(app_time/1000000);
    std::string sys_duration = zBaseUtil::to_string(sys_time/1000000);

    std::vector<std::pair<std::string, std::string>> tuple_KV;
    tuple_KV.push_back(std::make_pair(ac_keys[AcKeysIdx::idx_act], strAct));
    tuple_KV.push_back(std::make_pair("app_count", app_count));
    tuple_KV.push_back(std::make_pair("app_duration", app_duration));
    tuple_KV.push_back(std::make_pair("sys_duration", sys_duration));

    std::string response = std::move(make_json_response(tuple_KV));
    notifyWebrtc(response);
    LOG_D("notifyWebrtc response.c_str() == %s\n", response.c_str());
    return;
}

/*视频回放完成时需要关闭*/
void zP2PThread::stop_peer_video_replay() {
    stopVideoAndNotifyPeerWebrtc(m_video_replay_peer_name);
}
