#include "zImage.h"
#include "zCommonDef.h"
#include "zFile.h"
#include "zLog.h"
#include <unistd.h>
#include <fcntl.h>

//#include "stb_image.h"
//#include "stb_image_write.h"

#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#endif

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#endif


//#define STB_DEFINE
//#include "stb.h"
#ifndef PNGSUITE_PRIMARY
#define PNGSUITE_PRIMARY
#endif
namespace zImageUtil
{
    zImage::zImage()
        : data_(NULL)
        , dataLen_(0)
        , width_(0)
        , height_(0)
        , unpack_(false)
        , fileType_(Format::UNKNOWN)
    {
    }

    zImage::~zImage()
    {
        ZSAFE_FREE(data_);
    }

    bool zImage::isPng(const unsigned char* data, size_t dataLen)
    {
        if (dataLen <= 8) return false;
        static const unsigned char PNG_SIGNATURE[] = { 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a };
        return 0 == memcmp(PNG_SIGNATURE, data, sizeof(PNG_SIGNATURE));
    }

    bool zImage::isJpg(const unsigned char* data, size_t dataLen)
    {
        if (dataLen <= 4) return false;
        static const  unsigned char JPG_SOI[] = { 0xFF, 0xD8 };
        
        return (0 == memcmp(JPG_SOI, data, 2));
    }

    zImage::Format zImage::detectFormat(const unsigned char* data, size_t dataLen)
    {
        if (isPng(data, dataLen)) return Format::PNG;
        else if (isJpg(data, dataLen)) return Format::JPG;
        else
        {
            //LOG_D("cannot detect image format! \n");
            return Format::UNKNOWN;
        }
    }

    bool zImage::initWithFile(const std::string& fullPath)
    {
        int width;
        int height;
        int comp;
        int req_comp = 0;
        
        unsigned char* pBuffer = stbi_load(fullPath.c_str(), &width, &height, &comp, req_comp);
        if (NULL != pBuffer) 
        {
            initWithRawData(pBuffer, width * height * comp, width, height, comp, false);
        }
        
        free(pBuffer);
        pBuffer = NULL;
        return true;
    }

    bool zImage::initWithMem(const unsigned char* data, size_t dataLen)
    {
        int width;
        int height;
        int comp;
        int req_comp = 0;
        
        unsigned char* pBuffer = stbi_load_from_memory(data, dataLen, &width, &height, &comp, req_comp);
        if (NULL != pBuffer) 
        {
            initWithRawData(pBuffer, width * height * comp, width, height, comp, false);
        }
        free(pBuffer);
        pBuffer = NULL;

        return true;
    }

    bool zImage::initWithNV12Data(const unsigned char* data, size_t dataLen, int width, int height, int bitsPerComponent, bool preMulti)
    {
        int frameSize = width * height;

        int bytesPerComponent = 4;
        width_ = width;
        height_ = height;
        dataLen_ = frameSize * bytesPerComponent;
        data_ = static_cast<unsigned char*>(malloc(dataLen_ * sizeof(unsigned char)));
        if (!dataLen_)
        {
            LOG_E(" initWithNV12Data malloc(%d) failed! \n", dataLen_ * sizeof(unsigned char));
            return false;
        }
        int ii = 0;
        int ij = 0;
        int di = +1;
        int dj = +1;

        int a = 0;
        for (int i = 0, ci = ii; i < height; ++i, ci += di)
        {
            for (int j = 0, cj = ij; j < width; ++j, cj += dj)
            {
                int y = (0xff & ((int)data[ci * width + cj]));
                int v = (0xff & ((int)data[frameSize + (ci >> 1) * width + (cj & ~1) + 0]));
                int u = (0xff & ((int)data[frameSize + (ci >> 1) * width + (cj & ~1) + 1]));
                y = y < 16 ? 16 : y;

                int r = (int)(1.164f * (y - 16) + 1.596f * (v - 128));
                int g = (int)(1.164f * (y - 16) - 0.813f * (v - 128) - 0.391f * (u - 128));
                int b = (int)(1.164f * (y - 16) + 2.018f * (u - 128));

                r = r < 0 ? 0 : (r > 255 ? 255 : r);
                g = g < 0 ? 0 : (g > 255 ? 255 : g);
                b = b < 0 ? 0 : (b > 255 ? 255 : b);

                //data_[a] = (0xff000000 | (r << 16) | (g << 8) | b);
                data_[a++] = b & 0xff;
                data_[a++] = g & 0xff;
                data_[a++] = r & 0xff;
                data_[a++] = 0xff;
            }
        }
        return true;
    }


    bool zImage::initWithRawData(const unsigned char* data, size_t dataLen
        , int width, int height, int bitsPerComponent, bool preMulti)
    {
        bool result = false;
        do 
        {
            if ( 0 <= width || 0 <= height) break;

            height_ = height;
            width_ = width;
            comp_ = 4;
            //hasPremultipliedAlpha_ = preMulti;

            int bytesPerComponent = 4;
            dataLen_ = height * width * bytesPerComponent;
            data_ = static_cast<unsigned char*>(malloc(dataLen_ * sizeof(unsigned char)));
            if (NULL == data_) break;
            memcpy(data_, data, dataLen_);

            result = true;
        } while (false);

        return result;
    }

    bool zImage::saveImageToBMP(const std::string& filePath)
    {
        int result = stbi_write_bmp(filePath.c_str(), width_, height_, comp_, data_);
        return result;
    }

    bool zImage::saveImageToPNG(const std::string& filePath, bool is2RGB /* = true */)
    {
        int result = stbi_write_png(filePath.c_str(), width_, height_, 4, data_, width_ * 4);
        return result;
    }

    bool zImage::save2File(const std::string& filename, bool is2RGB /* = true */)
    {
        bool result = false;

        do 
        {
            if (filename.size() <= 4) break; 
            std::string strLowerCasePath(filename);
            for (unsigned int i = 0; i < strLowerCasePath.length(); ++i)
            {
                strLowerCasePath[i] = tolower(filename[i]);
            }

            if (std::string::npos != strLowerCasePath.find(".png"))
            {
                if (!saveImageToPNG(filename, is2RGB)) break;
            }
            else if (std::string::npos != strLowerCasePath.find(".bmp"))
            {
                if (!saveImageToBMP(filename)) break;
            }
            else
            {
                break;
            }

            result = true;
        } while (false);

        return result;
    }

    bool zImage::yuv_to_jpg_file(const std::string& output,
        unsigned char* yuv_buffer, int yuv_size, int width, int height, int subsample)
    {
        int padding = 1; // 1或4均可，但不能是0
        int need_size = tjBufSizeYUV2(width, padding, height, subsample);
        if (need_size != yuv_size)
        {
            LOG_E("yuv_to_jpg_file: "
                "we detect yuv size: %d, but you give: %d, check again.\n", need_size, yuv_size);
            return false;
        }

        tjhandle handle = tjInitCompress();
        if (NULL == handle)
        {
            LOG_E("tjInitCompress failed.\n");
            return false;
        }

        unsigned char * jpeg_buffer = NULL;
        unsigned long jpeg_size = 0;
        int flags = 0;
        int ret = tjCompressFromYUV(handle, yuv_buffer, width, padding, height, subsample, 
            &jpeg_buffer, &jpeg_size, 88, flags);
        if (ret < 0)
        {
            LOG_E("yuv_to_jpg_file: compress to jpeg failed: %s\n", tjGetErrorStr());

            tjDestroy(handle);
            return false;
        }

        int f = open(output.c_str(),  O_WRONLY | O_CREAT | O_TRUNC);
        if (-1 != f)
        {
            write(f, jpeg_buffer, jpeg_size);
            fsync(f);
            close(f);
            //LOG_D("yuv_to_jpg_file: filename=%s, jpeg_size=%d\n", output.c_str(), jpeg_size);
        }
        else
        {
            LOG_E("yuv_to_jpg_file: open file failed! filename=%s\n", output.c_str());

            tjFree(jpeg_buffer);
            tjDestroy(handle);
            return false;
        }

        tjFree(jpeg_buffer);
        tjDestroy(handle);
        return true;
    }

    /*****************************************************************************
     * 函 数 名  : zImageUtil.init_jpg_handle
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年4月4日
     * 函数功能  : 初始化jpg压缩上下文
     * 输入参数  : 无
     * 输出参数  : 无
     * 返 回 值  : tjhandle
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    tjhandle zImage::init_jpg_handle()
    {
        return tjInitCompress();
    }

    /*****************************************************************************
     * 函 数 名  : zImageUtil.zImage.yuv_to_jpg_buffer
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年4月4日
     * 函数功能  : yuv图像 转 jpg图像到内存
     * 输入参数  : tjhandle handle              jpg句柄
                   unsigned char* yuv_buffer    yuv内存
                   int yuv_size                 yuv大小
                   int width                    宽
                   int height                   高
                   int subsample                采样值
                   unsigned char **jpeg_buffer  返回jpg内存
                   unsigned long &jpeg_size     jpg大小
     * 输出参数  : 无
     * 返 回 值  : bool
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    bool zImage::yuv_to_jpg_buffer(tjhandle handle, 
        unsigned char* yuv_buffer, int yuv_size, int width, int height, int subsample,
        unsigned char **pp_jpeg_buffer, unsigned long *p_jpeg_size)
    {
        if (NULL == handle)
        {
            LOG_E("invalid parameter.\n");
            return false;
        }

        int padding = 1; // 1或4均可，但不能是0
        int need_size = tjBufSizeYUV2(width, padding, height, subsample);
        if (need_size != yuv_size)
        {
            LOG_E("yuv_to_jpg_file: "
                "we detect yuv size: %d, but you give: %d, check again.\n", need_size, yuv_size);
            return false;
        }

        int flags = 0;
        int ret = tjCompressFromYUV(handle, yuv_buffer, width, padding, height, subsample, 
            pp_jpeg_buffer, p_jpeg_size, 88, flags);
        if (ret < 0)
        {
            LOG_E("yuv_to_jpg_file: compress to jpeg failed: %s\n", tjGetErrorStr());
            return false;
        }

        return true;
    }

    /*****************************************************************************
     * 函 数 名  : zImageUtil.jpg_buffer_free
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年4月5日
     * 函数功能  : 释放jpg图像内存
     * 输入参数  : unsigned char **pp_jpeg_buffer  jpg图像内存
     * 输出参数  : 无
     * 返 回 值  : void
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    void zImage::jpg_buffer_free(unsigned char **pp_jpeg_buffer)
    {
        if (NULL != pp_jpeg_buffer)
        {
            tjFree(*pp_jpeg_buffer);
            *pp_jpeg_buffer = NULL;
        }
        return;
    }
    
    /*****************************************************************************
     * 函 数 名  : zImageUtil.destroy_jpg_handle
     * 负 责 人  : 刘春龙
     * 创建日期  : 2016年4月4日
     * 函数功能  : 关闭jpg句柄
     * 输入参数  : tjhandle handle  jpg句柄
     * 输出参数  : 无
     * 返 回 值  : void
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    void zImage::destroy_jpg_handle(tjhandle handle)
    {
        (void)tjDestroy(handle);
    }
}
