#ifndef __ZEAKONTHREAD_H__
#define __ZEAKONTHREAD_H__

#include <string>
#include <cstdint>
#include <vector>
#include <chrono>
#include <atomic>

#include "zThread.h"
#include "I2CTag.h"
#include "zUtil.h"
#include "zTimer.h"

#define GESTURE_DISABLE     0   // 禁止手势功能
#define GESTURE_AC_SWITCH   1   // 通过手势，开关空调
#define GESTURE_AC_WIND     2   // 通过手势，控制吹风方向

#define SMART_WIND_CONFIG_SIZE 8 // 智能风配置数组大小

typedef struct tagAirConditionerMsg
{
    std::uint32_t msg;
    std::int64_t time;
    zI2C::toAirConditioner pBuf;
    tagAirConditionerMsg()
        : msg(ZM_UNKNOWN)
    {
        memset(pBuf.buf, 0, sizeof(zI2C::toAirConditioner));
        time = zBaseUtil::getMilliSecond();
    }
}zAirConditionerMsg;

class zMsgQueue
{
public:
    zMsgQueue();
    ~zMsgQueue();

    int addMsg(const zAirConditionerMsg& msg);
    int getMsg(zAirConditionerMsg& msg);
    int clearMsg();
private:
    std::mutex m_msgMutex;
    std::list<zAirConditionerMsg> m_msgList;
};

class zAlgManager;
class zP2PThread;
class zWifiStateThread;
class zEakonThread : public zThread
{
public:
    enum wifiState
    {
        connect_unknown = 0,
        connect_none,
        connect_router ,//连上路由器，但是外网不通
        connect_internet,
    };
    enum kazeMuki
    {
        k_unknown = 0,      // 智能风关闭
        k_focuse = 0x10,    // 风吹人
        k_unfocuse = 0x20,  // 风避人
    };
public:
    static int getLastErr();
    static void setLastErr(int nErr);

    zEakonThread(zAlgManager* pParent, int width = 640, int height = 480,std::string strDevName = "/dev/mcu_dev");
    ~zEakonThread();

    void Start();
    void proc();
    //void wifiTest();

    //const ZDEVICE getDev();

    void closeDevice();
    int writeBuf2Device(const char* szBuf, int nLen);
    int readDevice(void* szBuf, size_t nLen);

    int getKazeParama(int& left, int& right);
    bool bFocus();
    //////////////////////////////////////////////////////////////////////////
    int sendMsg(zAirConditionerMsg& msg);

    int closeLightDelay(bool update_flag = false);
    int whiteLightDelay();
    int blueLightDelay();
    int blueWhiteLightGlintDelay();
    int blue_light_glint();
    int fault_light_glint();
    int send_light_state();
    int recoveryLightV2();
    
    int doUpdate();
    kazeMuki getKazeMuki();
    kazeMuki setKazeMuki(kazeMuki muki);
    void set_home_leave_mode(bool home);

    int getGestureFlag();
    int CloseAirConditioner();
    int OpenAirConditioner();
    int getDevid();
    int getSsid();
    int TemaneCtrl(bool bFocus);    // get or lose
    int TemaneCtrl2(bool bFocus);
    int auto_swipe_wind();
    int KazemukiCtrl_v2(int* arr, kazeMuki muki, bool gesture_flag);
    int KazemukiCtrl(int* arr, bool gesture_flag);
    int temperature_increase();
    int temperature_decrease();
    int ZoneStayTimeup(ZONE_POS zone);

    int send_person_flag(bool have_person);
    void write_person_flag(unsigned char *pbuf_config);

    int TaninnFind();               //しんにゅう けいこく sinnyuukeikoku    
    int TaninnLose();               

    void get_prev_smart_wind_config(
        int buf_config[SMART_WIND_CONFIG_SIZE], zEakonThread::kazeMuki &muki, bool& gesture_flag);
    void write_smart_wind_config(unsigned char *pbuf_config);
    void save_prev_smart_wind_config(const int* config_arr, zEakonThread::kazeMuki muki, bool gesture_flag);
    void reset_prev_smart_wind_config();

    int preLightState();
    int LightState();
    int WifiState(unsigned char state);
    int onEvent(int nEvent);

    void get_lighting_mode(bool &auto_mode, bool &day);
    bool autoLighting(bool bAuto);
    void chgLighting(bool bOpen);
    //////////////////////////////////////////////////////////////////////////

    void GetWifiInfo(std::string& ssid, std::string& passwd);
    int get_quick_check_mode();
    bool get_device_test_mode();
    void set_router_state(int is_connect);
    bool get_live_video_state();
    bool get_itelligent_switch();
    std::string GetDevID();

    bool get_eakon_opened();
    void set_gesture_flag(uint32_t flag);
    uint32_t get_gesture_flag();
    uint32_t get_gesture_state();
    void set_intelligent_enable_flag(bool flag);
    bool get_intelligent_enable_flag();

    void set_heat_source_enable_flag(bool flag);
    bool get_heat_source_enable_flag();
    
    int notify_intelligent_state();

private:
    ZDEVICE openDevice(std::string name);
    int taime();
    int analyzeFrom(zI2C::fromAirConditioner& buf);
    int tryWifi(std::string strSsid, std::string strPswd);
    int startP2P( );

    int getLightColor();

    int send_running_state();
    int send_state_quick_mode(int err);
    int set_light_quick_mode(int err);
    int get_last_error_quick_mode();

    int check_mcu(zI2C::fromAirConditioner& buf);
    bool is_reset_mcu_done();
    int reset_mcu();
    void print_buffer(const char *header, const char *buffer, int length);
    void limit_angle(int& angle);
    bool device_test_use_gateway();
    char get_intelligent_state();
    
	void update_mcu_version(uint8_t version);
	bool is_new_mcu_version();
	bool check_airconditioner_info(const zI2C::fromAirConditioner& buf);
	bool check_ac_config_info(const zI2C::fromAirConditioner& buf);

private:
    zAlgManager* m_pManager;

    static std::mutex errMutex_;
    static int globalErr_;
    std::mutex m_ctrlMutex;
    bool m_bUseP2p;
    volatile kazeMuki m_k;
    unsigned char m_ucWifiType;     //0c1u2n
    unsigned char m_ucWifiDetype;   //0o1e2a
    int m_nLeftBorder;
    int m_nRightBorder;
    int m_needRead;                 // time of N * 100ms
    int m_curLightState;                // 3w2b10c
    int m_preLightState;
    //int m_preErr;
    int m_nUpdateGap;               // day
    int m_nGlintColor;

    std::int64_t m_latestProc;
    std::string m_devName;
    std::mutex m_devMutex;
    ZDEVICE m_eakonDev;
    std::string m_strDevID;
    std::mutex m_ssidMutex;
    std::string m_strSSID;
    std::string m_strPasswd;

    std::string m_ssid_normal_mode;     // 正常模式下的wifi的用户名
    std::string m_passwd_normal_mode;   // 正常模式下的wifi的密码

    std::string m_ssid_quick_mode;      // 快检模式wifi的用户名
    std::string m_passwd_quick_mode;    // 快检模式wifi的密码

    zMsgQueue m_msgQueue; //lint !e1516    
    zP2PThread* m_p2pThread;
    wifiState m_nWifiState;     // 0n1i2r

    zBaseUtil::zTimer m_updateTimer;

    bool m_bSimulateMCB;
    bool m_bSimulateDeviceId = false;

    volatile int m_quick_check_mode;    // 快检模式
    volatile int m_connect_router_state; // 路由连接状态
    int m_last_error;           // 快检模式下的故障标识
    bool m_taninn_find_flag;    // 人员入侵标志 
    std::uint64_t m_proc_count; // 线程循环处理的次数

    // 处理闪灯
    std::uint64_t m_light_start_time; // 起始亮灯时间
    int m_fault_flag;           // 正常模式下的故障标志

    int m_mcu_read_error_cnt;   // 读取MCU设备连续故障次数
    int m_mcu_fault_cnt;        // 连续检测MCU故障次数
    std::atomic_uint m_gesture_flag; // 手势功能标志

    volatile bool m_itelligent_switch_enable; // 智能总开关

    /***** 配置数据 begin************************************************/
    std::mutex m_smart_wind_mutex; // 用于锁定上次的智能风配置
    int m_wind_arr[SMART_WIND_CONFIG_SIZE]; // 智能风
    zEakonThread::kazeMuki m_last_mk; // m_k
    bool m_last_gesture_flag; // 手势定向风标志
    /***** 配置数据 end  ************************************************/

    volatile bool m_home_leave_mode; // true:离家模式  false: 在家模式 
    bool m_live_video_switch; // true:实时视频使能  false:实时视频禁用
    std::int64_t m_last_reset_mcu_time; // 上次复位MCU 的时间

    volatile int m_limit_angle_left;     // 吹风角度左边界
    volatile int m_limit_angle_right;    // 吹风角度右边界

    volatile bool m_prev_have_person_flag; // true: 有人，false: 无人

    std::int64_t m_wifi_query_time; // 查询wifi信息时间
    int m_wifi_query_gap;           // 查询间隔
    bool m_wifi_query_first_flag;   // 第一次查询标志 
    bool m_update_wifi_done_flag;   // 更新wifi用户名和密码是否完成标志

    bool m_device_test_mode_before_acquire_wifi; // 工装(进货检) 模式, 在没有获取到wifi信息的情况下
    bool m_device_test_mode;                 // 工装(进货检) 模式
    std::string m_ssid_device_test_mode;    // 工装(进货检) 模式时，wifi的用户名
    std::string m_passwd_device_test_mode;  // 工装(进货检) 模式时，wifi的密码
    bool m_need_wakeup_device_test_flag;

    volatile bool m_wifi_enable_flag;  // wifi开关标志
    std::int64_t m_wifi_connect_time; // 执行wifi_connect后的时间 

    std::string m_simulate_wifi_ssid;  // 模拟时的WIFI SSID 
    std::string m_simulate_wifi_passwd;// 模拟时的WIFI 密码

    std::atomic_bool m_eakon_opened; // true: 空调已开机 false：空调已关机
    std::atomic_bool m_intelligent_enable; // true: 智能开， false: 智能关

    std::atomic_bool m_heat_source_enable; // true: 热源告警开， false: 热源告警关
    static constexpr int heat_source_count = 3; // 热源数量
    char m_temperatures[heat_source_count] {0, 0, 0};
    char m_angles[heat_source_count] {0, 0, 0};
    bool m_heat_source_debug = true; // true: 开启热源调试开关

    bool m_wifi_ssid_change_flag = false; // wifi用户名或密码变更标志

	bool m_state_ac_opened = true; // 从主控解析的空调状态值: true: 空调开, false: 空调关
	uint32_t m_state_ac_coninue_count = 0; // 从主控解析的相同的空调状态值，持续次数

	int32_t m_mcu_version_count = 0; // 统计MCU版本 
	bool m_is_new_mcu_version = true; // true: 新版本, false: 
};
#endif

