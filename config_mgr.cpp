#include <stdio.h>
#include <vector>
#include <errno.h>

#include "config_mgr.h"
#include "zLog.h"

#define MAX_LINE (256)
namespace zConfigManager
{
    std::string& trim(std::string &str)
    {
        if (str.empty())
        {
            return str;
        }

        str.erase(0, str.find_first_not_of(" "));
        str.erase(str.find_last_not_of(" ") + 1);
        return str;
    }

    zTxtConfigMgr::zTxtConfigMgr(std::string file, std::string yarikada)
        : m_fileName(file)
    {
        char line[MAX_LINE] = { 0 };
        char key[MAX_LINE] = { 0 };
        char val[MAX_LINE] = { 0 };
        std::string strKey;
        std::string strVal;
        FILE* pFile = fopen(file.c_str(), yarikada.c_str());
        if (NULL != pFile)
        {
            while (NULL != fgets(line, MAX_LINE, pFile))
            {
                int nResult = sscanf(line, "%255s%*s%255s", key, val);
                if (2 != nResult)
                {
                    // try "key=val
                    nResult = sscanf(line, "%255[^=]=%255s", key, val);
                }
                if (2 != nResult)
                {
                    // try "key =val
                    nResult = sscanf(line, "%255s%*[^=]=%255s", key, val);
                }
                if (2 != nResult) continue;
                strKey = key;
                strVal = val;
                m_valMap[strKey] = strVal;
            }
            fclose(pFile);
        }
        else
        {
            LOG_E("fopen failed for %s", strerror(errno));
        }
    }

    zTxtConfigMgr::~zTxtConfigMgr()
    {
        // sava file ?
    }

    bool zTxtConfigMgr::HasKey(std::string key)
    {
        std::map<std::string, std::string>::iterator it = m_valMap.find(key);
        return it != m_valMap.end();
    }

    std::string zTxtConfigMgr::getValue(const std::string &key)
    {
        std::string strResult;
        if (HasKey(key)) strResult = m_valMap[key];
        return strResult;
    }

    int zTxtConfigMgr::setValue(std::string key, std::string val)
    {
        int nResult = 0;
        if (HasKey(key)) nResult = 1;
        m_valMap[key] = val;
        return nResult;
    }

    bool zTxtConfigMgr::SaveConfig(std::string file /* = std::string() */)
    {
        bool bReturn = false;
        if (file.empty())
        {
            file = m_fileName;
        }
        FILE* pFile = fopen(file.c_str(), "w+");
        if (NULL != pFile)
        {
            std::map<std::string, std::string>::iterator it = m_valMap.begin();
            std::string strLine;
            while (it != m_valMap.end())
            {
                strLine = it->first + " = " + it->second + "\r\r\n";
                LOG_D(strLine.c_str());
                //fwrite(strLine.c_str(), strLine.length(), MAX_LINE, pFile);
                fputs(strLine.c_str(), pFile);
                //fputs("\\n", pFile);
                ++it;
            }
            fclose(pFile);
            bReturn = true;
        }
        return bReturn;
    }
}