
#ifdef __cplusplus
extern "C" {
#endif
#include <3rdParty/ffmpeg/libavutil/timestamp.h>
#include <3rdParty/ffmpeg/libavformat/avformat.h>
#ifdef __cplusplus
}
#endif
#include "3rdParty/V4L2ImageCap/jni/cap_types.h"
#include "3rdParty/V4L2ImageCap/jni/capture.h"
#include "zRokuUpThread.h"
#include "p2pThread.h"
#include "zVideoThread.h"
#include "zLog.h"

//////////////////////////////////////////////////////////////////////////

#define DEFAULT_FRAME_GAP (83)
zRokuUpThread* zRokuUpThread::instance_ = NULL;

void h264_mp4toannexb(AVPacket* pkt)
{
    uint8_t* cur = pkt->data;
    while (1)
    {
        if (pkt->data + pkt->size < cur + 4)
        {
            break;
        }
        int len = cur[0];
        len <<= 8;
        len += cur[1];
        len <<= 8;
        len += cur[2];
        len <<= 8;
        len += cur[3];

        cur[0] = cur[1] = cur[2] = 0;
        cur[3] = 1;
        cur += 4 + len;
    }
}
//////////////////////////////////////////////////////////////////////////

zRokuUpThread* zRokuUpThread::getInstance()
{
    if (NULL == instance_)
    {
        instance_ = new zRokuUpThread();
    }
    return instance_;
}

void zRokuUpThread::destroyInstance()
{
    if (NULL != instance_)
    {
        delete instance_;
        instance_ = NULL;
    }

    return;
}

zRokuUpThread::zRokuUpThread()
    : needUp_(false)
    , ifmt_ctx_(NULL)
{
    LOG_I("line=%d\n", __LINE__);
}

zRokuUpThread::~zRokuUpThread()
{
    LOG_I("line=%d\n", __LINE__);
}

void zRokuUpThread::initReader(std::string file)
{
    std::string filename = file;
    LOG_D("zRokuUpThread::initReader file name == %s \n", filename.c_str());
    int ret;
    av_register_all();
    if ((ret = avformat_open_input(&ifmt_ctx_, filename.c_str(), 0, 0)) < 0)
    {
        if (NULL != ifmt_ctx_) 
        {
            avformat_close_input(&ifmt_ctx_);
        }
        ifmt_ctx_ = NULL;
        LOG_E("Could not open input file %s", filename.c_str());
        return;
    }

    if ((ret = avformat_find_stream_info(ifmt_ctx_, 0)) < 0)
    {
        if (NULL != ifmt_ctx_) 
        {
            avformat_close_input(&ifmt_ctx_);
        }
        ifmt_ctx_ = NULL;
        LOG_E("Failed to retrieve input stream information \n");
        return;
    }

    av_dump_format(ifmt_ctx_, 0, filename.c_str(), 0);
    LOG_D("zRokuUpThread::initReader end \n");
}

void zRokuUpThread::uninitReader()
{
    LOG_D("zRokuUpThread::uninitReader begin \n");
    if (NULL != ifmt_ctx_) avformat_close_input(&ifmt_ctx_);
    ifmt_ctx_ = NULL;
    LOG_D("zRokuUpThread::uninitReader end \n");
}

void zRokuUpThread::dealFrame(const AVPacket& pkt_)
{
    LOG_D("zRokuUpThread::dealFrame %d \n", pkt_.size);
    if ("" != zP2PThread::m_deviceId ||
    	zBaseUtil::zVideoState::UPROKU_VIDEO == zVideoThread::getInstance()->getVideoState())
    {
        //LOG_D("sendFrameToWebrtc buf.BytesUsed == %u \n", pkt_.size);
        bool IsKeyFrame = false;
        if (pkt_.flags == AV_PKT_FLAG_KEY)
        {
        	IsKeyFrame = true;
        }
        zVideoThread* vthread = zVideoThread::getInstance();
        int width = vthread->m_nWidth;
        int height = vthread->m_nHeight;
        //LOG_D("sendFrameToWebrtc m_nVideoState:%d \n", zVideoThread::getInstance()->getVideoState());
        sendFrameToWebrtc((unsigned char*)pkt_.data, pkt_.size, width, height, IsKeyFrame);
    }
}

void zRokuUpThread::init(std::string filename, std::uint64_t nGap /* = 83 */)
{
    LOG_D("zRokuUpThread::init begin\n");
    if (busy() || filename.empty()) return;
    setThreadGap(nGap);
    setFilename(filename);
    initReader(filename);
    needUp_ = true;
    LOG_D("zRokuUpThread::init end \n");
}

void zRokuUpThread::unInit()
{
    upFilename_ = "";
    needUp_ = false;
    uninitReader();
}
void zRokuUpThread::setFilename(std::string filename)
{
    LOG_D("zRokuUpThread::setFilename %s \n", filename.c_str());
    upFilename_ = filename;
}

bool zRokuUpThread::proc()
{
    std::unique_lock<std::mutex> lock(ctrlMutex_);
    // todo up
    //////////////////////////////////////////////////////////////////////////
    do 
    {
        if (NULL == ifmt_ctx_){
            LOG_D("NULL == ifmt_ctx_ \n");
            onFileEnd();
            break;
        }

        int ret = av_read_frame(ifmt_ctx_, &pkt_);
        if (ret < 0)                                //出错或读完 都结束上传
        {
            LOG_I("av_read_frame == %d \n", ret);
            onFileEnd();
            break;
        }

        AVStream *in_stream;
        in_stream = ifmt_ctx_->streams[pkt_.stream_index];

        //log_packet(ifmt_ctx, &pkt, "in");

        if (in_stream->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            h264_mp4toannexb(&pkt_);
            //fwrite(pkt_.data, 1, pkt_.size, fp_video_);
            // up to
            dealFrame(pkt_);
        }
        else
        {
            //fwrite(pkt_.data, 1, pkt_.size, fp_audio);
            LOG_D("in_stream->codec->codec_type != AVMEDIA_TYPE_VIDEO \n");
        }

        av_free_packet(&pkt_);
    } while (false);

    //////////////////////////////////////////////////////////////////////////
    return needUp_;
}

int zRokuUpThread::onMsg(const zBaseUtil::zMsg& msg)
{
    LOG_I("zRokuUpThread::onMsg == %d \n", msg.msg);
    std::unique_lock<std::mutex> lock(ctrlMutex_);
    if (ZM_ROKUUP == msg.msg)
    {
        //
        if (!busy())
        {
            std::string filename;
            if (NULL != msg.pparam)
            {
                filename = *((std::string*)msg.pparam);
            }
            if (filename.empty())
            {
                LOG_E("empty upload file name do nothing \n");
                return -1;
            }
            int nGap = msg.nparam;

            if (nGap < 0) nGap = DEFAULT_FRAME_GAP;
            init(filename, nGap);

            startThread( );
        }
    }
    else if (ZM_ROKUUPSTOP == msg.msg)
    {
        unInit();
    }
    return 0;
}

std::string zRokuUpThread::filename()
{
    LOG_D("zRokuUpThread::filename == %s \n", upFilename_.c_str());

    int nPos = upFilename_.rfind('/');
    int nLen = upFilename_.length() - nPos;
    std::string fileName = upFilename_.substr(nPos + 1, nLen);
    LOG_D("zRokuUpThread::filename == %s \n", fileName.c_str());
    return fileName;
}

void zRokuUpThread::onFileEnd()
{
    unInit();
    
    if (NULL != zVideoThread::getInstance())
    {
        zVideoThread::getInstance()->setVideoState(zBaseUtil::zVideoState::CLOSED_VIDEO);
    }

    if (NULL != zP2PThread::getInstance())
    {
        zP2PThread::getInstance()->NotifyRokuFileEnd(filename());
        zP2PThread::getInstance()->stop_peer_video_replay();
    }
}

