#ifndef __ZFILE_H__
#define __ZFILE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstdint>
#include <list>

namespace zFileUtil
{
    std::string getTimeString();

    class zFileManager
    {
    public:
        static zFileManager* getInstance();
        static void destroyInstance();

        void setDefaultRootPath(const std::string& path);

        void normalizePath(std::string& str);
        void normalizeDir(std::string& str);
        bool isFileExist(const std::string& filename);
        bool isAbsolutePath(const std::string& path) const ;
        //bool isDirectory(const std::string& path);
        bool isDirectoryExist(const std::string& dirPath) const;
        bool createDirectory(const std::string& dirPath);
        bool removeDirectory(const std::string& dirPath);
        bool appendDirectory(std::string& dirPath, const std::string& subDir);
        bool removeFile(const std::string& filePath);
        bool renameFile(const std::string& path, const std::string& oldName, const std::string& name);
        long getFileSize(const std::string& filePath);
        int getDirectorySize(const std::string& dir);

        std::string getWritablePath();
        void setWritablePath();

        //void addSearchPath(const std::string& path, const bool front = false);
        //const std::vector<std::string>& getSearchPaths() const;

        std::uint64_t getDiskFreeSize(const std::string& path, std::uint64_t *total_size = NULL);

        std::string fullPathForFilename(const std::string &filename) const;
        std::string getNewFilename(const std::string& rootDir = "", std::string prefix = "0", std::string suffix = "" );
        std::string getNewFilenameByTime(const std::string& suffix = "");

        int EnumDirFiles(const std::string& rootPath, std::list<std::string>& ls, std::string suffix = "", int limit_size = 0);
        int EnumDirFilesWithFullPath(const std::string& rootPath, std::list<std::string>& ls, std::string suffix = "", bool brev = true);
        std::string path2Dir(const std::string& path);
        std::string dir2Path(const std::string& dir);

        void timeSync();
        bool get_time_sync();
        void init_sync_time();

    private:
        zFileManager();
        ~zFileManager();

    private:
        volatile bool bSyncTime_;
        std::string rootPath_;
        static zFileManager* instance_;
    };

}

#endif