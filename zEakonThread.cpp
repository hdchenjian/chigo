#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#ifdef _WINDOWS
#include <Windows.h>
#else 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#endif
#include <cstdint>
#include "I2CTag.h"
#include "p2pThread.h"
#include "config_mgr.h"
#include "zAlgorithmManager.h"
#include "zWifiStateThread.h"
#include "zEakonThread.h"
#include "zVideoThread.h"
#include "zUtil.h"
#include "zFile.h"
#include "zLog.h"
#include "zWifiConnect.h"
#include "device_test.h"
#include "cJSON.h"


#define MCU_PROTOCOL_MIDEA 0 // MCU通信协议

#if MCU_PROTOCOL_MIDEA
#define COM_TO_BGN_CODE 0xab           // 通信开始识别码
#define COM_TO_PROTOCOL_VERSION 0x04   // 低四位协议版本V16
#define COM_TO_WIND_INFO 0xa0          // 发送吹风信息 & 故障状态 & 人员信息等

#define COM_FROM_BGN_CODE 0xaa         // 通信开始识别码
#define COM_FROM_STATE_INFO 0xa1       // 收到的状态信息
#define COM_FROM_SN_CODE 0xa1          // 回复的SN码命令
#define COM_FROM_WIFI_CODE 0xb0        // 回复的WIFI信息
#define COM_FROM_CONFIG_INFO 0xa0      // 收到的配置信息

#else // CHIGO

#define COM_TO_BGN_CODE 0xac           // 通信开始识别码
#define COM_TO_PROTOCOL_VERSION 0x01   // 低四位协议版本V16
#define COM_TO_WIND_INFO 0x02          // 发送吹风信息 & 故障状态 & 人员信息等

#define COM_FROM_BGN_CODE 0xac         // 通信开始识别码
#define COM_FROM_STATE_INFO 0x10       // 收到的状态信息
#define COM_FROM_SN_CODE 0x20          // 回复的SN码命令
#define COM_FROM_WIFI_CODE 0x30        // 回复的WIFI信息
#define COM_FROM_CONFIG_INFO 0x40      // 收到的配置信息
#endif

#define LIGHTING_ENABLE 1  // 闪灯使能开关: 1: 开， 0: 关
#define MCU_RESET_SWITCH 1 // 相互唤醒功能开关

#define DEVICE_TEST_SIMULATE 0 // 仅用于调试: 工装测试模拟

//#include <android/log.h>
#define DEV_OPEN_CLOSE_READ (1)

#if 1
#define EAKON_SLEEP_TM (100)    // 单位：毫秒
#define QUERY_RATE (20)
#define DEV_IDLE_READ (8)
#define SEND_STATE_GAP (20)     // 单位: 次数, 总时间= SEND_STATE_GAP * EAKON_SLEEP_TM
#else
#define EAKON_SLEEP_TM (200)    // 单位：毫秒
#define QUERY_RATE (10)
#define DEV_IDLE_READ (18)
#define SEND_STATE_GAP (10)     // 单位: 次数, 总时间= SEND_STATE_GAP * EAKON_SLEEP_TM
#endif


#define UPDATETIEMR_ID (1008)
//#define WIFITEST  (300) 

#define MAX_MCU_FAULT_CNT   5 // MCU故障阈值
#define MAX_MCU_RERROR_CNT  5 // MCU读故障阈值
#define RESET_MCU_INTERVAL (600) // 两次复位MCU间隔 -- 10分钟

#define FIRST_WIFI_QUERY_INTERVAL 1 // 第一次查询间隔，单位: 秒
#define WIFI_QUERY_INTERVAL 4
#define WIFI_QUERY_INTERVAL_DEVICE_TEST 3 // 工装模式下，3秒获取一次WIFI信息


#define WIFI_RECONNECT_GAP (15*60) // 15分钟，单位: 秒

int zEakonThread::globalErr_ = EAKON_NO_ERR;
std::mutex zEakonThread::errMutex_;

int zEakonThread::getLastErr()
{
    std::unique_lock<std::mutex> lock(errMutex_);
    return globalErr_;
}

void zEakonThread::setLastErr(int nErr)
{
    std::unique_lock<std::mutex> lock(errMutex_);
    globalErr_ = nErr;
}
//////////////////////////////////////////////////////////////////////////

typedef unsigned char   uchar;
typedef unsigned int    uint;
typedef unsigned long   ulint;

/*----------------------------------------------------------------*/
#ifdef      MCU_M9S8LL36C

#pragma     CONST_SEG  DEFAULT                  //将常量 定义在 FLASH_ROM 区    

#endif//#endif  MCU_M9S8LL36C 
/*----------------------------------------------------------------*/

/*******************************************************************

*******************************************************************/
const   uchar   Decryption_Table_one[64] =
{
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41, 9, 49, 17, 57, 25,
    32, 0, 40, 8, 48, 16, 56, 24
};

/*******************************************************************

*******************************************************************/
const   uchar   Decryption_Table_two[64] =
{
    49, 43, 63, 4, 47, 0, 13, 28,
    35, 42, 30, 44, 45, 48, 22, 33,
    5, 10, 37, 17, 7, 46, 8, 60,
    50, 31, 26, 61, 16, 24, 62, 36,
    55, 2, 25, 11, 1, 41, 12, 3,
    19, 59, 23, 58, 39, 40, 34, 56,
    18, 29, 38, 15, 51, 6, 32, 57,
    54, 53, 21, 9, 27, 52, 14, 20
};

/*******************************************************************

*******************************************************************/
const   uchar   Taxis_Converse_Array[10][20] =
{
    {                                           //第0组
        7, 18, 19, 12, 1,
        14, 2, 8, 3, 9,
        4, 16, 5, 11, 10,
        17, 13, 6, 0, 15
    },
    {                                           //第1组
        2, 16, 8, 11, 10,
        0, 7, 17, 19, 14,
        4, 5, 15, 9, 12,
        13, 18, 1, 6, 3
    },
    {                                           //第2组
        4, 0, 11, 1, 6,
        17, 5, 3, 9, 14,
        2, 15, 8, 19, 18,
        10, 13, 12, 7, 16
    },
    {                                           //第3组
        19, 2, 6, 16, 8,
        9, 10, 0, 14, 11,
        17, 12, 18, 7, 13,
        15, 3, 1, 5, 4
    },
    {                                           //第4组
        6, 17, 7, 10, 14,
        13, 12, 3, 2, 5,
        0, 8, 18, 9, 16,
        19, 1, 11, 4, 15
    },
    {                                           //第5组
        16, 13, 9, 5, 1,
        0, 2, 14, 4, 8,
        17, 18, 3, 15, 6,
        12, 19, 7, 11, 10
    },
    {                                           //第6组
        5, 3, 15, 2, 6,
        14, 11, 8, 10, 13,
        0, 19, 18, 16, 9,
        4, 7, 17, 1, 12
    },
    {                                           //第7组
        16, 9, 4, 15, 14,
        5, 3, 17, 13, 8,
        0, 12, 19, 11, 6,
        10, 2, 7, 18, 1
    },
    {                                           //第8组
        16, 17, 10, 5, 8,
        12, 7, 1, 6, 14,
        18, 2, 0, 9, 3,
        15, 13, 11, 4, 19
    },
    {                                           //第9组
        7, 9, 3, 4, 18,
        16, 19, 5, 11, 15,
        14, 10, 17, 1, 13,
        2, 6, 12, 0, 8
    }
};
/*----------------------------------------------------------------*/

/*******************************************************************

*******************************************************************/
static void IDToBarcode(uchar   *pucBarcode, uchar  *pucID)
{
    uchar   ucTemp, ucTemp1;
    uchar   ucTempArray[22];
    /*----------------------------------------------------------------*/
    ucTemp1 = *(pucID + 21);                    //提取最后一字节，表示采用的解码排序表
    if (ucTemp1>9)
    {
        ucTemp1 = 9;                            //默认 0 ～ 9 ，大于9则被默认为9
    }
    /*----------------------------------------------------------------*/
    for (ucTemp = 0; ucTemp < 22; ucTemp++)     //将 ucTempArray、pucBarcode所有元素清零
    {
        ucTempArray[ucTemp] = 0x00;
        *(pucBarcode + ucTemp) = 0x00;
    }
    /*----------------------------------------------------------------*/
    for (ucTemp = 1; ucTemp < 21; ucTemp++)     //将机器码 字节1 ～ 字节20 第一次解码变换 存入临时数组 字节0 ～ 字节19
    {
        ucTempArray[ucTemp - 1] = Decryption_Table_one[*(pucID + ucTemp)];
    }
    /*----------------------------------------------------------------*/
    for (ucTemp = 0; ucTemp < 20; ucTemp++)     //将第一次变换的中间码做顺序还原 存入条形码数组
    {
        *(pucBarcode + Taxis_Converse_Array[ucTemp1][ucTemp]) = ucTempArray[ucTemp];
    }
    /*----------------------------------------------------------------*/
    for (ucTemp = 0; ucTemp < 20; ucTemp++)     //将顺序还原后的中间码做第二次解密 存入临时数组
    {
        ucTempArray[ucTemp] = Decryption_Table_two[*(pucBarcode + ucTemp)];
    }
    /*----------------------------------------------------------------*/
    for (ucTemp = 0; ucTemp < 20; ucTemp++)     //将临时数组 字节0～字节19 转换为ASCII码 存入条形码数组 字节1～字节21  
    {
        if (ucTempArray[ucTemp]<10)
        {
            *(pucBarcode + ucTemp + 1) = ucTempArray[ucTemp] + 48;
        }
        else
        {
            *(pucBarcode + ucTemp + 1) = ucTempArray[ucTemp] + 55;
        }
    }
    /*----------------------------------------------------------------*/
    ucTemp = *(pucID + 0) - 36;             //获取条形码 字节0 ASCII码
    if (ucTemp<10)
    {
        *(pucBarcode + 0) = ucTemp + 48;
    }
    else
    {
        *(pucBarcode + 0) = ucTemp + 55;
    }
    /*----------------------------------------------------------------*/
    ucTemp = *(pucID + 21);                 //获取条形码 字节21 ASCII码
    if (ucTemp<10)
    {
        *(pucBarcode + 21) = ucTemp + 48;
    }
    else
    {
        *(pucBarcode + 21) = ucTemp + 55;
    }
    /*----------------------------------------------------------------*/
}

/*****************************************************************************
 * 函 数 名  : check_msg
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月13日
 * 函数功能  : 校验消息合法性
 * 输入参数  : const zAirConditionerMsg& msg  消息
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int check_msg(const zAirConditionerMsg& msg)
{
    (void)msg;
    // to do
    return 0;
}

//////////////////////////////////////////////////////////////////////////
zMsgQueue::zMsgQueue()
{
    // todo: optimize memory.
    //m_msgVector.reserve(ZM_SIZE);
}

zMsgQueue::~zMsgQueue()
{
    m_msgList.clear();
}

int zMsgQueue::addMsg(const zAirConditionerMsg& msg)
{
    LOG_D("zMsgStack::addMsg msg == %d\n", msg.msg);
    if (0 != check_msg(msg))
    {
        LOG_E("invalid message. \n");
        return -1;
    }

    std::unique_lock<std::mutex> lock(m_msgMutex);
    if (m_msgList.size() >= 100)
    {
        LOG_E("msg. queue full.\n");
        return -1;
    }

    m_msgList.push_back(msg);
    return 0;
}

int zMsgQueue::getMsg(zAirConditionerMsg& msg)
{
    std::unique_lock<std::mutex> lock(m_msgMutex);
    if (m_msgList.empty())
    {
        return -1;
    }

    msg = m_msgList.front();
    m_msgList.pop_front();
    return 0;
}

int zMsgQueue::clearMsg()
{
    std::unique_lock<std::mutex> lock(m_msgMutex);
    int size = m_msgList.size();
    m_msgList.clear();
    return size;
}

/*****************************************************************************
 * 函 数 名  : make_heat_source_json_response
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月31日
 * 函数功能  : json响应字符串
 * 输入参数  : char *temperatures  温度
               char *angles        角度
               int count           数量
 * 输出参数  : 无
 * 返 回 值  : static::string
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static std::string make_heat_source_json_response(
    char *temperatures, char *angles, int count)
{
    std::string json_str;
    cJSON *root = cJSON_CreateObject();
    if (NULL != root)
    {
        cJSON *arr, *ta;
        cJSON_AddItemToObject(root, "heat_sources", arr =cJSON_CreateArray());
        for (int i=0; i<count; i++)
        {
            cJSON_AddItemToArray(arr, ta=cJSON_CreateObject());
            cJSON_AddStringToObject(ta, "temperature", zBaseUtil::to_string((int)temperatures[i]).c_str());
            cJSON_AddStringToObject(ta, "angle", zBaseUtil::to_string((int)angles[i]).c_str());
        }

        json_str = cJSON_PrintUnformatted(root);
        cJSON_Delete(root);
    }
    return json_str;
}

zEakonThread::zEakonThread(zAlgManager* pParent, int width /* = 640 */
    , int height /* = 480 */,std::string strDevName /* = "/dev/mcu_dev" */)
    : m_pManager(pParent)
    , m_k(kazeMuki::k_focuse)
    , m_ucWifiType(-1)
    , m_ucWifiDetype(-1)
    , m_nLeftBorder(45)
    , m_nRightBorder(135)
    , m_needRead(0)
    , m_curLightState(0)
    , m_nUpdateGap(7)
    , m_nGlintColor(-1)
    , m_latestProc(0)
    , m_devName(strDevName)
    , m_p2pThread(NULL)
    , m_nWifiState(wifiState::connect_none)
    , m_quick_check_mode(0)
    , m_connect_router_state(0)
    , m_last_error(EAKON_NO_ERR)
    , m_taninn_find_flag(false)
    , m_proc_count(0)
    , m_light_start_time(0)
    , m_fault_flag(0)
    , m_mcu_read_error_cnt(0)
    , m_mcu_fault_cnt(0)
    , m_gesture_flag(GESTURE_AC_SWITCH)
    , m_itelligent_switch_enable(true)
    , m_home_leave_mode(false)
    , m_live_video_switch(true)
    , m_last_reset_mcu_time(0)
    , m_limit_angle_left(45)
    , m_limit_angle_right(135)
    , m_prev_have_person_flag(false)
    , m_wifi_query_time(0)
    , m_wifi_query_first_flag(true)
    , m_update_wifi_done_flag(true)
    , m_device_test_mode_before_acquire_wifi(false)
    , m_device_test_mode(false)
    , m_need_wakeup_device_test_flag(false)
    , m_wifi_enable_flag(true)
    , m_wifi_connect_time(0)
    , m_eakon_opened(true)
    , m_intelligent_enable(true)
    , m_heat_source_enable(true)
    , m_wifi_ssid_change_flag(false)
    , m_state_ac_opened(true)
    , m_state_ac_coninue_count(0)
    , m_mcu_version_count(0)
    , m_is_new_mcu_version(true)
{
    (void)reset_mcu();
    m_eakonDev = openDevice(m_devName.c_str());

    (void)closeLightDelay(true);
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
    std::string strNeedC = conf.getValue("needConnect");
    std::string strP2p = conf.getValue("p2p");
    m_bUseP2p = (strP2p == "true");
    std::string strSimulate_mcb = conf.getValue("simulate_mcb");
    m_bSimulateMCB = (strSimulate_mcb == "on");
    if (m_bSimulateMCB)
    {
        m_simulate_wifi_ssid = conf.getValue("ssid");
        m_simulate_wifi_passwd = conf.getValue("passwd");
        LOG_I("m_simulate_wifi_ssid=%s, m_simulate_wifi_passwd=%s\n",
            m_simulate_wifi_ssid.c_str(), m_simulate_wifi_passwd.c_str());
    }

    m_bSimulateDeviceId = ("on" == conf.getValue("simulate_devid"));
    if (m_bSimulateDeviceId)
    {
        m_strDevID = conf.getValue("devid");
        (void)zBaseUtil::write_deviceid_file(m_strDevID);
        LOG_I("m_strDevID=%s\n", m_strDevID.c_str());
    }

    std::string strUpdate = conf.getValue("update");
    if (!strUpdate.empty()) m_nUpdateGap = atoi(strUpdate.c_str());

    m_ssid_quick_mode = conf.getValue("qm_ssid");
    if (m_ssid_quick_mode.empty())
    {
        m_ssid_quick_mode = "QTEST";
    }

    m_passwd_quick_mode = conf.getValue("qm_passwd");
    if (m_passwd_quick_mode.empty())
    {
        m_passwd_quick_mode = "87654321";
    }
    LOG_I("qm_ssid=%s, qm_passwd=%s\n", m_ssid_quick_mode.c_str(), m_passwd_quick_mode.c_str());

    // 打开MCU失败，设置通信故障标志
    int nErr = zEakonThread::getLastErr();
    if (-1 == m_eakonDev)
    {
        nErr |= (EAKON_COMMU_ERR | EAKON_ERR_EXIST);
    }

    nErr |= EAKON_WIFI_ERR;
    nErr |= EAKON_NOT_CONNECT_AP;
    zEakonThread::setLastErr(nErr);

    // 初始化
    (void)memset(m_wind_arr, 0, sizeof(m_wind_arr));
    m_last_mk = k_unknown;
    m_last_gesture_flag = false;

    m_wifi_query_gap = FIRST_WIFI_QUERY_INTERVAL;
    m_wifi_query_time = zBaseUtil::getSecond();
    m_wifi_query_first_flag= true;

    // 工装模式，wifi用户名和密码
    m_ssid_device_test_mode = conf.getValue("device_test_ssid");
    if (m_ssid_device_test_mode.empty())
    {
        m_ssid_device_test_mode = "DEVICE_TEST";
    }

    m_passwd_device_test_mode = conf.getValue("device_test_passwd");
    if (m_passwd_device_test_mode.empty())
    {
        m_passwd_device_test_mode = "87654321";
    }

    LOG_I("m_ssid_device_test_mode=%s, m_passwd_device_test_mode=%s\n", 
        m_ssid_device_test_mode.c_str(), m_passwd_device_test_mode.c_str());

    m_heat_source_debug = zBaseUtil::to_bool(conf.getValue("heat_source_debug"));
    LOG_I("m_heat_source_debug=%d\n", m_heat_source_debug);
}

zEakonThread::~zEakonThread()
{
    // save 
    if (!m_strSSID.empty())
    {
        zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
        conf.setValue("ssid", m_strSSID);
        conf.setValue("passwd", m_strPasswd);
        int nTry = 10;
        while (!conf.SaveConfig("/mnt/sdcard/conf.txt")
            && nTry-- > 0);
        if (nTry <= 0)
        {
            LOG_I("SAVE ssid = %s, passwd = %s FAILDED! \n", m_strSSID.c_str(), m_strPasswd.c_str());
        }
        else
        {
            LOG_I("SAVE ssid = %s, passwd = %s SUCC! \n", m_strSSID.c_str(), m_strPasswd.c_str());
        }
    }
    if ((ZDEVICE)-1 != m_eakonDev)
    {
        closeDevice();
    }
    if (NULL != m_p2pThread)
    {
        delete m_p2pThread;
        m_p2pThread = NULL;
    }
    LOG_I(" zEakonThread::~zEakonThread() end \n");
}

void zEakonThread::Start()
{
    LOG_I("zEakonThread::Start\n");
    // when start query the devid
    getDevid();
    
    //getSsid(); //35秒后查询
    m_latestProc = zBaseUtil::getMilliSecond();
    //LOG_D("zEakonThread::Start time == %lld \n", m_latestProc);

    zThread::Start();

    zWifiStateThread* pThread = zWifiStateThread::getInstance();
    pThread->init((void*)this);
    zWifiStateThread::getInstance()->startThread();

    return;
}

void zEakonThread::proc()
{
    // do timer
    int nErr = getLastErr();
    m_proc_count++;

    //LOG_D("zEakonThread::proc nErr == %d befor \n", nErr);
    do 
    {
        std::int64_t cur = zBaseUtil::getMilliSecond();
        std::int64_t nGap = cur - m_latestProc;
        LOG_D("zEakonThread::proc cur == %lld, pre == %lld, gap == %lld \n", cur, m_latestProc, nGap);

        m_latestProc = cur;
        m_updateTimer.elapse(nGap);

        bool taime_skip_lock = false;
        bool need_sendmsg_flag = false;
        zAirConditionerMsg msg;
        {
            std::unique_lock<std::mutex> lock(m_ctrlMutex);
            if (m_needRead > 0)
            {
                if (0 == m_msgQueue.getMsg(msg))
                {
                    need_sendmsg_flag = true;
                    if (    msg.msg == ZM_CLOSE
                        ||  msg.msg == ZM_OPEN)
                    {
                        // need read after x * 100ms
                        m_needRead = DEV_OPEN_CLOSE_READ;
                    }
                }
                m_needRead--;
            }
            else
            {
                taime_skip_lock = true;
                m_needRead = DEV_IDLE_READ;
            }
            //LOG_D("m_needRead == %d\n", m_needRead);
        }

        // 将sendMsg提到m_ctrlMutex锁外面，减少锁粒度。
        if (need_sendmsg_flag)
        {
            (void)sendMsg(msg);
        }

        // 防止死锁，原因: zEakonThread::m_ctrlMutex 和 zAlgorithmDetectThread::ctrlMutex_ 相互等待。
        if (taime_skip_lock) 
        {
            (void)taime();
        }

        // 2秒发送一次状态，这样，就可以读取到功能配置的更改
        if (0 == (m_proc_count % SEND_STATE_GAP))
        {
            (void)send_running_state();
            (void)send_light_state();
        }

        //LOG_D("zEakonThread::proc: m_quick_check_mode=%d", m_quick_check_mode);
        // 快检模式下发送状态并设置LED灯
        if (0 != m_quick_check_mode)
        {
            int curr_error = get_last_error_quick_mode();
            // 400毫秒发送一次故障状态值到MCU
            if (0 == (m_proc_count & 0x3))
            {
                (void)send_state_quick_mode(curr_error);
            }

            LOG_D("proc: curr_error=0x%x, m_last_error=0x%x\n", curr_error, m_last_error);
            if (curr_error != m_last_error)
            {
                if ((!!curr_error) != (!!m_last_error)) // 故障和非故障
                {
                    (void)set_light_quick_mode(curr_error);
                }
                m_last_error = curr_error;
            }
        }
        else
        {
            int nLightColor = getLightColor();
            int nLightState = LightState();
            LOG_D("zEakonThread::proc: nErr=0x%x, nLightColor=%d, nLightState=%d\n",
                nErr, nLightColor, nLightState);

            if (nErr) // 故障闪灯处理
            {
                LOG_D("zEakonThread::proc: nErr wifi=%d, m_fault_flag=%d \n", nErr & EAKON_WIFI_ERR, m_fault_flag);
                if (nErr & EAKON_WIFI_ERR) // wifi故障, 十秒闪烁
                {
                    if (!(m_fault_flag & EAKON_WIFI_ERR))
                    {
                        m_light_start_time = zBaseUtil::getMilliSecond();
                        (void)blueWhiteLightGlintDelay();
                        //LOG_D("zEakonThread::proc: nErr=%d, m_light_start_time=%lld\n", nErr, m_light_start_time);
                    }
                    else if (0 != m_light_start_time) // 十秒闪烁期间
                    {
                        std::int64_t curr_time = zBaseUtil::getMilliSecond();
                        if ((curr_time - m_light_start_time) >= 10*1000)
                        {
/*
                            LOG_D("zEakonThread::proc: nErr=%d, curr_time=%lld, m_light_start_time=%lld\n", 
                                nErr, curr_time, m_light_start_time);
*/
                            (void)closeLightDelay(true);
                            m_light_start_time = 0; // 故障 -> 故障，已经十秒闪烁完毕
                        }
                    }
                }
                else // 其他故障
                {
                    if (!(m_fault_flag & (~EAKON_WIFI_ERR)))
                    {
                        m_light_start_time = zBaseUtil::getMilliSecond();
                        (void)fault_light_glint();
                    }
                    else if (0 != m_light_start_time) // 20秒闪烁期间
                    {
                        std::int64_t curr_time = zBaseUtil::getMilliSecond();
                        if ((curr_time - m_light_start_time) >= 20*1000)
                        {
                            (void)closeLightDelay(true);
                            m_light_start_time = 0; // 故障 -> 故障，已经20秒闪烁完毕
                        }
                    }
                }
                m_fault_flag = nErr;
            }
            else if (!nErr && (nLightColor != nLightState))
            {
                /*LOG_D("zEakonThread::proc recoveryLight %d nLightColor == %d nLightState == %d \n",
                    nErr, nLightColor, nLightState);*/
                recoveryLightV2();
                
                m_fault_flag = EAKON_NO_ERR;
            }
        }
    } while (false);

    //LOG_D("zEakonThread::proc nErr == %d end \n", nErr);
    std::this_thread::sleep_for(std::chrono::milliseconds(EAKON_SLEEP_TM));
    return;
}

ZDEVICE zEakonThread::openDevice( std::string name)
{
    ZDEVICE pDevice = (ZDEVICE)-1;
    do
    {
        if (name.empty()) break;
        pDevice = open(name.c_str(), O_RDWR); /*以读写方式打开设备*/
        if (-1 == pDevice)
        {
            LOG_E("open(%s, O_RDWR) failed\n", name.c_str());
        }
    } while (false);

    LOG_I("device opened\n");
    return pDevice;
}

void zEakonThread::closeDevice( )
{
    std::unique_lock<std::mutex> lock(m_devMutex);
    close(m_eakonDev);
    m_eakonDev = (ZDEVICE)(-1);
    LOG_I("device closed\n");
    return;
}

int zEakonThread::writeBuf2Device( const char* szBuf, int nLen)
{
    std::unique_lock<std::mutex> lock(m_devMutex);
    return zBaseUtil::writeBuf2Device(m_eakonDev, szBuf, nLen);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.readDevice
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月12日
 * 函数功能  : 通过I2C 读取MCU寄存器内容
 * 输入参数  : void* szBuf  178个字节的缓存
               size_t nLen  字节数
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::readDevice( void* szBuf, size_t nLen)
{
    //LOG_D(" zEakonThread::readDevice befor \n");
    int err_ret = -1;

    std::unique_lock<std::mutex> lock(m_devMutex);
    if (m_eakonDev == (ZDEVICE)(-1))
    {
        LOG_E("readDevice from invalid device! \n");
        return err_ret;
    }

    int ret = read(m_eakonDev, szBuf, nLen);
    if (ret < 0)
    {
        LOG_E("read from device %0x failed %s. ret=%d\n", m_eakonDev, strerror(errno), ret);
        return err_ret;
    }

    return 0;
}

int zEakonThread::getKazeParama(int& left, int& right)
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    //LOG_D("zEakonThread::getKazeParama m_ucLeftBorder == %d, m_ucRightBorder == %d \n", m_nLeftBorder, m_nRightBorder);
    left = m_nLeftBorder;
    right = m_nRightBorder;
    return 0;
}

bool zEakonThread::bFocus()
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    return zEakonThread::kazeMuki::k_focuse == m_k;
}

int zEakonThread::sendMsg(zAirConditionerMsg& msg)
{
    int nErr = zEakonThread::getLastErr();
    if (msg.msg == ZM_OPEN
        || msg.msg == ZM_CLOSE
        || msg.msg == ZM_DEVICEID
        || msg.msg == ZM_SSID
        || msg.msg == ZM_TEMPERATURE)
    {
        msg.pBuf.buf[2] = zBaseUtil::transEakonErr(nErr, EAKON_ERR_0x04);
    }
    // ab a0
    else if (msg.msg == ZM_TEMANEIN
        || msg.msg == ZM_TEMANEOUT
        || msg.msg == ZM_KAZECTRL
        || msg.msg == ZM_TANINN
        || msg.msg == ZM_TANINNLOSE
        || msg.msg == ZM_JUST_SEND)
    {
        if (0 == m_quick_check_mode)
        {
            msg.pBuf.buf[10] = zBaseUtil::transEakonErr(nErr, EAKON_ERR_0xa0);
            if ( !(nErr & EAKON_NOT_CONNECT_AP))
            {
                msg.pBuf.buf[8] |= (0x1 << 3);
            }
        }
        else
        {
            nErr = get_last_error_quick_mode(); // 快检模式
            msg.pBuf.buf[10] = zBaseUtil::transEakonErrV2(nErr, EAKON_ERR_0xa0);
            if ( !(nErr & EAKON_NOT_CONNECT_AP))
            {
                msg.pBuf.buf[8] |= (0x1 << 3);
            }
        }

        // 离家模式时，智能风信息字段全部置为零。
        if (m_home_leave_mode)
        {
            // 发送离家模式配置
            msg.pBuf.buf[9] |= (0x1 << 0);
        }
        else if (m_intelligent_enable) // 智能使能
        {
            write_smart_wind_config(msg.pBuf.buf);
        }

        write_person_flag(msg.pBuf.buf);

        // 以ab,a0为命令头的命令，需带上当前人员入侵标志
        if (    (msg.msg != ZM_TANINNLOSE)
            &&  (msg.msg != ZM_TANINN))
        {
            if (m_taninn_find_flag) 
            {
                msg.pBuf.buf[8] |= 0x04;
            }
            else
            {
                msg.pBuf.buf[8] &= ~0x04;
            }
        }
    }

    if (    (msg.msg != ZM_OPEN)
        &&  (msg.msg != ZM_CLOSE)
        &&  (msg.msg != ZM_JUST_SEND))
    {
        m_pManager->onEvent(msg.msg);
    }

    #if !LIGHTING_ENABLE
    if (0xc0 != msg.pBuf.buf[0]) // 首字节为0xc0， 为指示灯命令
    #endif
    {
        int nSize = writeBuf2Device((const char*)msg.pBuf.buf, sizeof(zI2C::toAirConditioner));
        // 调试打印
        LOG_D("mcu. msg=%d, nSize=%d\n", msg.msg, nSize);
        print_buffer("mcu. write_buf", (const char *)msg.pBuf.buf, sizeof(zI2C::toAirConditioner));
    }

    #if 0
    if (ZM_TEMANEIN == msg.msg)
    {
        LOG_E("ding. ding. 0x%x, %d\n", msg.pBuf.buf[8], msg.pBuf.buf[8] & (0x1 << 6));
    }
    #endif

    if (    msg.msg == ZM_BLUELIGHT
        ||  msg.msg == ZM_WHITELIGHT
        ||  msg.msg == ZM_WBGLINT
        ||  msg.msg == ZM_BGLINT
        ||  msg.msg == ZM_WGLINT )
    {
        int nCur = LightState();

        m_curLightState = msg.msg - LIGHTSTATE;
        m_preLightState = nCur;

        LOG_D("sendMsg: m_curLightState=%d, m_preLightState=%d \n", 
            m_curLightState, m_preLightState);
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.whiteLightDelay
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月5日
 * 函数功能  : 白灯常亮，延迟200ms执行
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::whiteLightDelay()
{
    //int nCur = LightState();
    LOG_D("zEakonThread::light white delay \n");
    if (LightState() == 3)
    {
        return 0; // 状态不变
    }

    (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。

    zAirConditionerMsg msg;
    msg.msg = ZM_WHITELIGHT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = 3; 

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.blueLightDelay
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月5日
 * 函数功能  : 蓝灯常亮，验证200ms执行
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::blueLightDelay()
{
    LOG_D("zEakonThread::light blue delay \n");
    if (LightState() == 2)
    {
        return 0; // 状态不变
    }

    (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。 

    zAirConditionerMsg msg;
    msg.msg = ZM_BLUELIGHT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = 2; 

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.blue_light_glint
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月30日
 * 函数功能  : 蓝灯闪烁 -- 延迟200毫秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::blue_light_glint()
{
    LOG_D("zEakonThread::light blue glint delay \n");
    if (LightState() == 5)
    {
        return 0; // 状态不变
    }
    
    (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。 

    zAirConditionerMsg msg;
    msg.msg = ZM_BGLINT;
    
    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = 5;

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.blueLightDelay
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月5日
 * 函数功能  : 蓝白灯交替闪，验证200ms执行
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::blueWhiteLightGlintDelay()
{
    LOG_D("zEakonThread: blue white light glint delay. \n");
    if (4 == LightState())
    {
        return 0; // 状态不变
    }

    (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。 

    zAirConditionerMsg msg;
    msg.msg = ZM_WBGLINT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = 4; 

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.send_light_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月5日
 * 函数功能  : 循环发送当前摄像头灯状态，规避发一次后，闪灯不正确的问题
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::send_light_state()
{
    int curr_state = LightState();
    LOG_D("curr_state=%d\n", curr_state);

    zAirConditionerMsg msg;
    msg.msg = LIGHTSTATE;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = curr_state; 

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.send_running_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 发送运行状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::send_running_state()
{
    LOG_D("\n");
    zAirConditionerMsg msg;
    msg.msg = ZM_JUST_SEND;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.send_state_quick_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月3日
 * 函数功能  : 向mcu上传故障消息
 * 输入参数  : int err  故障状态
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::send_state_quick_mode(int err)
{
    LOG_D("zEakonThread::send state in quick mode \n");
    zAirConditionerMsg msg;
    msg.msg = ZM_JUST_SEND;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    //info.buf[10] = zBaseUtil::transEakonErrV2(err, EAKON_ERR_0xa0);
    //LOG_D("send_state_quick_mode: info.buf[10]=0x%x\n", info.buf[10]);

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.closeLightDelay
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月5日
 * 函数功能  : 关闭摄像头灯
 * 输入参数  : bool update_flag  ture: 更新闪灯状态，false:不更新，作为切换亮灯状态的中间过渡
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::closeLightDelay(bool update_flag)
{
    //int nCur = LightState();
    LOG_D("closeLightDelay: update_flag = %d.\n", update_flag);

    zAirConditionerMsg msg;
    msg.msg = ZM_CLOSELIGHT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = 0;

    int ret = m_msgQueue.addMsg(msg);
    if ((0 == ret) && (update_flag))
    {
        m_preLightState = m_curLightState;
        m_curLightState = 0;
/*
        LOG_D("closeLightDelay: m_curLightState=%d, m_preLightState=%d. \n",
            m_curLightState, m_preLightState);
*/
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.fault_light_glint
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月5日
 * 函数功能  : 非wifi故障时，闪灯
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::fault_light_glint()
{
    int nCur = getLightColor();

    LOG_D("fault_light_glint: LightState()=%d, nCur=%d \n", LightState(), nCur);
    if (LightState() == nCur)
    {
        return 0;
    }
    
    (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。 

    int nGlintColor = 0;
    if ((0 == nCur) || (2 == nCur))
    {
        nGlintColor = 5;
    }
    else if (3 == nCur)
    {
        nGlintColor = 6;
    }

    zAirConditionerMsg msg;
    msg.msg = nGlintColor + LIGHTSTATE;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = 0xc0;
    info.buf[1] = nGlintColor;
    LOG_D("fault_light_glint: nGlintColor=%d \n", nGlintColor);

    return m_msgQueue.addMsg(msg);
}

int zEakonThread::recoveryLightV2()
{
    int nCur = getLightColor();
    int nResult = 0;
    LOG_D("zEakonThread::light recoveryv2 to == %d \n", nCur);
    do
    {
        (void)closeLightDelay(false); // 切换灯状态前，先关闭灯。 

        zI2C::toAirConditioner info = { 0 };
        info.tagInfo.header.chProHead = 0xc0;
        info.buf[1] = nCur;
        zAirConditionerMsg msg;
        msg.pBuf = info;
        msg.msg = nCur + LIGHTSTATE;

        if (0 == nCur)
        {
            m_preLightState = m_curLightState;
            m_curLightState = 0;
/*
            LOG_D("closeLightDelay: m_curLightState=%d, m_preLightState=%d. \n",
                m_curLightState, m_preLightState);
*/
        }
        nResult = m_msgQueue.addMsg(msg);
        m_nGlintColor = -1;
    } while (false);

    return nResult;
}

//////////////////////////////////////////////////////////////////////////
zEakonThread::kazeMuki zEakonThread::getKazeMuki()
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    return m_k;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.getGestureFlag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 获取手势功能标志
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::getGestureFlag()
{
    std::unique_lock<std::mutex> lock(m_ctrlMutex);
    return m_gesture_flag;
}

zEakonThread::kazeMuki zEakonThread::setKazeMuki(kazeMuki muki)
{
    zEakonThread::kazeMuki oldMuki = zEakonThread::kazeMuki::k_unknown;
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        oldMuki = m_k;
        m_k = muki;
    }

    if (m_k != oldMuki)
    {
        (void)notify_intelligent_state();
    }

    //int arr[SMART_WIND_CONFIG_SIZE] = { 1, 4, 4, 90, 90, 90, 90, 1};
    //KazemukiCtrl_v2(arr, muki, false);
    return oldMuki;
}

void zEakonThread::set_home_leave_mode(bool home)
{
    bool new_mode = !home;
    if (m_home_leave_mode != new_mode)
    {
        m_home_leave_mode = new_mode;
        (void)notify_intelligent_state();
    }
}

int zEakonThread::CloseAirConditioner( )
{
    LOG_I("zEakonThread close eakon\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    info.buf[3] = 0x10;
    //info.buf[3] = 0x12;
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_CLOSE;

    return m_msgQueue.addMsg(msg);
}

int zEakonThread::OpenAirConditioner()
{
    LOG_I("zEakonThread open eakon\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    //info.buf[3] = 0x10;
    info.buf[3] = 0x12;
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_OPEN;

    return m_msgQueue.addMsg(msg);
}

int zEakonThread::getDevid()
{
    //return 0;
    LOG_D("zEakonThread getDevid\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    //info.buf[3] = 0x10;
    info.buf[3] = 0x7b;
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_DEVICEID;

    return m_msgQueue.addMsg(msg);
}

int zEakonThread::getSsid()
{
    //return 0;
    LOG_D("zEakonThread getSsid\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    //info.buf[3] = 0x10;
    info.buf[3] = 0x7C;
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_SSID;

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.auto_swipe_wind
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 自动扫风
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::auto_swipe_wind()
{
    if (NULL == this) return -1;
    LOG_D("auto_swipe_wind. \n");

    // 清除前面的智能风配置
    reset_prev_smart_wind_config();

    zAirConditionerMsg msg;
    msg.msg = ZM_JUST_SEND;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.TemaneCtrl
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月28日
 * 函数功能  : 播放手势提示音
 * 输入参数  : bool bFocus  true：手势进入提示音 false：手势退出提示音
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::TemaneCtrl(bool bFocus)
{
    if (NULL == this) return -1;
    LOG_E("bFocus=%d \n", bFocus);

    zAirConditionerMsg msg;
    msg.msg = bFocus ? ZM_TEMANEIN : ZM_TEMANEOUT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    if (bFocus)
    {
        info.buf[8] = (0x01 << 0); // 手势进入提示音: ding
    }
    else
    {
        info.buf[9] = (0x01 << 1); // 手势退出提示音: ??
    }

    return m_msgQueue.addMsg(msg);
}

int zEakonThread::TemaneCtrl2(bool bFocus)
{
    if (NULL == this) return -1;
    LOG_D("bFocus=%d \n", bFocus);

    zAirConditionerMsg msg;
    msg.msg = bFocus ? ZM_TEMANEIN : ZM_TEMANEOUT;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    info.buf[8] = 0x40; // 0x01 << 6 

    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.write_prev_smart_wind_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 保存上次的空调吹风配置，注: 涉及多线程访问，已加锁
 * 输入参数  : int* config_arr              智能风配置数组
               zEakonThread::kazeMuki muki  智能风配置（吹入 or 避人 or 无）
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::save_prev_smart_wind_config(
    const int* config_arr, zEakonThread::kazeMuki muki, bool gesture_flag)
{
    if (NULL == config_arr)
    {
        LOG_E("null pointer.\n");
        return;
    }

    std::unique_lock<std::mutex> lock(m_smart_wind_mutex);
    (void)memcpy(m_wind_arr, config_arr, SMART_WIND_CONFIG_SIZE*sizeof(int));
    m_last_mk = muki;
    m_last_gesture_flag = gesture_flag;
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.write_smart_wind_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 填写智能风配置
 * 输入参数  : unsigned char *pbuf_config  确保：为配置buffer，30个字节
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::write_smart_wind_config(
    unsigned char *pbuf_config)
{
    if (NULL == pbuf_config)
    {
        LOG_E("null pointer.\n");
        return;
    }

    std::unique_lock<std::mutex> lock(m_smart_wind_mutex);
    if (    ((k_unknown != m_last_mk) && (k_unknown != m_k))
        ||  (m_last_gesture_flag && (GESTURE_AC_WIND == m_gesture_flag)) )
    {
        pbuf_config[8] |= ((int)m_last_mk & 0xFF);
        if (m_last_gesture_flag)
        {
            pbuf_config[8] |= (0x1 << 7);
        }

        // only two group
        for (int i = 1; i < SMART_WIND_CONFIG_SIZE; i++)
        {
           pbuf_config[i + 10] = (unsigned char )m_wind_arr[i];
        }
    }
    else
    {
        pbuf_config[8] &= ~(k_focuse | k_unfocuse);
        pbuf_config[8] &= ~(0x1 << 7);
        for (int i = 1; i < SMART_WIND_CONFIG_SIZE; i++)
        {
           pbuf_config[i + 10] = (unsigned char )0;
        }
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_prev_smart_wind_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月9日
 * 函数功能  : 获取上次的智能风配置
 * 输入参数  : int buf_config[SMART_WIND_CONFIG_SIZE]  配置数组
               int &muki                               智能风配置（吹入 or 避人 or 无）
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::get_prev_smart_wind_config(
    int buf_config[SMART_WIND_CONFIG_SIZE], zEakonThread::kazeMuki &muki, bool& gesture_flag)
{
    std::unique_lock<std::mutex> lock(m_smart_wind_mutex);
    (void)memcpy(buf_config, m_wind_arr, SMART_WIND_CONFIG_SIZE*sizeof(int));
    muki = m_last_mk;
    gesture_flag = m_last_gesture_flag;
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.reset_prev_smart_wind_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月14日
 * 函数功能  : 复位上次智能风配置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::reset_prev_smart_wind_config()
{
    int buf_config[SMART_WIND_CONFIG_SIZE] = {0};
    zEakonThread::kazeMuki muki = k_unknown;
    save_prev_smart_wind_config(buf_config, muki, false);
    return;
}

static std::string getPersonRangeType(int nCnt)
{
    if (0 >= nCnt) return "zero";
    else if (nCnt <= 3) return "few";
    else return "many";
}

/*****************************************************************************
 * 函 数 名  : p2p_notify_wind_angle
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月12日
 * 函数功能  : 通知P2P客户端，吹风参数
 * 输入参数  : int face_count  人数
               int distance    距离
               int min_angle   左角度
               int max_angle   右角度
 * 输出参数  : 无
 * 返 回 值  : static int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int p2p_notify_wind_angle(
    int flags, int face_count, int distance, int min_angle, int max_angle)
{
    char szBuf[512] = { 0 };
    const std::string& type = getPersonRangeType(face_count);
    sprintf(szBuf, "AC flags = 0x%x, averageDis = %d, angle min = %d, angle max = %d person = %s\n", 
        flags, distance, min_angle, max_angle, type.c_str());

    if (NULL != zP2PThread::getInstance())
    {
        //zP2PThread::getInstance()->NotifyDebugInfo(szBuf);
    }
    LOG_D(szBuf);
    //DtThread::getInstance()->dt_send_message(GESUTER_MSG, szBuf);
    return 0;
}

void zEakonThread::limit_angle(int& angle)
{
    if (angle < m_limit_angle_left)
    {
        angle = m_limit_angle_left;
    }
    else if (angle > m_limit_angle_right)
    {
        angle = m_limit_angle_right;
    }
    return;
}

int zEakonThread::KazemukiCtrl_v2(
    int* arr, zEakonThread::kazeMuki muki, bool gesture_flag)
{
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;

    // 确保: 手势定向风时, 发送到空调端的智能风标志, 不能为零。
    if (gesture_flag)
    {
        if (zEakonThread::k_unknown == muki)
        {
            muki = zEakonThread::k_focuse;
        }
    }

    info.buf[8] = ((int)muki & 0xFF);
    if (gesture_flag)
    {
        info.buf[8] |= (0x1 << 7); // 手势定向送风确认
    }

    // 吹风角度边界限制
    for (int i=3; i<7; i++)
    {
        limit_angle(arr[i]);
    }

    // only two group
    for (int i = 1; i <= (2 * 3); i++)
    {
        info.buf[i + 10] = arr[i];
    }
    if (arr[7] < 0) arr[7] = 0;
    if (3 <= arr[7]) arr[7] = 3;
    info.buf[17] = arr[7];

    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_KAZECTRL;

    (void)p2p_notify_wind_angle(info.buf[8], arr[7], arr[1], arr[3], arr[4]);
    save_prev_smart_wind_config(arr, muki, gesture_flag);
    return m_msgQueue.addMsg(msg);
}

int zEakonThread::KazemukiCtrl(int* arr, bool gesture_flag)
{
    zEakonThread::kazeMuki muki = getKazeMuki();
    return KazemukiCtrl_v2(arr, muki, gesture_flag);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.send_person_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 发送是否有人的标志
 * 输入参数  : bool have_person  true: 有人  false: 无人
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::send_person_flag(bool have_person)
{
    LOG_D("have_person=%d\n", have_person);
    
    zAirConditionerMsg msg;
    msg.msg = ZM_JUST_SEND;

    zI2C::toAirConditioner &info = msg.pBuf;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    
    if (have_person)
    {
        info.buf[8] |= (0x01 << 1); // 1：有人0：无人(开机、开智能的时候下发)
    }

    if (m_prev_have_person_flag != have_person)
    {
        m_prev_have_person_flag = have_person;
    }
    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : write_person_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 写"是否有人"标志
 * 输入参数  : unsigned char *pbuf_config  配置
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::write_person_flag(unsigned char *pbuf_config)
{
    if (NULL == pbuf_config)
    {
        LOG_E("null pointer.\n");
        return;
    }

    // 1：有人0：无人(开机、开智能的时候下发)
    if (m_prev_have_person_flag)
    {
        pbuf_config[8] |= (0x01 << 1); 
    }
    else
    {
        pbuf_config[8] &= ~(0x01 << 1);
    }
    return;
}

int zEakonThread::TaninnFind()
{
    LOG_D("find stranger \n");
    zI2C::toAirConditioner info = { 0 };
    //info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    //info.buf[1] = 0xA0;
    //info.buf[2] = 0x04;
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    info.buf[8] = 0x04;

    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_TANINN;

    m_taninn_find_flag = true;
    //LOG_D("find stranger. m_taninn_find_flag=%d\n", m_taninn_find_flag);
    return m_msgQueue.addMsg(msg);
}

int zEakonThread::TaninnLose()
{
    LOG_D("lose stranger \n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = COM_TO_BGN_CODE;
    info.buf[6] = COM_TO_BGN_CODE;
    info.buf[7] = COM_TO_WIND_INFO;
    info.buf[8] = 0x00; // 人员入侵结束

    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_TANINNLOSE;

    m_taninn_find_flag = false;
    //LOG_D("lose stranger. m_taninn_find_flag=%d\n", m_taninn_find_flag);
    return m_msgQueue.addMsg(msg);
}

int zEakonThread::preLightState()
{
    return m_preLightState;
}

int zEakonThread::LightState()
{
    return m_curLightState;
}
// no 0 c 1 r 2
int zEakonThread::WifiState(unsigned char state)
{
    LOG_D("EakonThread WifiState %d\n", state);
    wifiState oldstate = m_nWifiState;
    {
        std::unique_lock <std::mutex> lock(m_ctrlMutex);
        m_nWifiState = (0 == state) ? wifiState::connect_none
            : (1 == state ? wifiState::connect_internet : wifiState::connect_router);
        //m_bC = (0 != state);

    }

    //LOG_E("oldstate=%d, m_nWifiState=%d\n", oldstate, m_nWifiState);
    if (oldstate != m_nWifiState)
    {
        (void)notify_intelligent_state();
    }

    if (m_nWifiState == wifiState::connect_router && oldstate != wifiState::connect_router)
    {
        LOG_I("connect_router %s\n", zFileUtil::getTimeString().c_str());
    }
    else if (m_nWifiState == wifiState::connect_internet && oldstate != wifiState::connect_internet)
    {
        LOG_I("connect_internet %s\n", zFileUtil::getTimeString().c_str());
    }

    if (1 == state)
    {
        int nErr = zEakonThread::getLastErr();
        nErr &= ~EAKON_WIFI_ERR;
        zEakonThread::setLastErr(nErr);
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        nErr |= EAKON_WIFI_ERR;
        zEakonThread::setLastErr(nErr);
    }
    
    if (0 != state)
    {
        int nErr = zEakonThread::getLastErr();
        nErr &= ~EAKON_NOT_CONNECT_AP;
        zEakonThread::setLastErr(nErr);
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        nErr |= EAKON_NOT_CONNECT_AP;
        zEakonThread::setLastErr(nErr);
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////
/*****************************************************************************
 * 函 数 名  : zEakonThread.get_lighting_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月17日
 * 函数功能  : 获取补光状态
 * 输入参数  : bool &auto_mode  获取自动补光标志
               bool &day   获取补光开关标志
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::get_lighting_mode(bool &auto_mode, bool &day)
{
    m_pManager->get_lighting_mode(auto_mode, day);
}

bool zEakonThread::autoLighting(bool bAuto)
{
    return m_pManager->autoLighting(bAuto);
}

void zEakonThread::chgLighting(bool bOpen)
{
    m_pManager->chgLighting(bOpen);
}
//////////////////////////////////////////////////////////////////////////
int zEakonThread::onEvent(int nEvent)
{
    if (NULL == this) return 0;
    int nErr = getLastErr();
    LOG_I(" zEakonThread::onEvent %d nErr == %d start \n", nEvent, nErr);
    if (ZM_NEEDREAD == nEvent)
    {
        std::unique_lock<std::mutex> lock(m_ctrlMutex);
        m_needRead = 0;
    }
    if (0 != nErr) return  0;

    if (ZM_IEMODEL == nEvent)
    {
        if (m_pManager->getEakonState() == zAlgManager::EakonState::OPENED_EAKON)
        {
            LOG_D("zEakonThread::onEvent %d, open eakon.\n", ZM_IEMODEL);
            if (0 == m_quick_check_mode) // 非快检模式
            {
                blueLightDelay();
            }
        }
        else if (m_pManager->getEakonState() == zAlgManager::EakonState::CLOSED_EAKON)
        {
            LOG_D("zEakonThread::onEvent %d, close ekon.\n", ZM_IEMODEL);
            if (0 == m_quick_check_mode) // 非快检模式
            {
                closeLightDelay(true);
            }
        }
    }
    else if (ZM_DEKAKEMODEL == nEvent)
    {
        LOG_D("zEakonThread::onEvent %d \n", ZM_DEKAKEMODEL);
        if (0 == m_quick_check_mode) // 非快检模式
        {
            whiteLightDelay();
        }
    }
    else if (ZM_OPEN == nEvent)
    {
        if (m_pManager->getEakonModel() == zBaseUtil::zEakonModel::IE_MODEL)
        {
            LOG_D("zEakonThread::onEvent %d, IE_MODEL.\n", ZM_OPEN);
            if (0 == m_quick_check_mode) // 非快检模式
            {
                blueLightDelay();
            }
        }
    }
    else if (ZM_CLOSE == nEvent)
    {
        if (m_pManager->getEakonModel() == zBaseUtil::zEakonModel::IE_MODEL)
        {
            LOG_D("zEakonThread::onEvent %d, IE_MODEL \n", ZM_CLOSE);
            if (0 == m_quick_check_mode) // 非快检模式
            {
                closeLightDelay(true);
            }
        }
    }
    LOG_D(" zEakonThread::onEvent %d end \n", nEvent);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.print_buffer
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月4日
 * 函数功能  : 打印从MCU 读取和发送的信息 
 * 输入参数  : const char *header  缓存信息头
               const char *buffer  缓存
               int length          缓存长度
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::print_buffer(
    const char *header, const char *buffer, int length)
{
    #define MAX_BUFFER_LEN 2048
    #define PRINT_INTERVAL 30

    char mbuf[MAX_BUFFER_LEN];
    char * pbuf = mbuf;

    for (int i=0; i < length; i++)
    {
        if (0 == (i % PRINT_INTERVAL))
        {
            if (NULL != header)
            {
                pbuf += sprintf(pbuf, "\n%s,%3d: ", 
                    ((NULL != header) ? header : ""), i);
            }
        }
        pbuf += sprintf(pbuf, "%2x ", buffer[i]);
    }

    LOG_D("%s\n", mbuf);
    return;
}

int zEakonThread::taime()
{
    // write or read test if connected
    //std::unique_lock<std::mutex> lock(m_devMutex);
    zI2C::fromAirConditioner buf = { 0 };
    int nResult = readDevice((void*)&buf.buf, sizeof(zI2C::fromAirConditioner));
    // 调试打印
    print_buffer("mcu. read_buf", (const char *)buf.buf, sizeof(zI2C::fromAirConditioner));

    #if MCU_RESET_SWITCH
    if (is_reset_mcu_done())
    {
        if (0 != nResult)
        {
            m_mcu_read_error_cnt++;
            LOG_D("read mcu faild. m_mcu_read_error_cnt=%d.\n", m_mcu_read_error_cnt);
        }
        else if (m_mcu_read_error_cnt > 0)
        {
            LOG_D("restore mcu read. m_mcu_read_error_cnt=%d.\n", m_mcu_read_error_cnt);
            m_mcu_read_error_cnt = 0;
        }
    }
    #endif
    
    if ((0 == nResult) || m_bSimulateMCB)
    {
        nResult = analyzeFrom(buf);
    }

    #if MCU_RESET_SWITCH
    // 复位MCU 仲裁
    if (    (m_mcu_read_error_cnt >= MAX_MCU_RERROR_CNT)
        ||  (m_mcu_fault_cnt >= MAX_MCU_FAULT_CNT) )
    {
        (void)reset_mcu();

        m_mcu_read_error_cnt = 0;
        m_mcu_fault_cnt = 0;
    }
    #endif

    return nResult;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_last_error_quick_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月30日
 * 函数功能  : 获取快检模式下的状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::get_last_error_quick_mode()
{
    int err = getLastErr();
    //err &= (EAKON_ERR_EXIST | EAKON_CHIP_ERR | EAKON_VIDEO_ERR);
    err &= (EAKON_CHIP_ERR | EAKON_VIDEO_ERR);
    if (!m_connect_router_state)
    {
        err |= EAKON_NOT_CONNECT_AP;
    }
    return err;
    // return getLastErr() & (EAKON_ERR_EXIST | EAKON_CHIP_ERR | EAKON_VIDEO_ERR | EAKON_NOT_CONNECT_AP);
    // return getLastErr() & (EAKON_ERR_EXIST | EAKON_CHIP_ERR | EAKON_VIDEO_ERR);
}

/*****************************************************************************
 * 函 数 名  : set_light_quick_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月30日
 * 函数功能  : 设置快检模式下的LED灯模式,并发送状态值给MCU
 * 输入参数  : int err  故障状态
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::set_light_quick_mode(int err)
{
    int ret = 0;
    if (0 != err)
    {
        ret = blue_light_glint();
    }
    else
    {
        ret = blueLightDelay();
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.check_mcu
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 校验mcu运行是否正常
 * 输入参数  : zI2C::fromAirConditioner& buf  同mcu通信的, 读缓存（178字节）内容
 * 输出参数  : 无
 * 返 回 值  : int: 0 : ok, -1: err
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::check_mcu(zI2C::fromAirConditioner& buf)
{
    //LOG_D("enter. buf[19]=%d, buf[177]=%d\n", buf.buf[19], buf.buf[177]);
    if (    (19 == buf.buf[19]) 
        &&  (177 == buf.buf[177]))
    {
        return 0;
    }

    LOG_E("exception. buf[19]=%d, buf[177]=%d\n", buf.buf[19], buf.buf[177]);
    return -1;
}

/*****************************************************************************
 * 函 数 名  : is_reset_mcu_ing
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月3日
 * 函数功能  : 判断是否复位MCU不久，两次复位MCU的时间需超过阈值
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : false: 刚复位过不久， true: MCU处于正常态
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::is_reset_mcu_done()
{
    if (0 != m_last_reset_mcu_time)
    {
        std::int64_t elapse = zBaseUtil::getSecond() - m_last_reset_mcu_time;
        if (elapse < RESET_MCU_INTERVAL)
        {
            return false;
        }
        m_last_reset_mcu_time = 0;
    }
    return true;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.reset_mcu
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月11日
 * 函数功能  : 复位MCU，MCU会重新启动
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int: 0 : ok, -1: err
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::reset_mcu()
{
    (void)zBaseUtil::mcu_reset();
    m_last_reset_mcu_time = zBaseUtil::getSecond();
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.device_test_use_gateway
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月22日
 * 函数功能  : 判断工装Server的IP地址，用网关还是.2网段
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool: true: 使用网关地址,    false: 使用.2网段
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::device_test_use_gateway()
{
    return (0 == m_ssid_normal_mode.find("DEVICE_TEST"));
}

int zEakonThread::analyzeFrom(zI2C::fromAirConditioner& buf)
{
    static int nQuery = QUERY_RATE;
    int nResult = 0;

    // 用于仲裁是否需要更新wifi用户名和密码
    bool need_update_wifi_flag = false;
    bool need_close_wifi_flag = false;
    std::string wifi_ssid, wifi_passwd;

    // 上次wifi更新失败，尝试重新更新
    if (!m_update_wifi_done_flag)
    {
        need_update_wifi_flag = true;
        wifi_ssid = m_strSSID;
        wifi_passwd = m_strPasswd;
    }

    #if DEVICE_TEST_SIMULATE 
    buf.buf[43] = 0x0;
    buf.buf[44] = COM_FROM_BGN_CODE;
    buf.buf[45] = COM_FROM_STATE_INFO;
    buf.buf[46] |= (0x1 << 3) | (0x1 << 1);

    m_ssid_normal_mode = "DEVICE_TEST";
    m_passwd_normal_mode = "87654321";
    #endif

    // 工装和快检标志同时存在时，忽略快检标志
    if (    COM_FROM_BGN_CODE == buf.buf[44]
        &&  COM_FROM_STATE_INFO == buf.buf[45])
    {
        unsigned char flags = buf.buf[46];
        if (flags & (0x1 << 0))
        {
            if (flags & (0x1 << 3))
            {
                buf.buf[46] &= ~(0x1 << 0);
            }
        }
    }

    // err check
    if (0xff == buf.buf[43])
    {
        int nErr = zEakonThread::getLastErr();
        if (    !(nErr & EAKON_COMMU_ERR)
            ||  !(nErr & EAKON_ERR_EXIST))
        {
            nErr |= (EAKON_COMMU_ERR | EAKON_ERR_EXIST);
            zEakonThread::setLastErr(nErr);
            LOG_I("mcu&eakon communicate err! nErr=%d.\n", nErr);
        }

        /* add by 刘春龙, 2016-04-08, 原因: 进入工装后，后续没有工装标志，
           仍然继续执行，防止MCU和主控通信故障导致工装启动失败 */
        if (!m_bSimulateMCB && !m_device_test_mode)
        {
            return -1;
        }
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        if (nErr & (EAKON_COMMU_ERR | EAKON_ERR_EXIST))
        {
            nErr &= ~(EAKON_COMMU_ERR | EAKON_ERR_EXIST);
            zEakonThread::setLastErr(nErr);
            LOG_I("mcu&eakon communicate restore! nErr=%d.\n", nErr);
        }
    }

    #if MCU_RESET_SWITCH
    // 统计连续MCU 故障次数
    if ( is_reset_mcu_done())
    {
        if (0 != check_mcu(buf))
        {
            ++m_mcu_fault_cnt;
            LOG_D("increase. m_mcu_fault_cnt =%d\n", m_mcu_fault_cnt);
        }
        else if (0 != m_mcu_fault_cnt)
        {
            LOG_D("restore. m_mcu_fault_cnt =%d\n", m_mcu_fault_cnt);
            m_mcu_fault_cnt = 0;
        }
    }
    #endif

    (void)update_mcu_version(buf.buf[18]); // 更新版本

    if (    COM_FROM_BGN_CODE == buf.buf[44]
        &&  COM_FROM_STATE_INFO == buf.buf[45])
    {
        // eakon state
        bool airconditioner_opened = !!(buf.buf[49] & (0x1 << 6));
        if (airconditioner_opened != m_state_ac_opened)
        {
            m_state_ac_opened = airconditioner_opened;
            m_state_ac_coninue_count = 1;
            LOG_D("Airconditioner: airconditioner_opened=%d", airconditioner_opened);
        }
        else
        {
            #define AC_STATE_THD 2 // 阈值: 连续N次获取到空调状态未变，才改变空调状态。
            if (++m_state_ac_coninue_count >= AC_STATE_THD)
            {
                //LOG_D("Airconditioner state: m_state_ac_opened=%d", m_state_ac_opened);
                m_pManager->setEakonState(
                    m_state_ac_opened ? zAlgManager::EakonState::OPENED_EAKON 
                    : zAlgManager::EakonState::CLOSED_EAKON);
                --m_state_ac_coninue_count; // 防止越界
            }
        }

        bool wifi_enable_flag = !!(buf.buf[46] & (0x1 << 1));
        bool wifi_enable_flag_changed = false;
        if (m_wifi_enable_flag != wifi_enable_flag)
        {
            wifi_enable_flag_changed = true;
            m_wifi_enable_flag = wifi_enable_flag;
            LOG_I("m_wifi_enable_flag=%d\n", m_wifi_enable_flag);

            /* add by 刘春龙, 2016-03-30, 原因: wifi关闭时，停止录像 */
            if (!m_wifi_enable_flag)
            {
                zVideoThread::getInstance()->onEvent(ZM_ROKUSTOP);
            }

            /* add by 刘春龙, 2016-03-31, 原因: wifi关闭时，退出离家模式 */
            if (m_home_leave_mode)
            {
                zVideoThread::getInstance()->chgModel(zBaseUtil::zEakonModel::IE_MODEL);
                set_home_leave_mode(true);
                LOG_D("m_home_leave_mode=%d\n", m_home_leave_mode);
            }
        }

        if (m_wifi_enable_flag)
        {
            bool wifi_reconnect_flag = false;
            if (wifi_enable_flag_changed)
            {
                wifi_reconnect_flag = true;
            }
            else if ((zBaseUtil::getSecond() - m_wifi_connect_time) >= WIFI_RECONNECT_GAP)
            {
                // 时间超过WIFI_RECONNECT_GAP后，仍没有连接成功，尝试重连接
                if (    m_quick_check_mode 
                    ||  m_device_test_mode)
                {
                    if (    (m_nWifiState != wifiState::connect_router)
                        &&  (m_nWifiState != wifiState::connect_internet))
                    {
                        wifi_reconnect_flag = true;
                    }
                }
                else if (m_nWifiState != wifiState::connect_internet)
                {
                    wifi_reconnect_flag = true;
                }
            }
  
            if (    wifi_reconnect_flag
                &&  !m_strSSID.empty() 
                &&  !m_strPasswd.empty() )
            {
                //tryWifi(m_strSSID,m_strPasswd);
                need_update_wifi_flag = true;
                wifi_ssid = m_strSSID;
                wifi_passwd = m_strPasswd;

                m_wifi_connect_time = zBaseUtil::getSecond();
                LOG_D("WiFi model is opened.! m_nWifiState=%d \n", m_nWifiState);
            }
        }     
        else if (wifi_enable_flag_changed)
        {
            need_close_wifi_flag = true; // 关闭wifi连接
        }
    }

    if (m_strDevID.empty() && COM_TO_BGN_CODE == buf.buf[66] && COM_FROM_SN_CODE == buf.buf[67])
    {
        // dev id
        std::string strDevid = "";
        int nRec = buf.buf[68];
        LOG_D("nRec = %d \n", nRec);
        if (2 == nRec) // 32
        {
            unsigned char snbuf[32] = { 0x30 };
            memset(snbuf, 0x30, 32);

            //LOG_D("befor IDToBarcode \n");
            IDToBarcode(&snbuf[6], &buf.buf[70]);
            //LOG_D("end IDToBarcode \n");
            for (int i = 0; i < 32; i++)
            {
                //strDevid += (char)buf.buf[69 + i];
                //LOG_D("IDToBarcode result is: %x  char== %c \n", snbuf[i], snbuf[i]);
                strDevid += (char)snbuf[i];
            }
        }
        else if (1 == nRec)     // 32
        {
            //LOG_D("1 == nRec \n");
            int nLen = buf.buf[69];
            if (nLen > 32) { 
                LOG_E("nLen=%d\n", nLen);
                nLen = 32;
            }

            for (int i = 0; i < nLen; i++)
            {
                #if 1 // ascii码
                strDevid += (char)buf.buf[70 + i];
                #else
                LOG_D("%d\n", (char)buf.buf[70 + i]);

                char buf_t[32];
                sprintf(buf_t, "%02d", buf.buf[70 + i]);
                strDevid += buf_t;
                #endif
            }
        }

        LOG_I("dev id == %s \n", strDevid.c_str());
        m_strDevID = strDevid;
        (void)zBaseUtil::write_deviceid_file(strDevid);
    }
    // p2p logic

    //LOG_D("m_strDevID=%s, m_p2pThread=%p, m_nWifiState=%d\n", m_strDevID.c_str(), m_p2pThread, m_nWifiState);
    if (    !m_strDevID.empty()
        &&  (NULL == m_p2pThread))
    {
        if (m_nWifiState == wifiState::connect_internet)
        {
            LOG_I("$$$$$startP2P !!\n");
            startP2P();
        }
    }
    //else if (m_strDevID.empty() && nQuery == 0)
    else if (m_strDevID.empty())
    {
        //LOG_D("befor getDevid \n");
        getDevid();
        //LOG_D("end getDevid \n");
    }

    if ((COM_FROM_BGN_CODE == buf.buf[125] && COM_FROM_WIFI_CODE == buf.buf[126])
        || m_bSimulateMCB)
    {
        while(1) // 用于跳出, 相当于设置一个goto标记
        {
            // ssid  pwd
            std::string strSsid   = "";
            std::string strPasswd = "";

            // 模拟开关打开时，使用配置文件的wifi用户名和密码
            if (m_bSimulateMCB)
            {
                strSsid   = m_simulate_wifi_ssid  ;
                strPasswd = m_simulate_wifi_passwd;
            }
            else
            {
                m_ucWifiType = buf.buf[127];
                m_ucWifiDetype = buf.buf[128];
                int ssidLen = buf.buf[129];
                int pswLen = buf.buf[130];

                int max_len = FROMEAKON_BUF_SIZE - 131 - 2; // 2: 校验和 & 结束码
                if (ssidLen + pswLen > max_len) // 越界校验
                {
                    LOG_E("Too long: ssidLen=%, pswLen=%d, max_len=%d\n", ssidLen, pswLen, max_len);
                    break; // 退出while
                }

                for (int i = 0; i < ssidLen; i++)
                {
                    strSsid += (char)buf.buf[131 + i];
                }

                for (int i = 0; i < pswLen; i++)
                {
                    strPasswd += (char)buf.buf[131 + ssidLen + i];
                }
            }

            #if DEVICE_TEST_SIMULATE
            strSsid = m_ssid_normal_mode;
            strPasswd = m_passwd_normal_mode;
            #endif
            
            // 保存正常模式下的用户名密码
            if (    m_strSSID != m_ssid_normal_mode
                ||  m_strPasswd != m_passwd_normal_mode)
            {
                m_ssid_normal_mode = m_strSSID;
                m_passwd_normal_mode = m_strPasswd;
            }

            if (!m_quick_check_mode)
            {
                LOG_D("m_strSSID=%s, strSsid=%s, m_nWifiState=%d \n", m_strSSID.c_str(), strSsid.c_str(), m_nWifiState);
                if (m_strSSID != strSsid)
                {
                    // assert 
                    LOG_I("ssid chg !!\n");
                    m_fault_flag &= (~EAKON_WIFI_ERR); // 强制重新处理wifi故障
                }

                if (    m_strSSID != strSsid
                    ||  m_strPasswd != strPasswd)
                {
                    //(void)tryWifi(strSsid, strPasswd);
                    need_update_wifi_flag = true;
                    wifi_ssid = strSsid;
                    wifi_passwd = strPasswd;
                }
            }

            break; // 退出while
        }
    }

    // EAKON_NO_ROUTERINFO if need  add || m_strDevID.empty()
    if (m_strSSID.empty() || m_strDevID.empty())
    {
        int nErr = zEakonThread::getLastErr();
        if (!(nErr & EAKON_NO_ROUTERINFO))
        {
            nErr |= EAKON_NO_ROUTERINFO;
            zEakonThread::setLastErr(nErr);
        }
    }
    else
    {
        int nErr = zEakonThread::getLastErr();
        if (nErr & EAKON_NO_ROUTERINFO)
        {
            nErr &= ~EAKON_NO_ROUTERINFO;
            zEakonThread::setLastErr(nErr);
        }
    }

    // query wifi ssid
    /* add by 刘春龙, 2017-01-16, 原因: 
    N秒循环查询wifi用户名和密码，防止当wifi用户名和密码变更标志没有收到，引起联网失败问题 */
    //if (m_strSSID.empty())
    {
        std::int64_t curr_time = zBaseUtil::getSecond();
        if ((curr_time - m_wifi_query_time) >= m_wifi_query_gap)
        {
            if (m_wifi_query_first_flag)
            {
                if (!m_device_test_mode_before_acquire_wifi) // 非工装
                {
                    m_wifi_query_gap = WIFI_QUERY_INTERVAL;
                }
                else
                {
                    m_wifi_query_gap = WIFI_QUERY_INTERVAL_DEVICE_TEST;
                }
                m_wifi_query_first_flag = false;
            }

            //LOG_D("curr_time=%lld, m_wifi_query_time=%lld\n", curr_time, m_wifi_query_time);
            (void)getSsid();
            m_wifi_query_time = curr_time;
        }
    }

    if (COM_FROM_BGN_CODE == buf.buf[103] && COM_FROM_CONFIG_INFO == buf.buf[104])
    {
        const unsigned char ucF = buf.buf[105];
        const unsigned char ucFunc2 = buf.buf[106];

        //智能总功能
        //bool itelligent_switch_enable = !(ucFunc2 & (0x1 << 3));
        bool itelligent_switch_enable = true;
        LOG_D("ucF=0x%x, ucFunc2=0x%x, itelligent_switch_enable=%d\n", 
            ucF, ucFunc2, itelligent_switch_enable);
        if (itelligent_switch_enable != m_itelligent_switch_enable)
        {
            m_itelligent_switch_enable = itelligent_switch_enable;
        }

        // 手势识别开关机, 1：使能, 0：禁止
        bool gesture_ac_switch_flag = !!(ucF & (0x1 << 4));
        // 手势控制定向送风功能, 1：使能,0：禁止
        bool gesture_ac_wind_flag = !!(ucFunc2 & (0x1 << 2));
        
        if (itelligent_switch_enable && gesture_ac_wind_flag)
        {
            m_gesture_flag = GESTURE_AC_WIND;
        }
        else if (gesture_ac_switch_flag)
        {
            m_gesture_flag = GESTURE_AC_SWITCH;
        }
        else 
        {
            m_gesture_flag = GESTURE_DISABLE;
        }

        //m_gesture_flag = GESTURE_AC_WIND;
        LOG_D("m_gesture_flag=%d\n", m_gesture_flag.load());

        if (!m_bSimulateMCB)
        {
            // 远实时视频功能
            bool live_video_flag = !!(ucF & (0x1 << 0));
            if (m_live_video_switch != live_video_flag)
            {
                m_live_video_switch = live_video_flag;
                LOG_I("m_live_video_switch =%d\n", m_live_video_switch);

                /* add by 刘春龙, 2016-03-30, 原因: 关闭远程视频时，停止录像 */
                if (!m_live_video_switch)
                {
                    zVideoThread::getInstance()->onEvent(ZM_ROKUSTOP);
                }
            }
        }
        //m_live_video_switch = true;
        LOG_D("m_live_video_switch=%d\n", m_live_video_switch);

        // 手势定向风的优先级高于 头肩智能风
        unsigned char ucTmp = ucF & 0xc;
        //LOG_D("ucTmp == %2x \n", ucTmp);
        if (ucTmp & 0x04) // 0x04, 0x0c
        {
            m_k = zEakonThread::kazeMuki::k_focuse;
        }
        else if (0x08 == ucTmp)
        {
            m_k = zEakonThread::kazeMuki::k_unfocuse;
        }
        else if (0x00 == ucTmp)
        {
            m_k = zEakonThread::kazeMuki::k_unknown;
        }

        //m_k = kazeMuki::k_focuse;
        LOG_D("smart_wind =%d\n", m_k);

        // 入侵检测使能配置: 智能风优先级高于安防
        bool leave_mode = !!(ucF & (0x1 << 1));
        /* add by 刘春龙, 2016-03-31, 原因: wifi关闭的情况下，忽略离家标志 */
        if (    (zEakonThread::kazeMuki::k_unknown != m_k)
            ||  (GESTURE_DISABLE != m_gesture_flag )
            ||  !m_wifi_enable_flag )
        {
            leave_mode = false;
        }

        //leave_mode = true;
        if (m_home_leave_mode != leave_mode)
        {
            zVideoThread::getInstance()->chgModel(
                leave_mode ? zBaseUtil::zEakonModel::DEKAKE_MODEL : zBaseUtil::zEakonModel::IE_MODEL);
            set_home_leave_mode(!leave_mode);
        }
        LOG_D("m_home_leave_mode=%d\n", m_home_leave_mode);
        
        #if 0 // 使用默认值，忽略空调主板传入值。
        if (    0 != buf.buf[108]
            &&  0 != buf.buf[109])
        {
            // 使用默认的、不使用他们的！！！
            if (m_limit_angle_left != buf.buf[108])
            {
                m_limit_angle_left = buf.buf[108];
            }

            if (m_limit_angle_right != buf.buf[109])
            {
                m_limit_angle_right = buf.buf[109];
            }
            LOG_D("m_limit_angle_left == %d, m_limit_angle_right == %d\n",
                m_limit_angle_left, m_limit_angle_right);
        }
        #endif

        // 获取温度和角度
        {
            bool changed = false;
            int t_idx = 110; // 温度索引
            int a_idx = 113; // 角度索引
            for(int i=0; i < heat_source_count; i++)
            {
                if (m_temperatures[i] != buf.buf[t_idx + i])
                {
                    m_temperatures[i] = buf.buf[t_idx + i];
                    changed = true;
                }

                if (m_angles[i] != buf.buf[a_idx + i])
                {
                    m_angles[i] = buf.buf[a_idx + i];
                    changed = true;
                }
            }

            if (changed) // P2P通知
            {
                if (    "" != zP2PThread::m_deviceId
                    &&  (m_heat_source_enable))
                {
                    std::string res = make_heat_source_json_response(
                        m_temperatures, m_angles, heat_source_count);
                    notifyWebrtc(res);
                    LOG_D("heat_souces: %s \n", res.c_str());
                }
            }
        }

        bool wifi_ssid_change_flag = !!(ucFunc2 & (0x1 << 1));
        if (m_wifi_ssid_change_flag != wifi_ssid_change_flag) // wifi密码变更标志
        {
            m_wifi_ssid_change_flag = wifi_ssid_change_flag;
            if (m_wifi_ssid_change_flag)
            {
                {
                    std::unique_lock<std::mutex> lock(m_ssidMutex);
                    m_strSSID = "";
                    m_wifi_query_time = 0;
                }
                LOG_D("ssid change, m_wifi_query_time=%lld\n", m_wifi_query_time);
            }
        }
    }

    // 如果第88位为0xff，进入快检模式，如果为0x00，退出快检模式
    if (COM_FROM_BGN_CODE == buf.buf[44] && COM_FROM_STATE_INFO == buf.buf[45])
    {
        int check_mode = buf.buf[46] & (0x1 << 0);
        if (check_mode != m_quick_check_mode)
        {
            if (0 != check_mode)
            {
                int curr_error = get_last_error_quick_mode();
                (void)set_light_quick_mode(curr_error);
                (void)send_state_quick_mode(curr_error);

                //LOG_D("analyzeFrom: curr_error=0x%x \n", curr_error);
                m_last_error = curr_error;
            }
            else
            {
                recoveryLightV2();
            }

            m_quick_check_mode = check_mode;
            LOG_I("m_quick_check_mode= %d\n", m_quick_check_mode);
        }

        if (m_quick_check_mode)
        {
            // 快检模式，使用定死的 密码
            if (m_strSSID != m_ssid_quick_mode)
            {
                LOG_I("quick_mode: ssid chg. old=%s, new=%s !!\n", 
                    m_strSSID.c_str(), m_ssid_quick_mode.c_str());
                m_fault_flag &= (~EAKON_WIFI_ERR); // 强制重新处理wifi故障
            }

            if (    m_strSSID != m_ssid_quick_mode
                ||  m_strPasswd != m_passwd_quick_mode)
            {
                //(void)tryWifi(m_ssid_quick_mode, m_passwd_quick_mode);
                need_update_wifi_flag = true;
                wifi_ssid = m_ssid_quick_mode;
                wifi_passwd = m_passwd_quick_mode;
            }
        }
    }

    // 判断，工装(进货检) 模式 -- 注: 快检优先级高。
    /* add by 刘春龙, 2016-04-08, 原因: 进入工装后，后续没有工装标志，
    仍然继续执行，防止MCU和主控通信故障导致工装启动失败 */
    if (    (COM_FROM_BGN_CODE == buf.buf[44] && COM_FROM_STATE_INFO == buf.buf[45])
        ||  m_device_test_mode)
    {
        // 唤醒工装线程: wifi重连后，不考虑路由是否通，仅执行完wifi_connect进程。
        if (    m_device_test_mode
            &&  m_need_wakeup_device_test_flag
            &&  m_update_wifi_done_flag)
        {
            // 执行wifi_connect完毕后，才唤醒工装，否则，可能获取错误的网关地址
            zWifiConnect* pConnect = zWifiConnect::getInstance();
            if (pConnect->is_wifi_connect_done())
            {
                DtThread *pDt = DtThread::getInstance();
                if (NULL != pDt)
                {
                    bool use_gateway = device_test_use_gateway();
                    pDt->WakeUp(true, use_gateway);
                    m_need_wakeup_device_test_flag = false;
                    LOG_D("device test: wakeup. use_gateway=%d\n", use_gateway);
                }
            }
        }

        bool device_test_mode = !!(buf.buf[46] & (0x1 << 3));
        m_device_test_mode_before_acquire_wifi = device_test_mode;
        if (m_device_test_mode_before_acquire_wifi)
        {
            m_wifi_query_gap = WIFI_QUERY_INTERVAL_DEVICE_TEST;
        }
        //LOG_D("m_device_test_mode_before_acquire_wifi=%d\n", m_device_test_mode_before_acquire_wifi);

        // 进入工装前，必须获取到WIFI用户名和密码
        if (    !m_quick_check_mode
            &&  m_device_test_mode != device_test_mode
            &&  !m_ssid_normal_mode.empty()
            &&  !m_passwd_normal_mode.empty())
        {
            m_device_test_mode = device_test_mode;
            LOG_D("m_device_test_mode=%d\n", m_device_test_mode);

            if (device_test_mode)
            {
                m_need_wakeup_device_test_flag = true;

                if (    m_strSSID != m_ssid_normal_mode
                    ||  m_strPasswd != m_passwd_normal_mode)
                {
                    //(void)tryWifi(m_ssid_device_test_mode, m_passwd_device_test_mode);
                    need_update_wifi_flag = true;
                    wifi_ssid = m_ssid_normal_mode;
                    wifi_passwd = m_passwd_normal_mode;
                }
            }
            else
            {
                // 阻塞工装线程
                DtThread *pDt = DtThread::getInstance();
                if (NULL != pDt)
                {
                    pDt->Wait();
                    m_need_wakeup_device_test_flag = false;
                    LOG_D("device test: wait.\n");
                }
            }
        }
    }

    // 仲裁是否需要更新wifi用户名和密码
    if (    need_close_wifi_flag 
        &&  !m_quick_check_mode
        &&  !m_device_test_mode )
    {
        zWifiConnect* pConnect = zWifiConnect::getInstance();
        if (NULL != pConnect)
        {
            if (!pConnect->IsConnecting())
            {
                (void)pConnect->disconnect();
            }
        }
    }
    else if (need_update_wifi_flag)
    {
        if (0 == tryWifi(wifi_ssid, wifi_passwd))
        {
            m_update_wifi_done_flag = true;
            m_wifi_connect_time = zBaseUtil::getSecond();

            // 复位wifi状态
            zWifiStateThread* pWifiState = zWifiStateThread::getInstance();
            if (NULL != pWifiState)
            {
                pWifiState->reset_wifi_state();
            }
        }
        else 
        {
            m_update_wifi_done_flag = false;
        }
    }

    nQuery = (1 > nQuery) ? QUERY_RATE : (nQuery - 1);
    //LOG_I("nQuery == %d, m_strDevID == %s", nQuery, m_strDevID.c_str());
    return nResult;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.tryWifi
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月17日
 * 函数功能  : WIFI重连
 * 输入参数  : std::string strSsid  wifi 用户名
               std::string strPswd  wifi 密码
 * 输出参数  : 无
 * 返 回 值  : int: 0: ok, -1:未重连
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::tryWifi(std::string strSsid, std::string strPswd)
{
    int ret = -1;

    {
        // 保存wifi用户名和密码
        std::unique_lock<std::mutex> lock(m_ssidMutex);
        m_strSSID = strSsid;
        m_strPasswd = strPswd;
    }

    zWifiConnect* pConnect = zWifiConnect::getInstance();
    if (NULL != pConnect)
    {
        pConnect->init(strSsid, strPswd);
        ret = pConnect->startConnect();
        LOG_D("try wifi ssid=%s, pswd=%s, ret=%d\n", strSsid.c_str(), strPswd.c_str(), ret);
    }

    return ret;
}

int zEakonThread::startP2P()
{
    std::string strdDevID = m_strDevID;
    if (strdDevID.empty())
    {
        LOG_E("startP2P cannot use the Empty dev id!!!!!!!!! \n");
        return -1;
    }

    LOG_I("zP2PThread::start befor\n");
    zThread* pThread = new zP2PThread(this, strdDevID.c_str());
    if (NULL != pThread)
    {
        pThread->Start();
        m_p2pThread = (zP2PThread*)pThread;
        LOG_I("zP2PThread::start end\n");
    }
    return 0;
}

int zEakonThread::getLightColor()
{
    int nColor = 0;
    int nErr = getLastErr();
    //LOG_D("zEakonThread::getLightColor nErr == %d \n", nErr);
    if (nErr) return nColor;
    int nState = m_pManager->getEakonState();
    zBaseUtil::zEakonModel nModel = m_pManager->getEakonModel();
    LOG_D("zEakonThread::getLightColor nErr == %d, nState == %d, nModel == %d \n", nErr, nState, (int)nModel);
    if (zBaseUtil::zEakonModel::IE_MODEL == nModel)
    {
        if (nState == zAlgManager::EakonState::OPENED_EAKON)
        {
            nColor = 2;
        }
        else if (nState == zAlgManager::EakonState::CLOSED_EAKON)
        {
            nColor = 0;
        }
    }
    else if (zBaseUtil::zEakonModel::DEKAKE_MODEL == nModel)
    {
        nColor = 3;
    }
    return nColor;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.ssid
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月22日
 * 函数功能  : 获取SSID
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::GetWifiInfo(std::string& ssid, std::string& passwd)
{
    std::unique_lock<std::mutex> lock(m_ssidMutex);
    ssid = m_strSSID;
    passwd = m_strPasswd;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_quick_check_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 获取快检模式
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::get_quick_check_mode()
{
    return m_quick_check_mode;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_device_test_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月25日
 * 函数功能  : 获取工装模式
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_device_test_mode()
{
    return m_device_test_mode;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.set_router_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 设置路由连接状态，用于快检模式
 * 输入参数  : int is_connect  
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::set_router_state(int is_connect)
{
    m_connect_router_state = is_connect;
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_live_video_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月5日
 * 函数功能  : 获取"远程实时视频功能"开关配置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool: true: 使能， false: 禁止
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_live_video_state()
{
    return m_live_video_switch;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_itelligent_switch
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 获取智能总开关配置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool: true: 使能， false: 禁止
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_itelligent_switch()
{
    return m_itelligent_switch_enable;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.GetWifiInfo
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月8日
 * 函数功能  : 获取设备ID
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std::string，设备ID号
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::string zEakonThread::GetDevID()
{
    return m_strDevID;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_eakon_opened
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月25日
 * 函数功能  : 获取空调开 & 关状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_eakon_opened()
{
    return m_eakon_opened;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.set_gesture_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月25日
 * 函数功能  : 标志值
 * 输入参数  : uint32_t flag  设置手势功能标志位
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::set_gesture_flag(uint32_t flag)
{
    if (flag <= GESTURE_AC_WIND)
    {
        if (m_gesture_flag != flag)
        {
            m_gesture_flag = flag;
            (void)notify_intelligent_state();
        }
    }
    else
    {
        LOG_E("flag= %u\n", flag);
    }
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_gesture_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月25日
 * 函数功能  : 获取手势功能标志位
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : uint32_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
uint32_t zEakonThread::get_gesture_flag()
{
    return m_gesture_flag;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_gesture_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 获取手势状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : uint32_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
uint32_t zEakonThread::get_gesture_state()
{
    if (GESTURE_AC_WIND == m_gesture_flag)
    {
        uint32_t gesture, raise_count;
        m_pManager->get_gesture_flags(gesture, raise_count);
        if (GESTURE_AC_WIND == gesture)
        {
            if (    (0 != raise_count)
                &&  !(raise_count & 0x1))
            {
                return m_gesture_flag + 1; // 手势定向风 (智能风 或 自动扫风)
            }
        }
    }
    return m_gesture_flag;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.set_intelligent_enable_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 设置智能开关
 * 输入参数  : bool flag  智能标志
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::set_intelligent_enable_flag(bool flag)
{
    if (m_intelligent_enable != flag)
    {
        m_intelligent_enable = flag;
        (void)notify_intelligent_state();
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_intelligent_enable_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 获取智能开关
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_intelligent_enable_flag()
{
    return m_intelligent_enable;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.set_heat_source_enable_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年6月17日
 * 函数功能  : 设置热源告警开关标志
 * 输入参数  : bool flag  
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::set_heat_source_enable_flag(bool flag)
{
    m_heat_source_enable = flag;

    #if 1
    if (m_heat_source_enable)
    {
        std::string res = make_heat_source_json_response(
            m_temperatures, m_angles, heat_source_count);
        notifyWebrtc(res);
        LOG_D("heat_souces: %s \n", res.c_str());
    }

    #else // 热源测试代码
    if (m_heat_source_debug)
    {
        if (    "" != zP2PThread::m_deviceId))
            &&  (m_heat_source_enable))
        {
            for (int i=0; i<heat_source_count; i++)
            {
                m_temperatures[i] = zBaseUtil::randint(time(0) + 100 + i, 0, 255);
                m_angles[i] = zBaseUtil::randint(time(0) + 200 + i, 0, 90);
            }

            std::string res = make_heat_source_json_response(
                m_temperatures, m_angles, heat_source_count);
            notifyWebrtc(res;
            LOG_D("heat_souces: %s \n", res.c_str());
        }
    }
    #endif

    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_heat_source_enable_flag
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年6月17日
 * 函数功能  : 获取热源告警开关标志
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::get_heat_source_enable_flag()
{
    return m_heat_source_enable;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.temperature_increase
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月27日
 * 函数功能  : 温度加
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::temperature_increase()
{
    LOG_E("\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    info.buf[3] = 0x13;
    
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_TEMPERATURE;
    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.temperature_decrease
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月27日
 * 函数功能  : 温度减
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::temperature_decrease()
{
    LOG_E("\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    info.buf[3] = 0x14;
    
    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_TEMPERATURE;
    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.ZoneStayTimeup
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月27日
 * 函数功能  : 执行动作
 * 输入参数  : ZONE_POS zone  悬停局域
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::ZoneStayTimeup(ZONE_POS zone)
{
    if (TOP_ZONE == zone)
    {
        zP2PThread::print_msg( "temp inc");
        // 温度加
        return temperature_increase();
    }
    else if (BOTTOM_ZONE == zone)
    {
        zP2PThread::print_msg("temp dec");
        // 温度减
        return temperature_decrease();
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.get_intelligent_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年8月10日
 * 函数功能  : 返回智能状态值
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : char
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
char zEakonThread::get_intelligent_state()
{
    char state_value = 0;

    if (wifiState::connect_internet == m_nWifiState)
    {
        state_value |= (0x01 << 0); // wifi 已连接
    }

    if (m_home_leave_mode)
    {
        state_value |= (0x01 << 1); // 安防功能开启
    }
    else if (m_intelligent_enable)  // 智能开关开
    {
        if (GESTURE_AC_SWITCH == m_gesture_flag)
        {
            state_value |= (0x01 << 2); // 手势开关机功能开启
        }
        else if (GESTURE_AC_WIND == m_gesture_flag)
        {
            state_value |= (0x01 << 3); // 手势定向风功能开启
        }

        if (zEakonThread::kazeMuki::k_focuse == m_k)
        {
            state_value |= (0x01 << 4); // 智能风吹人功能已开启
        }
        else if (zEakonThread::kazeMuki::k_unfocuse == m_k)
        {
            state_value |= (0x01 << 5); // 智能风避人功能已开启
        }
    }

    return state_value;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.notify_intelligent_state
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年8月10日
 * 函数功能  : 发送智能状态值
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zEakonThread::notify_intelligent_state()
{
    LOG_E("\n");
    zI2C::toAirConditioner info = { 0 };
    info.tagInfo.header.chProHead = 0x02;
    info.buf[1] = COM_TO_PROTOCOL_VERSION;
    info.buf[2] = 0x00;
    info.buf[3] = 0x81;
    info.buf[4] = get_intelligent_state();
    LOG_D("state= 0x%x\n", info.buf[4]);

    zAirConditionerMsg msg;
    msg.pBuf = info;
    msg.msg = ZM_TEMPERATURE; // 暂借用温度命令
    return m_msgQueue.addMsg(msg);
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.update_mcu_version
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月15日
 * 函数功能  : 统计MCU版本, 判断是否为新版MCU
 * 输入参数  : uint8_t version  MCU 版本号
 * 输出参数  : 无
 * 返 回 值  : void: 更新m_is_new_mcu_version
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zEakonThread::update_mcu_version(uint8_t version)
{
    #define NEW_MCU_VERSION 0x01
    #define MAX_COUNT_THD   1000
    #define MAX_DIFF_THD    10 // 最大差值

    bool update_flag = false;
    bool new_version = (version >= NEW_MCU_VERSION);
    if (new_version)
    {
        if (m_mcu_version_count < MAX_COUNT_THD)
        {
            ++m_mcu_version_count;
            update_flag = true;
        }
    }
    else
    {
        if (m_mcu_version_count > -MAX_COUNT_THD)
        {
            --m_mcu_version_count;
            update_flag = true;
        }
    }

    if (update_flag)
    {
        if (m_mcu_version_count > MAX_DIFF_THD)
        {
            m_is_new_mcu_version = true;
        }
        else if (m_mcu_version_count < -MAX_DIFF_THD)
        {
            m_is_new_mcu_version = false;
        }
        else
        {
            m_is_new_mcu_version = new_version;
        }
        LOG_D("mcu version: %s", m_is_new_mcu_version ? "new" : "old");
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.is_new_mcu_version
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月15日
 * 函数功能  : 是否为新版MCU
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::is_new_mcu_version()
{
    return m_is_new_mcu_version;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.check_airconditioner_info
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月15日
 * 函数功能  : 校验接受的信息内容是否正确
 * 输入参数  : zI2C::fromAirConditioner& buf  消息缓存
 * 输出参数  : 无
 * 返 回 值  : bool, true: 正确, false: 错误
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::check_airconditioner_info(const zI2C::fromAirConditioner& buf)
{
    #define INFO_LENGTH 22 
    #define INFO_INDEX 44 // 信息起始位: 数组下标

    bool ret = true;
    if (is_new_mcu_version())
    {
        ret = zBaseUtil::crc_checksum_with_endcode(&buf.buf[INFO_INDEX], INFO_LENGTH);
    }

    if (!ret)
    {
        LOG_E("error info: is_new_mcu_version=%d", is_new_mcu_version());
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zEakonThread.check_ac_config_info
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月16日
 * 函数功能  : 校验主控发送的配置信息
 * 输入参数  : const zI2C::fromAirConditioner& buf  消息缓存
 * 输出参数  : 无
 * 返 回 值  : bool: true: 正确, false: 错误
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zEakonThread::check_ac_config_info(const zI2C::fromAirConditioner& buf)
{
    #define CONFIG_INFO_LENGTH 22
    #define CONFIG_INFO_INDEX 103 // 信息起始位: 数组下标

    bool ret = true;
    if (is_new_mcu_version())
    {
        ret = zBaseUtil::crc_checksum_with_endcode(&buf.buf[CONFIG_INFO_INDEX], CONFIG_INFO_LENGTH);
    }

    if (!ret)
    {
        LOG_E("error info: is_new_mcu_version=%d", is_new_mcu_version());
    }
    return ret;
}

