#include <stdlib.h>
#include <stdio.h>

#include "zVideoThread.h"
#include "zEakonThread.h"
#include "config_mgr.h"
#include "zUtil.h"
#include "zLog.h"
#include "device_test.h"
#include "process_start.hpp"

// 版本号
#define VERSION "CG_V100R001B001SP08"

int main(void)
{
    ProcessStartCount *proc_cnt = ProcessStartCount::get_instance();

    LOG_I("main enter. count=%d\n", proc_cnt->count());
    //zLog::zLog2File_begin();  // 写日志到日志文件

    //std::this_thread::sleep_for(std::chrono::seconds(1));
    LOG_I("Date: %s, Time: %s, Version: %s\n", __DATE__, __TIME__, VERSION);

    (void)zBaseUtil::mcu_reset();
    (void)zBaseUtil::program_begin();
    
    DtThread::dt_set_otg_mode(OTG_MODE_DEVICE);
    DtThread::dt_set_start_time();

    DtThread* pDt = DtThread::getInstance();
    pDt->Start();
    LOG_I("device test booted.\n");

    zFileUtil::zFileManager *fm = zFileUtil::zFileManager::getInstance();
    if (NULL != fm)
    {
        fm->init_sync_time();
    }

    zVideoThread* pVideo = zVideoThread::getInstance();
    pVideo->init(0);
    pVideo->Start();

    zBaseUtil::startMCUEvent();

    int nEvent = ZM_INVALID;
    while (!pVideo->Is(zThread::DEAD_THREAD))
    {
        nEvent = zBaseUtil::getMCUEvent();
        if (ZM_INVALID != nEvent)
        {
            LOG_I("#####nEvent %d\n", nEvent);
            pVideo->onEvent(nEvent);
        }
    }
    zBaseUtil::endMCUEvent();
    LOG_I("zThread::DEAD_THREAD end\n");

    pDt->destroyInstance();
    pVideo->destroyInstance();

    // 程序退出, 复位OTG模式
    DtThread::dt_set_otg_mode(OTG_MODE_AUTO);
    proc_cnt->destroy_instance();

    LOG_I("main exit.\n");
    return 0 ;
}

