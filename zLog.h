#ifndef __ZLOG_H__
#define __ZLOG_H__

#include <stdarg.h>
#include <stdio.h>
#include <android/log.h>

#include "p2pThread.h"

#define DEBUG_BUFFER_MAX_LENGTH (1024)


//#define LOG_D(...) 
//#define LOG_I(...) 
//#define LOG_E(...) 

// 添加 __LINE__ 宏打印，会导致程序挂掉，原因尚不清楚，可能与传入的参数个数有关
#define LOG_D(format, ...) zLog::zLogPrint_debug(__FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define LOG_I(format, ...) zLog::zLogPrint_info (__FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)
#define LOG_E(format, ...) zLog::zLogPrint_error(__FILE__, __FUNCTION__, __LINE__, format, ##__VA_ARGS__)

#define ZDEBUGlOG(...)
//#define ZDEBUGlOG(format, ...) zLog::zLogPrint_debug(__FILE__, __FUNCTION__, format, ##__VA_ARGS__)


namespace zLog
{
    int zLogPrint(const char *format, ...);
    
    int zLogPrint_debug(const char *filename, const char *func, const int line, const char *format, ...);
    int zLogPrint_info (const char *filename, const char *func, const int line, const char *format, ...);
    int zLogPrint_error(const char *filename, const char *func, const int line, const char *format, ...);

    void zLog2File_begin();
    void zLog2File_end();
    int save_prev_logfile();
    int remove_logfile();
    int merge_logfile();
    int upload_logfile(const std::string& render);
}
#endif
