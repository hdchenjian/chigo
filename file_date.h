#ifndef __RW_FILE_DATE_HPP__
#define __RW_FILE_DATE_HPP__

#include <sstream>
#include <vector>
#include <string>
#include <chrono>
#include <time.h>

#include "p2pThread.h"
#include "zLog.h"

namespace RW {

class file_date 
{
public:
    file_date()
        : valid_(false) 
    {
    }

    file_date(int y, int m, int d, int h, int min, int s)
        : valid_( true)
        , year_(y)
        , month_(m)
        , day_(d)
        , hour_(h)
        , minute_(min)
        , second_(s)
    {
    }

    // string example : 2016-01-01_12-00-00
    file_date(const std::string &date);

    int compare(const file_date & date) const;
    std::string get_date_string() const;
    std::time_t to_time_t() const;

    // 默认构造，为无效日期。 有参构造，有效。
    bool is_valid() const
    {
        return valid_;
    }

private:
    static std::tm & mk_timeinfo( std::tm &timeinfo, 
        int year, int month, int day, int hour, int minute, int second);

private:
    bool valid_  = false;
    int year_    = 0;
    int month_   = 0;
    int day_     = 0;
    int hour_    = 0;
    int minute_  = 0;
    int second_  = 0;
};
}
#endif
