#include <string>
#include <vector>
#include <algorithm>
#include <math.h>

#include "3rdParty/FaceRecov2/cFaceProc.h"
#include "zAlgorithmDetectThread.h"
#include "config_mgr.h"
#include "zAlgorithmManager.h"
#include "p2pThread.h"
#include "zVideoThread.h"
#include "zEakonThread.h"
#include "device_test.h"
#include "zRokugaThread.h"
#include "zLog.h"

//////////////////////////////////////////////////////////////////////////
#define GETFRAME_GAP_0 (10)     // 单位: 毫秒
#define GETFRAME_GAP_1 (1)      // 单位: 毫秒
#define ANALYZE_FRAME_GAP (40)  // 单位: 毫秒
#define ALG_INIT_GAP 1000       // 算法初始化失败重试间隔，单位: 毫秒

#define GDEFAULT_ALARM_GAP (10 * G2IXX) // 单位: 毫秒 
#define GKEIKAIKANKAKU (10)
#define GANGLEDIFF (10)

#define MIN_LIGHTING_DURATION (10*60*1000) // 开补光，至少10分钟
#define THD_LIGHTING_DURATION (20*1000) // 切换补光灯，大于阈值MAX_LIGHTING_THD的持续时间: 单位: 毫秒

#define GLIGHTINGF (0.2)
#define GLIGHTINGC (0.5) /* modify by 刘春龙, 2015-12-01, 原因: 0.5有些小，容易误判 */
#define MAX_LIGHTING_THD (0.98) // 非常亮
#define MAX_BRIGHTNESS 100 // 补光最大亮度

#define VOICE_INTERVAL_TIME 500 // 0.5秒 -- 500毫秒
#define GESTURE_TIME 1000 // 1秒 -- 1000毫秒
#define GESTURE_LOSE_TIME 3000 // 3秒

// 连续检测到多次手势丢失，才认为手势丢失，规避算法跟踪阶段即使手没有丢失也会返回 "手势丢失"的问题
#define GTECNT (2)

#define GFACE_BODY_ADJUST_DIS (177)
#define GFACE_BODY_MIN_DIS (20)
#define GFACE_BODY_MAX_DIS (800)

// type=0： 开手势，type=1: 闭手势(握拳)，type=-1：背景（在开闭手势切换，和手消失后跟踪丢失阶段都会出现type = -1)
#define GESTURE_TYPE_OPEN           0
#define GESTURE_TYPE_CLOSE          1
#define GESTURE_TYPE_TRACK_LOSE     -1
#define INTRUSION_TYPE_IMAGE        2

#define MAX_GESTURE_TRACK_LOSE      3
//#define BLACKLIST_HAND_LOSE_COUNT   4
#define BLACKLIST_DURATION  (60000) // 超过N毫秒，加入黑名单

#define ANGLE_INTERVAL 10 // 角度间隔10度

#define MOTION_DETECTION_GAP 3000 // 补光变化后，移动侦测3秒内无效, 单位: 毫秒
#define BODY_FACE_DETECTION_GAP 1000 // 补光变化后，头脸识别1秒内无效, 单位: 毫秒
#define MAX_LIGHT_CHANGE_COUNT 4 // 连续灯光变化次数阈值

#define MAX_SAVING_ENERGY_DURATION 3000 // 3秒无人，进入节能，单位:毫秒 
#define MAX_NO_PERSON_DURATION  4000    // 没有检测到人的时间间隔，单位:毫秒

#define AC_DISTANCE_PRECITION   50      // 空调控制的距离精度
#define MAX_BLOW_WIND_GAP       4000    // 吹风间隔时间，单位: 毫秒
#define WIND_DISTANCE_THRESHOLD 8       // 强&&弱风阈值, 等于8为强风

#define TANINN_FRAMES_THRESHOLD  2 // 连续多次检测到入侵，才认为入侵开始
#define MAX_TANINNLOSE_COUNT 5     // 连续多次检测到入侵结束，才认为结束
#define TANINNLOSE_DELAY (30*1000)  // 单位: 毫秒

// 算法状态
#define ALG_GESTURE_BODY_FACE 0     // 正常模式
#define ALG_GESTURE_TRACKING  1     // 手势跟踪模式
#define ALG_MOTION_BOBY       2     // 夜晚移动侦测模式
#define ALG_BODY_FACE         3     // 白天安防头肩检测
#define ALG_MOTION_BODY_FACE  4     // 白天安防头肩检测, 在既有人头也有移动的时候，才告警。
#define ALG_MOTION            5     // 待机模式

// 手势开关机相关
#define FIST_CNT_THD 2          // 握拳响应N次数阈值
#define FIST_HAND_LOST_THD 2    // 连续手势丢失N次阈值
#define FIST_HAND_TRACK_LOST_THD 4 // 连续跟踪失败N次阈值
#define HAND_LOST_DURATION 3000 // 退出手势稳定态的无手时长，单位: 毫秒
#define MIN_SWITCH_GAP 5000     // 两次开关机间隔最小值，单位: 毫秒

/* add by 刘春龙, 2017-01-03, 原因: 算法GAP处理 */
#define MIN_FRAME_GAP 0.0
#define MAX_FRAME_GAP 1.5

#define PROG_REBOOT_DURATION_SHORT (14*24*60*60*1000) // 14天
#define PROG_REBOOT_DURATION_LONG  (21*24*60*60*1000) // 21天

#define NO_PERSON_THD (5*60*1000) // 单位: 毫秒
#define LIGHTING_THD  (5*60*1000) // 单位: 毫秒

#define MIN_FREE_TIME 2 // 凌晨2点
#define MAX_FREE_TIME 6 // 凌晨6点

static const std::string gesture_text_filename = "/sdcard/gestureImage/gesture_result.txt";
static const std::string gesture_blacklist_filename = "/sdcard/gestureImage/gesture_blacklist.txt";
static const std::string gesture_intrusion_filename = "/sdcard/gestureImage/intrusion_result.txt";


/*****************************************************************************/
static int device_test_security(const char *msg);
static int device_test_security_bgn();
static int device_test_security_end();

static int device_test_smart_wind_impl(DtThread *pDt,
    bool focus, int face_count, int distance, int left_angle, int right_angle);
#if 0
static int device_test_smart_wind(
    bool focus, int face_count, int distance, int left_angle, int right_angle);
#endif
static int device_test_gesture(const char *msg);

/*****************************************************************************
 * 函 数 名  : position_distance
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月13日
 * 函数功能  : 获取位置之间的距离 (距离的平方)
 * 输入参数  : position_t pos0  位置
               position_t pos1  位置
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int position_distance_square(position_t& pos0, position_t& pos1)
{
    int x_dis = pos0.x_pos - pos1.x_pos;
    int y_dis = pos1.y_pos - pos1.y_pos;
    return (x_dis*x_dis) + (y_dis*y_dis);
}

/*****************************************************************************
 * 函 数 名  : distance_square
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月13日
 * 函数功能  : 获取距离的平方
 * 输入参数  : int x0  值
               int x1  值
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int distance_square(int x0, int x1)
{
    return (x0 - x1) * (x0 - x1);
}

//////////////////////////////////////////////////////////////////////////

zAlgorithmDetectThread::zAlgorithmDetectThread(
    zAlgManager* pManager, int ctype, int nw /* = 1280  */, int nh /* = 720 */)
    : algState_(0)
    , pManager_(pManager)
    , bAutoLighting_(true)
    , bDay_(true)
    , bFirstDet_(true)
    , bUseHandRaise_(true)
    , bUseFist_(false)
    , bUseBlackList_(false)
    , bUseHandFaceTrack(true)
    , bRobot(true)
    , bTrackTaninn_(false)
    , m_taninn_frame_count(0)
    , m_start_alarm_record_flag(false)
    , m_taninn_lose_count(0)
    , m_taninn_lose_report(false)
    , bTrackHand_(false)
    , bAku_(false)
    , bFocus_(false)
    , nWidth_(nw)
    , nHeight_(nh)
    , cameraType_(ctype)
    , alarmGap_(GDEFAULT_ALARM_GAP)
    , angleL_(0)
    , angleR_(0)
    , angleMin_(-GANGLEDIFF)
    , angleMax_(-GANGLEDIFF)
    , avDis_(0)
    , lightingF_(GLIGHTINGF)
    , lightingC_(GLIGHTINGC)
    , nTe_(0)
    , latestCenterX_(0)
    , latestCenterY_(0)
    , pseudoHand_(0)
    , eakonCtrlGap_(5000)
    , m_preFrame(0)
    , latestKeikoku_(0)
    , latestHandFind_(0)
    , preEakonCtrl_(0)
    , m_bSleep(false)
    , m_sleepPeriod(MIN_LIGHTING_DURATION)
    , m_preLightOnTime(0)
    , m_last_daynight_change_time(0)
    , m_light_change_count(0)
    , m_hand_raise_response_count(0)
    , m_prev_gesture_flag(GESTURE_DISABLE)
    , m_hand_raise_smart_wind_flag(1)
    , m_prev_emodel(zBaseUtil::zEakonModel::IE_MODEL)
    , m_stable_hand_find_flag(false)
    , m_stable_hand_change_flag(false)
    , m_stable_hand_time(0)
    , m_stable_hand_width(0)
    , m_stable_hand_height(0)
    , m_stable_hand_width_square(0)
    , m_hand_move_flag(false)
    , m_gesture_track_lose_count(0)
    , m_gesture_lose_count(0)
    , m_hand_lose_count(0)
    , m_gesture_prev_lose_time(0)
    , m_center_fist_count(0)
    , m_prev_gesture_voice_time(0)
    , m_stable_hand_response_flag(false)
    , m_stable_hand_blacklist_flag(false)
    , m_vda_index(0)
    , m_last_wind_angle(0)
    , m_last_no_person_time(0)
    , m_prev_wind_time(0)
    , m_prev_wind_dist(0)
    , m_adjust_count(0)
    , m_image_nomotion(0)
    , bMotionDetected(false)
    , m_night_monitor_switch_time(0)
    , m_hands_blacklist_total(0)
    , m_hands_blacklist_index(0)
    , m_algorithm_initialized_flag(false)
    , m_curr_smart_wind_flag(zEakonThread::k_focuse)
    , m_intelligent_enable(true)
    , m_prev_lighting_(0.0)
    , m_first_hand_lost_time(0)
    , m_temperature_operated(false)
    , m_prev_switch_time(0)
    , m_no_person_flag(false)
    , m_no_person_time(0)
    , m_reboot_duration_short(PROG_REBOOT_DURATION_SHORT)
    , m_reboot_duration_long(PROG_REBOOT_DURATION_LONG)
    , m_no_person_thd(NO_PERSON_THD)
    , m_lighting_thd(LIGHTING_THD)
    , m_min_free_time(MIN_FREE_TIME)
    , m_max_free_time(MAX_FREE_TIME)
{
    zConfigManager::zTxtConfigMgr conf("/system/etc/conf.txt", "r");
    std::string value = conf.getValue("alarmGap");
    if (!value.empty())
    {
        alarmGap_ = atoi(value.c_str()) * G2IXX;
    }

    value = conf.getValue("lightingF");
    if (!value.empty())
    {
        if (1 != sscanf(value.c_str(), "%lf", &lightingF_))
        {
            lightingF_ = GLIGHTINGF;
        }
    }

    value = conf.getValue("lightingC");
    if (!value.empty())
    {
        if (1 != sscanf(value.c_str(), "%lf", &lightingC_))
        {
            lightingC_ = GLIGHTINGC;
        }
    }

    m_irled_brightness = MAX_BRIGHTNESS;
    value = conf.getValue("irled_brightness");
    if (!value.empty())
    {
        m_irled_brightness = atoi(value.c_str());
    }
    LOG_I("m_irled_brightness=%d\n", m_irled_brightness);

    value = conf.getValue("angleL");
    if (!value.empty())
    {
        angleL_ = atoi(value.c_str());
    }

    value = conf.getValue("angleR");
    if (!value.empty())
    {
        angleR_ = atoi(value.c_str());
    }

    value = conf.getValue("lightOnDuration");
    if (!value.empty())
    {
        m_sleepPeriod = atoi(value.c_str()) * 1000;
    }

    value = conf.getValue("eakonCtrlGap");
    if (!value.empty())
    {
        eakonCtrlGap_ = atoi(value.c_str());            // ms
    }

	value = conf.getValue("reboot_duration_short");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str()); 
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        m_reboot_duration_short = n_value * 60 * 1000;
    }
    LOG_D("m_reboot_duration_short=%lld", m_reboot_duration_short);

	value = conf.getValue("reboot_duration_long");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str()); 
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        m_reboot_duration_long = n_value * 60 * 1000;
    }
    LOG_D("m_reboot_duration_long=%lld", m_reboot_duration_long);

	value = conf.getValue("no_person_thd");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str()); 
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        m_no_person_thd = n_value * 60 * 1000;
    }
    LOG_D("m_no_person_thd=%d", m_no_person_thd);

	value = conf.getValue("lighting_thd");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str());
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        m_lighting_thd = n_value * 60 * 1000;
    }
    LOG_D("m_lighting_thd=%d", m_lighting_thd);

	value = conf.getValue("min_free_time");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str());
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        if (n_value >= 0 && n_value <=24)
        {
            m_min_free_time = n_value;
        }
    }
    LOG_D("m_min_free_time=%d", m_min_free_time);

	value = conf.getValue("max_free_time");
    if (!value.empty())
    {
        int n_value = atoi(value.c_str());
        LOG_D("line=%d, n_value =%d", __LINE__, n_value);
        if (n_value >= 0 && n_value <=24)
        {
            m_max_free_time = n_value;
        }
    }
    LOG_D("m_max_free_time=%d", m_max_free_time);
    
    value = conf.getValue("handRaise");
    bUseHandRaise_ = "on" == value;

    value = conf.getValue("fist");
    bUseFist_ = "on" == value;

    value = conf.getValue("blackList");
    bUseBlackList_ = "on" == value;

    value = conf.getValue("handfacetrack");
    bUseHandFaceTrack = ("on" == value);

    value = conf.getValue("head_type_filter");
    bUseFilterHeadType = ("on" == value);

    m_model_dir = conf.getValue("modeDir");
    if (m_model_dir.empty()) m_model_dir = "/system/etc/model/";
    LOG_D("m_model_dir=%s\n", m_model_dir.c_str());

    m_licence_dir = conf.getValue("lcsDir");
    if (m_licence_dir.empty()) m_licence_dir = "/data/license";
    LOG_D("m_licence_dir=%s\n", m_licence_dir.c_str());

    init_energy_config();

    #if BLACKLIST_PROCESS_V2_ENABLE
    m_prev_hand_lost_time = 0;
    #endif

    return;
}

zAlgorithmDetectThread::~zAlgorithmDetectThread()
{
    (void)GestureProcessor_GestureFinalize();
    (void)GestureBodyFaceVideoProcessor_GestureBodyFaceVideoFinalize();
}

static bool onTaninnLose_(void* param)
{
    if (NULL == param) return true;

    zAlgorithmDetectThread* pThread = (zAlgorithmDetectThread*)param;
    pThread->taninnLose_endStranger();

    pThread->taninnLose_notify();
    return true;
}

void zAlgorithmDetectThread::proc()
{
    static S_VCBuf frame = { 0 };

    /* add by 刘春龙, 2016-03-31, 原因: 算法初始化存在失败概率，需间隔时间尝试重新初始化 */
    if (!m_algorithm_initialized_flag)
    {
        if (!InitAlg(m_model_dir, m_licence_dir))
        {
            LOG_D("InitAlg failed.\n");
            // 设置加密芯片故障标志
            int nErr = zEakonThread::getLastErr();
            if ( !(nErr & EAKON_CHIP_ERR))
            {
                nErr |= EAKON_CHIP_ERR;
                zEakonThread::setLastErr(nErr);
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(ALG_INIT_GAP));
            return;
        }
        else
        {
            // 清除加密芯片故障标志
            int nErr = zEakonThread::getLastErr();
            if (nErr & EAKON_CHIP_ERR)
            {
                nErr &= ~EAKON_CHIP_ERR;
                zEakonThread::setLastErr(nErr);
            }

            /* add by 刘春龙, 2017-01-09, 原因: 借用m_algorithm_initialized_flag标志，
                启动时，关闭补光灯。 修正程序自动重启后，补光灯一直亮的问题 */
            set_daynight_mode(bDay_);
            m_algorithm_initialized_flag = true;
        }
    }

    // 程序重启仲裁。
    program_restart_arbitrate();

    /* add by 刘春龙, 2016-03-03, 原因: 限制识别取帧速度，降低CPU占用率，但在有手的情况下，不限制 */
    if (    m_stable_hand_find_flag
        ||  ((zBaseUtil::getMilliSecond() - m_preFrame) >= ANALYZE_FRAME_GAP))
    {
        if (pManager_->getFrame(frame, zAlgManager::Threads::CBTHREAD))
        {
            analyzeFrame(frame);
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(GETFRAME_GAP_1));
        }
    }
    else
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(GETFRAME_GAP_0));
    }

    return;
}

bool zAlgorithmDetectThread::InitGestureAlg(const std::string& mdir, const std::string& lcsDir)
{
    std::string strGestureOpen = mdir + "detection_hand_ex4/hand_boosting_open_cascade_cbf_lv19_20160311_1154.bin";
    std::string strGestureClose = mdir + "detection_hand_ex3/android_model_handc_ex3.dat";

    bool ret = GestureProcessor_GestureInit( false, strGestureOpen.c_str(), strGestureClose.c_str(),
        NULL, 0, lcsDir.empty() ? NULL : lcsDir.c_str(), NULL);
    LOG_D("GestureProcessor_GestureInit. %s!!\n", ret ? "success" : "failed");
    return ret;
}

bool zAlgorithmDetectThread::InitAlg(const std::string& mdir, const std::string& lcsDir)
{
    if (NULL == this) return false;

    (void)InitGestureAlg(mdir, lcsDir);

    //std::string strFaceDetect = mdir + "detection_face_ex/android_model_faced_ex.dat";
    std::string strFaceDetect = mdir + "detection_face_ex2/face_20160307_0000_19.bin";

    //std::string strGestureOpen = mdir + "detection_hand_ex2/android_model_hando_ex2.dat";
    //std::string strGestureOpen = mdir + "detection_hand_ex3/hand_boosting_open_cascade_cbf_lv19_20160306_2152.bin";
    std::string strGestureOpen = mdir + "detection_hand_ex4/hand_boosting_open_cascade_cbf_lv19_20160311_1154.bin";

    std::string strGestureClose = mdir + "detection_hand_ex3/android_model_handc_ex3.dat";
    //std::string strGestureOpenRight = mdir + "detection_hand_ex/android_model_hando_ex.dat";
    //std::string strGestureCloseRight = mdir + "detection_hand_ex/android_model_handc_ex.dat";
    
    //std::string strModelBody = mdir + "detection_body_ex/android_model_body_ex.dat";
    std::string strModelBody = mdir + "detection_body_ex3/head_boosting_20160306_1010_19.bin";
    
    std::string strModelLeftFace = mdir + "detection_lsface_ex/android_model_faced_left_ex.dat";
    std::string strModelRightFace = mdir + "detection_rsface_ex/android_model_faced_right_ex.dat";

    std::string strCNNModelBody = mdir + "detection_cnn_ex2/head_20150928_1114_fix.bin";
    std::string strCNNModelFace = mdir + "detection_cnn_ex/face_20150821_1913_fix.bin";
    
    //std::string strCNNModelGestOpen = mdir + "detection_cnn_ex2/hand_cnn_cls_iter_1000000_0925_1835_mean_100.fixedbin";
    std::string strCNNModelGestOpen = mdir + "detection_cnn_ex5/hand_run_cls_20160306_iter_1000000_mean_100.fixedbin";
        
    //std::string strCNNModelGestTrack = mdir + "detection_cnn_ex4/hand_parsing_1018_0407_iter_1000000_mean128.bin";
    std::string strCNNModelGestTrack = mdir + "detection_cnn_ex5/hand_parsing_2016_0306_0229_iter_1000000_mean128.bin";    

    bool bReturn = GestureBodyFaceVideoProcessor_GestureBodyFaceVideoInit(
        strGestureOpen.c_str(), strGestureClose.c_str()
        , strModelBody.c_str(), strFaceDetect.c_str()
        , strModelLeftFace.c_str(), strModelRightFace.c_str()
        , strCNNModelBody.c_str(), strCNNModelFace.c_str()
        , strCNNModelGestOpen.c_str(), strCNNModelGestTrack.c_str()
        , NULL, lcsDir.empty() ? NULL : lcsDir.c_str(), NULL);

    // false: 手势检测时，不跟踪人脸
    if (false == bUseHandFaceTrack)
    {
        float hand_face_init = 0.0f;
        GestureBodyFaceVideoProcessor_GestureSetParam("-hand_face_init", hand_face_init);
        
        float hand_face_init_check = GestureBodyFaceVideoProcessor_GestureGetParam("-hand_face_init");
        LOG_I("hand_face_init_check=%f\n", hand_face_init_check);
    }

    // 避免单人捂嘴打哈气的误检
    (void)GestureBodyFaceVideoProcessor_GestureSetParam("-hand_adapt_init", 1.0f);
    float hand_adapt = GestureBodyFaceVideoProcessor_GestureGetParam("-hand_adapt_init");
    LOG_I("hand_adapt=%f \n", hand_adapt);

    #if 0
    //hand_mode_init：手掌检测初始化灵敏度，mode=2为目前的参数，mode=3更灵敏，mode=1更不灵敏
    (void)GestureBodyFaceVideoProcessor_GestureSetParam("-hand_mode_init", 3.0);
    // motion_mode_init：运动检测的灵敏都，mode=2为目前的参数，mode=3更灵敏，mode=1更不灵敏
    (void)GestureBodyFaceVideoProcessor_GestureSetParam("-motion_mode_init", 3.0);

    float hand_mode = GestureBodyFaceVideoProcessor_GestureGetParam("-hand_mode_init");
    float motion_mode = GestureBodyFaceVideoProcessor_GestureGetParam("-motion_mode_init");
    LOG_I("hand_mode=%f, motion_mode=%f\n", hand_mode, motion_mode);
    #endif

    switchState(ALG_MOTION);
    bMotionDetected = false;
    m_image_nomotion = 0;

    LOG_D("GestureBodyFaceVideoProcessor_GestureBodyFaceVideoInit %s \n", bReturn ? "succ" : "failed");
    return bReturn;
}

void zAlgorithmDetectThread::onEvent(int nEvent)
{
    if (NULL == this) return;
    // TODO
    LOG_D("zAlgorithmDetectThread::onEvent %d enter \n", nEvent);
    if (ZM_DEKAKEMODEL == nEvent)
    {
        std::unique_lock<std::mutex> lock(ctrlMutex_);
        reset();
        bTrackHand_ = false;
        pseudoHand_ = 0;

        clear_gesture_context();
        (void)switchState_standby(bDay_ ? ALG_MOTION_BODY_FACE: ALG_MOTION_BOBY);
        clear_taninn_context();
    }
    else if (ZM_IEMODEL == nEvent)
    {
        std::unique_lock<std::mutex> lock(ctrlMutex_);
        (void)switchState_standby(ALG_GESTURE_BODY_FACE);

        if (bTrackTaninn_) 
        {
            // 取消定时
            m_timer.removeTimer(ZM_END_ROKUSTRANGER);
            onTaninnLose_(this);
        }
    }
    else if (ZM_CLOSE == nEvent)
    {
        //std::unique_lock<std::mutex> lock(ctrlMutex_);
        clear_context();
    }
    else if (ZM_OPEN == nEvent)
    {
        //std::unique_lock<std::mutex> lock(ctrlMutex_);
        //reset();
        //switchState(ALG_GESTURE_BODY_FACE);
    }
    //else if (ZM_TANINN == nEvent)
    //{
    //  std::unique_lock<std::mutex> lock(ctrlMutex_);
    //  latestKeikoku_ = 0;
    //}
    else
    {
        // TODO
    }
    LOG_D("zAlgorithmDetectThread::onEvent %d out \n", nEvent);
}

void zAlgorithmDetectThread::onMsg(const zBaseUtil::zMsg& msg)
{
    if (NULL == this) return;
    LOG_I("zAlgorithmDetectThread::onMsg %d \n", msg.msg);
}

int zAlgorithmDetectThread::algState()
{
    return algState_;
}

void zAlgorithmDetectThread::reset()
{
    bAku_ = false;
    nTe_ = 0;
    latestCenterX_ = 0;
    latestCenterY_ = 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.set_daynight_mode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 设置摄像头的 "白天or夜间"模式
 * 输入参数  : bool bDay  true：白天（无补光）, false：夜晚（开启补光）
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::set_daynight_mode(bool bDay)
{
    LOG_D("bDay=%d\n", bDay);

    if (bDay)
    {
        //zBaseUtil::RW_System("echo day > /proc/camera_daynight");
        (void)zBaseUtil::RW_Echo("day", strlen("day"), "/proc/camera_daynight");

        //zBaseUtil::RW_System("echo 0 > /sys/class/misc/irled_dev/irled/enable");
        (void)zBaseUtil::RW_Echo("0", strlen("0"), "/sys/class/misc/irled_dev/irled/enable");

        //zBaseUtil::RW_System("echo 100 > /sys/class/misc/irled_dev/irled/brightness");
        (void)zBaseUtil::RW_Echo("100", strlen("100"), "/sys/class/misc/irled_dev/irled/brightness");
    }
    else
    {
        //zBaseUtil::RW_System("echo night > /proc/camera_daynight");
        (void)zBaseUtil::RW_Echo("night", strlen("night"), "/proc/camera_daynight");

        //zBaseUtil::RW_System("echo 1 > /sys/class/misc/irled_dev/irled/enable");
        (void)zBaseUtil::RW_Echo("1", strlen("1"), "/sys/class/misc/irled_dev/irled/enable");

        //zBaseUtil::RW_System("echo 80 > /sys/class/misc/irled_dev/irled/brightness");
        char buf[16]; (void)sprintf(buf, "%d", m_irled_brightness);
        (void)zBaseUtil::RW_Echo(buf, strlen(buf), "/sys/class/misc/irled_dev/irled/brightness");
    }

    m_last_daynight_change_time = zBaseUtil::getMilliSecond();
    m_light_change_count = 0;

    bool old_mode = bDay_;
    bDay_ = bDay;

    (void)zVideoThread::getInstance()->gamma_correct(!bDay_);
    return old_mode;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.chgLighting
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 设置摄像头补光模式， 有锁
 * 输入参数  : bool bOpen:  true：白天模式（关补光） false：夜间模式（开补光）
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::chgLighting(bool bOpen)
{
    std::unique_lock<std::mutex> lock(m_light_mutex);
    (void)set_daynight_mode(!bOpen);

    // 非自动补光
    if (bAutoLighting_)
    {
        bAutoLighting_ = false;
    }
}

bool zAlgorithmDetectThread::checkDiff(const std::list<zBaseUtil::v2Hand>& handList)
{
    bool bReturn = false;
    do
    {
        if (handList.empty()) break;
        std::list<zBaseUtil::v2Hand>::const_iterator it = handList.begin();

        zBaseUtil::v2Hand handIn = !pseudoHand_? *it : pseudoHandPosition_;

        int nWidth = handIn.right_ - handIn.left_;
        int nMinDiff = nWidth * nWidth / 100;
        int nCentx = (handIn.left_ + handIn.right_) >> 1;
        int nCenty = (handIn.top_ + handIn.bottom_) >> 1;

        //++it;

        while (it != handList.end())
        {
            zBaseUtil::v2Hand handOut = *it;
            int nPosX = (handOut.left_ + handOut.right_) >> 1;
            int nPosY = (handOut.top_ + handOut.bottom_) >> 1;

            int nDiff = (nPosX - nCentx) * (nPosX - nCentx) + (nPosY - nCenty) * (nPosY - nCenty);
            bReturn = nDiff < nMinDiff;
            LOG_D("checkDiff nDiff == %d, nMinDiff == %d bReturn == %s \n", nDiff, nMinDiff, bReturn ? "true" : "false");
            if (!bReturn) break;
            ++it;
        }
    } while (false);
    return bReturn;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.clear_taninn_context
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月18日
 * 函数功能  : 复位入侵检测上下文
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::clear_taninn_context()
{
    bTrackTaninn_ = false;
    m_start_alarm_record_flag = false;

    m_taninn_frame_count = 0;
    m_taninn_lose_count = 0;
    m_taninn_lose_report = false;
    return;
}

void zAlgorithmDetectThread::onTaninnFind_notify(const zBaseUtil::v2ImgResult& result)
{
    LOG_I("onTaninnFind \n");
    // 取消定时
    m_timer.removeTimer(ZM_END_ROKUSTRANGER);

    // 通知主控，
    pManager_->TaninnFind_EakonNotify();

    // P2P通知
    char szBuf[256] = { 0 };
    sprintf(szBuf, "{ \"act\":\"alarm\", \"person\":\"%d\"}", result.nHead_);
    if ("" != zP2PThread::m_deviceId)
    {
        notifyWebrtc(szBuf);
    }
    LOG_D("onTaninnFind: %s \n", szBuf);

    // 工装测试打印
    (void)device_test_security_bgn();

    bTrackTaninn_ = true;
    m_taninn_lose_report = false;
    return;
}

void zAlgorithmDetectThread::onTaninnFind_bgnStranger()
{
    // 启动录像: 使用try_lock，防止在切换分辨率时，进入长时间等待
    std::unique_lock<std::mutex> lck(zBaseUtil::g_resolution_record_mutex, std::defer_lock);
    if (lck.try_lock())
    {
        DtThread *pDt = DtThread::getInstance();
        // 工装态，不允许录像
        if (    NULL == pDt
            ||  !pDt->dt_is_started())
        {
            pManager_->TaninnFind_BgnStranger();
        }
        else
        {
            LOG_D("record disable.\n");
        }
        m_start_alarm_record_flag = true;
    }
    else
    {
        LOG_D("mutex busy.\n");
    }
    return;
}

void zAlgorithmDetectThread::onTaninnLose()
{
    // delay 30s and then stop taninn roku
    m_timer.setTimer(ZM_END_ROKUSTRANGER, 1, 1, 100, TANINNLOSE_DELAY, onTaninnLose_, this);
}

void zAlgorithmDetectThread::taninnLose_endStranger()
{
    LOG_I("\n");
    pManager_->TaninnLose_EndStranger();
    m_start_alarm_record_flag = false;
    return;
}

void zAlgorithmDetectThread::taninnLose_notify()
{
    LOG_I("\n");

    // 通知主控
    pManager_->TaninnLose_EakonNotify();

    // P2P通知
    char szBuf[256] = { 0 };
    sprintf(szBuf, "{ \"act\":\"alarm_finish\", \"person\":\"%d\"}", 0);
    if ("" != zP2PThread::m_deviceId)
    {
        notifyWebrtc(szBuf);
    }
    LOG_D("taninnLose: %s \n", szBuf);

    // 工装通知
    (void)device_test_security_end();

    bTrackTaninn_ = false;
    return;
}

static bool dSort(const zBaseUtil::Da& s, const zBaseUtil::Da& d)
{
    return s.a < d.a;
}

int zAlgorithmDetectThread::find_min_max(
    int lb, int rb, int &min_angle, int &max_angle)
{
    (void)lb;
    int min_a = rb;
    int max_a = 0;
    bool null_flag = true;

    for (int i=0; i<FRAME_COUNT; i++)
    {
        std::vector<zBaseUtil::Da> &vda = m_vda[i];
        for (uint32_t j=0; j<vda.size(); j++)
        {
            if (vda[j].a < min_a)
            {
                min_a = vda[j].a;
            }

            if (vda[j].a > max_a)
            {
                max_a = vda[j].a;
            }

            if (null_flag)
            {
                null_flag = false;
            }
        }
    }

    if (null_flag)
    {
        return -1;
    }

    min_angle = min_a;
    max_angle = max_a;
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.push_persons
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月25日
 * 函数功能  : 人员检测误检规避
 * 输入参数  : const zBaseUtil::v2ImgResult& result  头脸分析结果
               int lb                                左角度
               int rb                                右角度
               std::vector<zBaseUtil::Da> &da_vec    输出人脸队列
 * 输出参数  : 无
 * 返 回 值  : int ; -1: 没到5帧  0: 没有人脸，其他: 人脸个数
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::push_persons(
    const zBaseUtil::v2ImgResult& result, int lb, int rb, std::vector<zBaseUtil::Da> &da_vec, int &avg_person)
{
    int person_count = -1;
    std::vector<zBaseUtil::Da> &vda = m_vda[m_vda_index++];

    ZDEBUGlOG("m_vda_index=%d, head=%d\n", m_vda_index, result.nHead_);
    int face_count = 0;
    for (int i = 0; i < result.nHead_; i++)
    {
        zBaseUtil::v2Head& head = result.listHead_[i];
        if ((-1 == head.type_) && bUseFilterHeadType)
        {
            continue;
        }

        int dis = zBaseUtil::getDistance(head.type_, cameraType_, head.right_ - head.left_);
        if (dis < GFACE_BODY_MIN_DIS)
        {
            dis = GFACE_BODY_MIN_DIS;
        }
        else if (dis > GFACE_BODY_MAX_DIS)
        {
            dis = GFACE_BODY_MAX_DIS;
        }

        int angle = zBaseUtil::getAngleV2(head, cameraType_, nWidth_, lb);

        face_count++;
        vda.push_back({ dis, angle });
        ZDEBUGlOG("face%d: dis=%d, angle=%d \n", face_count, dis, angle);
    }

    if (FRAME_COUNT == m_vda_index)
    {
        int min_a, max_a;
        int ret = find_min_max(lb, rb, min_a, max_a);
        ZDEBUGlOG("ret=%d, min_a=%d, max_a=%d, lb=%d, rb=%d\n", ret, min_a, max_a, lb, rb);
        if (0 != ret)
        {
            person_count = 0;
        }
        else
        {
            // 分区段，获取距离和角度
            int interval_count = ((max_a - min_a) + ANGLE_INTERVAL) / ANGLE_INTERVAL;
            ZDEBUGlOG("interval_count=%d, min_a=%d, max_a=%d\n", interval_count, min_a, max_a);
            
            std::vector<zBaseUtil::Da> *vdas = new std::vector<zBaseUtil::Da>[static_cast<uint32_t>(interval_count)];

            int person_sum = 0;
            for (int i=0; i<FRAME_COUNT; i++)
            {
                std::vector<zBaseUtil::Da> &vda_t = m_vda[i];
                person_sum += vda_t.size();
                for (uint32_t j=0; j<vda_t.size(); j++)
                {
                    int vdas_index = (vda_t[j].a - min_a) / ANGLE_INTERVAL;
                    ZDEBUGlOG("vdas_index=%d, j=%d, vda_t[j].d=%d, vda_t[j].a=%d\n", vdas_index, j, vda_t[j].d, vda_t[j].a);
                    vdas[vdas_index].push_back(vda_t[j]);
                }
            }

            avg_person = (person_sum + FRAME_COUNT/2) / FRAME_COUNT;

            // 求平均距离，平均角度
            da_vec.clear();
            for (int i=0; i<interval_count; i++)
            {
                int size = vdas[i].size();
                if (size > 1)
                {
                    int avg_dis_sum = 0;
                    int avg_angle_sum = 0;
                    for (uint32_t j=0; j<vdas[i].size(); j++)
                    {
                        ZDEBUGlOG("line=%d, j=%d\n", __LINE__, j);
                        avg_dis_sum += vdas[i][j].d;
                        avg_angle_sum += vdas[i][j].a;
                    }

                    da_vec.push_back({ avg_dis_sum/size, avg_angle_sum/size });
                    ZDEBUGlOG("avg_dis=%d, avg_angle=%d \n", avg_dis_sum/size, avg_angle_sum/size);
                }
            }

            delete [] vdas;
            person_count = da_vec.size();
        }

        clear_persons();
    }
    return person_count;
}

void zAlgorithmDetectThread::clear_persons()
{
    m_vda_index = 0;
    for (int i=0; i<FRAME_COUNT; i++)
    {
        m_vda[i].clear();
    }
    return;
}

int zAlgorithmDetectThread::blow_wind_angle(
    int lb, int rb, int left, int right)
{
    
    if (lb == left)
    {
        return lb;
    }
    
    if (rb == right)
    {
        return rb;
    }
    
    return (left + right) >> 1;
}

int zAlgorithmDetectThread::get_curr_wind_angle(
    int lb, int rb, std::vector<zBaseUtil::Da> &vda, 
    int &curr_angle_left, int &curr_angle_right)
{
    (void)rb;
    int size = vda.size();
    int prev_angle = lb;
    for (int i=0; i<size; i++)
    {
        if (vda[i].a >= m_last_wind_angle)
        {
            curr_angle_left = prev_angle;
            curr_angle_right = vda[i].a;
            return 0;
        }
        prev_angle = vda[i].a;
    }

    ZDEBUGlOG("error: it is impossible. \n");
    return -1;
}

/*****************************************************************************
 * 函 数 名  : get_wind_angle
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月24日
 * 函数功能  : 获取吹风角度
 * 输入参数  : int lb                           最小角度
               int rb                           最大角度
               std::vector<zBaseUtil::Da> &vda  人员信息
               wind_angle                       风向
 * 输出参数  : 无
 * 返 回 值  : 0: 扫风， 其他: 角度
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::get_wind_angle(
    int lb, int rb, std::vector<zBaseUtil::Da> &vda, int &wind_angle, int &wind_distance)
{
    std::sort(vda.begin(), vda.end(), dSort);
    vda.push_back({0, rb});

    int prev_a = lb;
    int size = vda.size();
    int max_angle = 0;
    int sub_angle = 0;

    int max_angle_left = 0;
    int max_angle_right = 0;
    int distance_sum = 0;
    for (int i=0; i<size; i++)
    {
        ZDEBUGlOG("vda%d: dis=%d, angle=%d\n", i, vda[i].d, vda[i].a);
        
        sub_angle = vda[i].a - prev_a;
        if (0 == i)
        {
            max_angle = sub_angle;
            max_angle_left = prev_a;
            max_angle_right = vda[i].a;
        }
        else if (sub_angle > max_angle)
        {
            max_angle = sub_angle;
            max_angle_left = prev_a;
            max_angle_right = vda[i].a;
        }
        
        prev_a = vda[i].a;
        distance_sum += vda[i].d;
    }

    wind_distance = distance_sum / (size - 1);
    ZDEBUGlOG("distance_sum=%d, size=%d, wind_distance=%d\n", distance_sum, size, wind_distance);

    // 人员分布均匀，则扫风
    if (max_angle < 30)
    {
        wind_angle = 0;
        return 0;
    }

    if (    (0 == m_last_wind_angle)
        ||  ((m_last_wind_angle >= max_angle_left) && (m_last_wind_angle <= max_angle_right)))
    {
        ZDEBUGlOG("m_last_wind_angle=%d, max_angle_left=%d, max_angle_right=%d\n", 
            m_last_wind_angle, max_angle_left, max_angle_right);
        wind_angle = blow_wind_angle(lb, rb, max_angle_left, max_angle_right);
        return 0;
    }

    int curr_angle_left = 0;
    int curr_angle_right = 0;
    (void)get_curr_wind_angle(lb, rb, vda, curr_angle_left, curr_angle_right);
    int curr_angle = curr_angle_right - curr_angle_left;

    ZDEBUGlOG("max_angle=%d, curr_angle=%d\n", max_angle, curr_angle);
    if ((max_angle - curr_angle) > (curr_angle >> 1))
    {
        ZDEBUGlOG("max_angle_left=%d, max_angle_right=%d\n", max_angle_left, max_angle_right);
        wind_angle = blow_wind_angle(lb, rb, max_angle_left, max_angle_right);
        return 0;
    }
    else
    {
        ZDEBUGlOG("curr_angle_left=%d, curr_angle_right=%d\n", curr_angle_left, curr_angle_right);
        wind_angle = blow_wind_angle(lb, rb, curr_angle_left, curr_angle_right);
        return 0;
    }
}

/*****************************************************************************
 * 函 数 名  : device_test_smart_wind_impl
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 发送工装智能风消息
 * 输入参数  : int focus        风吹人 or 风避人
               int face_count   人脸数
               int distance     距离
               int left_angle   左角度
               int right_angle  右角度
 * 输出参数  : 无
 * 返 回 值  : static int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int device_test_smart_wind_impl(DtThread *pDt,
    bool focus, int face_count, int distance, int left_angle, int right_angle)
{
    char buf[128];
    char *tbuf = buf;
    int tbuf_size = 128;
    int tlen = snprintf(tbuf, tbuf_size, "组别个数:%d ", face_count);
    tbuf += tlen; tbuf_size -= tlen;

    tlen = snprintf(tbuf, tbuf_size, "平均距离:%d ", distance);
    tbuf += tlen; tbuf_size -= tlen;

    tlen = snprintf(tbuf, tbuf_size, "角度:(%d,%d)", left_angle, right_angle);
    tbuf += tlen; tbuf_size -= tlen;

    (void)snprintf(tbuf, tbuf_size, " %s", ((focus) ? "风吹人" : "风避人"));

    (void)pDt->dt_send_message(WIND_MSG, buf);
    return 0;
}

#if 0
// 工装测试打印 : 智能风
static int device_test_smart_wind(
    bool focus, int face_count, int distance, int left_angle, int right_angle)
{
    DtThread *pDt = DtThread::getInstance();
    if (    NULL == pDt
        ||  !pDt->dt_message_enable(WIND_MSG))
    {
        return -1;
    }

    device_test_smart_wind_impl(pDt, 
        focus, face_count, distance, left_angle, right_angle);
    return 0;
}
#endif

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.device_test_prev_smart_wind
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 发送前一次的送风角度，优化工装智能风响应速度
 * 输入参数  : int focus  风吹人 or 风避人
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::device_test_prev_smart_wind(int focus)
{
    zEakonThread* pEakon = pManager_->getEakon();
    if (NULL == pEakon)
    {
        return -1;
    }

    DtThread *pDt = DtThread::getInstance();
    if (    NULL == pDt
        ||  !pDt->dt_message_enable(WIND_MSG))
    {
        return -1;
    }

    int w_conf[ SMART_WIND_CONFIG_SIZE ] = {0};
    zEakonThread::kazeMuki energy_muki_temp;
    bool energy_gestrue_flag_temp;
    pEakon->get_prev_smart_wind_config(w_conf, energy_muki_temp, energy_gestrue_flag_temp);

    (void)device_test_smart_wind_impl(pDt, 
        focus, w_conf[0], w_conf[1], w_conf[3], w_conf[4]);
    return 0;
}

// 工装测试打印 : 安防
static int device_test_security(const char *msg)
{
    DtThread *pDt = DtThread::getInstance();
    if (    NULL == pDt
        ||  !pDt->dt_message_enable(SECURITY_MSG))
    {
        return -1;
    }

    (void)pDt->dt_send_message(SECURITY_MSG, msg);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : device_test_security_bgn
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 人员入侵告警
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int device_test_security_bgn()
{
    return device_test_security("离家模式: 人员入侵告警");
}

/*****************************************************************************
 * 函 数 名  : device_test_security_end
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 人员入侵告警结束
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int device_test_security_end()
{
    return device_test_security("人员入侵告警结束");
}

/*****************************************************************************
 * 函 数 名  : device_test_gesture
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月5日
 * 函数功能  : 工装时，发送手势信息
 * 输入参数  : const char *msg  手势信息 - utf8编码
 * 输出参数  : 无
 * 返 回 值  : static int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int device_test_gesture(const char *msg)
{
    DtThread *pDt = DtThread::getInstance();
    if (    NULL == pDt
        ||  !pDt->dt_message_enable(GESTURE_MSG))
    {
        return -1;
    }

    (void)pDt->dt_send_message(GESTURE_MSG, msg);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.wind_strength_adjust
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月22日
 * 函数功能  : 强弱风转换时，调整吹风距离
 * 输入参数  : int& wind_dist  吹风距离值, 也是输出值
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::wind_strength_adjust(int& wind_dist)
{
    #define MAX_ADJUST_COUNT 4 // 连续3次调整后，不再调整

    // 当前为强风
    if (m_prev_wind_dist >= WIND_DISTANCE_THRESHOLD)
    {
        if (wind_dist == (WIND_DISTANCE_THRESHOLD - 1)) // 弱风
        {
            if (m_adjust_count <= MAX_ADJUST_COUNT)
            {
                wind_dist = WIND_DISTANCE_THRESHOLD;
                m_adjust_count++;
            }
        }
        else if (0 != m_adjust_count)
        {
            m_adjust_count = 0;
        }
    }
    else // 当前为弱风
    {
        if (wind_dist == WIND_DISTANCE_THRESHOLD) // 强风
        {
            if (m_adjust_count <= MAX_ADJUST_COUNT)
            {
                wind_dist = WIND_DISTANCE_THRESHOLD - 1;
                m_adjust_count++;
            }
        }
        else if (0 != m_adjust_count)
        {
            m_adjust_count = 0;
        }
    }

    return;
}

int zAlgorithmDetectThread::wind_aviod_person(const zBaseUtil::v2ImgResult& result)
{
    if (bFocus_)
    {
        // 初始化风避人的上下文
        clear_persons();
        m_last_wind_angle = 0; // 0: 自动扫风

        bFocus_ = false;
    }

    int lb, rb;
    pManager_->getKazeParama(lb, rb);

    std::vector<zBaseUtil::Da> vda;
    int avg_person = 0;
    int persion_count = push_persons(result, lb, rb, vda, avg_person);
    if (persion_count <= 0)
    {
        return 0;
    }

    int wind_angle = 0;
    int wind_distance = 0;
    int nFace = vda.size();
    
    (void)get_wind_angle(lb, rb, vda, wind_angle, wind_distance);
    ZDEBUGlOG("wind_angle=%d, wind_distance=%d\n", wind_angle, wind_distance);

    int ac_control_dis = (wind_distance + AC_DISTANCE_PRECITION/2) / AC_DISTANCE_PRECITION;
    if (0 == wind_angle)
    {
        int arr[SMART_WIND_CONFIG_SIZE] = 
            { nFace, ac_control_dis, ac_control_dis, lb, rb, lb, rb, nFace};
        (void)KazemukiCtrl_Energy(arr, false);

        m_last_wind_angle = wind_angle;
    }
    else
    {
        wind_angle -= wind_angle % 5; // 角度必须为5的倍数
        if (wind_angle > rb) wind_angle = rb;
        else if (wind_angle < lb) wind_angle = lb;

        int arr[SMART_WIND_CONFIG_SIZE] = 
            { nFace, ac_control_dis, ac_control_dis, wind_angle, wind_angle, wind_angle, wind_angle, nFace};
        (void)KazemukiCtrl_Energy(arr, false);

        m_last_wind_angle = wind_angle;
    }

    return 0;
}

int zAlgorithmDetectThread::analyzeHead(const zBaseUtil::v2ImgResult& result)
{
    bool bFocus = pManager_->bFocus();

    // 优化风吹人和风避人，工装速度。
    (void)device_test_prev_smart_wind(bFocus);

    if (!bFocus)
    {
        (void)wind_aviod_person(result);
        return 0;
    }

    // kaze ctrl
    //////////////////////////////////////////////////////////////////////////
    if (result.nHead_ < 1)
    {
        std::int64_t curr_time = zBaseUtil::getMilliSecond();
        if (0 == m_last_no_person_time)
        {
            m_last_no_person_time = curr_time;
        }
        else if ((curr_time - m_last_no_person_time) >= MAX_NO_PERSON_DURATION)
        {
            if (!dataBuf_.empty())
            {
                dataBuf_.Clear();
            }
            m_last_no_person_time = curr_time;
        }
        //LOG_D("no persion.\n");
        return -1;
    }
    else if (0 != m_last_no_person_time)
    {
        m_last_no_person_time = 0;
    }

    std::vector<zBaseUtil::Da> vda;
    int disSum = 0;
    int nFace = 0;
    int lb, rb;
    pManager_->getKazeParama(lb, rb);
    for (int i = 0; i < result.nHead_; i++)
    {
        zBaseUtil::v2Head& head = result.listHead_[i];
        if ((-1 == head.type_) && bUseFilterHeadType)
        {
            LOG_D("head.type=%d\n", head.type_);
            continue;
        }

        int head_width = head.right_ - head.left_;
        int distance = zBaseUtil::getDistance(head.type_, cameraType_, head_width);
        if (distance < GFACE_BODY_MIN_DIS)
        {
            distance = GFACE_BODY_MIN_DIS;
        }
        else if (distance > GFACE_BODY_MAX_DIS)
        {
            distance = GFACE_BODY_MAX_DIS;
        }

        int angle = zBaseUtil::getAngleV2(head, cameraType_, nWidth_, lb);
        #if 0
        LOG_D("i=%d, head.type_=%2d, width=%d, distance=%d, angle=%d\n",
            i, head.type_, head_width, distance, angle);
        #endif
        
        ++nFace;
        disSum += distance;
        vda.push_back({ distance, angle });
    }

    if (nFace < 1)
    {
        //LOG_D("nFace=%d \n", nFace);
        return -1;
    }

    int avDis = disSum / nFace;
    //LOG_D("avDis=%d, disSum=%d, nFace=%d\n", avDis, disSum, nFace);

    int avMin = 0;
    int avMax = 0;

    //LOG_D("getKazeParama nLeft == %d, nRight = %d \n", lb, rb);
    
    if (bFocus != bFocus_)
    {
        // reset
        dataBuf_.Clear();
        bFocus_ = bFocus;
    }

    int lA = 0;
    int rA = 0;

    std::sort(vda.begin(), vda.end(), dSort);

    if (bFocus)
    {
        lA = vda.front().a;
        rA = vda.back().a;
    }
    else
    {
        zBaseUtil::getmg(vda, lb, rb, lA, rA, avDis);
    }

    int frame_count = dataBuf_.addInfo(avDis, lA, rA);
    dataBuf_.getAverage(avDis, avMin, avMax);

    if (!bFocus)
    {
        //LOG_D("adjust angle befor avMin == %d, avMax == %d, left == %d, right == %d \n", avMin, avMax, lb, rb);
        int disA = avMax - avMin;
        if (avDis < 200)
        {
            if (disA < 30)
            {
                avMin = lb;
                avMax = rb;
            }
        }
        else if (avDis < 400)
        {
            if (disA < 25)
            {
                avMin = lb;
                avMax = rb;
            }
        }
        else /*if (avDis >= 400)*/
        {
            if (disA < 20)
            {
                avMin = lb;
                avMax = rb;
            }
        }
        if (avMin == lb && avMax != rb) avMin = avMax = lb;
        else if (avMin != lb && avMax == rb) avMin = avMax = rb;
        else if (avMin != lb && avMax != rb) avMin = avMax = (avMin + avMax) >> 1;
        else (avMin == lb && avMax == rb) /* do nothing*/;
        //LOG_D("adjust angle end avMin == %d, avMax == %d, left == %d, right == %d \n", avMin, avMax, lb, rb);
    }
    //LOG_D("getKazeParama avMin == %d, avMax == %d, m_angleL = %d, m_angleR = %d\n", avMin, avMax, angleL_, angleR_);
    if (bFocus)
    {
        avMin += angleL_;
        avMax += angleR_;
    }
    angleMin_ = avMin;
    angleMax_ = avMax;
    if (avMax > rb) angleMax_ = rb;
    else if (avMax < lb) angleMax_ = lb;

    if (avMin > rb) angleMin_ = rb;
    else if (avMin < lb) angleMin_ = lb;

    avDis_ = avDis;
    //LOG_D("avDis_=%d, curFrame=%d\n", avDis_, curFrame);

    int disDiff, minDiff, maxDiff;
    dataBuf_.diff(disDiff, minDiff, maxDiff);
    int angleDiff = std::abs(minDiff) + std::abs(maxDiff);

    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    // "当累计帧数大于等于8帧，且时间大于4秒时", 或者, "角度差大于15度，且累计帧数大于等于4帧时"，输出送风配置
    if (    ((frame_count >= (DATABUF_DEFAUT_SIZE >> 2)) && ((curr_time - m_prev_wind_time) >= MAX_BLOW_WIND_GAP))
        ||  ((angleDiff >= GANGLEDIFF) && (frame_count >= (DATABUF_DEFAUT_SIZE >> 3))))
    {
        int ac_control_dis = (avDis + AC_DISTANCE_PRECITION/2) / AC_DISTANCE_PRECITION;
        LOG_D("before adjust. ac_control_dis=%d\n", ac_control_dis);
        wind_strength_adjust(ac_control_dis);

        LOG_D("frame_count=%d, angleDiff=%d, curr_time=%lld, m_prev_wind_time=%lld\n", 
            frame_count, angleDiff, curr_time, m_prev_wind_time);
        LOG_D("avDis=%d, ac_control_dis =%d, angleMin_=%d, angleMax_= %d, nFace=%d, result.nHead_=%d \n",
            avDis, ac_control_dis, angleMin_, angleMax_, nFace, result.nHead_);
        angleMin_ = angleMin_ - angleMin_ % 5;
        angleMax_ = angleMax_ - angleMax_ % 5;
        int arr[SMART_WIND_CONFIG_SIZE] = 
            { nFace, ac_control_dis, ac_control_dis, angleMin_, angleMax_, angleMin_, angleMax_, result.nHead_ };

        (void)KazemukiCtrl_Energy(arr, false);

        m_prev_wind_dist = ac_control_dis;
        m_prev_wind_time = curr_time;
    }

    //////////////////////////////////////////////////////////////////////////
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.stable_hand_enter
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 进入稳定手势跟踪状态
 * 输入参数  : zBaseUtil::v2Hand& hand  手
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::stable_hand_enter(zBaseUtil::v2Hand& hand)
{
    LOG_E("\n");
    zP2PThread::print_msg("hand enter.");

    m_stable_hand_change_flag = true;
    m_stable_hand_find_flag = true;
    
    // 保存手参数
    m_stable_hand_center_pos.x_pos = (hand.left_ + hand.right_) >> 1;
    m_stable_hand_center_pos.y_pos = (hand.top_ + hand.bottom_) >> 1;

    m_stable_hand_width = abs(hand.right_ - hand.left_);
    m_stable_hand_height = abs(hand.bottom_ - hand.top_);
    m_stable_hand_width_square = distance_square(hand.left_, hand.right_);

    m_stable_hand_left_top_pos.x_pos = hand.left_;
    m_stable_hand_left_top_pos.y_pos = hand.top_;
    
    m_stable_hand_right_bottom_pos.x_pos = hand.right_;
    m_stable_hand_right_bottom_pos.y_pos = hand.bottom_;

    m_stable_hand_time = zBaseUtil::getMilliSecond();

    // 初始化
    m_prev_hand_center_pos = m_stable_hand_center_pos;
    m_gesture_lose_count = 0;
    m_center_fist_count = 0;
 
    LOG_D("m_stable_hand_time=%lld, rect=%d, %d, %d, %d\n", 
        m_stable_hand_time, hand.left_, hand.top_, hand.right_, hand.bottom_);

    #if BLACKLIST_PROCESS_V2_ENABLE
    m_prev_hand_lost_time = 0;
    #endif
    
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.clear_gesture_context
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月3日
 * 函数功能  :  清除手势的全局上下文变量
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::clear_gesture_context()
{
    m_stable_hand_find_flag = false;
    m_stable_hand_change_flag = true;

    m_gesture_lose_count = 0;
    m_hand_lose_count = 0;
    m_gesture_prev_lose_time = 0;

    reset_fist_count();
    m_temperature_operated = false;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.stable_hand_exit
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 退出手势跟踪状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::stable_hand_exit(bool is_play_voice)
{
    LOG_E("\n");
    zP2PThread::print_msg("hand exit.");

    clear_gesture_context();
    switchState(ALG_GESTURE_BODY_FACE);

    if (is_play_voice) {
        (void)pManager_->TemaneCtrl(false);
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.hand_move_check
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 检查手是否移动
 * 输入参数  : zBaseUtil::v2Hand& hand  手
               gesture_track_lose_count: 连续手势类型为 GESTURE_TYPE_CLOSE 的次数
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::hand_move_check(
    zBaseUtil::v2Hand& hand, int gesture_track_lose_count)
{
    #define MAX( a, b) ( ((a) > (b)) ? (a) : (b) )
    
    position_t curr_hand_pos;
    curr_hand_pos.x_pos = (hand.left_ + hand.right_) >> 1;
    curr_hand_pos.y_pos = (hand.top_ + hand.bottom_) >> 1;

    bool hand_move_flag = false;
    int move_distance = position_distance_square(curr_hand_pos, m_stable_hand_center_pos);

    #if 0
    LOG_E("move_distance=%d, m_stable_hand_width_square=%d, gesture_track_lose_count=%d\n", 
        move_distance, gesture_track_lose_count, m_stable_hand_width_square);
    LOG_E("curr_hand_pos=(%d,%d), m_stable_hand_center_pos=(%d,%d)\n", 
        curr_hand_pos.x_pos, curr_hand_pos.y_pos, 
        m_stable_hand_center_pos.x_pos, m_stable_hand_center_pos.y_pos);
    LOG_E("m_stable_hand=(%d,%d),(%d,%d), hand=(%d,%d),(%d,%d)\n", 
        m_stable_hand_left_top_pos.x_pos, m_stable_hand_left_top_pos.y_pos,
        m_stable_hand_right_bottom_pos.x_pos, m_stable_hand_right_bottom_pos.y_pos,
        hand.left_, hand.top_, hand.right_, hand.bottom_);
    #endif

    if (move_distance > (m_stable_hand_width_square >> 3))
    {
        LOG_D("hand move: %d, %d\n", move_distance, m_stable_hand_width_square);
        hand_move_flag = true;
    }
    else if (gesture_track_lose_count >= MAX_GESTURE_TRACK_LOSE)
    {
        // 连续跟踪超过阈值后，手是否移动的判断条件更加严格
        uint horizon_move_distance = abs(curr_hand_pos.x_pos - m_stable_hand_center_pos.x_pos);
        if (horizon_move_distance >= MAX((m_stable_hand_width / 10), 2))
        {
            LOG_D("hand move: horizon_move_distance=%d, m_stable_hand_width=%d\n", 
                horizon_move_distance, m_stable_hand_width);

            hand_move_flag = true;
        }
        else
        {
            uint vertical_move_distance = abs(curr_hand_pos.y_pos - m_stable_hand_center_pos.y_pos);
            if (vertical_move_distance >= MAX((m_stable_hand_height / 10), 2))
            {
                LOG_D("hand move: vertical_move_distance=%d, m_stable_hand_height=%d\n", 
                    vertical_move_distance, m_stable_hand_height);

                hand_move_flag = true;
            }
        }
    }

    return hand_move_flag;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.hand_algrithm_ajust
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 如果开手势时,两次识别的手位置偏移过大，设置算法从手势跟踪态切
               换到手势检测态
 * 输入参数  : zBaseUtil::v2Hand& hand  手
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::hand_algrithm_ajust(zBaseUtil::v2Hand& hand)
{
    position_t m_curr_hand_center_pos;
    m_curr_hand_center_pos.x_pos = (hand.left_ + hand.right_) / 2;
    m_curr_hand_center_pos.y_pos = (hand.top_ + hand.bottom_) / 2;

    int hand_distance = position_distance_square(m_curr_hand_center_pos, m_prev_hand_center_pos);
    int hand_width = distance_square(hand.left_, hand.right_);

    if ((100 * hand_distance) > (225 * hand_width))
    {
        switchState(ALG_GESTURE_BODY_FACE); // 切换到手势检测状态
    }

    m_prev_hand_center_pos = m_curr_hand_center_pos;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.hand_lose_check
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 检查是否手丢失，连续两次手丢失，且间隔小于2秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::hand_lose_check()
{
    if (!m_stable_hand_find_flag)
    {
        //LOG_D("line=%d. no stable.\n", __LINE__);
        return false;
    }

    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    if (0 == m_gesture_lose_count++)
    {
        m_gesture_prev_lose_time = curr_time;
    }
    else
    {
        if ((curr_time - m_gesture_prev_lose_time) <= GESTURE_LOSE_TIME)
        {
            //LOG_D("line=%d, m_gesture_lose_count=%d\n", __LINE__, m_gesture_lose_count);
            return true;
        }
        else
        {
            m_gesture_prev_lose_time = curr_time;
        }
    }
    //LOG_E("line=%d, m_gesture_lose_count=%d\n", __LINE__, m_gesture_lose_count);
    return false;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.save_gesture_image
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 存储手势图片，用于调试识别是否正确
 * 输入参数  : const S_VCBuf& frame  图像
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::save_gesture_image(const S_VCBuf& frame, int type)
{
    zP2PThread * p2p_thread = zP2PThread::getInstance();
    if ((NULL != p2p_thread) && p2p_thread->bDebug())
    {
        // 只有存在U盘时，才存储手势图片。
        zRokugaThread* pRoku = zVideoThread::getInstance()->getRokugaThread();
        if (NULL != pRoku)
        {
            // U盘功能去掉了
            //if (zRokugaThread::rokuStoreType::st_usb == pRoku->stType())
            {
                pRoku->save_gesture_image_thread(frame, type);
                
                if (INTRUSION_TYPE_IMAGE != type)
                {
                    save_txt_result(gesture_text_filename, m_curr_analyze_result);
                }
            }
        }
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.analyzeTemane
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月2日
 * 函数功能  : 1. 设置手势状态 2.设置开关空调标志
               注意: 返回值可能为多个状态与在一起的值
 * 输入参数  : const zBaseUtil::v2ImgResult& result  图像分析结果
               const S_VCBuf& frame                  图像
 * 输出参数  : 无
 * 返 回 值  : int: -1: 无手势，0: 有手势
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::gesture_analyze(const zBaseUtil::v2ImgResult& result)
{
    // 初始化为false
    m_stable_hand_change_flag = false;

    //LOG_D("result: nHand_=%d \n", result.nHand_);
    if (result.nHand_ < 1)
    {
        if (hand_lose_check())
        {
            (void)device_test_gesture("手势丢失\n");
            stable_hand_exit(true);
            LOG_D("stable_hand_exit. hand lose. result.nHand_=%d \n", result.nHand_);
            return 0;
        }

        (void)device_test_gesture("手势丢失\n");
        return -1;
    }

    zBaseUtil::v2Hand& hand = result.listHand_[0];
    LOG_D("hand.type_=%d \n", hand.type_);

    // 工装测试打印
    if (GESTURE_TYPE_OPEN == hand.type_)
    {
        (void)device_test_gesture("手打开.\n");
    }
    else if (GESTURE_TYPE_CLOSE == hand.type_)
    {
        (void)device_test_gesture("手握拳.\n");
    }

    // 手势
    if (false == m_stable_hand_find_flag)
    {
        if (    (1 == hand.stable_)
            &&  (GESTURE_TYPE_OPEN == hand.type_))
        {
            (void)stable_hand_enter(hand);
        }

    }
    else
    {
        // 连续跟踪失败处理
        if (GESTURE_TYPE_TRACK_LOSE == hand.type_)
        {
            m_gesture_track_lose_count++;
        }
        else if (0 != m_gesture_track_lose_count)
        {
            m_gesture_track_lose_count = 0;
        }

        // 判断是否有手移动，或者握拳
        if (GESTURE_TYPE_CLOSE == hand.type_)
        {
            stable_hand_exit(true);
            LOG_D("stable_hand_exit. GESTURE_TYPE_CLOSE.\n");
        }
        else
        {
            if (hand_move_check(hand, m_gesture_track_lose_count))
            {
                stable_hand_exit(true);
                LOG_D("stable_hand_exit. hand move.\n");
            }
            
            hand_algrithm_ajust(hand);
        }
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.reset_gesture_response_flags
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 复位手势响应相关标志
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::reset_gesture_response_flags()
{
    m_stable_hand_response_flag = false;
    m_stable_hand_blacklist_flag = false;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.reset_hand_raise_response_count
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月14日
 * 函数功能  : 复位举手响应次数:  todo: 需整理配置状态，将所有配置放在一起，现在有些乱
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::reset_hand_raise_response_count()
{
    m_hand_raise_response_count = 0;
    m_hand_raise_smart_wind_flag = 1;
}

#if BLACKLIST_PROCESS_V2_ENABLE

/*****************************************************************************
 * 函 数 名  : get_area
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月12日
 * 函数功能  : 获取手面积
 * 输入参数  : zBaseUtil::v2Hand &hand  手
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int get_area(zBaseUtil::v2Hand &hand)
{
    int width = hand.right_ - hand.left_;
    int height = hand.bottom_ - hand.top_;
    return width * height;
}

/*****************************************************************************
 * 函 数 名  : get_intersect_area
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月12日
 * 函数功能  : 获取手相交的面积
 * 输入参数  : zBaseUtil::v2Hand &h1  手
               zBaseUtil::v2Hand &h2  手
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static int get_intersect_area(zBaseUtil::v2Hand &h1, zBaseUtil::v2Hand &h2)
{
    #define MAX( a, b) ( ((a) > (b)) ? (a) : (b) )
    #define MIN( a, b) ( ((a) < (b)) ? (a) : (b) )

    int top = MAX(h1.top_, h2.top_);
    int bottom = MIN(h1.bottom_, h2.bottom_);
    int left = MAX(h1.left_, h2.left_);
    int right = MIN(h1.right_, h2.right_);

    // 矩形没有重叠的部分
    if (top >= bottom || left >= right) 
        return 0;

    return (bottom - top) * (right - left);
}

/*****************************************************************************
 * 函 数 名  : is_intersect
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月12日
 * 函数功能  : 判断手是否重叠
 * 输入参数  : zBaseUtil::v2Hand &h1   手
               zBaseUtil::v2Hand &h2   手
               double intersect_ratio  相交比率
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static bool is_intersect(
    zBaseUtil::v2Hand &h1, zBaseUtil::v2Hand &h2, double intersect_ratio)
{
    int intersect_area = get_intersect_area(h1, h2);
    int h1_area = get_area(h1);
    int h2_area = get_area(h2);

    return (intersect_area >= h1_area * intersect_ratio 
        &&  intersect_area >= h2_area * intersect_ratio);
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.has_been_in_blacklist
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 判断是否已经加入黑名单
 * 输入参数  : zBaseUtil::v2Hand &hand  手
 * 输出参数  : 无
 * 返 回 值  : bool:　true: 已加入, false: 没有加入
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::has_been_in_blacklist(
    zBaseUtil::v2Hand &hand)
{
    #define INTERSECT_RATIO_THD (0.75)
    unsigned int startIdx = (m_hands_blacklist_index - m_hands_blacklist_total + MAX_BLACKLIST_CNT) % MAX_BLACKLIST_CNT;
    for (uint32_t i=0; i<m_hands_blacklist_total; i++)
    {
        unsigned int idx = (startIdx + i) % MAX_BLACKLIST_CNT;
        if (is_intersect(m_hands_blacklist[idx].hand, hand, INTERSECT_RATIO_THD))
        {
            return true;
        }
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.add_hand_blacklist
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 添加黑名单
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::add_hand_blacklist(hand_time_t &hand)
{
    if (has_been_in_blacklist(hand.hand))
    {
        LOG_D("This hand has been in blacklist.\n");
        return;
    }

    LOG_E("blacklist process.\n");
    zBaseUtil::v2Hand &v2_hand = hand.hand;
    GestureBodyFaceVideoProcessor_GestureBodyFaceAddBlackRect(
        v2_hand.left_, v2_hand.top_, v2_hand.right_, v2_hand.bottom_, -1);

    m_hands_blacklist[m_hands_blacklist_index++] = hand;
    if (m_hands_blacklist_index >= MAX_BLACKLIST_CNT)
    {
        m_hands_blacklist_index = 0;
    }

    if (m_hands_blacklist_total < MAX_BLACKLIST_CNT)
    {
        m_hands_blacklist_total++;
    }

    zP2PThread * p2p_thread = zP2PThread::getInstance();
    if ((NULL != p2p_thread) && p2p_thread->bDebug())
    {
        char buf[128];
        (void)sprintf(buf, "blacklist: rect= (%d, %d, %d, %d)",
            v2_hand.left_, v2_hand.top_, v2_hand.right_, v2_hand.bottom_);

        // 保存黑名单结果
        save_txt_result(gesture_blacklist_filename, buf);

        // 打印进入黑名单信息
        p2p_thread->NotifyDebugInfo(buf);
    }
    return;
}

#else
/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.add_hand_blacklist
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 添加手势黑名单
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::add_hand_blacklist()
{ 
    if (!m_stable_hand_blacklist_flag)
    {
        LOG_E("blacklist process.\n");
        GestureBodyFaceVideoProcessor_GestureBodyFaceAddBlackRect(
            m_stable_hand_left_top_pos.x_pos, m_stable_hand_left_top_pos.y_pos,
            m_stable_hand_right_bottom_pos.x_pos, m_stable_hand_right_bottom_pos.y_pos,
            -1);
        m_stable_hand_blacklist_flag = true;

        zP2PThread * p2p_thread = zP2PThread::getInstance();
        if ((NULL != p2p_thread) && p2p_thread->bDebug())
        {
            char buf[128];
            (void)sprintf(buf, "blacklist: rect= (%d, %d, %d, %d)",
                m_stable_hand_left_top_pos.x_pos, m_stable_hand_left_top_pos.y_pos,
                m_stable_hand_right_bottom_pos.x_pos, m_stable_hand_right_bottom_pos.y_pos);

            // 保存黑名单结果
            save_txt_result(gesture_blacklist_filename, buf);

            // 打印进入黑名单信息
            p2p_thread->NotifyDebugInfo(buf);
        }
    }
    return;
}
#endif

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.gesture_response
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月17日
 * 函数功能  : 响应手势动作
 * 输入参数  : const S_VCBuf& frame  图像
               int gesture_flag      手势功能
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::gesture_response(const S_VCBuf& frame, int gesture_flag)
{
    if (!bUseHandRaise_) return 0;

    LOG_D("m_stable_hand_find_flag=%d, m_stable_hand_change_flag=%d, m_gesture_lose_count=%d\n", 
        m_stable_hand_find_flag, m_stable_hand_change_flag, m_gesture_lose_count);

    if (!m_stable_hand_find_flag)
    {
        return 0;
    }

    if (m_stable_hand_change_flag)
    {
        // 状态跳变
        reset_gesture_response_flags();
        (void)save_gesture_image(frame, GESTURE_TYPE_OPEN);
    }
    else
    {
        //if (BLACKLIST_HAND_LOSE_COUNT == m_gesture_lose_count)
        std::int64_t curr_time = zBaseUtil::getMilliSecond();
        if ((curr_time - m_stable_hand_time) > BLACKLIST_DURATION)
        {
            #if !BLACKLIST_PROCESS_V2_ENABLE
            add_hand_blacklist();
            #endif
        }

        if (m_stable_hand_response_flag)
        {
            // 已经响应过了，不执行下面的代码
            return 0;
        }
    }

    std::int64_t curr_time = zBaseUtil::getMilliSecond();

    if ((curr_time - m_stable_hand_time) >= GESTURE_TIME)
    {
        if (GESTURE_AC_SWITCH == gesture_flag)
        {
            std::int64_t nGap = curr_time - preEakonCtrl_;
            if (nGap >= eakonCtrlGap_)
            {
                bool bOpen = zAlgManager::CLOSED_EAKON & eakonState();
                LOG_I("eakon. bOpen =%d\n", bOpen);

                //LOG_E("ding. ding. \n");
                (void)pManager_->eakonCtrl(bOpen);
                preEakonCtrl_ = curr_time;

                // 一次举手，仅响应一次
                m_stable_hand_response_flag = true;
            }
        }
        else if (GESTURE_AC_WIND == gesture_flag)
        {
            if (zAlgManager::EakonState::OPENED_EAKON & eakonState())
            {
                LOG_D("m_hand_raise_response_count=%d, %d\n", 
                    m_hand_raise_response_count, 
                    (m_hand_raise_response_count & 0x1));
                
                #if 0 // 调试
                int smart_wind = pManager_->get_smart_wind();
                
                char buf[128];
                sprintf(buf, "m_hand_raise_response_count=%d, gesture_flag=%d, smart_wind=%d\n", m_hand_raise_response_count, gesture_flag, smart_wind);
                DtThread::getInstance()->dt_send_message(GESUTER_MSG, buf);
                #endif
                
                //if (0 == (m_hand_raise_response_count & 0x1))
                if (1) // 志高测试需求: 举手，风吹手. 不切到智能风.
                {
                    // 风吹手
                    (void)hand_raise_wind();

                    //LOG_E("ding. ding. \n");
                    (void)pManager_->TemaneCtrl2(true);
                    m_hand_raise_smart_wind_flag = 0;
                }
                else
                {
                    // 风吹人: 非智能风禁止态
                    if (zEakonThread::kazeMuki::k_unknown != pManager_->get_smart_wind())
                    {
                        m_hand_raise_smart_wind_flag = 1;

                        //LOG_E("ding. ding. \n");
                        (void)pManager_->TemaneCtrl2(true);
                        clear_smart_wind_context();
                    }
                    else 
                    {
                        // 发送空调命令，设置为 "自动扫风"
                        //LOG_E("ding. ding. \n");
                        (void)pManager_->TemaneCtrl2(true);
                        pManager_->auto_swipe_wind();
                    }
                }

                //m_hand_raise_response_count++;
                // 一次举手，仅响应一次
                m_stable_hand_response_flag = true;
            }
        }
    }

    // 手势提示音
    if (!m_stable_hand_response_flag)
    {
        if (    (m_stable_hand_change_flag)
            ||  ((curr_time - m_prev_gesture_voice_time) >= VOICE_INTERVAL_TIME))
        {
            //LOG_E("ding. \n");
            (void)pManager_->TemaneCtrl(true);
            m_prev_gesture_voice_time = curr_time;
        }
    }

    return 0;
}

int zAlgorithmDetectThread::analyzeTemane(const zBaseUtil::v2ImgResult& result, const S_VCBuf& frame)
{
    //LOG_D("zAlgorithmDetectThread::analyzeTemane tecnt == %d, nTe_=%d \n", result.nHand_, nTe_);
    int teResult = zAlgorithmDetectThread::zTesen::teIgnore;
    if (result.nHand_ < 1)
    {
        if (nTe_ > 0)
        {
            LOG_D("hand lose. nTe_=%d\n", nTe_);
            if (0 == (--nTe_))
            {
                (void)device_test_gesture("手势丢失\n");
                teResult |= zAlgorithmDetectThread::zTesen::teLose;

                reset();
                switchState(ALG_GESTURE_BODY_FACE); // 主动设置为检测态
            }
        }
        return teResult;
    }

    zBaseUtil::v2Hand& hand = result.listHand_[0];

    // 工装测试打印
    if (GESTURE_TYPE_OPEN == hand.type_)
    {
        (void)device_test_gesture("手打开.\n");
    }
    else if (GESTURE_TYPE_CLOSE == hand.type_)
    {
        (void)device_test_gesture("手握拳.\n");
    }

    // nTe_: 既表示当前手势状态，同时也侦测连续手势丢失的次数
    if (!nTe_)
    {
        // stable_可以认为是手势确信指数
        if ((1 == hand.stable_) && (hand.type_ == GESTURE_TYPE_OPEN))
        {
            teResult |= zAlgorithmDetectThread::zTesen::teFind;
            nTe_ = GTECNT; // 检测到开手势
            //LOG_D("hand find. nTe_=%d\n", nTe_);

            /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
            zP2PThread * p2p_thread = zP2PThread::getInstance();
            if ((NULL != p2p_thread) && p2p_thread->bDebug())
            {
                // 只有存在U盘时，才存储手势图片。
                zRokugaThread* pRoku = zVideoThread::getInstance()->getRokugaThread();
                if ((NULL != pRoku) && (zRokugaThread::rokuStoreType::st_usb != pRoku->stType()))
                {
                    pRoku->save_gesture_image_thread(frame, hand.type_);
                }
            }

            m_stable_hand_center_pos.x_pos = (hand.left_ + hand.right_) >> 1;
            m_stable_hand_center_pos.y_pos = (hand.top_ + hand.bottom_) >> 1;
            m_stable_hand_width_square = distance_square(hand.left_, hand.right_);
            //LOG_D("hand param. %d, %d, %d\n", hand.left_, hand.right_, m_stable_hand_width_square);
            m_hand_move_flag = false;

            m_stable_hand_left_top_pos.x_pos = hand.left_;
            m_stable_hand_left_top_pos.y_pos = hand.top_;
            
            m_stable_hand_right_bottom_pos.x_pos = hand.right_;
            m_stable_hand_right_bottom_pos.y_pos = hand.bottom_;
        }
    }
    else if ( nTe_ > 0 )
    {
        // nTe_: 既表示当前手势状态，同时也侦测连续手势丢失的次数
        if (GTECNT != nTe_)
        {
            nTe_ = GTECNT;
        }

        // 如果手移动距离比较大，不执行开关空调的动作
        if (false == m_hand_move_flag)
        {
            position_t curr_hand_pos;
            curr_hand_pos.x_pos = (hand.left_ + hand.right_) >> 1;
            curr_hand_pos.y_pos = (hand.top_ + hand.bottom_) >> 1;

            int move_distance = position_distance_square(curr_hand_pos, m_stable_hand_center_pos);
            #if 0
            LOG_D("hand move. %d, %d, %.6f, hand type=%d, stable=%d\n", 
                move_distance, m_hand_width_square,
                (move_distance * 1.0)/(m_hand_width_square >> 3),
                hand.type_, hand.stable_);
            #endif
            if (move_distance > (m_stable_hand_width_square >> 3))
            {
                LOG_D("hand move. %d, %d, %.6f\n", 
                    move_distance, m_stable_hand_width_square, 
                    (move_distance * 1.0)/(m_stable_hand_width_square >> 3));
                m_hand_move_flag = true;
            }
        }

        // 在跟踪状态时，收到非手掌打开手势，推迟响应手势动作 (利用修改时间实现)。
        if (GESTURE_TYPE_OPEN != hand.type_)
        {
            latestHandFind_ = zBaseUtil::getMilliSecond();
        }
    }

    if (GESTURE_TYPE_OPEN == hand.type_)
    {
        bAku_ = true;
        latestCenterX_ = (hand.left_ + hand.right_) / 2;
        latestCenterY_ = (hand.top_ + hand.bottom_) / 2;
    }
    else if (GESTURE_TYPE_CLOSE == hand.type_)
    {
        int centx = (hand.left_ + hand.right_) / 2;
        int centy = (hand.top_ + hand.bottom_) / 2;
        int dis = (latestCenterX_ - centx)*(latestCenterX_ - centx) + (latestCenterY_ - centy)*(latestCenterY_ - centy);

        if (    bAku_
            &&  (dis < (hand.left_ - hand.right_)*(hand.left_ - hand.right_)))
        {
            teResult |= zAlgManager::CLOSED_EAKON & eakonState() ?
                zAlgorithmDetectThread::zTesen::openEakon : zAlgorithmDetectThread::zTesen::closeEakon;

            LOG_I("%s for 1 == hand.type_ \n", zAlgManager::CLOSED_EAKON & eakonState() ? "eakon open" : "eakon close");

            /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
            zP2PThread * p2p_thread = zP2PThread::getInstance();
            if ((NULL != p2p_thread) && p2p_thread->bDebug())
            {
                // 只有存在U盘时，才存储手势图片。
                zRokugaThread* pRoku = zVideoThread::getInstance()->getRokugaThread();
                if ((NULL != pRoku) && (zRokugaThread::rokuStoreType::st_usb != pRoku->stType()))
                {
                    pRoku->save_gesture_image_thread(frame, hand.type_);
                }
            }
        }
        else
        {
            LOG_D("1 == hand.type_ bAku_ == false or dis^2 == %d too far dignore\n ", dis);
        }
        bAku_ = false;
    }

    // 如果开手势时,两次识别的手位置偏移过大，设置算法从手势跟踪态切换到手势检测态
    if ((GESTURE_TYPE_OPEN != hand.type_) && nTe_)
    {
        int centx = (hand.left_ + hand.right_) / 2;
        int centy = (hand.top_ + hand.bottom_) / 2;
        int dis = 100 * ((latestCenterX_ - centx)*(latestCenterX_ - centx) + (latestCenterY_ - centy)*(latestCenterY_ - centy));

        if (dis > 225 * (hand.left_ - hand.right_)*(hand.left_ - hand.right_))
        {
            switchState(ALG_GESTURE_BODY_FACE); // 切换到手势检测状态
        }
    }

    return teResult;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.hand_raise_wind
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月14日
 * 函数功能  : 风吹手功能
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::hand_raise_wind()
{
    #define OFF_ANGEL 5 // 偏转角度

    int lb, rb;
    pManager_->getKazeParama(lb, rb);
    //LOG_D("getKazeParama nLeft == %d, nRight = %d \n", lb, rb);

    int dis = zBaseUtil::getDistance(FACE_F, cameraType_, 
        m_stable_hand_right_bottom_pos.x_pos - m_stable_hand_left_top_pos.x_pos);

    if (dis < GFACE_BODY_MIN_DIS)
    {
        dis = GFACE_BODY_MIN_DIS;
    }
    else if (dis > GFACE_BODY_MAX_DIS)
    {
        dis = GFACE_BODY_MAX_DIS;
    }

    int angle = zBaseUtil::getAngleV3(
        cameraType_, nWidth_, lb, m_stable_hand_center_pos.x_pos);

    int lA = angle - OFF_ANGEL;
    int rA = angle + OFF_ANGEL;

    if (lA < lb) { lA = lb;}
    else if (lA > rb) {lA = rb;}
    
    if (rA > rb) {rA = rb;}
    else if (rA < lb) {rA = rb;}

    int ac_control_dis = (dis + AC_DISTANCE_PRECITION/2) / AC_DISTANCE_PRECITION;
    lA = lA - lA % 5;
    rA = rA - rA % 5;
    int arr[SMART_WIND_CONFIG_SIZE] = { 1, ac_control_dis, ac_control_dis, lA, rA, lA, rA, 1};

    (void)KazemukiCtrl_Energy(arr, true);
    LOG_D("ac_control_dis = %d, leftAngle = %d, rightAngle = %d\n",
        ac_control_dis, lA, rA);
    
    #if 0
    char buf[128];
    sprintf(buf, "distance = %d, leftAngle = %d, rightAngle = %d\n",
        dis, lA, rA);

    (void)device_test_gesture(buf);
    #endif
    
    return 0;
}

void zAlgorithmDetectThread::clear_smart_wind_context()
{
    angleMin_ = -GANGLEDIFF;
    angleMax_ = -GANGLEDIFF;
    m_last_no_person_time = 0;
    m_prev_wind_time = 0;
    m_prev_wind_dist = 0;
    return;
}

bool zAlgorithmDetectThread::handRaise(int tesen, int gesture_flag)
{
    if (!bUseHandRaise_) return false;

    bool result = false;
    bool gestrue_find_first_flag = false;

    // 20151024
    if (zAlgorithmDetectThread::zTesen::teFind & tesen)
    {
        std::int64_t curr_time = zBaseUtil::getMilliSecond();

        //if (GESTURE_AC_SWITCH == gesture_flag)
        {
            (void)pManager_->TemaneCtrl(true);
            m_prev_gesture_voice_time = curr_time;
        }

        gestrue_find_first_flag = true;
        latestHandFind_ = curr_time;
        result = true;
        bTrackHand_ = true;
    }

    if (zAlgorithmDetectThread::zTesen::teLose & tesen)
    {
        //pManager_->TemaneCtrl(false); // 需求更改，不发送手势提示音
        result = true;
        bTrackHand_ = false;
    }

    //LOG_D("bTrackHand=%d\n", bTrackHand_);
    if (    bTrackHand_ 
        &&  !gestrue_find_first_flag)
    {
        std::int64_t nCur = zBaseUtil::getMilliSecond();
        std::int64_t nElapse = nCur - latestHandFind_;

        // 未打开或关闭空调前，发送手势提示音
        //if (GESTURE_AC_SWITCH == gesture_flag)
        {
            if ((nCur - m_prev_gesture_voice_time) >= VOICE_INTERVAL_TIME)
            {
                (void)pManager_->TemaneCtrl(true);
                m_prev_gesture_voice_time = nCur;
            }
        }

        LOG_D("preEakonCtrl=%lld, latestHandFind=%lld, nElapse=%lld, nCur=%lld \n",
            preEakonCtrl_, latestHandFind_, nElapse, nCur);
        if (    (nElapse >= GESTURE_TIME)
            &&  (!m_hand_move_flag))
        {
            if (GESTURE_AC_SWITCH == gesture_flag)
            {
                std::int64_t nGap = nCur - preEakonCtrl_;
                if (nGap >= eakonCtrlGap_)
                {
                    bool bOpen = zAlgManager::CLOSED_EAKON & eakonState();
                    LOG_I("eakon. bOpen =%d\n", bOpen);

                    (void)pManager_->eakonCtrl(bOpen);
                    result = true;
                    preEakonCtrl_ = nCur;

                    bTrackHand_ = false; // 一次举手，仅响应一次
                }
            }
            else if (GESTURE_AC_WIND == gesture_flag)
            {
                if (zAlgManager::EakonState::OPENED_EAKON & eakonState())
                {
                    LOG_D("m_hand_raise_response_count=%d, %d\n", 
                        m_hand_raise_response_count, (m_hand_raise_response_count & 0x1));
                    if (0 == (m_hand_raise_response_count & 0x1))
                    {
                        // 风吹手
                        (void)hand_raise_wind();
                    }
                    else
                    {
                        // 风吹人: 非智能风禁止态
                        if (zEakonThread::kazeMuki::k_unknown != pManager_->get_smart_wind())
                        {
                            m_hand_raise_smart_wind_flag = 1;
                            clear_smart_wind_context();
                        }
                        else 
                        {
                            // 发送空调命令，设置为 "自动扫风"
                            pManager_->auto_swipe_wind();
                        }
                    }

                    m_hand_raise_response_count++;

                    //switchState(ALG_GESTURE_BODY_FACE);
                    bTrackHand_ = false; // 一次举手，仅响应一次
                }
            }
        }
    }

    return result;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.get_daynight_change_gap
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 补光模式改变时，会导致算法失效，该函数获取失效的时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int64_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int64_t zAlgorithmDetectThread::get_daynight_change_gap()
{
    int alg_mode = algState();
    if (ALG_MOTION_BODY_FACE == alg_mode)
    {
        return BODY_FACE_DETECTION_GAP;
    }
    else if (ALG_MOTION_BOBY == alg_mode)
    {
        return MOTION_DETECTION_GAP;
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : daynight_mode_arbitrate
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 自动补光仲裁
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::daynight_mode_arbitrate(double image_light)
{
    std::unique_lock<std::mutex> lock(m_light_mutex);

    if (!bAutoLighting_)
    {
        zP2PThread * p2p_thread = zP2PThread::getInstance();
        if (NULL != p2p_thread)
        {
            int client_size = p2p_thread->get_client_count();
            if (0 == client_size)
            {
                LOG_I("bAutoLighting_=%d\n", bAutoLighting_);
                bAutoLighting_ = true;
            }
        }
        return 0;
    }

    // 补光模式变化，至少要持续一段时间，防止频繁切换
    int64_t curr_time = zBaseUtil::getMilliSecond();
    int64_t time_gap = curr_time - m_last_daynight_change_time;
    // 开补光无条件至少持续30秒，如果亮度小于MAX_LIGHTING_THD，至少持续10分钟
    if (    (time_gap <= THD_LIGHTING_DURATION)
        ||  (!bDay_ && (time_gap <= m_sleepPeriod) && (image_light <= MAX_LIGHTING_THD)))
    {
        if (0 != m_light_change_count)
        {
            m_light_change_count = 0;
        }
        return 0;
    }

    // 亮度连续变化处理
    if (image_light < 0)
    {
        m_light_change_count = 0;
    }
    else
    {
        if (    (bDay_ && (image_light < lightingF_))
            ||  (!bDay_ && (image_light > lightingC_)))
        {
            m_light_change_count++;
        }
        else
        {
            m_light_change_count = 0;
        }
    }

    #if 0
    LOG_D("bDay_=%d, image_light=%f, m_light_change_count=%d, lightingC_=%f, m_sleepPeriod=%lld\n", 
        bDay_, image_light, m_light_change_count, lightingC_, m_sleepPeriod);
    #endif

    //if (m_light_change_count >= (!bDay_ ? MAX_LIGHT_CHANGE_COUNT : 1))
    if (m_light_change_count >= MAX_LIGHT_CHANGE_COUNT)
    {
        (void)set_daynight_mode(!bDay_);

        if (zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel())
        {
            (void)switchState_standby(bDay_ ? ALG_MOTION_BODY_FACE: ALG_MOTION_BOBY);
        }
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.standby_mode_arbitrate
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月30日
 * 函数功能  : 待机模式仲裁
 * 输入参数  : zBaseUtil::v2ImgResult& imgResult  识别结果
 * 输出参数  : 无
 * 返 回 值  : bool : true : 识别模式有改变，false: 无改变
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::standby_mode_arbitrate(
    zBaseUtil::v2ImgResult& imgResult)
{
    #define MAX_NOMOTION_COUNT 600

    if (bMotionDetected)
    {
        if (m_image_nomotion >= MAX_NOMOTION_COUNT)
        {
            switchState(ALG_MOTION);
            m_image_nomotion = 0;
            bMotionDetected = false;
            LOG_D("change to motion detection. state==%d\n", algState());
            return true;
        }

        if (    imgResult.nHand_ <= 0 
            &&  imgResult.nHead_ <= 0
            &&  imgResult.nRect_ <= 0)
        {
            m_image_nomotion++;
        }
        else
        {
            m_image_nomotion = 0;
        }
    }
    else
    {
        /* add by 刘春龙, 2016-03-03, 原因: 
        实测发现，在ALG_MOTION模式时，可能没有Rect,直接有Head，所以添加(imgResult.nHead_>0)的判断。*/
        if (    (imgResult.nRect_ > 0)
            ||  (imgResult.nHead_ > 0))
        {
            if (    (1 == imgResult.nRect_)
                &&  (0 == imgResult.listRect_[0].left_)
                &&  (0 == imgResult.listRect_[0].top_)
                &&  (nWidth_ == imgResult.listRect_[0].right_)
                &&  (nHeight_ == imgResult.listRect_[0].bottom_) )
            {
                LOG_D("ignore motion detection outputs the whole frame at the beginning.\n");
                return false;
            }

            if (zBaseUtil::zEakonModel::DEKAKE_MODEL == eakonModel())
            {
                /* add by 刘春龙, 2016-01-24, 原因: 白天安防模式，忽略本次移动侦测结果 */
                if (bDay_)
                {
                    imgResult.nRect_ = 0;
                }
                
                (void)switchState(bDay_ ? ALG_MOTION_BODY_FACE: ALG_MOTION_BOBY);
            }
            else
            {
                (void)switchState(ALG_GESTURE_BODY_FACE);
            }

            m_image_nomotion = 0;
            bMotionDetected = true;
            LOG_D("change to motion detection. state==%d\n", algState());
            return true;
        }
    }

    return false;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.init_energy_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 初始化，节能仲裁相关变量
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::init_energy_config()
{
    std::unique_lock<std::mutex> lock(m_energy_mutex);

    m_energy_time = 0;
    m_saving_energy_flag = false;
    m_energy_have_persion = false;

    (void)memset(m_energy_buf_config, 0, sizeof(m_energy_buf_config));
    m_energy_muki = zEakonThread::kazeMuki::k_unknown;
    m_energy_gestrue_flag = false;
    m_itelligent_switch = false;

    // 不需要发送无人标志
    #if 0 
    if (NULL != pManager_)
    {
        zEakonThread* pEakon = pManager_->getEakon();
        if (NULL != pEakon)
        {
            pEakon->send_person_flag(false);
        }
    }
    #endif

    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.send_energy_config
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 发送智能风配置 -- 是否节能
 * 输入参数  : bool open_saving  true: 开节能  false: 关节能
 * 输出参数  : 无
 * 返 回 值  : void  0: ok, else: no
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::send_energy_config(bool open_saving)
{
    zEakonThread* pEakon = pManager_->getEakon();
    if (NULL == pEakon)
    {
        LOG_E("null pointer. \n");
        return -1;
    }

    int buf_config[ SMART_WIND_CONFIG_SIZE ] = {0};
    if (open_saving)
    {
        // 保存吹风配置
        pEakon->get_prev_smart_wind_config(
            m_energy_buf_config, m_energy_muki, m_energy_gestrue_flag);

        LOG_D("m_energy_muki: %d, m_energy_gestrue_flag=%d\n", 
            m_energy_muki, m_energy_gestrue_flag);

        (void)memcpy(buf_config, m_energy_buf_config, sizeof(buf_config));
        buf_config[0] = 0; // 人数
        buf_config[1] = 0; // 距离
        buf_config[2] = 0; // 距离
        buf_config[7] = 0; // 人数
    }
    else
    {
        (void)memcpy(buf_config, m_energy_buf_config, sizeof(buf_config));
    }

    pEakon->KazemukiCtrl_v2(buf_config, m_energy_muki, m_energy_gestrue_flag);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.KazemukiCtrl_Energy
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月18日
 * 函数功能  : 向主控发送吹风配置，如果处于节能状态，退出节能状态
 * 输入参数  : int* arr           吹风配置
               bool gesture_flag  手势标志
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::KazemukiCtrl_Energy(int* arr, bool gesture_flag)
{
    int ret = -1;
    // 节能吹风时，复位energy上下文，退出节能状态
    if (m_saving_energy_flag)
    {
        init_energy_config();
    }

    if (NULL != pManager_)
    {
        ret = pManager_->KazemukiCtrl(arr, gesture_flag);
    }

    return ret;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.save_energy_arbitrate
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月14日
 * 函数功能  : 节能仲裁
 * 输入参数  : const zBaseUtil::v2ImgResult& result  分析结果
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::save_energy_arbitrate(const zBaseUtil::v2ImgResult& result)
{
    zEakonThread* pEakon = pManager_->getEakon();
    if (NULL == pEakon)
    {
        LOG_E("NULL pointer. \n");
        return;
    }

    bool itelligent_switch = pEakon->get_itelligent_switch();
    if (itelligent_switch != m_itelligent_switch)
    {
        //LOG_D("itelligent_switch=%d, m_itelligent_switch=%d\n", itelligent_switch, m_itelligent_switch);
        init_energy_config();
        m_itelligent_switch = itelligent_switch;
    }

    // 空调开 且 智能总开关开，才进行仲裁
    if (    !(zAlgManager::EakonState::OPENED_EAKON & eakonState())
        ||  (!m_itelligent_switch) )
    {
        return;
    }

    std::unique_lock<std::mutex> lock(m_energy_mutex);

    #if 0 
    LOG_D("m_energy_have_persion=%d, m_saving_energy_flag=%d, m_energy_time=%lld\n", 
        m_energy_have_persion, m_saving_energy_flag, m_energy_time);
    #endif

    if (    (result.nHead_ < 1)
        &&  (result.nHand_ < 1))
    {
        // 4秒无人, 进入节能送风
        if (0 == m_energy_time)
        {
            m_energy_time = zBaseUtil::getMilliSecond();
        }
        else if (!m_saving_energy_flag)
        {
            std::int64_t curr_time = zBaseUtil::getMilliSecond();
            if ((curr_time - m_energy_time) >= MAX_SAVING_ENERGY_DURATION)
            {
                (void)(pEakon->send_person_flag(false));
                m_energy_have_persion = false;

                if (0 == send_energy_config(true))
                {
                    m_saving_energy_flag = true;
                }
            }
        }
    }
    else
    {
        if (!m_energy_have_persion)
        {
            (void)(pEakon->send_person_flag(true));
            m_energy_have_persion = true;
        }

        if (0 != m_energy_time)
        {
            m_energy_time = 0;
        }

        // 恢复送风
        if (m_saving_energy_flag)
        {
            if (0 == send_energy_config(false))
            {
                m_saving_energy_flag = false;
            }
        }
    }
    return;
}


void zAlgorithmDetectThread::clear_context(bool wind_reset)
{
    reset_hand_raise_response_count();
    init_energy_config();

    if (wind_reset)
    {
        zEakonThread* pEakon = pManager_->getEakon();
        if (NULL != pEakon) {
            pEakon->reset_prev_smart_wind_config();
        }
    }
    return;
}

void zAlgorithmDetectThread::analyzeFrame(const S_VCBuf& frame)
{
    std::unique_lock<std::mutex> lock(ctrlMutex_);

    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    std::int64_t nElapse = 0;
    if (0 != m_preFrame)
    {
        nElapse = curr_time - m_preFrame;
    }

    LOG_D("Program Duration: %lld, nElapse=%lld, curr_time=%lld, m_preFrame=%lld\n", 
		zBaseUtil::program_duration(), nElapse, curr_time, m_preFrame);
    m_preFrame = curr_time;
    m_timer.elapse(nElapse);

    //LOG_D("GestureBodyFaceVideoDetect frameGap == %llu \n", frame.TimeStamp);
    double frameGap = frame.TimeStamp / G2DXX;
    if (bFirstDet_)
    {
        frameGap = 0;
        bFirstDet_ = false;
    }
    else
    {
        if (frameGap < MIN_FRAME_GAP) {
            frameGap = MIN_FRAME_GAP;
        }else if (frameGap > MAX_FRAME_GAP) {
            frameGap = MAX_FRAME_GAP;
        }
    }

    // 白天阈值设置高一些，安防模式需要尽量避免误检
    float body_threshold = 0.6;
    float face_threshold = 0.6;
    if (bDay_) 
    {
        body_threshold = 0.7;
        face_threshold = 0.7;
    }

    zBaseUtil::v2ImgResult imgResult;
    if (complex_detect_disable()) // 判断，是否禁用综合检测算法
    {
        imgResult.lighting_ = m_prev_lighting_;
        LOG_D("complex detect disableb !!!!\n");
    }
    else
    {
        std::int64_t t_bgn = zBaseUtil::getMilliSecond();
        std::string strResult = GestureBodyFaceVideoProcessor_GestureBodyFaceVideoDetect(
            frame.buf, nWidth_, nHeight_, frameGap, body_threshold, face_threshold, 0.6, 0.6);
        std::int64_t t_end = zBaseUtil::getMilliSecond();
        std::int64_t t_duration = t_end - t_bgn;
        if (t_duration >= 300)
        {
            //LOG_E("GestureBodyFaceVideoDetect duration = %lld \n", t_duration);
            LOG_D("GestureBodyFaceVideoDetect: %s \n", strResult.c_str());
        }
        m_curr_analyze_result = strResult;

        (void)zBaseUtil::analyzeV2String(strResult, imgResult);
        m_prev_lighting_ = imgResult.lighting_;
        //LOG_D("bMotionDetected=%d, m_image_nomotion=%d, algstate=%d\n", bMotionDetected, m_image_nomotion, algState());
        //LOG_D("imgResult.lighting_ == %lf \n", imgResult.lighting_);
    }

    #if 0 // P2P端，输出图像亮度
    {
        char buf[128];
        (void)sprintf(buf, "imgResult.lighting_ == %lf", imgResult.lighting_);
        zP2PThread::print_msg(buf);
    }
    #endif

    // 自动补光仲裁处理
    (void)daynight_mode_arbitrate(imgResult.lighting_);

    // 待机模式仲裁
    (void)standby_mode_arbitrate(imgResult);

    // 节能模式仲裁
    (void)save_energy_arbitrate(imgResult);

    #if BLACKLIST_PROCESS_V2_ENABLE
    // 手势黑名单仲裁
    (void)hand_blacklist_arbitrate(imgResult);
    #endif

    // 检测到智能风标志变更后，先切换到扫风状态。
    int smart_wind_flag = pManager_->get_smart_wind();
    if (m_curr_smart_wind_flag != smart_wind_flag)
    {
        pManager_->auto_swipe_wind();
        m_curr_smart_wind_flag = smart_wind_flag;
    }

    do
    {
        zBaseUtil::zEakonModel emodel = eakonModel();
        if (m_prev_emodel != emodel)
        {
            m_prev_emodel = emodel;
            clear_context();
        }
        LOG_D("emodel =%d, bTrackTaninn_=%d\n", emodel, bTrackTaninn_);

        if (zBaseUtil::zEakonModel::IE_MODEL == emodel)
        {
            int gesture_flag = pManager_->get_gesture_flag();
            bool intelligent_enable = pManager_->getEakon()->get_intelligent_enable_flag();

            LOG_D("gesture_flag=%d, m_prev_gesture_flag=%d, "
                "m_hand_raise_smart_wind_flag=%d, smart_wind=%d, ac_state=%s, "
                "m_hand_raise_response_count =%u, intelligent_enable=%s\n", 
                gesture_flag, m_prev_gesture_flag, m_hand_raise_smart_wind_flag, pManager_->get_smart_wind(), 
                ((zAlgManager::EakonState::OPENED_EAKON & eakonState()) ? "open" : "close"),
                m_hand_raise_response_count, zBaseUtil::to_string(intelligent_enable).c_str());

            // 手势功能切换处理
            if (m_prev_gesture_flag != gesture_flag)
            {
                clear_context(false); // FIX: 手势功能切换，不复位当前定向风状态。
                stable_hand_exit(false);
                m_prev_gesture_flag = gesture_flag;
            }

            // 智能使能标志处理
            if (m_intelligent_enable != intelligent_enable)
            {
                m_intelligent_enable = intelligent_enable;
            }
            if (!m_intelligent_enable) {
                return;
            }

            if (    GESTURE_DISABLE != gesture_flag
                ||  DtThread::getInstance()->is_check_enable(GESTURE_MSG))
            {
                #if 0
                int teSen = analyzeTemane(imgResult, frame);

                int need_proc_smart_wind = 0;
                bool result = handRaise(teSen, gesture_flag);

                if (!result) fist(imgResult, teSen);
                #endif

                if (GESTURE_AC_WIND == gesture_flag)
                {
                    if (0 == gesture_analyze(imgResult))
                    {
                        (void)gesture_response(frame, gesture_flag);
                    }
                }
                else // GESTURE_AC_SWITCH
                {
                    if (m_stable_hand_find_flag) // 手势开关机，且手势进入"稳定态"。
                    {
                        //std::int64_t t_bgn = zBaseUtil::getMilliSecond();
                        std::string gesture_result = GestureProcessor_GestureDetect(
                            frame.buf, nWidth_, nHeight_, 0.6);
                        //std::int64_t t_end = zBaseUtil::getMilliSecond();
                        //LOG_E("gesture_result duration = %lld\n", t_end - t_bgn);
                        //LOG_E("gesture_result=%s\n", gesture_result.c_str());

                        zBaseUtil::v2ImgResult gesResult;
                        (void)zBaseUtil::analyzeV2String(gesture_result, gesResult);
                        if (gesResult.nHand_ > 0) {
                            swap_hand(imgResult, gesResult);
                            //LOG_E("swap_hand !!!");
                        }
                    }

                    gesture_analyze_zone(imgResult);
                }
            }

            if (    (GESTURE_AC_WIND != gesture_flag)
                ||  (0 != m_hand_raise_smart_wind_flag)
                ||  DtThread::getInstance()->is_check_enable(WIND_MSG))
            {
                if (zAlgManager::EakonState::OPENED_EAKON & eakonState())
                {
                    // 非智能风禁止态
                    if (zEakonThread::kazeMuki::k_unknown != pManager_->get_smart_wind())
                    {
                        analyzeHead(imgResult);
                    }
                    else
                    {
                        DtThread::getInstance()->dt_send_message(WIND_MSG, "智能风已禁用");
                    }
                }
                else
                {
                    // 工装测试打印
                    DtThread::getInstance()->dt_send_message(WIND_MSG, "空调关闭");
                }
            }
        }
        else if (zBaseUtil::zEakonModel::DEKAKE_MODEL == emodel)
        {
            // 切换补光模式后，或者切换到夜间移动侦测，一段时间内的移动侦测检测结果认为无效
            if (   (curr_time - m_last_daynight_change_time) <= get_daynight_change_gap()
                || (curr_time - m_night_monitor_switch_time) <= get_daynight_change_gap())
            {
                LOG_D("curr_time=%lld, change_time=%lld, change_gap=%lld, m_night_monitor_switch_time=%lld\n", 
                    curr_time, m_last_daynight_change_time, get_daynight_change_gap(), m_night_monitor_switch_time);

                imgResult.nRect_ = 0; // 复位移动帧测结果为零
            }

            // 统计连续检测到头肩的次数
            if (imgResult.nHead_ > 0 && imgResult.nRect_ > 0)
            {
                ++m_taninn_frame_count;
                //LOG_D("m_taninn_frame_count=%d\n", m_taninn_frame_count);
            }
            else if (0 != m_taninn_frame_count)
            {
                m_taninn_frame_count = 0;
            }

            // state 3
            //LOG_D("nRect_=%d, nHead_=%d\n", imgResult.nRect_, imgResult.nHead_);
            if (    (bMotionDetected && (imgResult.nRect_ > 0) && !bDay_)
                ||  ((imgResult.nHead_ > 0) && (imgResult.nRect_ > 0)))
            {
                if (!bTrackTaninn_)
                {
                    #if 0
                    LOG_E("bMotionDetected=%d, imgResult.nRect_=%d, imgResult.nHead_=%d, algState_=%d\n", 
                        bMotionDetected, imgResult.nRect_, imgResult.nHead_, algState_);
                    #endif

                    // 白天安防模式，检测到连续多帧头肩，才上报告警
                    if (! ((imgResult.nHead_ > 0) && (m_taninn_frame_count < TANINN_FRAMES_THRESHOLD)))
                    {
                        //LOG_D("m_taninn_frame_count=%d, imgResult.nHead_=%d\n", m_taninn_frame_count, imgResult.nHead_);

                        onTaninnFind_notify(imgResult);
                        onTaninnFind_bgnStranger();

                        zP2PThread * p2p_thread = zP2PThread::getInstance();
                        if ((NULL != p2p_thread) && p2p_thread->bDebug())
                        {
                            save_gesture_image(frame, INTRUSION_TYPE_IMAGE);
                            save_txt_result(gesture_intrusion_filename, m_curr_analyze_result);
                        }                        
                    }
                }
                else 
                {
                    /* add by 刘春龙, 2016-01-25, 原因: 入侵结束30秒内，收到入侵告警，取消定时 */
                    if (m_taninn_lose_report)
                    {
                        m_timer.removeTimer(ZM_END_ROKUSTRANGER);
                        m_taninn_lose_report = false;
                    }
                    
                    if (!m_start_alarm_record_flag)
                    {
                        onTaninnFind_bgnStranger();
                    }
                }

                // 复位连续检测到告警结束的统计值
                if (0 != m_taninn_lose_count)
                {
                    m_taninn_lose_count = 0;
                }
            }
            else
            {
                if (bTrackTaninn_)
                {
                    if (!m_taninn_lose_report)
                    {
                        if (++m_taninn_lose_count >= MAX_TANINNLOSE_COUNT)
                        {
                            onTaninnLose();
                            m_taninn_lose_report = true;
                        }
                    }
                }
            }
        }
        else
        {
            // TODO
        }
    } while (false);
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.switchState
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 算法功能切换
 * 输入参数  : int nState  状态值
 * 输出参数  : 无
 * 返 回 值  : int -1: 非法参数 ，否则，返回上一次状态值
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::switchState(int nState)
{
    if (    (NULL == this)
        ||  (nState < ALG_GESTURE_BODY_FACE)
        ||  (nState > ALG_MOTION))
    {
        return -1;
    }

    // 状态为0, 不管上次的状态，都执行状态切换
    int ret_state = algState_;
    if (    (ALG_GESTURE_BODY_FACE == nState)
        ||  (algState_ != nState))
    {
        LOG_D("chang state. %d to %d\n", algState_, nState);
        (void)GestureBodyFaceVideoProcessor_GestureBodyFaceSwitchState(nState);

        #if 0 // GestureBodyFaceVideoProcessor_GestureBodyFaceSetDetThresholds have been deleted !!!
        /* add by 刘春龙, 2016-03-31, 原因: 针对白天安防的误检问题，增加了一个函数调整阈值 */
        if (ALG_MOTION_BODY_FACE == nState)
        {
            (void)GestureBodyFaceVideoProcessor_GestureBodyFaceSetDetThresholds(0.5f, 0.68f);
        }
        else
        {
            (void)GestureBodyFaceVideoProcessor_GestureBodyFaceSetDetThresholds(0.5f, 0.5f);
        }
        #endif

        //GestureBodyFaceVideoProcessor_GestureBodyFaceVideoClear();
        algState_ = nState;
    }
    return ret_state;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.switchState_standby
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月24日
 * 函数功能  : 待机模式下，不切换算法状态
 * 输入参数  : int nState  算法状态
 * 输出参数  : 无
 * 返 回 值  : int
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::switchState_standby(int nState)
{
    if (bMotionDetected)
    {
        int old_state = switchState(nState);
        // 记录进入夜间安防的时间
        if (    ALG_MOTION_BOBY == nState
            &&  -1 != old_state)
        {
            m_night_monitor_switch_time = zBaseUtil::getMilliSecond();
        }
        return old_state;
    }
    return -1;
}

//////////////////////////////////////////////////////////////////////////
bool zAlgorithmDetectThread::autoLighting(bool bAuto)
{
    std::unique_lock<std::mutex> lock(m_light_mutex);
    bool old = bAutoLighting_;
    bAutoLighting_ = bAuto;

    // 设置为非补光模式
    if (!bDay_)
    {
        (void)set_daynight_mode(true);
    }
    return old;
}

void zAlgorithmDetectThread::get_lighting_mode(bool &auto_mode, bool &day)
{
    std::unique_lock<std::mutex> lock(m_light_mutex);
    auto_mode = bAutoLighting_;
    day = bDay_;
}
//////////////////////////////////////////////////////////////////////////


zBaseUtil::zEakonModel zAlgorithmDetectThread::eakonModel()
{
    return pManager_->getEakonModel();
}

int zAlgorithmDetectThread::eakonState()
{
    return pManager_->getEakonState();
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.save_txt_result
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 写日志文件
 * 输入参数  : const std::string &filename  日志文件名
               const std::string &result    识别结果
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::save_txt_result(
    const std::string &filename, const std::string &result)
{
    FILE *fp = fopen(filename.c_str(), "a+");
    if (NULL == fp)
    {
        LOG_E("open failed. filename=%s\n", filename.c_str());
        return;
    }

    (void)fprintf(fp, "%s: %lldms: %s \r\n", 
        zFileUtil::getTimeString().c_str(), zBaseUtil::getMilliSecond(), 
        result.c_str());
    fclose(fp);
    return;
}

#if BLACKLIST_PROCESS_V2_ENABLE
/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.hand_lost
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 判断手势丢失
 * 输入参数  : const zBaseUtil::v2ImgResult& result  识别结果
               bool &invalid                         返回值： true: 本次结果无效，false：有效
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::hand_lost(const zBaseUtil::v2ImgResult& result)
{
    if (result.nHand_ > 0)
    {
        return false;
    }

    if (!m_stable_hand_find_flag)
    {
        return true;
    }

    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    if (0 == m_prev_hand_lost_time)
    {
        m_prev_hand_lost_time = curr_time;
    }
    else if ((curr_time - m_prev_hand_lost_time) <= GESTURE_LOSE_TIME)
    {
        return true;
    }
    else
    {
        m_prev_hand_lost_time = curr_time;
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : hand_compare
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 比较手是否移动
 * 输入参数  : const zBaseUtil::v2Hand &h1  手
               const zBaseUtil::v2Hand &h2  手
 * 输出参数  : 无
 * 返 回 值  : 0: 没有移动， -1: 移动
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zAlgorithmDetectThread::hand_compare(
    const zBaseUtil::v2Hand &h1, const zBaseUtil::v2Hand &h2)
{
    position_t p1, p2;

    p1.x_pos = (h1.left_ + h1.right_) >> 1;
    p1.y_pos = (h1.top_ + h1.bottom_) >> 1;

    p2.x_pos = (h2.left_ + h2.right_) >> 1;
    p2.y_pos = (h2.top_ + h2.bottom_) >> 1;

    int w1 = distance_square(h1.left_, h1.right_);
    int move_distance = position_distance_square(p1, p2);
    if (move_distance > (w1 >> 3))
    {
        LOG_D("hand move: %d, %d\n", move_distance, w1);
        return -1;
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : hand_blacklist_arbitrate
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月11日
 * 函数功能  : 手势黑名单仲裁
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::hand_blacklist_arbitrate
    (const zBaseUtil::v2ImgResult& result)
{
    // 无手处理
    if (hand_lost(result))
    {
        m_hands_for_blacklist.clear();
        return;
    }

    // 识别结果无效，不处理，直接返回
    if (result.nHand_ <= 0)
    {
        return;
    }

    std::int64_t curr_time = zBaseUtil::getMilliSecond();

    // 有手处理
    std::list<hand_time_t> new_hands;
    for (int i=0; i < result.nHand_; i++)
    {
        if (    GESTURE_TYPE_OPEN == result.listHand_[i].type_
            ||  GESTURE_TYPE_TRACK_LOSE == result.listHand_[i].type_)
        {
            hand_time_t hand_time;
            hand_time.hand = result.listHand_[i];
            hand_time.detect_start_time = curr_time;
            new_hands.push_back(hand_time);
        }
    }

    // 去掉移动的手
    std::list<hand_time_t>::iterator iter = m_hands_for_blacklist.begin();
    while (iter != m_hands_for_blacklist.end())
    {
        bool hand_remove_flag = true;

        std::list<hand_time_t>::iterator iter_new_hand = new_hands.begin();
        while (iter_new_hand != new_hands.end())
        {
            if (0 == hand_compare(iter->hand, iter_new_hand->hand))
            {
                new_hands.erase(iter_new_hand++);
                hand_remove_flag = false;
                break;
            }else ++iter_new_hand;
        }

        if (hand_remove_flag)
        {
            m_hands_for_blacklist.erase(iter++);
        }
        else ++iter;
    }

    // 加入新的手
    if (0 != new_hands.size())
    {
        m_hands_for_blacklist.insert(
            m_hands_for_blacklist.end(), new_hands.begin(), new_hands.end());
    }

    // 判断是否超过时间阈值，加入黑名单
    iter = m_hands_for_blacklist.begin();
    while (iter != m_hands_for_blacklist.end())
    {
        if ((curr_time - iter->detect_start_time) >= BLACKLIST_DURATION)
        {
            LOG_D("curr_time=%lld, detect_time=%lld\n", curr_time, iter->detect_start_time);
            add_hand_blacklist(*iter);
            m_hands_for_blacklist.erase(iter++);
        }else ++iter;
    }

    return;
}
#endif

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.palm_expand
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 计算扩展后的手位置
 * 输入参数  : zBaseUtil::v2Hand &palm  手掌位置
               int width                图像宽
               int height               图像高
               int expand               扩展像素数
 * 输出参数  : 无
 * 返 回 值  : zBaseUtil
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
zBaseUtil::v2Hand & zAlgorithmDetectThread::palm_expand(
    zBaseUtil::v2Hand &palm, int width, int height, int expand)
{
    palm.left_ -= expand;
    if (palm.left_ < 0) { palm.left_ = 0; }

    palm.top_ -= expand;
    if (palm.top_ < 0) { palm.top_ = 0; }

    palm.right_ += expand;
    if (palm.right_ > width) { palm.right_ = width; }

    palm.bottom_ += expand;
    if (palm.bottom_ > height) { palm.bottom_ = height; }

    return palm;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.set_gesture_roi
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 设置手势独立检测ROI
 * 输入参数  : const zBaseUtil::v2Hand& hand : 手位置
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::set_gesture_roi(
    const zBaseUtil::v2Hand& hand)
{
    #define EXPAND_SIZE 50
    
    zBaseUtil::v2Hand hand_expanded = hand;
    (void)palm_expand(hand_expanded, nWidth_, nHeight_, EXPAND_SIZE);

    bool ret = GestureProcessor_GestureAddROI(
        hand_expanded.left_, hand_expanded.top_,
        hand_expanded.right_,hand_expanded.bottom_);
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.clear_gesture_roi
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 清除手势ROI设置
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::clear_gesture_roi()
{
    return GestureProcessor_GestureAddROI(0, 0, nWidth_, nHeight_);
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.get_gesture_flags
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年5月26日
 * 函数功能  : 获取手势状态
 * 输入参数  : uint32_t &gesture      手势功能标志
               uint32_t &raise_count  手势定向风时，举手次数统计值
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::get_gesture_flags(uint32_t &gesture, uint32_t &raise_count)
{
    gesture = m_prev_gesture_flag;
    raise_count = m_hand_raise_response_count;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.gesture_analyze_zone
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月27日
 * 函数功能  : 支持手势悬停
 * 输入参数  : const zBaseUtil::v2ImgResult& result  识别结果
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::gesture_analyze_zone(
    const zBaseUtil::v2ImgResult& result)
{
    if (result.nHand_ <= 0)
    {
        if (hand_lose_check())
        {
            ++m_hand_lose_count;
            // 连续N次检测到手丢失，认为握拳动作。
            if (FIST_HAND_LOST_THD == m_hand_lose_count)
            {
                LOG_E("fist: caused by hand lost!!!");
                add_fist_count(FIST_CNT_THD);
            }

            // 判断持续无手N秒，退出手势稳定态。
            if (1 == m_hand_lose_count)
            {
                m_first_hand_lost_time = zBaseUtil::getMilliSecond();
            }
            else
            {
                std::int64_t currtime = zBaseUtil::getMilliSecond();
                std::int64_t duration = currtime - m_first_hand_lost_time;
                if (    m_temperature_operated
                    ||  (duration >= HAND_LOST_DURATION))
                {
                    stable_hand_exit(true);
                    LOG_E("exit hand stable state: caused by timeout!!!");
                    (void)clear_gesture_roi();
                }
            }
        }

        (void)device_test_gesture("手势丢失\n");
        return;
    }

    // 检测到手，复位m_hand_lose_count为0.
    m_hand_lose_count = 0;

    zBaseUtil::v2Hand& hand = result.listHand_[0];
    //LOG_E("hand.type_=%d, hand.stable_=%d\n", hand.type_, hand.stable_);

    // 工装测试打印
    if (GESTURE_TYPE_OPEN == hand.type_)
    {
        (void)device_test_gesture("手打开.\n");
    }
    else if (GESTURE_TYPE_CLOSE == hand.type_)
    {
        (void)device_test_gesture("手握拳.\n");
    }

    // 手势
    if (false == m_stable_hand_find_flag)
    {
        if (    (1 == hand.stable_)
            &&  (GESTURE_TYPE_OPEN == hand.type_))
        {
            std::int64_t duration = zBaseUtil::getMilliSecond() - m_prev_switch_time;
            if (duration >= MIN_SWITCH_GAP)
            {
                (void)stable_hand_enter(hand);
                (void)set_gesture_roi(hand);

                // 初始化，手势中心区
                recorder.init(hand);
                (void)pManager_->TemaneCtrl(true); // ding
            }
            else
            {
                LOG_E("switch duration: %lld\n", duration);
                std::string msg = "switch duration= ";
                zP2PThread::print_msg(msg + zBaseUtil::to_string(duration));
            }
        }
        return;
    }

    // 连续跟踪失败处理
    if (GESTURE_TYPE_TRACK_LOSE == hand.type_)
    {
        ++m_gesture_track_lose_count;
        if (FIST_HAND_TRACK_LOST_THD <= m_gesture_track_lose_count)
        {
            if (!m_temperature_operated)
            {
                // 连续跟踪丢失N次，认为是"握拳动作"
                add_fist_count(FIST_CNT_THD);
                LOG_E("fist: caused by track lose!!!");
            }
            else
            {
                recorder.clearStayMillis(); // 复位驻留时长
            }
            m_gesture_track_lose_count = 0;
        }
    }
    else
    {
        if (0 != m_gesture_track_lose_count)
        {
            m_gesture_track_lose_count = 0;
        }
    }

    recorder.record(hand); // 更新区域
    if (GESTURE_TYPE_CLOSE == hand.type_) // 握拳处理
    {
        ZONE_POS zone = recorder.getZone();
        if (CENTER_ZONE == zone)
        {
            add_fist_count(1);
        }
        else
        {
            reset_fist_count();
        }
        recorder.clearStayMillis(); // 复位驻留时长
    }
    else if (GESTURE_TYPE_OPEN == hand.type_)
    {
        ZONE_POS zone = recorder.getZone();
        if (    (CENTER_ZONE == zone) 
            &&  (m_center_fist_count >= FIST_CNT_THD))
        {
            // if (1)
            {
                // 开关机
                bool bOpen = zAlgManager::CLOSED_EAKON & eakonState();
                (void)pManager_->eakonCtrl(bOpen);
                m_prev_switch_time = zBaseUtil::getMilliSecond();
                LOG_E("eakon. bOpen =%d\n", bOpen);
                zP2PThread::print_msg(bOpen ? "open": "close");

                stable_hand_exit(false);
                (void)clear_gesture_roi();
            }
        }
        else
        {
            reset_fist_count(); // 复位

            #define HAND_STAY_TIMEUP_MILLIS 1000
            if (recorder.getZoneStayMillis() >= HAND_STAY_TIMEUP_MILLIS)
            {
                if (zone != CENTER_ZONE)
                {
                    recorder.clearStayMillis();
                    (void)pManager_->ZoneStayTimeup(zone);
                    m_temperature_operated = true;
                }
            }
        }
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.add_fist_count
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 统计总握拳次数
 * 输入参数  : int cnt  握拳次数
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::add_fist_count(int cnt)
{
    m_center_fist_count += cnt;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.reset_fist_count
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 复位握拳次数
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::reset_fist_count()
{
    if (0 != m_center_fist_count)
    {
        m_center_fist_count = 0;
    }
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.swap_hand
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月26日
 * 函数功能  : 交互识别到的手
 * 输入参数  : zBaseUtil::v2ImgResult &dst  目的结果
               zBaseUtil::v2ImgResult &src  源结果
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::swap_hand(
    zBaseUtil::v2ImgResult &dst, zBaseUtil::v2ImgResult &src)
{
    int hands_temp = dst.nHand_;
    zBaseUtil::v2Hand *pHand_temp = dst.listHand_;

    dst.nHand_ = src.nHand_;
    dst.listHand_ = src.listHand_;

    src.nHand_ = hands_temp;
    src.listHand_ = pHand_temp;
    return;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.complex_detect_disable
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年11月27日
 * 函数功能  : 判断是否需要综合检测算法
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool: true: 禁用  false: 使能
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::complex_detect_disable()
{
    if (    (GESTURE_AC_SWITCH == m_prev_gesture_flag)
        &&  (zBaseUtil::zEakonModel::IE_MODEL == m_prev_emodel)
        &&  (m_stable_hand_find_flag)
        &&  (m_center_fist_count >= FIST_CNT_THD) )
    {
        return true;
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.get_lighting_duration
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月3日
 * 函数功能  : 获取连续开补光时长，单位：毫秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std::int64_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t zAlgorithmDetectThread::get_lighting_duration()
{
    std::int64_t duration = 0;    
    if (!bDay_)
    {
        duration = zBaseUtil::getMilliSecond() - m_last_daynight_change_time;
    }
    return duration;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.get_no_person_duration
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月3日
 * 函数功能  : 获取持续无手无头时长，单位: 毫秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t zAlgorithmDetectThread::get_no_person_duration()
{
    std::int64_t duration = 0;
    if (m_no_person_flag)
    {
        duration = zBaseUtil::getMilliSecond() - m_no_person_time;
    }
    return duration;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.is_hand_wind
 * 负 责 人  : lcl
 * 创建日期  : 2017年1月4日
 * 函数功能  : 判断是否处于手势定向风状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool: true: 手势定向风状态
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::is_hand_wind()
{
    if (GESTURE_AC_WIND == m_prev_gesture_flag)
    {
        if (0 != (m_hand_raise_response_count & 0x1))
        {
            return true;
        }
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.is_free_time
 * 负 责 人  : lcl
 * 创建日期  : 2017年1月4日
 * 函数功能  : 判断是否为闲时间段
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool：true: 闲时
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool zAlgorithmDetectThread::is_free_time()
{
    zFileUtil::zFileManager* pm = zFileUtil::zFileManager::getInstance();
    if (pm->get_time_sync())
    {
        time_t tt = zBaseUtil::ntp_time();
        struct tm* ptm = localtime(&tt);
        if (    (ptm->tm_hour >= m_min_free_time)
            &&  (ptm->tm_hour < m_max_free_time))
        {
            return true;
        }
        return false;
    }
    return true;
}

/*****************************************************************************
 * 函 数 名  : zAlgorithmDetectThread.program_restart_arbitrate
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月3日
 * 函数功能  : 程序重启仲裁

     midea 定时重启需求：
    1. 重启应用，不重启系统
    2. 当连续运行15天后，满足以下条件：重启应用
       1）无P2P客户登陆
       2）先不加：如存在网络时间，且系统时间为闲时 - 凌晨3:00 ~ 6:00 。 
       3）连续没有人头与手势, 持续5分钟。 或 4）补光灯一直处于开启状态60分钟。
       5）没有录像。
	   6) 非手势定向风状态
    3. 重启后，需恢复之前的状态：
       1）如果当前为手势定向风，保存当前手势定向风角度。
       2）待定 ？？
    4. 当连续运行21天后，直接重启应用。 -- 后续也可考虑，重启系统。
    5. 连续录像超过24小时，自动停止。

 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zAlgorithmDetectThread::program_restart_arbitrate()
{
    std::int64_t prog_duration = zBaseUtil::program_duration();
    if (prog_duration >= m_reboot_duration_long)
    {
        LOG_D("long: reboot enter!! prog_duration=%lld", prog_duration);
        if (!zBaseUtil::reboot_program())
        {
            LOG_E("reboot failed!! ");
        }
    }
    else if (prog_duration >= m_reboot_duration_short)
    {
        static uint32_t print_count = 0;
        #define PRINT_SEQUENCE 30

        ++print_count;
		if (0 == (print_count % PRINT_SEQUENCE))
		{
            LOG_D("pManager_->get_p2pclient_count()= %d", pManager_->get_p2pclient_count());
            LOG_D("zVideoThread::getInstance()->getRokuState()= %d", zVideoThread::getInstance()->getRokuState());
            LOG_D("is_hand_wind()= %d", is_hand_wind());
            LOG_D("is_free_time()= %d", is_free_time());
		}
		
        if (    (pManager_->get_p2pclient_count() <= 0)
            &&  (zVideoThread::getInstance()->getRokuState() <= 0)
            &&  (!is_hand_wind())
            &&  is_free_time() )
        {
            std::int64_t curr_no_person_duration = get_no_person_duration();
            std::int64_t curr_lighting_duration = get_lighting_duration();

			if (0 == (print_count % PRINT_SEQUENCE))
			{
                LOG_D("get_no_person_duration()= %lld", curr_no_person_duration);
                LOG_D("get_lighting_duration()= %lld", curr_lighting_duration);
			}

            if (    (curr_no_person_duration >= m_no_person_thd)
                ||  (curr_lighting_duration >= m_lighting_thd))
            {
                if (0 != (print_count % PRINT_SEQUENCE))
                {
                    LOG_D("get_no_person_duration()= %lld", curr_no_person_duration);
                    LOG_D("get_lighting_duration()= %lld", curr_lighting_duration);
                }
                
                LOG_D("short: reboot enter!! prog_duration=%lld", prog_duration);
                if (!zBaseUtil::reboot_program())
                {
                    LOG_E("reboot failed!! ");
                }
            }
        }
    }
    return;
}

