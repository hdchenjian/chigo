#ifndef __ZALGORITHMMANAGER_H__
#define __ZALGORITHMMANAGER_H__

#include <cstdint>
#include <vector>
#include <mutex>
#include <chrono>
#include "3rdParty/V4L2ImageCap/jni/cap_types.h"
#include "zUtil.h"
//#include "faceBodyThread.h"
//#include "faceThread.h"
//#include "tamaneThread.h"
//#include "zEakonThread.h"
//#include "zVideoThread.h"

#define ENCODED_FRAMES_COUNT  25 // 存储H264编码图像帧的数组大小

// 带时间戳的编码图像结构
typedef struct encoded_frame_s
{
    S_H264EncBuf m_enc_img;
    std::int64_t m_enc_time_stamp;
}encoded_frame_t;


class zEakonThread;
class zTamaneThread;
class zFaceBodyThread;
class zAlgorithmDetectThread;
class zAlgManager
{
public:

    enum EakonState{
        UNKNOWN_EAKON = 0,
        CLOSED_EAKON = 1,
        OPENED_EAKON = 2,
        KAZE_EAKON = 4,
    };
    enum Threads
    {
        TEMANETHREAD = 0,
        FACEBODYTHREAD,
        FACETHREAD,
        ROKUGATHREAD,
        CBTHREAD ,
        THREADSIZE,
        THREADANONYMOUS ,
    };

public:
    zAlgManager(int nWidth, int nHeight, std::string strDevName = "/dev/mcu_dev");
    ~zAlgManager();
    //////////////////////////////////////////////////////////////////////////
    static void free_frame(S_VCBuf& src);
    static bool alloc_frame(S_VCBuf& src, int frame_size);
    static bool copy_frame(S_VCBuf& dst, S_VCBuf& src);
    bool addFrame(const S_VCBuf& buf);
    bool getFrame(S_VCBuf& buf, zAlgManager::Threads who = TEMANETHREAD);
    bool getFrame2(S_VCBuf& buf, int &width, int &height, zAlgManager::Threads who = TEMANETHREAD);

    static void free_encoded_frame(S_H264EncBuf &encoded_frm);
    void encoded_frame_queue_init();
    void encoded_frame_queue_end();
    bool copy_encoded_frame(encoded_frame_t &dst, const S_H264EncBuf &src_buf, std::int64_t TimeStamp);
    bool addEncFrame(const S_H264EncBuf& buf, std::int64_t TimeStamp);
    bool getEncFrames(encoded_frame_t frms[ENCODED_FRAMES_COUNT], int &frms_size);
    void clrEncFrames();

    void onEvent(int nEvent);
    void setEakonModel(zBaseUtil::zEakonModel model);
    zBaseUtil::zEakonModel getEakonModel();
    //void setEakonDev(ZDEVICE device);
    void setVideoState(zBaseUtil::zVideoState state);
    zBaseUtil::zVideoState getVideoState();
    void setEakonState(int state);
    int getEakonState();
    int getKazeParama(int& x, int& y);
    bool bFocus();
    int KazemukiCtrl(int* arr, bool gesture_flag); // arr[0] is the mura num, so arr size = arr[0] << 1
    int get_smart_wind();
    int get_gesture_flag();
    bool get_live_video_state();

    //////////////////////////////////////////////////////////////////////////
    int TaninnFind_BgnStranger();
    int TaninnLose_EndStranger();

    int TaninnFind_EakonNotify();
    int TaninnLose_EakonNotify();

    void get_lighting_mode(bool &auto_mode, bool &day);
    bool autoLighting(bool bAuto);
    void chgLighting(bool bOpen);
    //////////////////////////////////////////////////////////////////////////
    int eakonCtrl(bool bOpen);
    int TemaneCtrl(bool bFocus);    // get or lose
    int TemaneCtrl2(bool bFocus);   // get or lose
    int auto_swipe_wind();
    int ZoneStayTimeup(ZONE_POS zone);
    //////////////////////////////////////////////////////////////////////////

    int StartThreads();
    int p2pEvent(int nEvent);

    zEakonThread* getEakon();
    zAlgorithmDetectThread* getAlgorithmDetect();
    void get_gesture_flags(uint32_t &gesture, uint32_t &raise_count);

    int get_p2pclient_count();
    bool get_dt_enable();

private:
    int m_cameraType;
    int m_nImgWidth;
    int m_nImgHeght;
    std::string m_strDevName;
    zTamaneThread* m_temaneThread;
    //zFaceThread* m_faceThread;
    zFaceBodyThread* m_faceBodyThread;
    zAlgorithmDetectThread* m_algDetThread;
    zEakonThread* m_eakonThread;
    //zVideoThread* m_videoThread;

    //state
    std::mutex m_modelMutex;
    std::mutex m_stateMutex;
    zBaseUtil::zEakonModel m_ieModel;
    zBaseUtil::zVideoState m_videoState;
    int m_eakonState;

    bool g_bFresh[THREADSIZE];          // 0t1f2d
    std::mutex m_bufMutex;
    S_VCBuf m_curFrame;

    // 编码帧缓存管理，防止录像丢帧，导致花屏
    std::mutex m_enc_frm_mutex;
    encoded_frame_t m_encoded_frame_queue[ENCODED_FRAMES_COUNT];
    unsigned int m_encoded_frame_rindex;
    unsigned int m_encoded_frame_windex;
    unsigned int m_encoded_frame_queue_size;

    std::int64_t m_latestCBFrameTime;
    std::int64_t m_curCBFrameTime;

    std::int64_t m_curTemaneFrameTime;
    std::int64_t m_lastestTemaneFrame;
};
#endif
