#ifndef __DEVICE_TEST_H__
#define __DEVICE_TEST_H__

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>  // for close
#include <netinet/in.h>  // for compling under FreeBSD

#include "zThread.h"

/*
 * 在Windows下使用的一些定义在UNIX下没有，为了统一定义一下。
 */
#define SOCKET  int
#define SOCKET_ERROR    -1
#define INVALID_SOCKET  -1
#define socket_error    errno
// Windows下SOCKET和文件是完全分开的，不过它上面非缓冲文件系统都用了下划
// 线，如close为_close，所以可以在Windows上把closesocket定义为close，但
// 这样一来如果同时使用socket和非缓冲文件操作并且要求跨平台的话就比较麻烦，
// 而反过来在UNIX上定义一个closesocket不会有什么副作用。
#define closesocket     close

using std::list;

// 这里可以写成一致的，只用下面一种方式即可。分开做主要是想示意一下Windows
// 下和UNIX下的一些区别。
#ifdef WIN32

#define IPoct(addr)   addr.sin_addr.S_un.S_un_b
#if 0
#define IPoct1(addr)  IPoct(addr).s_b1
#define IPoct2(addr)  IPoct(addr).s_b2
#define IPoct3(addr)  IPoct(addr).s_b3
#define IPoct4(addr)  IPoct(addr).s_b4
#endif

#else

#if 0
// 用移位必须转换为本地字节序。
#define IPoct(addr, no)  ((ntohl(addr.sin_addr.s_addr) >> ((4 - no) * 8)) & 0xff)
#define IPoct1(addr)    IPoct(addr, 1)
#define IPoct2(addr)    IPoct(addr, 2)
#define IPoct3(addr)    IPoct(addr, 3)
#define IPoct4(addr)    IPoct(addr, 4)
#endif
struct nIP
{
    unsigned char s_b1;
    unsigned char s_b2;
    unsigned char s_b3;
    unsigned char s_b4;
};
#define IPoct(addr)   (*(nIP *)&addr.sin_addr.s_addr)

#endif  // WIN32

#define IPoct1(addr)  IPoct(addr).s_b1
#define IPoct2(addr)  IPoct(addr).s_b2
#define IPoct3(addr)  IPoct(addr).s_b3
#define IPoct4(addr)  IPoct(addr).s_b4


#define MAX_CONNECTIONS 16
#define MAX_CMD_LEN     128
#define MIN_CMD_LEN     12
#define MAX_DATA_LEN    (MAX_CMD_LEN - MIN_CMD_LEN) // type, len, data, 0xdead

#define SOCKET_OK       0
#define SOCKET_CLOSED   1
#define SOCKET_FAILED   -1
#define SOCKET_ECHO     2

/* 命令类型 */
typedef enum 
{
    CMD_TYPE_NULL       = 0, // 无效命令类型
    ALL_CHECK           = 1, // 全检
    ALL_CHECK_STOP      = 2, // 停止全检
    GESUTER_CHECK       = 3, // 手势检测
    GESTURE_CHECK_STOP  = 4, // 停止手势检测
    WIND_CHECK          = 5, // 智能风检测 
    WIND_CHECK_STOP     = 6, // 停止智能风检测
    SECURITY_CHECK      = 7, // 安防检测 
    SECURITY_CHECK_STOP = 8, // 停止安防检测
    LIGHT_CHECK         = 9, // 打开红外灯检测
    LIGHT_CHECK_STOP    = 10,// 关闭红外灯检测
    GET_IMAGE           = 11,// 获取一张图片, 这个与CAMERA_CHECK共用一条命令

    VERSION_CHECK       = 12, // 开始版本检测
    COMMUNICATE_CHECK   = 13, // 开始通讯校验
    CHIPSET_CHECK       = 14, // 开始芯片检测
    WIFI_CHECK          = 15, // 开始 wifi检测
    CAMERA_CHECK        = 16, // 开始摄像头检测
    HEARTBEAT           = 17, // 心跳
    CMD_TYPE_BUTT       = 18
}client_cmd_type_t;

typedef enum
{
    UPLOAD_IMAGE        = 0,
    MESSAGE             = 1,
    VERSION_OK          = 2,
    VERSION_FAILED      = 3,
    COMMUNICATE_OK      = 4,
    COMMUNICATE_FAILED  = 5,
    CHIPSET_OK          = 6,
    CHIPSET_FAILED      = 7,
    WIFI_OK             = 8,
    WIFI_FAILED         = 9,
    CAMERA_OK           = 10,
    CAMERA_FAILED       = 11,
    CMD_RESPOND_BUTT    = 12
}client_cmd_respond_t;

typedef enum 
{
    GESTURE_MSG     = 0, // 手势检测开关
    WIND_MSG        = 1, // 智能风检测开关
    SECURITY_MSG    = 2, // 安防检测开关 
    LIGHT_MSG       = 3, // 打开红外灯检测开关
    MSG_BUTT        = 4
}switch_type_t;

typedef enum
{
    OTG_MODE_AUTO   = 0, // 自动模式
    OTG_MODE_DEVICE = 1,   
    OTG_MODE_HOST   = 2,  
    OTG_MODE_BUTT   = 3  
}otg_mode_t;

typedef struct rcv_buffer_s
{
    SOCKET s;
    char buf[MAX_CMD_LEN];
    int pos;

    int switchs[MSG_BUTT]; // 消息开关
}rcv_buffer_t;

typedef struct wired_rcv_buffer_s
{
    rcv_buffer_t rcv_buf;
    std::int64_t last_time;
}wired_rcv_buffer_t;

class DtThread : public zThread
{
public:
    static DtThread* getInstance();
    static void destroyInstance();

    void Wait();
    void WakeUp(bool wireless, bool bGateWay);

    bool is_check_enable(int type);
    bool dt_message_enable(int type);
    int dt_send_message(int type, const char *data);
    bool dt_is_started();

    static void dt_set_otg_mode(int mode);
    static int dt_get_otg_mode();
    static void dt_set_start_time();

private:
    DtThread();
    ~DtThread();

    void proc();

    void heartbeat_update();
    void heartbeat_send();

    int wait_connect_ready(const struct timeval *timeout);
    int socket_connect(const std::string &ip, unsigned short wPort);
    int socket_reconnect(const std::string &ip, unsigned short wPort);
    void clear_receive_data_buffer();

    int dt_send_packet(SOCKET client, int type, const char *data);
    int dt_send_image(SOCKET client, int type, S_VCBuf * pFrame);
    int dt_send_jpg_image(SOCKET client, int type, unsigned char *jpg_data, unsigned int jpg_size);

    int dt_proc_cmd(rcv_buffer_t &rSock, int type, char * para, int len);
    int dt_parse_cmd(rcv_buffer_t &rSock, int *read);
    int dt_handle_cmds(rcv_buffer_t &rSock);

    void wired_client_clear();
    int wired_init(const char *ip, unsigned short wPort);
    void wireless_process();
    void wired_process();

protected:

private:
    static DtThread* m_instance;
    volatile bool m_wireless_mode_enable;
    volatile bool m_use_gateway;

    std::mutex m_ctrlMutex;
    rcv_buffer_t m_rcv_buf;

    SOCKET tcpSock;
    SOCKET maxSockID;
    fd_set sockSet;

    bool bAutoLight;
    std::int64_t m_heartbeat_time;
    volatile bool m_received_heartbeat_flag;

    bool m_initialize_flag;

    // 有线工装
    static std::int64_t m_wired_start_time;
    static volatile otg_mode_t m_otg_mode;
    static bool m_wired_timeout;

    SOCKET m_wired_tcpSock;
    SOCKET m_wired_maxSockID;
    fd_set m_wired_sockSet;

    std::mutex m_wired_sock_mutex;
    wired_rcv_buffer_t m_wired_rcv_buf;
    volatile int m_wired_rcv_buf_size; // 连接数，限制最多1个

    volatile bool m_wired_initialize_flag;
    volatile bool m_wired_connected_flag;
};


#endif

