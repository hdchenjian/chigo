#ifndef __DEVICE_POWER_TEST_H__
#define __DEVICE_POWER_TEST_H__

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstdint>
#include <ctime>

#include "zThread.h"
#include "zFile.h"
#include "zRokugaThread.h"
#include "zAlgorithmManager.h"

class DtPower_OnAndOff : public zThread
{
public:
    DtPower_OnAndOff(int width,int height);
    ~DtPower_OnAndOff();

public:
    bool init();
    void Start();
    void proc();
    bool doAction();

    long getLastPowerOnTime() const;
    void setLastPowerOnTime(const long& time);
    long getPowerOntimes() const;
    void setPowerOntimes(const long& times);

    bool saveImageToStorage();

private:
    bool createXml(const std::string& filename);
    bool parseXml(const std::string& filename);
    bool changValueXml(const std::string& filename,const std::string& attribute,long value);
    void check_usb_storage();
    void close_internal();

private:
    zFileUtil::zFileManager* m_fileManager;
    std::string m_usbDir;
    volatile bool m_isUsbExist;
    long m_lastPowerOnTime; //最后一次开关机时间
    long m_powerOnTimes;//开关机次数

    int m_nWidth;
    int m_nHeight;

    int m_nThreadCount;
    int m_start_time;   // 启动时间
};

#endif