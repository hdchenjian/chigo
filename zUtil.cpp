#include "zUtil.h"
#include <algorithm>
#ifdef _WINDOWS
#include <Windows.h>
#else 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/poll.h>
#include <linux/input.h>
#include <sys/wait.h>
#include <sys/syscall.h>//Linux system call for thread id
#include <dirent.h>
#endif

#include <thread>
#include <chrono>
#include <cstdint>
#include <vector>

#include "tinyxml.h"
//#include "zImport.h"
//#include "faceThread.h"
#include "zLog.h"
#include "zFile.h"

#include <sys/system_properties.h>

/* define bsd_signal */
#if (__ANDROID_API__ > 19)
#include <android/api-level.h>
#include <android/log.h>
#include <signal.h>
#include <dlfcn.h>

extern "C" {
    typedef __sighandler_t (*bsd_signal_func_t)(int, __sighandler_t);
    bsd_signal_func_t bsd_signal_func = NULL;

    __sighandler_t bsd_signal(int s, __sighandler_t f) {
        if (bsd_signal_func == NULL) {
            // For now (up to Android 7.0) this is always available
            bsd_signal_func = (bsd_signal_func_t) dlsym(RTLD_DEFAULT, "bsd_signal");

            if (bsd_signal_func == NULL) {
                // You may try dlsym(RTLD_DEFAULT, "signal") or dlsym(RTLD_NEXT, "signal") here
                // Make sure you add a comment here in StackOverflow
                // if you find a device that doesn't have "bsd_signal" in its libc.so!!!

                __android_log_assert("", "bsd_signal_wrapper", "bsd_signal symbol not found!");
            }
        }

        return bsd_signal_func(s, f);
    }
}
#endif

namespace zBaseUtil
{

std::mutex g_resolution_video_mutex;  // 切分辨率和抓图，互斥锁
std::mutex g_resolution_record_mutex; // 切分辨率和录像，互斥锁

static std::mutex g_ntp_sync_mutex; // NTP 时间同步锁
static time_t g_before_sync_rtc_time = 0; // 同步执行前的 RTC时间
static std::int64_t g_before_sync_sys_time = 0; // 同步执行前的 系统时间: 单位: 秒
static volatile bool g_ntp_syncing_flag = false; // true: 同步中 false：同步完成


static std::int64_t g_program_begin_time = 0;

const char *sync_time_flag_file = "/cache/synced_time.flag";
static const char *sync_time_file = "/sdcard/sync_time.log";

//#define MAX_OPEN_DIS (500)            // cm
//#define MIN_OPEN_DIS (250)
//#define MAX_BULD_LUMI (1)            //
//#define MIN_BULD_LUMI (0.5)
//#define MAX_DIS       (10)
//#define PRECISION (0.001)

//#define DELAY_OPEN_TIME (3000)    // time_elapse = DELAY_OPEN_TIME * detect_gap
//#define ANGLE_HOSHOU (20)
#define DIS_HOSHOU (0.125)

#ifdef _WINDOWS
#define ZDEVICE void*
#else 
#define ZDEVICE int
#endif

//////////////////////////////////////////////////////////////////////////
#define MCU_INPUT_NAME  "mcu_input"
#define MAX_INPUT_COUNT  (10)

#define MCU_EVENT_TYPE  (0x01)
#define MCU_EVENT_CODE  (0xF2)
//////////////////////////////////////////////////////////////////////////

    // mod the src arr, when return
    int separateMura(int* arr, int nLen, int nGap)
    {
        if (nLen < 1) return 0;
        int index = 1;
        int nBegin = 0;
        int nEnd = 0;
        int nMura = 0;
        int nCnt = 1;

        int* tmpArr = (int*)malloc(sizeof(int) * nLen);
        memset(tmpArr, 0, sizeof(int) * nLen);

        for (; index < nLen; index++)
        {
            int dis = arr[index] - arr[nEnd];

            if (dis <= nGap)
            {
                nEnd = index;
                ++nCnt;
            }
            else
            {
                int nMid = ((nEnd - nBegin + 1) >> 1) + nBegin;
                tmpArr[nMid] = nCnt;
                nBegin = nEnd = index;
                nCnt = 0;
                ++nMura;
            }
        }
        int nMid = ((nEnd - nBegin + 1) >> 1) + nBegin;
        tmpArr[nMid] = nCnt + 1;
        ++nMura;

        memcpy(arr, tmpArr, sizeof(int) * nLen);
        free(tmpArr);
        return nMura;
    }
    int getmg(const std::vector<Da>& da, int lb, int rb, int& lA, int& rA, int& dis)
    {
        size_t nSize = da.size();

        std::vector<int> vc;
        int prea = lb;
        int pred = 0;
        int agap = 0;
        int dsum = 0;
        for (size_t i = 0; i < nSize; i++)
        {
            //LOG_D("getmg index == %d, angle == %d, dis == %d \n", i, da.at(i).a, da.at(i).d);
            agap = da.at(i).a - prea;
            //prea = da.at(i).a;
            if (0 == i)
            {
                agap = agap << 1;
                dsum = da.at(i).d << 1;
            }
            else
            {
                dsum = pred + da.at(i).d;
            }

            vc.push_back(agap * dsum);
            prea = da.at(i).a;
            pred = da.at(i).d;
        }
        vc.push_back(((rb - prea) << 1) * (pred << 1));
        //LOG_D("vc push %d\n", ((rb - prea) << 1) * (pred << 1));

        int max = 0;
        size_t np = 0;
        for (size_t i = 0; i < vc.size(); i++)
        {
            //LOG_D("vc.size == %zu \n", vc.size());
            int val = vc.at(i);
            //LOG_D("index == %d, val == %d \n", i, val);
            if (val > max)
            {
                np = i;
                max = val;
            }
        }

        //LOG_D("da.size == %zu, vc.size == %zu, np == %d \n", da.size(), vc.size(), np);
        if (0 == np)
        {
            //lA = rA = lb;
            lA = lb;
            rA = da.at(np).a;
            dis = da.at(np).d;
        }
        else if ((1 + np) == vc.size())
        {
            //lA = rA = rb;
            lA = da.at(np-1).a;
            rA = rb;
            dis = da.at(np-1).d;
        }
        else
        {
            //lA = rA = (da.at(np).a + da.at(np - 1).a) >> 1;
            lA = da.at(np - 1).a;
            rA = da.at(np).a;
            dis = (da.at(np).d + da.at(np - 1).d) >> 1;
        }
        //LOG_D("getmg LA == %d, RA == %d \n", lA, rA);
        return lA;
    }

    //int getmg(const std::vector<Da>& da, int lb, int rb, int& lA, int& rA, int& dis)
    //{
    //  size_t nSize = da.size();

    //  std::vector<int> vc;
    //  int prea = lb;
    //  int pred = 0;
    //  int agap = 0;
    //  int dsum = 0;
    //  for (size_t i = 0; i < nSize; i++)
    //  {
    //      agap = da.at(i).a - prea;
    //      //prea = da.at(i).a;
    //      if (0 == i)
    //      {
    //          agap << 1;
    //          dsum = da.at(i).d << 1;
    //      }
    //      else
    //      {
    //          dsum = pred + da.at(i).d;
    //      }

    //      //LOG_D("vc push %d\n", agap * dsum);
    //      vc.push_back(agap * dsum);
    //      prea = da.at(i).a;
    //      pred = da.at(i).d;
    //  }
    //  vc.push_back(((rb - prea) << 1) * (pred << 1));
    //  //LOG_D("vc push %d\n", ((rb - prea) << 1) * (pred << 1));

    //  int max = 0;
    //  size_t np = 0;
    //  for (size_t i = 0; i < vc.size(); i++)
    //  {
    //      //LOG_D("vc.size == %zu \n", vc.size());
    //      int val = vc.at(i);
    //      if (val > max)
    //      {
    //          np = i;
    //          max = val;
    //      }
    //  }

    //  LOG_D("da.size == %zu, vc.size == %zu, np == %d \n", da.size(), vc.size(), np);
    //  if (0 == np)
    //  {
    //      lA = rA = lb;
    //      dis = da.at(np).d;
    //  }
    //  else if ((1 + np) == vc.size())
    //  {
    //      lA = rA = rb;
    //      dis = da.at(np-1).d;
    //  }
    //  else
    //  {
    //      lA = rA = (da.at(np).a + da.at(np - 1).a) >> 1;
    //      dis = (da.at(np).d + da.at(np - 1).d) >> 1;
    //  }
    //  return lA;
    //}

    static std::string EnumChildFind(TiXmlElement* pElement, std::string mb);
    static std::string EnumSlibingFind(TiXmlElement* pElement, std::string mb);
    static std::string EnumAttrFind(TiXmlElement* pElement, std::string mb);

    std::string EnumAttrFind(TiXmlElement* pElement, std::string mb)
    {
        std::string val;
        TiXmlAttribute* pAttr = pElement->FirstAttribute();
        while (NULL != pAttr)
        {
            if (mb == pAttr->Name())
            {
                val = pAttr->Value();
            }
            pAttr = pAttr->Next();
        }
        return val;
    }
    
    std::string EnumSlibingFind(TiXmlElement* pElement, std::string mb)
    {
        std::string val;
        while (NULL != pElement)
        {
            val = EnumAttrFind(pElement, mb);
            if (!val.empty()) break;
            val = EnumChildFind(pElement, mb);

            pElement = pElement->NextSiblingElement();
        }

        return val;
    }

    std::string EnumChildFind(TiXmlElement* pElement, std::string mb)
    {
        std::string val;
        TiXmlElement* pChildElement = pElement->FirstChildElement();
        while (NULL != pChildElement)
        {
            val = EnumSlibingFind(pChildElement, mb);

            if (!val.empty()) break;
            pChildElement = pChildElement->FirstChildElement();
        }
        return val;
    }
    int getHeadersInfo(HeadersInfo& hdInfo, const char* szInfo)
    {
        int nResult = 0;
        TiXmlDocument doc("hdInfo.xml");
        doc.Parse(szInfo);
        TiXmlElement* pElement = doc.FirstChildElement("imageResult");
        TiXmlAttribute* attr = pElement->FirstAttribute();
        std::string key;
        std::string val;
        // enum attr
        while (NULL != attr)
        {
            key = attr->Name();
            if ("name" == key)
            {
                val = attr->Value();
                if (val.length() > MAX_NAME - 1)
                {
                    val = val.substr(0, MAX_NAME - 1);
                }
                sprintf(hdInfo.name, "%s", val.c_str());
                ++nResult;
            }
            else if ("timeStamp" == key)
            {
                hdInfo.timeStamp = attr->IntValue();
                ++nResult;
            }
            else if ("lighting" == key)
            {
                hdInfo.headLighting = attr->DoubleValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        TiXmlNode* pNode = pElement->FirstChild("listHead");
        if (NULL == pNode)
        {
            LOG_D("null pointer. listHead.\n");
            return nResult;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("cnt" == key)
            {
                hdInfo.headCnt = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        pNode = pNode->FirstChild("Head");
        while(NULL != pNode)
        {
            pElement = pNode->ToElement();
            // crash if err
            val = pElement->Attribute("label");
            int index = std::atoi(val.c_str());
            index = index < 0 ? 0 : (index > MAX_HEAD_CNT ? MAX_HEAD_CNT - 1 : index);
            attr = pElement->FirstAttribute();
            while (NULL != attr)
            {
                key = attr->Name();
                if ("label" == key)
                {
                    hdInfo.heads[index].headLable = attr->IntValue();
                    ++nResult;
                }
                else if ("left" == key)
                {
                    hdInfo.heads[index].headLeft = attr->IntValue();
                    ++nResult;
                }
                else if ("top" == key)
                {
                    hdInfo.heads[index].headTop = attr->IntValue();
                    ++nResult;
                }
                else if ("right" == key)
                {
                    hdInfo.heads[index].headRight = attr->IntValue();
                    ++nResult;
                }
                else if ("bottom" == key)
                {
                    hdInfo.heads[index].headBottom = attr->IntValue();
                    ++nResult;
                }
                else if ("type" == key)
                {
                    hdInfo.heads[index].headType = attr->IntValue();
                    ++nResult;
                }
                else if ("prob" == key)
                {
                    hdInfo.heads[index].headProb= attr->DoubleValue();
                    ++nResult;
                }
                attr = attr->Next();
            }
            pNode = pNode->NextSibling("Head");
        }
        return nResult;
    }

    int getUnndouInfo(UnndouInfo& unInfo, const char* szInfo)
    {
        int nResult = 0;
        TiXmlDocument doc("udinfo.xml");
        doc.Parse(szInfo);
        TiXmlElement* pElement = doc.FirstChildElement("imageResult");
        TiXmlAttribute* attr = pElement->FirstAttribute();
        std::string key;
        std::string val;
        // enum attr
        while (NULL != attr)
        {
            key = attr->Name();
            if ("name" == key)
            {
                val = attr->Value();
                if (val.length() > MAX_NAME - 1)
                {
                    val = val.substr(0, MAX_NAME - 1);
                }
                sprintf(unInfo.name, "%s", val.c_str());
                ++nResult;
            }
            else if ("timeStamp" == key)
            {
                unInfo.timeStamp = attr->IntValue();
                ++nResult;
            }
            else if ("lighting" == key)
            {
                unInfo.udLighting = attr->DoubleValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        TiXmlNode* pNode = pElement->FirstChild("listRect");
        if (NULL == pNode)
        {
            LOG_D("null pointer. listRect.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("cnt" == key)
            {
                unInfo.udCnt = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        pNode = pNode->FirstChild("Rect");
        if (NULL == pNode)
        {
            LOG_D("null pointer. Rect.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("label" == key)
            {
                unInfo.udLable = attr->IntValue();
                ++nResult;
            }
            else if ("left" == key)
            {
                unInfo.udLeft = attr->IntValue();
                ++nResult;
            }
            else if ("top" == key)
            {
                unInfo.udTop = attr->IntValue();
                ++nResult;
            }
            else if ("right" == key)
            {
                unInfo.udRight = attr->IntValue();
                ++nResult;
            }
            else if ("bottom" == key)
            {
                unInfo.udBottom = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        return unInfo.udCnt;
    }

    int getKaoInfo(KaoInfo& kaoInfo, const char* szInfo)
    {
        int nResult = 0;
        TiXmlDocument doc("kaoInfo.xml");
        doc.Parse(szInfo);
        TiXmlElement* pElement = doc.FirstChildElement("imageResult");
        TiXmlAttribute* attr = pElement->FirstAttribute();
        std::string key;
        std::string val;
        // enum attr
        while (NULL != attr)
        {
            key = attr->Name();
            if ("name" == key)
            {
                val = attr->Value();
                if (val.length() > MAX_NAME - 1)
                {
                    val = val.substr(0, MAX_NAME - 1);
                }
                sprintf(kaoInfo.name, "%s", val.c_str());
                ++nResult;
            }
            else if ("timeStamp" == key)
            {
                kaoInfo.timeStamp = attr->IntValue();
                ++nResult;
            }
            else if ("lighting" == key)
            {
                kaoInfo.kaoLighting = attr->DoubleValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        TiXmlNode* pNode = pElement->FirstChild("listFace");
        if (NULL == pNode)
        {
            LOG_D("null pointer. listFace.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("cnt" == key)
            {
                kaoInfo.kaoCnt = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        pNode = pNode->FirstChild("Face");
        if (NULL == pNode)
        {
            LOG_D("null pointer. Face.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("label" == key)
            {
                kaoInfo.kaoLable = attr->IntValue();
                ++nResult;
            }
            else if ("left" == key)
            {
                kaoInfo.kaoLeft = attr->IntValue();
                ++nResult;
            }
            else if ("top" == key)
            {
                kaoInfo.kaoTop = attr->IntValue();
                ++nResult;
            }
            else if ("right" == key)
            {
                kaoInfo.kaoRight = attr->IntValue();
                ++nResult;
            }
            else if ("bottom" == key)
            {
                kaoInfo.kaoBottom = attr->IntValue();
                ++nResult;
            }
            else if ("type" == key)
            {
                kaoInfo.kaoType = attr->IntValue();
                ++nResult;
            }
            else if ("pid" == key)
            {
                kaoInfo.kaoPid = attr->IntValue();
                ++nResult;
            }
            else if ("tid" == key)
            {
                kaoInfo.kaoTid = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        return 0;
    }

    int getTemaneInfo(TemaneInfo& teInfo, const char* szInfo)
    {
        int nResult = 0;
        TiXmlDocument doc("teInfo.xml");
        doc.Parse(szInfo);
        TiXmlElement* pElement = doc.FirstChildElement("imageResult");
        TiXmlAttribute* attr = pElement->FirstAttribute();
        std::string key;
        std::string val;
        // enum attr
        while (NULL != attr)
        {
            key = attr->Name();
            if ("name" == key)
            {
                val = attr->Value();
                if (val.length() > MAX_NAME-1)
                {
                    val = val.substr(0, MAX_NAME-1);
                }
                sprintf(teInfo.name, "%s", val.c_str());
                ++nResult;
            }
            else if ("timeStamp" == key)
            {
                teInfo.timeStamp = attr->IntValue();
                ++nResult;
            }
            else if ("lighting" == key)
            {
                teInfo.teLighting = attr->DoubleValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        TiXmlNode* pNode = pElement->FirstChild("listHand");
        if (NULL == pNode)
        {
            LOG_D("null pointer. listHand.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("cnt" == key)
            {
                teInfo.teCnt = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }
        
        pNode = pNode->FirstChild("Hand");
        if (NULL == pNode)
        {
            LOG_D("null pointer. Hand.\n");
            return 0;
        }

        pElement = pNode->ToElement();
        attr = pElement->FirstAttribute();
        while (NULL != attr)
        {
            key = attr->Name();
            if ("left" == key)
            {
                teInfo.teLeft = attr->IntValue();
                ++nResult;
            }
            else if ("top" == key)
            {
                teInfo.teTop = attr->IntValue();
                ++nResult;
            }
            else if ("right" == key)
            {
                teInfo.teRight = attr->IntValue();
                ++nResult;
            }
            else if ("bottom" == key)
            {
                teInfo.teBottom = attr->IntValue();
                ++nResult;
            }
            else if ("type" == key)
            {
                teInfo.teType = attr->IntValue();
                ++nResult;
            }
            else if ("prob" == key)
            {
                teInfo.teProb = attr->DoubleValue();
                ++nResult;
            }
            else if ("velx" == key)
            {
                teInfo.teVelx = attr->DoubleValue();
                ++nResult;
            }
            else if ("vely" == key)
            {
                teInfo.teVely = attr->DoubleValue();
                ++nResult;
            }
            else if ("vellogr" == key)
            {
                teInfo.teVellogr = attr->DoubleValue();
                ++nResult;
            }
            else if ("mouse_abs_x" == key)
            {
                teInfo.teMouseAbsx = attr->DoubleValue();
                ++nResult;
            }
            else if ("mouse_abs_y" == key)
            {
                teInfo.teMouseAbsy = attr->DoubleValue();
                ++nResult;
            }
            else if ("mouse_event" == key)
            {
                teInfo.teMouseEvent = attr->IntValue();
                ++nResult;
            }
            attr = attr->Next();
        }

        return 0;
    }

    double getLinearValue(double Intercept, double coefficient, int pram)
    {
        return Intercept + coefficient * pram ;
    }

    /*****************************************************************************
     * 函 数 名  : zBaseUtil.F2D_90
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年12月31日
     * 函数功能  : 当FACE_F时，计算距离
     * 输入参数  : int width  人脸宽度
     * 输出参数  : 无
     * 返 回 值  : int
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int F2D_90(int width) 
    {
        #if 0 // 线性拟合
        //int HEAD_WIDTHS_TYPE_F_90[] = { 90, 50, 32, 20, 15 }; // 单位px
        //int HEAD_DISTANCE_90[] = { 150, 300, 450, 600, 850 }; // 单位cm
        double Intercept = 803.522504892 ;
        double coefficient = -8.05609915 ;
        return (int)getLinearValue(Intercept, coefficient, width) ;
        #endif

        #define F2D_90_TAB_SIZE 7
        #define F2D_90_MAX_DIS 800 // 6米

        // 人脸宽度和距离映射表
        static const int w2d_table[F2D_90_TAB_SIZE][2] = 
        {
            {250,   0},
            {120, 100},
            {65 , 200},
            {42 , 300},
            {36 , 400},
            {32 , 500},
            {0  , 800},
        };

        for (int i=0; i<F2D_90_TAB_SIZE; i++)
        {
            if (width >= w2d_table[i][0])
            {
                if (0 == i)
                {
                    return w2d_table[i][1];
                }
                else
                {
                    int gap_wid = w2d_table[i-1][0] - w2d_table[i][0];
                    int gap_dis = w2d_table[i][1] - w2d_table[i-1][1];
                    int sub_wid = width - w2d_table[i][0];
                    return w2d_table[i][1] - (sub_wid * gap_dis)/gap_wid;
                }
            }
        }
        
        return F2D_90_MAX_DIS;
    }

    /*****************************************************************************
     * 函 数 名  : zBaseUtil.H2D_90
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年12月31日
     * 函数功能  : 当FACE_H时，计算距离
     * 输入参数  : int width  人脸宽度
     * 输出参数  : 无
     * 返 回 值  : int
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int H2D_90(int width)
    {
        #define H2D_90_TAB_SIZE 7
        #define H2D_90_MAX_DIS 800 // 6米

        // 人脸宽度和距离映射表
        static const int w2d_table[H2D_90_TAB_SIZE][2] = 
        {
            {250,   0},
            {110, 100},
            {58 , 200},
            {40 , 300},
            {32 , 400},
            {26 , 500},
            {0  , 800}
        };

        for (int i=0; i<H2D_90_TAB_SIZE; i++)
        {
            if (width >= w2d_table[i][0])
            {
                if (0 == i)
                {
                    return w2d_table[i][1];
                }
                else
                {
                    int gap_wid = w2d_table[i-1][0] - w2d_table[i][0];
                    int gap_dis = w2d_table[i][1] - w2d_table[i-1][1];
                    int sub_wid = width - w2d_table[i][0];
                    return w2d_table[i][1] - (sub_wid * gap_dis)/gap_wid;
                }
            }
        }
        
        return H2D_90_MAX_DIS;
    }

    int F2D_60(int width) 
    {
        //int HEAD_WIDTHS_TYPE_F_60[] = { 270, 200, 137, 98, 74, 55, 42, 36, 33, 32 };
        //int HEAD_DISTANCE_60[] = { 60, 100, 150, 200, 300, 400, 500, 600, 700, 800 };
        double Intercept = 644.384266675 ;
        double coefficient = -2.69584715 ;
        return (int)getLinearValue(Intercept, coefficient, width) ;
    }

    int L2D_60(int width) 
    {
        //int HEAD_WIDTHS_TYPE_L_60[] = { 270, 185, 138, 108, 72, 56, 47, 40, 33, 30 };
        //int HEAD_DISTANCE_60[] = { 60, 100, 150, 200, 300, 400, 500, 600, 700, 800 };
        double Intercept = 655.642906666 ;
        double coefficient = -2.80534123 ;
        return (int)getLinearValue(Intercept, coefficient, width) ;
    }

    int R2D_60(int width) 
    {
        //int HEAD_WIDTHS_TYPE_R_60[] = { 270, 180, 134, 111, 74, 55, 47, 42, 35, 29 };
        //int HEAD_DISTANCE_60[] = { 60, 100, 150, 200, 300, 400, 500, 600, 700, 800 };
        double Intercept = 658.273107945 ;
        double coefficient = -2.8380052 ;
        return (int)getLinearValue(Intercept, coefficient, width) ;
    }

    int getDistance(int headType, int camer_angle, int head_width) 
    {
        int nDistance = -1 ;

        if (CAMERA_90 == camer_angle)
        {
            if (headType == FACE_H)
            {
                nDistance = H2D_90(head_width);
            }
            else
            {
                nDistance = F2D_90(head_width);
            }
        }
        else if (CAMERA_60 == camer_angle)
        {
            switch(headType)
            {
            case FACE_F:
                {
                    nDistance = F2D_60(head_width);
                }
                break;
            case FACE_L:
                {
                    nDistance = L2D_60(head_width);
                }
                break ;
            case FACE_R:
                {
                    nDistance = R2D_60(head_width);
                }
                break; 
            default:
                break;
            }
        }
        else
        {
            LOG_E("error camera type. camer_angle=%d\n", camer_angle);
        }

        return nDistance;
    }

    int getAngle(const HeadInfo& hdInfo, int camera_angle, int img_width)
    {
        int center_x = (hdInfo.headLeft + hdInfo.headRight)>>1 ;
        //
        //int hoshou = img_width * DIS_HOSHOU;
        //int hoshou = 0;
  //    center_x = center_x - hoshou;
  //    img_width = img_width - (hoshou << 1);
  //    center_x = center_x < 0 ? 0 : (center_x > img_width ? img_width : center_x);
        //
        double dRet = (double)(center_x) / img_width;
        dRet = 1 - dRet;
        //LOG_D("getAngle center_x == %d, img_widht == %d, dRet == %lf \n", center_x, img_width, dRet);
        return (45 + (int)(dRet * camera_angle));
    }

    int getAngleV2(const v2Head& head, int camera_angle, int img_width, int lb)
    {
        int center_x = (head.left_ + head.right_) >> 1;
        //
        //int hoshou = img_width * DIS_HOSHOU;
        //center_x = center_x - hoshou;
        //img_width = img_width - (hoshou << 1);
        //center_x = center_x < 0 ? 0 : (center_x > img_width ? img_width : center_x);
        //
        double dRet = (double)(center_x) / img_width;
        dRet = 1 - dRet;
        return (lb + (int)(dRet * camera_angle));
    }

    int getAngleV3(int camera_angle, int img_width, int lb, int xpos)
    {
        double dRet = (double)(xpos) / img_width;
        dRet = 1 - dRet;
        return (lb + (int)(dRet * camera_angle));
    }
    
    std::string& trim(std::string &str)
    {
        if (str.empty())
        {
            return str;
        }

        str.erase(0, str.find_first_not_of(" "));
        str.erase(str.find_last_not_of(" ") + 1);
        return str;
    }

    //////////////////////////////////////////////////////////////////////////
#ifdef _WINDOWS

#else
    struct pollfd mcuifds[1] = { 0 };
#endif
    static int read_data(char const *path, char *buffer, int buflen)
    {
        int fd;
        int ret = -1;
        do 
        {
            fd = open(path, O_RDONLY);
            if (fd < 0) {
                LOG_E("open %s failed (%s)\n", path, strerror(errno));
                break;
            }

            ret = read(fd, buffer, buflen);
            if (ret < 0) {
                LOG_E("read data from %s error (%s)\n", path, strerror(errno));
                break;
            }
        } while (0);

        if (fd > 0) {
            close(fd);
        }
        return ret;
    }

    static int search_mcu_input(char *buffer, int buflen)
    {
        int i;
        char input_name_path[256] = { 0 };
        char input_name_path_fmt[] = "/sys/class/input/input%d/name";
        char tmpbuf[256] = { 0 };

        for (i = 0; i < MAX_INPUT_COUNT; i++) {
            sprintf(input_name_path, input_name_path_fmt, i);
            if (read_data(input_name_path, tmpbuf, 256) > 0) {
                if (strncmp(tmpbuf, MCU_INPUT_NAME, strlen(MCU_INPUT_NAME)) == 0) {
                    sprintf(buffer, "/dev/input/event%d", i);
                    return 0;
                }
            }
        }
        return -1;
    }

    int startMCUEvent()
    {
        char buffer[256] = { 0 };

        if (search_mcu_input(buffer, 256) == 0)
        {
            LOG_D("mcu event device path is: %s\n", buffer);
        }
        mcuifds[0].fd = open(buffer, O_RDWR | O_NONBLOCK);
        LOG_D("mcuifds[0].fd = open(buffer, O_RDWR | O_NONBLOCK); == %d, buffer == %s \n", mcuifds[0].fd, buffer);
        if (mcuifds[0].fd < 0) {
            LOG_E("open %s error (%s)\n", buffer, strerror(errno));
            return -1;
        }
        return 0;
    }
    
    int endMCUEvent()
    {
        if (mcuifds[0].fd >= 0 )
        {
            close(mcuifds[0].fd);
        }
        return 0;
    }

    int process_event(struct input_event *event)
    {
        // mcu有数据上报时模拟key event， type: 0x01, code: 0xf2, value: 0(弹起)/1(按下)
        //LOG_D("event type: %d, code: %d, value: %d\n", event->type, event->code, event->value);
        if (event->type == MCU_EVENT_TYPE && event->code == MCU_EVENT_CODE && event->value == 0) {
            //LOG_D("start reading data from mcu\n");
            return ZM_NEEDREAD;
        }
        return ZM_INVALID;
    }

    int getMCUEvent()
    {
        int nResult = ZM_INVALID;
        int pollres, res;
        int nfds;
        struct input_event event;

        nfds = 1;
        mcuifds[0].events = POLLIN;
        //LOG_D(" getMCUEvent fd = %d \n", mcuifds[0].fd);
        //while (1) {
            //LOG_D("befor poll(&mcuifds[0], nfds, -1); \n");
            //pollres = poll(mcuifds, nfds, -1);
            pollres = poll(mcuifds, nfds, 1000);
            //LOG_D("end poll(&mcuifds[0], nfds, -1); \n");
            if (0 != pollres 
                && mcuifds[0].revents & POLLIN) 
            {
                res = read(mcuifds[0].fd, &event, sizeof(event));
                if (res < static_cast<int>(sizeof(event))) {
                    LOG_E("read event form mcu_input error (not enough data)\n");
                    return ZM_INVALID;
                }
                nResult = process_event(&event);
            }
        //}
        return nResult;
    }

    int get_mountpoint(char const *label, char *mountpoint)
    {
        int nReturn = -1;
        LOG_D("get_mountpoint lable == %s, mountpoint == %s \n", label, mountpoint);

        int ret;
        int fd;
        char mount_proc[] = "/proc/mounts";
        char buffer[4096] = { 0 };
        char mp_prefix[50];
        char *start, *end;

        fd = open(mount_proc, O_RDONLY | O_NONBLOCK);
        do 
        {
            if (fd < 0) {
                //LOG_E("udisk_demo2: Failed to open %s (%s)\n", mount_proc, strerror(errno));
                break;
            }

            ret = read(fd, buffer, 4096);
            if (ret < 0) {
                //LOG_E("udisk_demo2: Failed to read data from %s (%s)\n", mount_proc, strerror(errno));
                break;
            }

            snprintf(mp_prefix, 50, "/mnt/%s", label);
            start = strstr(buffer, mp_prefix);
            if (start == NULL) {
                //LOG_E("udisk_demo2: Failed to search start of %s from %s\n", mp_prefix, buffer);
                break;
            }

            end = strchr(start, ' ');
            if (end == NULL) {
                //LOG_E("udisk_demo2: Failed to search end of  %s from %s\n", mp_prefix, buffer);
                break;
            }

            strncpy(mountpoint, start, end - start);
            nReturn = 0;

        } while (false);

        if (fd > 0) {
            close(fd);
        }
        //return -1;

        LOG_D("get_mountpoint lable == %s, mountpoint == %s , nReturn == %d \n", label, mountpoint, nReturn);
        return nReturn;
    }

/*****************************************************************************
 * 函 数 名  : zBaseUtil.connectState
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月25日
 * 函数功能  : wifi 连接状态
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : unsigned : 1:wifi连接正常，2: 路由连接正常，0: 无连接
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
    unsigned char connect_wan()
    {
        std::int64_t time = zBaseUtil::getMilliSecond();
        int nReturn = zBaseUtil::RW_System("ping -c 4 www.baidu.com > /dev/null");

        unsigned char state = 0;
        if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
        {
            state = 1;
        }

        time = zBaseUtil::getMilliSecond() - time;
        LOG_D("state=%d, time=%lld\n", state, time);
        return state;
    }

/*****************************************************************************
 * 函 数 名  : zBaseUtil.connect_router
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月26日
 * 函数功能  : 连接路由
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : unsigned
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
    unsigned char connect_router()
    {
        std::string r_ip;

        // 用系统调用方式，不靠谱，存在些文件失败现象，导致wifi误认为连接失败。
        #if 1
        (void)get_gateway_addr(r_ip);

        #else
        // try router   
        //(void)zBaseUtil::RW_System("getprop dhcp.wlan0.gateway > /data/routerip.txt");
        (void)zBaseUtil::RW_System("getprop dhcp.wlan0.server > /data/routerip.txt");

        char line[1024] = { 0 };
        FILE* pFile = fopen("/data/routerip.txt","r");
        if (NULL != pFile)
        {
            if (NULL != fgets(line, 1024, pFile))
            {
                if (strlen(line) >= 4) //  TODO: 判断合法IP
                {
                    if ('\n' == line[strlen(line) - 1])
                    {
                        line[strlen(line) - 1] = '\0';
                    }
                    r_ip = line;
                }
            }
            fclose(pFile);
        }
        else
        {
            LOG_E("fopen failed for %s", strerror(errno));
        }
        remove("/data/routerip.txt");
        #endif

        unsigned char state = 0;
        if (!r_ip.empty())
        {
            std::string str = "ping -c 4 " + r_ip + " > /dev/null";
            LOG_D("line=%d, str=%s\n", __LINE__, str.c_str());

            int nReturn = zBaseUtil::RW_System(str.c_str());
            if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
            {
                state = 1;
            }
        }

        LOG_D("router ip == %s, state=%d\n", r_ip.c_str(), state);
        return state;
    }

/*****************************************************************************
 * 函 数 名  : zBaseUtil.getMicroSecond
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月12日
 * 函数功能  : 获取微秒值 -- 1/1000000秒
 * 输入参数  : void
 * 输出参数  : 无
 * 返 回 值  : std
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t getMicroSecond(void)
{
    #ifdef _WINDOWS
    static LONGLONG freq = GetFrequency()/1000000;
    LARGE_INTEGER count;
    QueryPerformanceCounter(&count);
    return count.QuadPart/freq;
    
    #else
    struct timespec ts = {0, 0};   
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (std::int64_t)ts.tv_sec * 1000000 + ts.tv_nsec/1000;
    #endif
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.getMilliSecond
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月03日
 * 函数功能  : 获取毫秒值 -- 1/1000秒
 * 输入参数  : void
 * 输出参数  : 无
 * 返 回 值  : std
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t getMilliSecond(void)
{
    struct timespec ts = {0, 0};
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (std::int64_t)ts.tv_sec * 1000 + ts.tv_nsec/1000000;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.getSecond
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月03日
 * 函数功能  : 获取系统启动到当前的秒值 -- 单位:秒
 * 输入参数  : void
 * 输出参数  : 无
 * 返 回 值  : std
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t getSecond(void)
{
    struct timespec ts = {0, 0};
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (std::int64_t)ts.tv_sec;
}

//////////////////////////////////////////////////////////////////////////
//<imageResult name = "result" timeStamp = "0" >
//<listHand cnt = "1" >
//<Hand label = "0" left = "415" top = "193" right = "628" bottom = "406" / >
//< / listHand>
//<listHead cnt = "2" >
//<Head label = "7" left = "827" top = "348" right = "954" bottom = "475" type = "0" prob = "0.248" stable = "1" liveness = "0.648" / >
//<Head label = "4" left = "722" top = "166" right = "896" bottom = "340" type = "0" prob = "1" stable = "1" liveness = "0.905" / >
//< / listHead>
//< / imageResult> ；
//
//<imageResult name = "result" timeStamp = "0" >
//<listHand cnt = "1" >
//<Hand label = "0" left = "421" top = "194" right = "622" bottom = "396" type = "0" prob = "0.856" / >
//< / listHand>
//< / imageResult>
//////////////////////////////////////////////////////////////////////////

static int EnumHandList(TiXmlElement* pElement, v2ImgResult& result)
{
    std::string key;
    std::string val;
    int nResult = 0;
    TiXmlNode* pNode = pElement->FirstChild("listHand");
    if (NULL != pNode)
    {
        TiXmlElement* pSubElement = pNode->ToElement();
        if (NULL == pSubElement) return -1;
        TiXmlAttribute* subAttr = pSubElement->FirstAttribute();
        while (NULL != subAttr)
        {
            key = subAttr->Name();
            if ("cnt" == key)
            {
                result.nHand_ = subAttr->IntValue();
                result.listHand_ = new v2Hand[result.nHand_];
                ++nResult;
            }
            subAttr = subAttr->Next();
        }
        int i = 0;
        pNode = pNode->FirstChild("Hand");
        if (NULL != pNode) pSubElement = pNode->ToElement();
        //if (NULL == pSubElement) return -1;
        while (NULL != pNode && NULL != pSubElement && i < result.nHand_)
        {
            v2Hand& hand = result.listHand_[i];
            subAttr = pSubElement->FirstAttribute();
            while (NULL != subAttr)
            {
                key = subAttr->Name();
                val = subAttr->Value();
                if ("label" == key)
                {
                    hand.label_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("left" == key)
                {
                    hand.left_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("top" == key)
                {
                    hand.top_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("right" == key)
                {
                    hand.right_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("bottom" == key)
                {
                    hand.bottom_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("type" == key)
                {
                    hand.type_ = subAttr->IntValue();
                    LOG_D("hand type == %s, ntype == %d \n", subAttr->Value(), hand.type_);
                    ++nResult;
                }
                else if ("prob" == key)
                {
                    hand.prob_ = subAttr->DoubleValue();
                    ++nResult;
                }
                else if ("stable" == key)
                {
                    hand.stable_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("liveness" == key)
                {
                    hand.liveness_ = subAttr->DoubleValue();
                    ++nResult;
                }
                subAttr = subAttr->Next();
            }
            ++i;
            pNode = pNode->NextSibling();
            if (NULL != pNode) pSubElement = pNode->ToElement();
        }
    }
    LOG_D(" nHand_ == %d \n", result.nHand_);
    return result.nHand_;
}

static int EnumHeadList(TiXmlElement* pElement, v2ImgResult& result)
{
    std::string key;
    std::string val;
    int nResult = 0;
    TiXmlNode* pNode = pElement->FirstChild("listHead");
    if (NULL != pNode)
    {
        TiXmlElement* pSubElement = pNode->ToElement();
        if (NULL == pSubElement) return -1;
        TiXmlAttribute* subAttr = pSubElement->FirstAttribute();
        while (NULL != subAttr)
        {
            key = subAttr->Name();
            if ("cnt" == key)
            {
                result.nHead_ = subAttr->IntValue();
                result.listHead_ = new v2Head[result.nHead_];
                ++nResult;
            }
            subAttr = subAttr->Next();
        }
        int i = 0;
        pNode = pNode->FirstChild("Head");
        if (NULL != pNode) pSubElement = pNode->ToElement();
        //if (NULL == pSubElement) return -1;
        while (NULL != pNode && NULL != pSubElement && i < result.nHead_)
        {
            v2Head& head = result.listHead_[i];
            subAttr = pSubElement->FirstAttribute();
            while (NULL != subAttr)
            {
                key = subAttr->Name();
                val = subAttr->Value();
                if ("label" == key)
                {
                    head.label_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("left" == key)
                {
                    head.left_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("top" == key)
                {
                    head.top_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("right" == key)
                {
                    head.right_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("bottom" == key)
                {
                    head.bottom_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("type" == key)
                {
                    head.type_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("prob" == key)
                {
                    head.prob_ = subAttr->DoubleValue();
                    ++nResult;
                }
                else if ("stable" == key)
                {
                    head.stable_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("liveness" == key)
                {
                    head.liveness_ = subAttr->DoubleValue();
                    ++nResult;
                }
                subAttr = subAttr->Next();
            }
            ++i;
            pNode = pNode->NextSibling();
            if (NULL != pNode) pSubElement = pNode->ToElement();
        }
    }

    LOG_D(" nHead_ == %d \n", result.nHead_);
    return result.nHead_;
}

static int EnumRectList(TiXmlElement* pElement, v2ImgResult& result)
{
    std::string key;
    std::string val;
    int nResult = 0;
    TiXmlNode* pNode = pElement->FirstChild("listRect");
    if (NULL != pNode)
    {
        TiXmlElement* pSubElement = pNode->ToElement();
        if (NULL == pSubElement) return -1;
        TiXmlAttribute* subAttr = pSubElement->FirstAttribute();
        while (NULL != subAttr)
        {
            key = subAttr->Name();
            if ("cnt" == key)
            {
                result.nRect_ = subAttr->IntValue();
                result.listRect_ = new v2Rect[result.nRect_];
                ++nResult;
            }
            subAttr = subAttr->Next();
        }

        int i = 0;
        pNode = pNode->FirstChild("Rect");
        if (NULL != pNode) pSubElement = pNode->ToElement();
        //if (NULL == pSubElement) return -1;
        while (NULL != pNode && NULL != pSubElement && i < result.nRect_)
        {
            v2Rect& rect = result.listRect_[i];
            subAttr = pSubElement->FirstAttribute();
            while (NULL != subAttr)
            {
                key = subAttr->Name();
                val = subAttr->Value();
                if ("label" == key)
                {
                    rect.label_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("left" == key)
                {
                    rect.left_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("top" == key)
                {
                    rect.top_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("right" == key)
                {
                    rect.right_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("bottom" == key)
                {
                    rect.bottom_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("type" == key)
                {
                    rect.type_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("prob" == key)
                {
                    rect.prob_ = subAttr->DoubleValue();
                    ++nResult;
                }
                else if ("stable" == key)
                {
                    rect.stable_ = subAttr->IntValue();
                    ++nResult;
                }
                else if ("liveness" == key)
                {
                    rect.liveness_ = subAttr->DoubleValue();
                    ++nResult;
                }
                subAttr = subAttr->Next();
            }
            ++i;
            pNode = pNode->NextSibling();
            if (NULL != pNode) pSubElement = pNode->ToElement();
        }
    }

    LOG_D(" nRect_ == %d \n", result.nRect_);
    return result.nRect_;
}

int analyzeV2String(const std::string& str, v2ImgResult& result)
{
    int nResult = 0;
    TiXmlDocument doc("imgresult.xml");
    doc.Parse(str.c_str());
    TiXmlElement* pElement = doc.FirstChildElement("imageResult");
    if(pElement == NULL) { return 0;}
    TiXmlAttribute* attr = pElement->FirstAttribute();
    std::string key;
    std::string val;
    // enum attr
    while (NULL != attr)
    {
        key = attr->Name();
        val = attr->Value();
        if ("name" == key)
        {
            if (val.length() > MAX_NAME - 1)
            {
                val = val.substr(0, MAX_NAME - 1);
            }
            sprintf(result.name_, "%s", val.c_str());
            ++nResult;
        }
        else if ("timeStamp" == key)
        {
            int nr = sscanf(val.c_str(), "%lld", &result.timeStamp_);
            if (nr != 1)
            {
                LOG_D("xml string ana failed for val == %s \n", val.c_str());
            }
            ++nResult;
        }
        else if ("lighting" == key)
        {
            result.lighting_ = attr->DoubleValue();
            ++nResult;
        }
        attr = attr->Next();
    }

    EnumHandList(pElement, result);
    pElement = doc.FirstChildElement("imageResult");
    EnumHeadList(pElement, result);
    pElement = doc.FirstChildElement("imageResult");
    EnumRectList(pElement, result);

    return 0;
}

std::uint32_t getThreadId()
{
#ifdef _WINDOWS
    return 0;
#else
    return syscall(224);
#endif
}

//int wifiCheck()
//{
//  bool bErr = zFileUtil::zFileManager::getInstance()->isFileExist("/dev/video0");
//  return bErr? 0 : -1;
//}
//
int videoCheck()
{
    bool bErr = zFileUtil::zFileManager::getInstance()->isFileExist("/dev/video0");
    return bErr ? 0 : -1;
}
//
//int chipCheck()
//{
//  bool bErr = zFileUtil::zFileManager::getInstance()->isFileExist("/dev/video0");
//  return bErr ? 0 : -1;
//}

int writeBuf2Device(ZDEVICE dev,const char* szBuf, int nLen)
{
    #define E_SLEEP_GAP 20     // 单位: 毫秒
    #define E_LOOP_CNT 2       // 轮询次数

    int nResult = -1;

    if (dev == (ZDEVICE)(-1))
    {
        LOG_E("writeBuf2Device to invalid device! \n");
    }

    if ((NULL == szBuf) || (0 >= dev))
    {
        LOG_E("null buffer. dev=%d\n", dev);
        return nResult;
    }

    int size = 0;
    for (int i=0; i < E_LOOP_CNT; i++)
    {
        size = write(dev, szBuf, nLen);
        if (size == nLen)
        {
            nResult = 0;
            break;
        }
        else
        {
            if (i != (E_LOOP_CNT - 1))
            {
                // 等待100毫秒后，重试
                std::this_thread::sleep_for(std::chrono::milliseconds(E_SLEEP_GAP));
            }

            LOG_E("i=%d: write to device %0x failed %s: %d,%d\n", 
                i, dev, strerror(errno), size, nLen);
        }
    }

    return nResult;
}

unsigned char transEakonErr(int nErr, int type)
{
    unsigned char ucErr = EAKON_NO_ERR;
    do 
    {
        if (type == EAKON_ERR_0x04)
        {
            if (nErr & EAKON_COMMU_ERR) ucErr |= 0x01;
            if (nErr & EAKON_NO_RUTURN) ucErr |= 0x02;
            if ( !(nErr & EAKON_NO_ROUTERINFO) ) ucErr |= 0x08;
            if ( !(nErr & EAKON_NOT_CONNECT_AP) ) ucErr |= 0x10;
            break;
        }
        else if (type == EAKON_ERR_0xa0)
        {
            //if (nErr & EAKON_ERR_EXIST) ucErr |= 0x01;
            if (nErr & EAKON_VIDEO_ERR) ucErr |= 0x02;
            if (nErr & EAKON_CHIP_ERR) ucErr |= 0x04;
            //if (nErr & EAKON_WIFI_ERR) ucErr |= 0x08;
            break;
        }
    } while (false);
    return ucErr;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.transEakonErrV2
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年10月30日
 * 函数功能  : 转换故障状态 到MCU
 * 输入参数  : int nErr  故障状态
               int type  类型
 * 输出参数  : 无
 * 返 回 值  : unsigned
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
unsigned char transEakonErrV2(int nErr, int type)
{
    unsigned char ucErr = EAKON_NO_ERR;
    do 
    {
        if (type == EAKON_ERR_0x04)
        {
            if (nErr & EAKON_COMMU_ERR) ucErr |= 0x01;
            if (nErr & EAKON_NO_RUTURN) ucErr |= 0x02;
            if ( !(nErr & EAKON_NO_ROUTERINFO) ) ucErr |= 0x08;
            if ( !(nErr & EAKON_NOT_CONNECT_AP) ) ucErr |= 0x10;
            break;
        }
        else if (type == EAKON_ERR_0xa0)
        {
            //if (nErr & EAKON_ERR_EXIST) ucErr |= 0x01;
            if (nErr & EAKON_VIDEO_ERR) ucErr |= 0x02;
            if (nErr & EAKON_CHIP_ERR)  ucErr |= 0x04;
            // 快检模式下，路由通了就认为无故障了
            //if (nErr & EAKON_NOT_CONNECT_AP) ucErr |= 0x08;
            break;
        }
    } while (false);
    return ucErr;
}

std::string getTempDir()
{
#ifdef _WINDOWS
    std::vector<std::string::value_type> buf(MAX_PATH + 1);
    size_t len = ::GetTempPath(MAX_PATH + 1, buf.data());
    return std::string(buf.cbegin(), buf.cbegin() + len);

#elif POSIX
    const char* buf = ::getenv("TMPDIR");

    if (NULL == buf) return std::string("/tmp/");

    std::string str(buf);

    if ('/' != str.back()) str.push_back('/');
    return str;

#else
#error "Undefine platform"
#endif
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.write_deviceid_file
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月19日
 * 函数功能  : 写设备ID到文件
 * 输入参数  : std::string &deviceid  : 设备ID: 如000000D1100050271159082B1236000
 * 输出参数  : 无
 * 返 回 值  : -1: fail 0: success
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int write_deviceid_file(std::string &deviceid)
{
    int err_ret = -1;
    
    const char *deviceid_filename = "/data/deviceid";
    FILE *fp = fopen(deviceid_filename, "w");
    if (NULL == fp)
    {
        LOG_E("open file %s failed.\n", deviceid_filename);
        return err_ret;
    }

    int deviceid_size = deviceid.size();
    int size = fwrite(deviceid.c_str(), 1, deviceid_size, fp);
    if (deviceid_size != size)
    {
        LOG_E("deviceid_size=%d, size=%d \n", deviceid_size, size);
        (void)fclose(fp);
        return err_ret;
    }

    (void)fflush(fp);
    (void)fclose(fp);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.write_status_file
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月19日
 * 函数功能  : 写程序状态文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int write_status_file()
{
    int err_ret = -1;
    
    const char *status_filename = "/data/facedis.status";
    FILE *fp = fopen(status_filename, "w");
    if (NULL == fp)
    {
        LOG_E("open file %s failed.\n", status_filename);
        return err_ret;
    }

    const char * str_status = "STARTED";
    int status_size = strlen(str_status);
    int size = fwrite(str_status, 1, status_size, fp);
    if (status_size != size)
    {
        LOG_E("status_size=%d, size=%d \n", status_size, size);
        (void)fclose(fp);
        return err_ret;
    }

    (void)fflush(fp);
    (void)fclose(fp);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.get_up_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月23日
 * 函数功能  : 获取系统启动到当前的时间
 * 输入参数  : 无
 * 输出参数  : 
 * 返 回 值  : std::int64_t: 单位: 毫秒
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t get_up_time()
{
    const char *uptime_filename = "/proc/uptime";
    FILE *fp = fopen(uptime_filename, "r");
    if (NULL == fp)
    {
        LOG_E("open file %s failed.\n", uptime_filename);
        return -1;
    }

    #define BUF_SIZE 256
    char buf[BUF_SIZE];
    int size = fread(buf, 1, BUF_SIZE, fp);
    LOG_D("size = %d\n", size);
    #if 0
    for (int i=0; i<size; i++)
    {
        LOG_D("%c", buf[i]);
    }
    #endif

    int second = -1;
    int nResult = sscanf(buf, "%d", &second);
    if (1 != nResult)
    {
        LOG_E("nResult=%d\n", nResult);
    }
    else
    {
        LOG_D("second=%d\n", second);
    }

    fclose(fp);
    return std::int64_t(second * 1000);
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.get_temperature
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月29日
 * 函数功能  : 获取结温
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int get_temperature()
{
    const char *temperature_filename = "/sys/class/thermal/thermal_zone0/temp";
    FILE *fp = fopen(temperature_filename, "r");
    if (NULL == fp)
    {
        LOG_E("open file %s failed.\n", temperature_filename);
        return -1;
    }

    #define BUF_SIZE 256
    char buf[BUF_SIZE];
    int size = fread(buf, 1, BUF_SIZE, fp);
    LOG_D("size = %d\n", size);
    #if 0
    for (int i=0; i<size; i++)
    {
        LOG_D("%c", buf[i]);
    }
    #endif

    int temperature = -1;
    int nResult = sscanf(buf, "%d", &temperature);
    if (1 != nResult)
    {
        LOG_E("nResult=%d\n", nResult);
    }
    else
    {
        LOG_D("temperature=%d\n", temperature);
    }

    fclose(fp);
    return temperature;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.need_close
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月11日
 * 函数功能  : 过滤文件名
 * 输入参数  : const char *filename  文件名
 * 输出参数  : 无
 * 返 回 值  : int : 1: 需关闭， 0: 不需关闭
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static inline int need_close(const char *filename)
{ 
    const char *filters[2] = {
        "/dev/video", 
        "/dev/cedar_dev"
    };

    for (int i=0; i < 2; i++)
    {
        if (0 == strncmp(filters[i], filename, strlen(filters[i])))
        {
            return 1;
        }
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.close_open_fds
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月11日
 * 函数功能  : 关闭 摄像头和编解码设备 
 * 输入参数  : void  
 * 输出参数  : 无
 * 返 回 值  : static
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static void close_open_fds(void)
{
    char fd_name[256] = {0};
    char fd_path[PATH_MAX] = {0};
    char link_path[256] = {0};

    /* Check for a /proc/$$/fd directory. */
    int len = snprintf(fd_path, sizeof(fd_path), "/proc/%ld/fd", (long)getpid());

    DIR *dirp = NULL;
    if (len > 0 && (size_t)len <= sizeof(fd_path) && (dirp = opendir(fd_path)))
    {
        struct dirent *dent = NULL;
        while (NULL != (dent = readdir(dirp)))
        {
            (void)sprintf(fd_name, "%s/%s", fd_path, dent->d_name);
            len = readlink(fd_name, link_path, 256);
            if (len >= 0 && len < 256)
            {
                link_path[len] = '\0';
                //LOG_D("link_path=%s, fd_name=%s\n", link_path, fd_name);
                if (need_close(link_path))
                {
                    char *endp = NULL;
                    long fd = strtol(dent->d_name, &endp, 10);
                    if ((dent->d_name != endp) && (*endp == '\0') && (fd != dirfd(dirp)))
                    {
                        //LOG_D("link_path=%s, fd_name=%s\n", link_path, fd_name);
                        (void)close((int)fd);
                    }
                }
            }
        }
        (void)closedir(dirp);
    }

    //LOG_D("line=%d\n", __LINE__);
    return;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.RW_System
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月9日
 * 函数功能  : 修改system实现，规避花屏问题
 * 输入参数  : const char *cmdstring
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int RW_System(const char *cmdstring)
{
    pid_t pid;
    int status = 0;
    sig_t intsave, quitsave;
    sigset_t mask, omask;

    if (NULL == cmdstring)
    {
        return 1;
    }
    
    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);
    sigprocmask(SIG_BLOCK, &mask, &omask);

    if ((pid = vfork()) < 0)
    {
        sigprocmask(SIG_SETMASK, &omask, NULL);
        status = -1;
    }
    else if (0 == pid)
    {
        sigprocmask(SIG_SETMASK, &omask, NULL);
        close_open_fds();
        (void)execl("/system/bin/sh", "sh", "-c", cmdstring, (char*)0);
        _exit(127);
    }
    else
    {
        intsave = (sig_t)  bsd_signal(SIGINT, SIG_IGN);
        quitsave = (sig_t) bsd_signal(SIGQUIT, SIG_IGN);
        while(waitpid(pid, &status, 0) < 0)
        {
            if (errno != EINTR)
            {
                status = -1;  
                break;
            }
        }
        sigprocmask(SIG_SETMASK, &omask, NULL);
        (void)bsd_signal(SIGINT, intsave);
        (void)bsd_signal(SIGQUIT, quitsave);
    }

    return status;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.RW_Echo
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月9日
 * 函数功能  : 模拟命令 echo 1 > filename
 * 输入参数  : const char *buf       内容
               int size              大小
               const char *filename  文件名
 * 输出参数  : 无
 * 返 回 值  : int : 0: 成功， 其他: 失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int RW_Echo(const void *buf, int size, const char *filename)
{
    if (NULL == buf)
    {
        LOG_E("buf NULL.\n");
        return -1;
    }

    if (NULL == filename)
    {
        LOG_E("filename NULL.\n");
        return -1;
    }

    int f = open(filename,  O_WRONLY);
    if (-1 != f)
    {
        write(f, buf, size);
        fsync(f);
        close(f);
    }
    else
    {
        LOG_E("filename=%s\n", filename);
        return -1;
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.zWifiStateThread.ntp_sync_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月7日
 * 函数功能  : NTP时间同步
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : bool : true: 同步成功，false:失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static bool ntp_sync_time_impl()
{
    int nReturn = -1;
    int nTry = 3;
    while (nTry > 0)
    {
        nReturn = zBaseUtil::RW_System("ntpdate");
        if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
        {
            LOG_D("sync time success!!");
            return true; // exit
        }
        --nTry;
    }
    return false;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.ntp_syncing_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月7日
 * 函数功能  : 在同步时，利用同步前的时间推导当前RTC时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : static time_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
static time_t ntp_syncing_time()
{
    std::int64_t elapse = zBaseUtil::getSecond() - g_before_sync_sys_time;
    return g_before_sync_rtc_time + elapse + 1;
}

bool ntp_sync_time()
{
    {
        std::unique_lock<std::mutex> lock(g_ntp_sync_mutex);
        g_before_sync_sys_time = zBaseUtil::getSecond();
        g_before_sync_rtc_time = time(NULL);
        g_ntp_syncing_flag = true;
    }

    // 注意: 这一段时间可能很久，不能加锁。
    bool ret = ntp_sync_time_impl();
    if (!ret)
    {
        LOG_E("ntp_sync_time_impl failed!!");

        time_t tt = ntp_syncing_time();
        sync_system_time( tt);
    }

    {
        std::unique_lock<std::mutex> lock(g_ntp_sync_mutex);
        g_ntp_syncing_flag = false;
    }
    return ret;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.ntp_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月7日
 * 函数功能  : 获取当前RTC时间
 * 输入参数  : 无
 * 输出参数  : time_t: 从1970年到现在的秒数
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
time_t ntp_time()
{
    std::unique_lock<std::mutex> lock(g_ntp_sync_mutex);
    if (g_ntp_syncing_flag)
    {
        return ntp_syncing_time();
    }
    return time(NULL);
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.write_sync_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月24日
 * 函数功能  : 写时间到 文件
 * 输入参数  : time_t& time  
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int write_sync_time(time_t& time)
{
    int f = open(sync_time_file,  O_CREAT | O_WRONLY);
    if (-1 != f)
    {
        write(f, &time, sizeof(time_t));
        fsync(f);
        close(f);
    }
    else
    {
        LOG_E("write failed. filename=%s\n", sync_time_file);
        return -1;
    }

    //LOG_D("time=%d\n", time);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.read_sync_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月24日
 * 函数功能  : 读取时间
 * 输入参数  : time_t& time  时间
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int read_sync_time(time_t& time)
{
    int f = open(sync_time_file,  O_RDONLY);
    if (-1 != f)
    {
        (void)read(f, &time, sizeof(time_t));
    }
    else
    {
        LOG_E("read failed. filename=%s\n", sync_time_file);
        return -1;
    }

    //LOG_D("time=%d\n", time);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.sync_system_time
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月24日
 * 函数功能  : 设置系统时间
 * 输入参数  : time_t& time  
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void sync_system_time(time_t& time)
{
    struct tm* tm = gmtime(&time);
    char buf[128];
    sprintf(buf, "busybox date -s \"%d-%02d-%02d %02d:%02d:%02d\"",
        tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
        tm->tm_hour, tm->tm_min, tm->tm_sec);

    LOG_D("buf=%s\n", buf);
    (void)RW_System(buf);
    return;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.has_synced_time_flag_file
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月25日
 * 函数功能  : 判断是否同步过时间
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int has_synced_time_flag_file()
{
    int f = open(sync_time_flag_file, O_RDONLY);
    return (-1 != f);
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.create_synced_time_flag_file
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年1月25日
 * 函数功能  : 创建时间同步标志文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int create_synced_time_flag_file()
{
    int f = open(sync_time_flag_file, O_CREAT);
    return (-1 != f);
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.zEakonThread.mcu_reset
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月17日
 * 函数功能  : 复位MCU
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : int: 0 : ok, -1: err
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int mcu_reset()
{
    #define MCU_RESET_GAP (100)
    
    LOG_I("enter.\n");
    //zBaseUtil::RW_System("echo 1 > /sys/class/misc/mcu_dev/mcu/mcu_ctl");
    zBaseUtil::RW_Echo("1", strlen("1"), "/sys/class/misc/mcu_dev/mcu/mcu_ctl");
    std::this_thread::sleep_for(std::chrono::milliseconds(MCU_RESET_GAP));

    //zBaseUtil::RW_System("echo 0 > /sys/class/misc/mcu_dev/mcu/mcu_ctl");
    zBaseUtil::RW_Echo("0", strlen("0"), "/sys/class/misc/mcu_dev/mcu/mcu_ctl");
    std::this_thread::sleep_for(std::chrono::milliseconds(MCU_RESET_GAP));

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.get_gateway_addr
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年2月24日
 * 函数功能  : 获取网关地址
 * 输入参数  : std::string& addr : 网关地址
 * 输出参数  : 无
 * 返 回 值  : int: 0: 获取成功， -1: 失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int get_gateway_addr(std::string& addr)
{
    #if 0
    std::string str_gateway;
    const char * gateway_tmp_filename = "/cache/gateway.txt";

    // try router
    (void)zBaseUtil::RW_System("getprop dhcp.wlan0.gateway > /cache/gateway.txt");

    char line[1024] = { 0 };
    FILE* pFile = fopen(gateway_tmp_filename, "r");
    if (NULL != pFile)
    {
        if (NULL != fgets(line, 1024, pFile))
        {
            if (strlen(line) >= 4) //  TODO: 判断合法IP
            {
                if ('\n' == line[strlen(line) - 1])
                {
                    line[strlen(line) - 1] = '\0';
                }
                str_gateway = line;
            }
        }
        fclose(pFile);
    }
    else
    {
        LOG_E("fopen failed for %s: filename=%s\n", strerror(errno), gateway_tmp_filename);
    }
    remove(gateway_tmp_filename);
    
    addr = str_gateway;
    return (str_gateway.empty()) ? -1 : 0;
    #else

    const char * key = "dhcp.wlan0.server";
    char buf[92];
    int len = zBaseUtil::property_get(key, buf, "");
    if (len >= 4)
    {
        addr = buf;
        LOG_D("addr= %s\n", addr.c_str());
        return 0;
    }

    addr.clear();
    return -1;
    #endif
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.get_device_test_ipaddr
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年4月6日
 * 函数功能  : 工装模式下， 获取服务端IP地址。
 * 输入参数  : std::string& addr  返回： ip地址
               bool bGateWay  true: 网关IP,    falee: .2网段
 * 输出参数  : 无
 * 返 回 值  : int: 0: 获取成功， -1: 失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int get_device_test_ipaddr(std::string& addr, bool bGateWay)
{
    // 网关地址
    if (bGateWay)
    {
        return get_gateway_addr(addr);
    }

    // .2网段
    const char * key = "dhcp.wlan0.ipaddress";
    char buf[92];
    int len = zBaseUtil::property_get(key, buf, "");
    if (len >= 4)
    {
        std::string ipaddr = buf;
        size_t nPos = ipaddr.rfind(".");
        if (nPos != std::string::npos)
        {
            addr = ipaddr.substr(0, nPos + 1) + "2";
            LOG_D("ipaddr=%s, addr= %s\n", ipaddr.c_str(), addr.c_str());
            return 0;
        }
    }

    addr.clear();
    return -1;
}

int property_get(const char *key, char *value, const char *default_value)
{
    int len;

    len = __system_property_get(key, value);
    if(len > 0) {
        return len;
    }

    if(default_value) {
        len = strlen(default_value);
        memcpy(value, default_value, len + 1);
    }
    return len;
}

void program_begin()
{
    g_program_begin_time = zBaseUtil::getMilliSecond();
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.program_duration
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月10日
 * 函数功能  : 返回程序运行时间： 单位：毫秒
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : std::int64_t
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
std::int64_t program_duration()
{
    std::int64_t curr_time = zBaseUtil::getMilliSecond();
    return curr_time - g_program_begin_time;
}

bool reboot_program()
{
    const char *szBuf = "setprop ctl.restart facedis";
    LOG_D("prog restart. szBuf=%s", szBuf);

    int nReturn = zBaseUtil::RW_System(szBuf);
    if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
    {
        return true;
    }
    return false;
}

std::string to_string(bool b)
{
  return b ? "true" : "false";
}

int to_int(std::string &s)
{
    return atoi(s.c_str());
}

bool to_bool(const std::string &s)
{
    return "true" == s;
}

double to_double(std::string &s)
{
    return std::atof(s.c_str());
}

void file_sync(const std::string &full_filename)
{
    int f = open(full_filename.c_str(), O_RDWR);
    if (-1 != f)
    {
        fsync(f);
        close(f);
    }
    else
    {
        LOG_D("file open failed. filename=%s\n", full_filename.c_str());
    }
    return;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.randint
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月29日
 * 函数功能  : 获取[min, max] 的随机值
 * 输入参数  : int min  整数
               int max  整数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int randint(int min, int max)
{
    return randint((unsigned)time(0), min, max);
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.randint
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年7月29日
 * 函数功能  : 生成随机数
 * 输入参数  : unsigned int seed  随机数种子，如time(0)
               int min            整数
               int max            整数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int randint(unsigned int seed, int min, int max)
{
    std::srand(seed);

    int span = max - min + 1;
    return (std::rand() % span) + min;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.crc_checksum_only
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月15日
 * 函数功能  : 计算校验和，并比对
 * 输入参数  : uint8_t *buf  无符号字符缓存
               int len       字符缓存长度
 * 输出参数  : 无
 * 返 回 值  : true: 校验成功，false: 校验失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool crc_checksum_only(const uint8_t *buf, int len)
{
    // 1: 校验码
    if (    (NULL == buf) 
        ||  (len <= 1) )
    {
        return false;
    }

    // 计算CRC 值。
    uint8_t crc = 0;
    for (int i=0; i <len-1; i++)
    {
        crc += buf[i];
    }
    crc = 0xFF - crc;
    crc += 1;

    // CRC 校验
    if (crc != buf[len - 1])
    {
        return false;
    }

    return true;
}

/*****************************************************************************
 * 函 数 名  : zBaseUtil.crc_checksum_with_endcode
 * 负 责 人  : 刘春龙
 * 创建日期  : 2017年1月15日
 * 函数功能  : 计算校验和，并比对结束码
 * 输入参数  : uint8_t *buf  无符号字符缓存
               int len       字符缓存长度
 * 输出参数  : 无
 * 返 回 值  : true: 校验成功，false: 校验失败
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
bool crc_checksum_with_endcode(const uint8_t *buf, int len)
{
    // 2: 校验码和结束码
    if (    (NULL == buf) 
        ||  (len <= 2) )
    {
        return false;
    }

    // 校验结束码
    if (0x55 != buf[len - 1])
    {
        return false;
    }

    return crc_checksum_only(buf, len -1);
}

}


