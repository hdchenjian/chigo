#ifndef __ZCOMMONDEF_H__
#define __ZCOMMONDEF_H__

#define ZSAFE_DELETE(p)           do { delete (p); (p) = nullptr; } while(0)
#define ZSAFE_DELETE_ARRAY(p)     do { if(p) { delete[] (p); (p) = nullptr; } } while(0)
#define ZSAFE_FREE(p)             do { if(p) { free(p); (p) = nullptr; } } while(0)

#endif