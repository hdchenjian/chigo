package com.reconova.processor;

/*
 * 修改说明：
 *
 * [2015-09-29] Cyberman Wu
 * 取帧的接口增加控制是否进行H.264编码的参数。原来的设计我们不想要某帧编码可以
 * 直接不取编码结果，下一帧自动就把它覆盖了；但H.264前面所有帧都可能是参考帧，
 * 这样做会导致取到的H.264码流图像质量下降。
 * 另外这两人个接口的输出数据我们改为long数据，因为Timestamp是毫秒，用32bit的
 * int表示有可能不够。
 *
 *
 */


/*
 * 除了GetFrame和H264GetEncFrame之外，其它接口和C接口基本上一一对应，所以不再
 * 重新在这里描述，具体参见capture.h文件中的描述，Pixel Foramt以及H264 Profile
 * 定义参见cap_types.h文件。
 */
class NativeImageCapture
{
    /*
     * 配置初始化时向驱动申请的buffer的个数。注意这个只是一个期望值，实际上有
     * 可能因为空间不够而实际分配的少；或因为驱动的原因比这个值大。
     * 这个函数需要在Initialize()之前调用，不配置的话初始化会使用缺省值。
     *
     * ==== 用PreConfig....()的函数名都是需要在实始化之前调用的，配置一些参数
     * ==== 不使用缺省值。这个本来可以直接做为初始化的参数，但这样就需要修改
     * ==== 接口了，不利于原来程序的兼容。
     * ++++ 目前我们不支持多实例，一个实例对应一个摄像头；如果后面要支持的话，
     * ++++ 这个接口有可能会合并到初始化中，或者采用预分配句柄的方式。
     *
     * 参数说明：
     *   count          -- 希望分配的驱动buffer个数。目前在全志的R16上测试，
     *                     发现驱动分配最少为3个，请求1、2都是分配3个；而再大
     *                     一直到14都是分配到申请的个数。再大的话申请会成功，
     *                     但mmap()失败（物理内存不够？），再加上疑似驱动问题，
     *                     这个地方失败直接返回，后面这个摄像头会一直busy，
     *                     只能重新启动系统。
     *                     似乎是驱动中少了buffer上限的判断处理。
     * 返回值：
     *   0表示成功，小于0失败。
     */
    public native static int PreConfigBufCnt(int count);

    public native static int Initialize(int dev_id, int width, int height, 
                                        int pixel_fmt, int frame_rate, 
                                        boolean reverse);
    public native static int Finalize();
    public native static int Start();
    // Finalize()之前并不需要调用这个Stop()，加上这个是考虑不需要视频帧的时候，
    // Stop()掉它会不会能使系统的性能更好，目前还没有测试过，一般也不用它。
    public native static int Stop();
    // img_buf需要分配足够的空间，可以比实际数据大。
    // img_info为输出参数：
    //   img_info[0] - 底层V4L2驱动返回的数据长度，用于发现一些异常情况，一般
    //                 不用。成功获取到的图像数据大小是固定的，不再参数返回。
    //   img_info[1] - 毫秒时间戳。
    // encode表示是否对这一帧进行H.264编码。
    public native static int GetFrame(byte img_buf[], long img_info[], int encode);
    // 和GetFrame()函数功能相同，但只获取灰度图像，少拷贝一些数据。
    public native static int GetGrayImage(byte img_buf[], long img_info[], int encode);

    // 打开H264编码之后，每GetFrame一帧，同时编码保存一帧。save_fname传入null
    // 表示不使能录相功能。
    public native static int H264EncStart(int dst_width, int dst_height, 
                                          int profile, int bit_rate, 
                                          String save_fname);
    public native static int H264EncStop();
    // 目前打开H264编码之后，每次GetFrame()时会编码一帧，如果初始化时save_fname
    // 非空，编码时会自动保存文件。
    // 同时编码的结果也会缓存起来，可以调用H264GetEncFrame()来把它获取上来。
    // enc_buf的大小一般是dst_width * dst_height就足够了。
    // enc_info为输出参数：
    //   enc_info[0] - 编码后的数据长度。
    //   enc_info[1] - 是否为关键值，1表示是关键帧，0表示非关键帧。
    //   enc_info[3] - 对于关键帧，这个值表示其中SPS&PPS部分的长度。
    public native static int H264GetEncFrame(byte enc_buf[], int enc_info[]);

    /*
    static {
        System.loadLibrary("V4L2ImageCapJni");
    }*/
}

