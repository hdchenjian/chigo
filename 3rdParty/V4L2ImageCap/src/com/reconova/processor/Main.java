package com.reconova.processor;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.reconova.processor.NativeImageCapture;
import android.util.Log;

public class Main extends Activity {

    //CameraPreview cp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.main);

        // NativeImageCapture capObj = new NativeImageCapture();
        // 目前支持的最小是640x480。
        int err = NativeImageCapture.Initialize(0, 1280, 720, 1, 0, 0);
        TextView tv = new TextView(this);
        tv.setText("err = " + err);
        Log.i("RecoImageCap", "err = " + err);
        
        err = NativeImageCapture.Start();

        byte[] img_buf = new byte[640*480*3/2];
        int[] img_info = new int[3];

        for (int i = 0; i < 10; ++i)
        {
            err = NativeImageCapture.GetFrame(img_buf, img_info);
            Log.i("RecoImageCap", "Frame " + i + ": err = " + err);
        }

        //cp = (CameraPreview) findViewById(R.id.cp);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NativeImageCapture.Finalize();
        Log.i("RecoImageCap", "NativeImageCapture finalized.");
    }

    static {
        System.loadLibrary("V4L2ImageCapJni");
    }
}
