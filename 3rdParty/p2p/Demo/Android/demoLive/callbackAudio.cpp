// callbackAudio.cpp 
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "pgLibLive.h"
#include "pgLibDevVideoIn.h"
#include "pgLibDevAudioIn.h"
#include "pgLibDevAudioOut.h"
#include "callbackAudio.h"


//-----------------------------------------------------------------------------
// In this demo, we use a audio loopback way to simulate audio output and audio input.
// When the SDK play a audio output data, we copy the audio output data into a buffer.
// And then in the audio record thread, we write the audio output data that in buffer back to SDK.
//
// -------
// |     | ----- AudioOutPlay() ------------+
// |     |                                  |
// |     |                             +---------+
// | SDK |                             | Buffer  |
// |     |                             +---------+
// |     |                                  |
// |     | <---- pgDevAudioInRecordProc()---+
// -------


// The buffer use cache the audio output data.
static unsigned char s_ucDataOut[1024] = {0};
static unsigned int s_uDataOutSize = 0;

// The total bytes has played.
static unsigned int s_uPlayedSize = 0;



//------------------------------------------------------------------------------
// Audio input callback

static int s_iDevID_AudioIn = -1;			
static pthread_t s_tThread_AudioIn;	
static unsigned int	s_uRunning_AudioIn= 0;

///
// In this demo, we use a thread to simulate Audio input.
static void* AudioInputThreadProc(void* lp)
{
  	printf("AudioInputThreadProc\n");

	if (pthread_detach(pthread_self()) != 0) {
		printf("AudioInputThreadProc, err=%d", errno);
	}

	while (s_uRunning_AudioIn != 0) {

		// Write the audio output data back to SDK as record data.
		if (s_uDataOutSize != 0) {
			pgDevAudioInRecordProc(s_iDevID_AudioIn, s_ucDataOut,
				s_uDataOutSize, PG_DEV_AUDIO_IN_FMT_PCM16, 0);
	
			// The total bytes has played.
			s_uPlayedSize += s_uDataOutSize;
			s_uDataOutSize = 0;
		}

		usleep(20 * 1000);
	}

	printf("AudioInputThreadProc end\n");
	
	pthread_exit(NULL);
}

static int AudioInOpen(unsigned int uDevNO, unsigned int uSampleBits,
	unsigned int uSampleRate, unsigned int uChannels, unsigned int uPackBytes)
{
	printf("AudioInOpen: uDevNO=%u, uSampleBits=%u, uSampleRate=%u, uChannels=%u,uPackBytes=%u\n",
		uDevNO, uSampleBits, uSampleRate, uChannels,uPackBytes);
	
	// In this demo, we use a thread to simulate Audio record.
	pthread_attr_t attr;
	pthread_attr_init(&attr);

	s_uRunning_AudioIn = 1;

	int iRet = pthread_create(&s_tThread_AudioIn, &attr, AudioInputThreadProc, 0);
	if (iRet != 0) {
		s_uRunning_AudioIn = 0;
	}
	else {
		s_iDevID_AudioIn = 1234;
	}

	pthread_attr_destroy(&attr);

	return s_iDevID_AudioIn;
}

static void AudioInClose(int iDevID)
{
	printf("AudioInClose: iDevID=%d\n", iDevID);

	if (iDevID == s_iDevID_AudioIn) {
		s_uRunning_AudioIn = 0;
		s_iDevID_AudioIn = -1;
	}
}

static PG_DEV_AUDIO_IN_CALLBACK_S s_stCallback_AudioIn = {
	AudioInOpen,
	AudioInClose
};
 

//------------------------------------------------------------------------------
// Audio input callback

static int s_iDevID_AudioOut = -1;
static pthread_t s_tThread_AudioOut;
static unsigned int	s_uRunning_AudioOut = 0;

///
// In this demo, we use a thread to simulate Audio output callback.
static void* AudioOutputThreadProc(void* lp)
{
	printf("AudioOutputThreadProc\n");
 
 	if (pthread_detach(pthread_self()) != 0) {
 		printf("AudioOutThreadProc, err=%d", errno);
 	}

	unsigned int uPlayedSize = s_uPlayedSize;

	while (s_uRunning_AudioOut != 0) {

		// Notify SDK the total bytes of data has played.
 		if (s_uPlayedSize > uPlayedSize) {
	 		pgDevAudioOutPlayedProc(s_iDevID_AudioOut, s_uPlayedSize, 0);
	 		uPlayedSize = s_uPlayedSize;
	 	}
 		
 		usleep(20 * 1000);
 	}
 	
 	printf("AudioOutputThreadProc end\n");
 	pthread_exit(NULL);
}
 
static int AudioOutOpen(unsigned int uDevNO, unsigned int uSampleBits,
	unsigned int uSampleRate, unsigned int uChannels, unsigned int uPackBytes)
{
 	printf("AudioOutOpen: uDevNO=%u, uSampleBits=%u, uSampleRate=%u, uChannels=%u,uPackBytes=%u\n",
 		uDevNO, uSampleBits, uSampleRate, uChannels,uPackBytes);
 	
 	// In this demo, we use a thread to simulate video playing.
 	pthread_attr_t attr;
 	pthread_attr_init(&attr);
 
 	s_uRunning_AudioOut = 1;
 
 	int iRet = pthread_create(&s_tThread_AudioOut, &attr, AudioOutputThreadProc, 0);
 	if (iRet != 0) {
 		s_uRunning_AudioOut = 0;
 	}
 	else {
 		s_iDevID_AudioOut = 1234;
 	}
 
 	pthread_attr_destroy(&attr);
 
 	return s_iDevID_AudioOut;
}
 
static void AudioOutClose(int iDevID)
{
 	printf("AudioOutClose: iDevID=%d\n", iDevID);
 
 	if (iDevID == s_iDevID_AudioOut) {
 		s_uRunning_AudioOut = 0;
 		s_iDevID_AudioOut = -1;
 	}
}
 
static int AudioOutPlay(int iDevID, const void* lpData, unsigned int uDataSize, unsigned int uFormat)
{
	// Play Audio data ...
	
	// We copy the audio output data to a buffer, and then it will be used
	// as the audio input data in other thread to simulate audio loopback.
	memcpy(s_ucDataOut, lpData, uDataSize);
	s_uDataOutSize = uDataSize;

   	return uDataSize;
}

static PG_DEV_AUDIO_OUT_CALLBACK_S s_stACallback_AudioOut = {
	AudioOutOpen,
	AudioOutClose,
	AudioOutPlay
};


void RegisterAudioCallback(void)
{
	pgDevAudioInSetCallback(&s_stCallback_AudioIn);
	pgDevAudioOutSetCallback(&s_stACallback_AudioOut);
}