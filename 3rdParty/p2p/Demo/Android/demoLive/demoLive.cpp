// demoLive.cpp 
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "pgLibLive.h"
#include "callbackVideo.h"
#include "callbackAudio.h"


void LogOutput(unsigned int uLevel, const char* lpszOut)
{
	printf(lpszOut);
}



int main(int argc, char* argv[])
{
	char szDevID[128] = {0};
	char szSvrAddr[128] = {0};

	printf("Input the device id: ");
	gets(szDevID);
	if (szDevID[0] == '\0') {
		strcpy(szDevID, "11223344");
	}

	printf("Input the server address: ");
	gets(szSvrAddr);
	if (szSvrAddr[0] == '\0') {
		strcpy(szSvrAddr, "connect.peergine.com:7781");
	}
	
	// Register the video input callback interface.
	RegisterVideoCallback();
	RegisterAudioCallback();
  
	// Init Live cfg.
	PG_LIVE_INIT_CFG_S stInitCfg;
	memset(&stInitCfg, 0, sizeof(stInitCfg));

	PG_LIVE_VIDEO_S stVideo;
	memset(&stVideo, 0, sizeof(stVideo));
	stVideo.uCode = 3;
	stVideo.uMode = 2;
	stVideo.uRate = 100;

	printf("Live module init start.\n");

	unsigned int uInstID = 0;
	if (pgLiveInitialize(&uInstID, szDevID, "", szSvrAddr,
		"", &stInitCfg, &stVideo, LogOutput) != PG_LIVE_ERROR_OK)
	{
		printf("Init peergine module failed.\n");
		return 0;
	}

	printf("Init Live module success.\n");

	unsigned int uCount = 0;
	while (1) {

		char szData[1024] = {0};
		char szRender[128] = {0};
		unsigned int uParam = 0;
		unsigned int uEventNow = PG_LIVE_EVENT_NULL;

		int iErr = pgLiveEvent(uInstID, &uEventNow, szData, sizeof(szData), &uParam, szRender, 20);
		if (iErr != PG_LIVE_ERROR_OK) {
			if (iErr != PG_LIVE_ERROR_TIMEOUT) {
				printf("pgLiveEvent: iErr=%d\n", iErr);
			}

			uCount++;
			continue;
		}

		if (uEventNow == PG_LIVE_EVENT_MESSAGE) {
			printf("MESSAGE: szData=%s, szRender=%s\n", szData, szRender);

			int iRet = pgLiveMessage(uInstID, szRender, "Done !");
		}
		else if (uEventNow == PG_LIVE_EVENT_RENDER_JOIN) {
			printf("RENDER_JOIN: szData=%s\n", szData);

			PG_LIVE_INFO_S stInfo;
			int iRet = pgLiveInfo(uInstID, szData, &stInfo);
			if (iRet == PG_LIVE_ERROR_OK) {
				printf("pgInfo: PeerID=%s, AddrPub=%s, AddrPriv=%s, CnntType=%u\n",
					stInfo.szPeerID, stInfo.szAddrPub, stInfo.szAddrPriv, stInfo.uCnntType);
			}
		}
		else if (uEventNow == PG_LIVE_EVENT_RENDER_LEAVE) {
			printf("RENDER_LEAVE: szData=%s\n", szData);
		}
		else if (uEventNow == PG_LIVE_EVENT_VIDEO_STATUS) {
			printf("VIDEO_STATUS: szData=%s\n", szData);
		}
		else if (uEventNow == PG_LIVE_EVENT_CAMERA_FILE) {
			printf("CAMERA_FILE: szData=%s\n", szData);
		}
		else if (uEventNow == PG_LIVE_EVENT_SVR_LOGIN) {
			printf("SVR_LOGIN: szData=%s\n", szData);

			int iRet = pgLiveAudioStart(uInstID);
     	 	printf("AudioStart = %d \n", iRet);

     	 	iRet = pgLiveVideoStart(uInstID);
			printf("VideoStart = %d \n", iRet);
		}
		else if (uEventNow == PG_LIVE_EVENT_SVR_LOGOUT) {
			printf("SVR_LOGOUT: szData=%s\n", szData);
     		pgLiveAudioStop(uInstID);
			pgLiveVideoStop(uInstID);
		}
		else if (uEventNow == PG_LIVE_EVENT_SVR_REPLY) {
			printf("SVR_REPLY: szData=%s\n", szData);
		}
		else if (uEventNow == PG_LIVE_EVENT_SVR_NOTIFY) {
			printf("SVR_NOTIFY: szData=%s\n", szData);
		}
		else if (uEventNow == PG_LIVE_EVENT_FORWARD_ALLOC_REPLY) {
			printf("FORWARD_ALLOC_REPLY: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FORWARD_FREE_REPLY) {
			printf("FORWARD_FREE_REPLY: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_PUT_REQUEST) {
			printf("FILE_PUT_REQUEST: szData=%s, szRender=%s\n", szData, szRender);
			int iRet = pgLiveFileAccept(uInstID, szRender, "testput.txt");
			if (iRet != PG_LIVE_ERROR_OK) {
				printf("pgLiveFileAccept: iRet=%d\n", szData, iRet);
			}
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_GET_REQUEST) {
			printf("FILE_GET_REQUEST: szData=%s, szRender=%s\n", szData, szRender);
			int iRet = pgLiveFileAccept(uInstID, szRender, "testget.txt");
			if (iRet != PG_LIVE_ERROR_OK) {
				printf("pgLiveFileAccept:szData=%s, iRet=%d\n", szData, iRet);
			}
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_ACCEPT) {
			printf("FILE_ACCEPT: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_REJECT) {
			printf("FILE_REJECT: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_PROGRESS) {
			printf("FILE_PROGRESS: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_FINISH) {
			printf("FILE_FINISH: szData=%s, szRender=%s\n", szData, szRender);
		}
		else if (uEventNow == PG_LIVE_EVENT_FILE_ABORT) {
			printf("FILE_ABORT: szData=%s, szRender=%s\n", szData, szRender);
		}
	}

	pgLiveCleanup(uInstID);
	printf("Clean peergine module.");

	getchar();
}

