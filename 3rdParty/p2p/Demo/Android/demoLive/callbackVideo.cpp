// callbackVideo.cpp 
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#include "pgLibLive.h"
#include "pgLibDevVideoIn.h"
#include "pgLibDevAudioIn.h"
#include "pgLibDevAudioOut.h"
#include "callbackVideo.h"

			


static int s_iDevID_VideoIn = -1;	
static pthread_t s_tThread_VideoIn;
static unsigned int s_uRunning_VideoIn = 0;

///
// In this demo, we use a thread to simulate video input.
static void* VideoInputThreadProc(void* lp)
{
	printf("VideoInputThreadProc\n");

	if (pthread_detach(pthread_self()) != 0) {
		printf("VideoInputThreadProc, err=%d", errno);
	}
 

	unsigned int uDataSize = 0;
	unsigned char ucData[16384] = {0};
	
	int ifrmSize = -1;
	
	FILE* pFile = fopen("one_h264_frm.frm", "rb");
	if (pFile != NULL) {
		fseek(pFile, 0, SEEK_END);
		ifrmSize = ftell(pFile);
	}

	while (s_uRunning_VideoIn != 0) {

		// Get H.264 video input frame data from codec.
		if (pFile != NULL) {
			fseek(pFile, 0, SEEK_SET);
			int iRead = fread(ucData, ifrmSize, 1, pFile);
			if (iRead > 0 || feof(pFile)) {
				uDataSize = ifrmSize; //
			}
			else {
				uDataSize = 1024; // assume the H.264 frame size.
			}
		}
		else {
			uDataSize = 1024; // assume the H.264 frame size.
		}

		unsigned uFlag = 0;
		if (ucData[4] == 0x67) { // H.264 Key frame (Has a SPS frame head).
			uFlag |= PG_DEV_VIDEO_IN_FLAG_KEY_FRAME;
		}

		// Call peergine video input API.
		pgDevVideoInCaptureProc(s_iDevID_VideoIn, ucData, uDataSize, PG_DEV_VIDEO_IN_FMT_H264, uFlag);
		
    	// Sleep frame interval: 500ms, as 2fps.
		usleep(500 * 1000);
	}
	
	if (pFile != NULL) {
		fclose(pFile);
	}
	
	printf("VideoInputThreadProc\n");
	pthread_exit(NULL);
}

///
// Video input callback functions.
static int VideoInOpen(unsigned int uDevNO, unsigned int uPixBytes,
	unsigned int uWidth, unsigned int uHeight, unsigned int uBitRate,
	unsigned int uFrmRate, unsigned int uKeyFrmRate)
{
	printf("VideoInOpen: uDevNO=%u, uWidth=%u, uHeight=%u, uBitRate=%u, uFrmRate=%u, uKeyFrmRate=%u\n",
		uDevNO, uWidth, uHeight, uBitRate, uFrmRate, uKeyFrmRate);
	
	if (uWidth != 320 || uHeight != 240) {
		return -1;
	}
	
	// Start to capture video ...

	// In this demo, we use a thread to simulate video input.
	pthread_attr_t attr;
	pthread_attr_init(&attr);

	s_uRunning_VideoIn = 1;

	int iRet = pthread_create(&s_tThread_VideoIn, &attr, VideoInputThreadProc, 0);
	if (iRet != 0) {
		s_uRunning_VideoIn = 0;
	}
	else {
		s_iDevID_VideoIn = 1234;
	}

	pthread_attr_destroy(&attr);

	return s_iDevID_VideoIn;
}

static void VideoInClose(int iDevID)
{
	printf("VideoInClose: iDevID=%d\n", iDevID);

	// Stop capture video ...
	
	if (iDevID == s_iDevID_VideoIn) {
		s_uRunning_VideoIn = 0;
		s_iDevID_VideoIn = -1;
	}
}


static void VideoInCtrl(int iDevID, unsigned int uCtrl, unsigned int uParam)
{
	printf("VideoInCtrl: iDevID=%d, uCtrl=%u\n", iDevID, uCtrl);

	if (uCtrl == PG_DEV_VIDEO_IN_CTRL_PULL_KEY_FRAME) {
		// Force the encoder to output a key frame immediately.
		// ...
	}
}

static PG_DEV_VIDEO_IN_CALLBACK_S s_stCallback_VideoIn = {
	VideoInOpen,
	VideoInClose,
	VideoInCtrl
};

void RegisterVideoCallback(void)
{
	pgDevVideoInSetCallback(&s_stCallback_VideoIn);  
}