/*******************************************************************************
  copyright   : Copyright (C) 2016, www.peergine.com, All rights reserved.
  filename    : pgLibDevAudioConvert.h
  discription : 
  modify      : Create, chenbichao, 2016/06/24

*******************************************************************************/
#ifndef _PG_LIB_DEV_AUDIO_CVT_H
#define _PG_LIB_DEV_AUDIO_CVT_H


#ifdef	__cplusplus
extern	"C"	{
#endif


///
// Audio convert format enum.
typedef enum tagPG_DEV_AUDIO_CVT_FMT_E {
	PG_DEV_AUDIO_CVT_FMT_PCM16,
	PG_DEV_AUDIO_CVT_FMT_G711A,
	PG_DEV_AUDIO_CVT_FMT_G711U,
	PG_DEV_AUDIO_CVT_FMT_AAC,
} PG_DEV_AUDIO_CVT_FMT_E;


///
// uDirect: [in] 0: input, 1: output.
// uDstFormat: [in] The destinstion format.
// uDevSampleRate: [in] The device side sample rate.
// uDevSampleSize: [in] The device side sample number of one packet.
// Return value:
//      <0: failed, >0: return the file descriptor id.
int pgDevAudioConvertAlloc(unsigned int uDirect, unsigned int uDstFormat,
	unsigned int uDevSampleRate, unsigned int uDevSampleSize);

///
// iCvtID: [in] The convert file descriptor id.
void pgDevAudioConvertFree(int iCvtID);

///
// iCvtID: [in] The convert file descriptor id.
// uSrcFormat: [in] The source format.
// lpSrcData: [in] The source data buffer
// uSrcDataSize: [in] The source data size.
// Return value:
//     <0: failed, >=0: size of data in buffer.
int pgDevAudioConvertPush(int iCvtID, unsigned int uSrcFormat,
	const void* lpSrcData, unsigned int uSrcDataSize);

///
// iCvtID: [in] The convert file descriptor id.
// ppDstData: [out] The destinstion data buffer.
// lpuDstDataSize: [out] The destinstion data size.
// Return value:
//     <0: failed, ==0: packet data has not ready, >0: output valid packet data.
int pgDevAudioConvertPop(int iCvtID, void **ppDstData, unsigned int* lpuDstDataSize);


#ifdef __cplusplus
}
#endif

#endif //_PG_LIB_DEV_AUDIO_CVT_H
