#ifndef WEBRTC_EXAMPLES_WEBSOCKETCLIENT_WEBSOCKET_CLIENT_H_
#define WEBRTC_EXAMPLES_WEBSOCKETCLIENT_WEBSOCKET_CLIENT_H_

#include <iostream>

typedef enum tagWEBRTC_ERROR {
    WEBRTC_SUCCESS = 0,                       // 成功
    WEBRTC_UNINIT = -1,                       // 没有调用initializeWebrtc()或者已经调用cleanupWebrtc()清理模块
    WEBRTC_INIT_ALREADY = -2,                 // 已调用initializeWebrtc()
    WEBRTC_PEER_NOT_EXIST = -3,               // 指定的PEER不存在
    WEBRTC_EXCEED_MAX_PEER = -4,              // 超过最大实例数
    WEBRTC_DOWNLOAD_FILE_NOT_EXIST = -5,      // 下载的文件不存在
    WEBRTC_DOWNLOAD_FILE_SIZE_ERROR = -5,      // 下载的文件大小为0
    WEBRTC_TIMEOUT = -7,                      // 超时
} WEBRTC_ERROR;

typedef enum tagWEBRTC_EVENT {
    WEBRTC_EVENT_NULL,                 // NULL
    WEBRTC_EVENT_MESSAGE,              // 接收到PEER发送的消息。
    WEBRTC_EVENT_RENDER_JOIN,          // Render加入视频直播组。
    WEBRTC_EVENT_RENDER_LEAVE,         // Render离开视频直播组。
    WEBRTC_EVENT_CONNECT_CLOSED,       // 连接已断开

    WEBRTC_EVENT_SERVER_LOGIN = 32,       // 登录到P2P服务器成功（上线）
    WEBRTC_EVENT_SERVER_LOGOUT,           // 从P2P服务器注销或掉线（下线）
    WEBRTC_EVENT_SERVER_KICK_OUT,         // 被服务器踢出，因为有另外一个相同ID的节点登录了。
	
    WEBRTC_EVENT_FILE_PUSH_REQUEST = 48,      // PEER请求上传文件
    WEBRTC_EVENT_FILE_PULL_REQUEST,           // PEER请求下载文件
    WEBRTC_EVENT_FILE_PROGRESS,              // 文件传输进度
    WEBRTC_EVENT_FILE_FINISH,                // 文件传输完成
    WEBRTC_EVENT_FILE_ABORT,                 // 文件传输中断
} WEBRTC_EVENT;

/*****************************************************************************
函数功能  : 等待WEBRTC事件
输入参数  : eventType见WEBRTC_EVENT，peerName为此事件相关的PEER，eventData为事件数据,
            若等待timeout毫秒后无事件则返回，timeout为0则一直等待
 *****************************************************************************/
int getEventWebrtc(int& eventType, std::string& peerName, std::string& eventData, int timeout);

/*****************************************************************************
函数功能  : 初始化,需保持deviceName的唯一,realDeviceName为服务端返回的实际deviceName
 *****************************************************************************/
int initializeWebrtc(const std::string& deviceName, const std::string& serverAddress, std::string& realDeviceName);

/*****************************************************************************
函数功能  : 将一帧图像发送给　PEER
输入参数  : videoFrame 帧图像数据
　　　　　　videoFrameLength　数据大小
　　　　　　frameInfo　帧信息
 *****************************************************************************/

/*****************************************************************************
函数功能  : 建立与PEER的连接
 *****************************************************************************/
void startVideoWebrtc(const std::string& peerName);

/*****************************************************************************
函数功能  : 关闭与PEER间的数据传输
 *****************************************************************************/
void stopVideoWebrtc(const std::string& peerName);

/*****************************************************************************
函数功能  : 关闭与PEER间的数据传输,并通知PEER
 *****************************************************************************/
void stopVideoAndNotifyPeerWebrtc(const std::string& peerName);

void sendFrameToWebrtc(uint8_t* videoFrame, size_t videoFrameLength, int width, int height, bool IsKeyFrame);

/*****************************************************************************
函数功能  : 给所有的连接PEER发送一条消息
 *****************************************************************************/
int notifyWebrtc(const std::string& message);

/*****************************************************************************
函数功能  : 给peerName发送一条消息
 *****************************************************************************/
int sendMessageWebrtc(const std::string& peerName, const std::string& message);

/*****************************************************************************
函数功能  : 接受PEER下载文件请求
 *****************************************************************************/
int fileAcceptWebrtc(const std::string& peerName, const std::string& filePath);

/*****************************************************************************
函数功能  : 拒绝PEER下载文件请求
 *****************************************************************************/
void fileRejectWebrtc(const std::string& peerName);

/*****************************************************************************
函数功能  : 取消正在下载的文件
 *****************************************************************************/
void fileCancelWebrtc(const std::string& peerName);


/*****************************************************************************
函数功能  : 清理
 *****************************************************************************/
int cleanupWebrtc();
#endif
