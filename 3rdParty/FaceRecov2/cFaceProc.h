/*
 * Copyright(c) Reconova Information Technololies Co., Ltd. All right reserves.
 *
 * 人脸视别库的C++接口。
 *
 * Created by Cyberman Wu on Jul 25th, 2015.
 */


#ifndef __C_FACE_PROC_H__
#define __C_FACE_PROC_H__

#ifndef __cplusplus
#error "This hearder can only used in C++ source code!"
#endif


#include <string>


#define RECO_FACE_NAME_LEN                  40


/*
 * 芯片控制接口。
 */
extern "C"
{
bool ChipsetConfig_LicenseParamInit(const char *dev_name, int baud_rate);
}



/*
 * 人脸检测、视别、分析接口。
 */
extern "C"
{
bool FaceProcessor_FaceInit
    (bool bFaceDetectorEx, 
     const char *fn_model,        const char *fn_model_ex,
     const char *fn_config_align, const char *fn_model_align, 
     const char *fn_config_recog, const char *fn_model_recog,
     const char *fn_metric_model, const char *fn_fdir_database,
     int face_display_width, int face_display_height, 
     void *context
    );


bool FaceProcessor_FaceClear(void);


bool FaceProcessor_FaceFinalize(void);


float FaceProcessor_FaceGetParam(const char *param_name);


bool FaceProcessor_FaceSetParam(const char *param_name, float param_value);


bool FaceProcessor_FaceRecogParam(int face_param[], int num);


bool FaceProcessor_FaceAddROI(int roi_left, int roi_top, int roi_right, int roi_bottom);


std::string FaceProcessor_FaceDetect
    (unsigned char *gray, int width, int height, float time_interval, float face_threshold);


/*
 * 五官定位的接口，特征提取和人脸分析依赖于此函数。
 */
std::string FaceProcessor_FaceDetectAlign
    (unsigned char *gray, int width, int height, float time_interval, float face_threshold);

/*
 * 指定人脸框的五官定位。
 */
std::string FaceProcessor_FaceAlignOneFace
    (unsigned char *gray, int width, int height, 
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval);


/*
 * 用于跟踪的接口。
 */
int FaceProcessor_FaceAddRect
    (int face_left, int face_top, int face_right, int face_bottom, int face_source);
std::string FaceProcessor_FaceTrack
    (unsigned char *gray, unsigned char *BGR888, int width, int height, float time_interval);



std::string FaceProcessor_FaceExtractOneFacial
    (unsigned char *gray, unsigned char *BGR888, int width, int height,
     int face_lefteye_x, int face_lefteye_y,
     int face_righteye_x, int face_righteye_y,
     int face_nose_x, int face_nose_y,
     int face_centermouth_x, int face_centermouth_y,
     float time_interval, bool displaying);


std::string FaceProcessor_FaceExtractOneFace
    (unsigned char *gray, unsigned char *BGR888, int width, int height, 
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval, bool displaying);


std::string FaceProcessor_FaceGetImage
    (int person_idx, int face_idx, unsigned char *facebgr, float feature[], int feature_size, int face_type);




/*
 * 返回值：
 *   成功返回新增人的ID（不小于0），失败返回-1。
 */
int64_t FaceProcessor_addPerson(const char *person_name);

/*
 * 返回值：
 *   返回成功删除的特征个数（不小于0），失败返回-1。
 */
int FaceProcessor_delPerson(int64_t person_id);

int FaceProcessor_delAllPersons(void);

int64_t FaceProcessor_getPersonId(const char *person_name);

int FaceProcessor_getPersonCount(void);

/*
 * 参数说明：
 *   limit          - 取几个。后面的两个数组的大小要不小于这个值。
 *   offset         - 跳过几个。用于支持循环取完所有的人。
 *   id_list        - 输出参数，取到的ID列表。
 *   name_list      - 输出参数，取到的名称列表，和id_list一一对应。
 *   real_count     - 输出参数，实际取到的个数。我们尽量保持和JNI
 *                    接口返回值一致，以前应该是外部先取到总数，
 *                    然后自己计算有效个数；增加这个参数更灵活。
 *
 * 返回值：
 *   true表示获取成功，false表示失败。
 */
bool FaceProcessor_getPersonList
    (int limit, int offset, 
     int64_t id_list[], 
     char name_list[][RECO_FACE_NAME_LEN], 
     int *real_count);

/*
 * 返回值：
 *   成功返回新增人脸的ID（不小于0），失败返回-1。
 */
int64_t FaceProcessor_addFace(int64_t person_id, 
                              unsigned char *img, int img_size, 
                              float *feature, int feature_size);

/*
 * 删除一组指定的人脸。
 */
bool FaceProcessor_delFaces(int64_t person_id, int64_t face_id_list[], int face_num);

int FaceProcessor_getFacesCount(int64_t person_id);

/*
 * 这个接口要求先查询对应的人脸个数，输出参数face_id_list[]大小要
 * 不小于这个查询到的个数。
 */
bool FaceProcessor_getFaceIdList(int64_t person_id, int64_t face_id_list[]);

/*
 * 和前面一个接口类似，不过可以通过limit限制要获取的id的个数，通过
 * offset跳过一些。
 */
bool FaceProcessor_getPersonFaceIdList
    (int64_t person_id, int limit, int offset, int64_t face_id_list[]);

/*
 * 获取注册的人脸图像。注意一个应用需要固定的归一化人脸图像的大小，
 * 这里的img_buf_len要不小于这个归一化的值。
 * 这个接口我们增加了一个buffer大小的参数，有些接口根本就无法外部
 * 输入这个参数，所以不固定大小很难使用。
 */
bool FaceProcessor_getFaceImage(int64_t face_id, unsigned char *img_buf, int img_buf_len);


#if 0
/*
 * TBD
 * 这个接口目前暂时还没有实现。
 * 
 * 如果和JNI保持一致，外部提供内部有两个要求：
 *   1. 要先查询人脸个数，然后确定img_list的大小。这样的话要求接口不要多线程使用。
 *   2. 人脸图像在整个应用中归一化为一个固定的大小，每一个img_list[i]要足够大，
 *      图像大小也不输出。
 *
 * 但这样的话，如果我们的JNI后面和C接口合并，在JNI上做一层简单封装（主要是处理
 * Java参数）就很难搞了，因为在JNI中，img_list每一个对象的大小是在接口中动态申请
 * 而不是外部固定的。
 * 最好的是自己定义一种数据结构；但这样的话，最好所有接口统一；简单的化可以增加
 * 一个返回大小列表的参数，但如果还是外部输入图像大小有些怪异；而如果内部输入则
 * 涉及到要增加一个单独的释放接口。
 * 考虑到V3s的项目上目前暂时也用不到查询所有人脸图像（用于回显），所以先不去实现
 * 这个接口了。
 *
 */
int FaceProcessor_getFaceImgList(int64_t person_id, unsigned char *img_list[]);
#endif



int FaceProcessor_searchOneFeature
    (float feature[], int feature_size, int search_top_k, float search_threshold,
     char name_list[][RECO_FACE_NAME_LEN], int64_t person_id_list[], 
     int64_t face_id_list[], float sim_list[]);

int FaceProcessor_matchOnePerson
  (float feature[], int feature_size, int64_t person_id, float search_threshold, int search_top_k, 
   float sim_list[], int64_t face_id_list[]);




/*
 * 图像比对接口。
 */
float FaceProcessor_FaceMetricMatchImage
    (unsigned char *gray_ref, unsigned char *ref_BGR888, int width_r, int height_r, 
        int face_left_r, int face_top_r, int face_right_r, int face_bottom_r,
     unsigned char *gray_qry, unsigned char *qry_BGR888, int width_q, int height_q, 
        int face_left_q, int face_top_q, int face_right_q, int face_bottom_q,
     unsigned char *facebgr_ref, unsigned char *facebgr_query);




/*
 * 特征比对接口，这个不对外提供，后面考虑用分拆到一个独立的只用于内部的文件中。
 */
float FaceProcessor_FaceMetricMatch
    (float query_feature[], float ref_feature[], int feature_size);







std::string FaceProcessor_FaceAnalyze
    (unsigned char *gray, unsigned char *BGR888, int width, int height,
     float time_interval, float face_threshold);

std::string FaceProcessor_FaceAnalyzeOneFace
    (unsigned char *gray, unsigned char *BGR888, int width, int height,
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval);

std::string FaceProcessor_FaceAnalyzeOneFaceNV21
    (unsigned char *img_nv21, int width, int height,
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval);

std::string FaceProcessor_FaceAnalyzeOneFaceYUV420
    (unsigned char *yuv420pY, unsigned char *yuv420pU, unsigned char *yuv420pV, int width, int height,
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval);





bool FaceProcessor_FaceKeyInit
    (const char *fn_facekey_model, int facekey_profile, 
     const char *jfn_fdir_license, void *context);

std::string FaceProcessor_FaceKeyDetectFit
    (unsigned char *gray, int width, int height, 
     float time_interval, float face_threshold);

std::string FaceProcessor_FaceKeyFitOneFace
    (unsigned char *gray, int width, int height, 
     int face_left, int face_top, int face_right, int face_bottom, 
     float time_interval);

bool FaceProcessor_FaceKeyFinalize(void);






// 这两个应该是初始化一个即可？这个是原来就有的，只是我们没用过。
bool FaceProcessor_FaceParseInit
    (bool bIsInfraRed, 
     const char *fn_face_front, 
     const char *fn_eye_parsing, 
     const char *fn_mouth_parsing, 
     int eye_profile, int mouth_profile, 
     const char *fn_fdir_license, void *context);
// 目前防疲劳算法团队给出的说明是用这个初始化。
bool FaceProcessor_FaceParseInitEx
    (bool bIsInfraRed, 
     const char *fn_face_front, 
     const char *fn_face_keypoint, 
     const char *fn_eye_parsing, 
     const char *fn_fdir_license, void *context);

// 注意第二个参数是int数组的大小，不是字节长度。
bool FaceProcessor_FaceParseParam(int ref_buffer[], int ref_length);

std::string FaceProcessor_FaceDetectParse
    (unsigned char *gray, int width, int height, float time_interval, 
     float face_threshold, float parsing_threshold, 
     unsigned char *eye_label, unsigned char *mouth_label, bool bOutputCoords);

std::string FaceProcessor_FaceParseOneFace
    (unsigned char *gray, int width, int height, float time_interval, 
     int face_left, int face_top, int face_right, int face_bottom, 
     float parsing_threshold, unsigned char *eye_label, unsigned char *mouth_label, bool bOutputCoords);

bool FaceProcessor_FaceParseClear(void);

bool FaceProcessor_FaceParseFinalize(void);


std::string FaceProcessor_FaceQualityFace
    (unsigned char *gray8, int width, int height, 
     int face_left, int face_top, int face_right, int face_bottom);





int FaceProcessor_Version(void);

};


/*
 * 手势检测接口。
 */
extern "C"
{
bool GestureProcessor_GestureInit
    (bool bHandTrackerFast, 
     const char *fn_gest_open, const char *fn_gest_close, 
     const char *fn_model_ex, 
     int method, 
     const char *fn_fdir_license, 
     void *context);


bool GestureProcessor_GestureAuxiDetector(const char *fn_body, const char *fn_face_front);

std::string GestureProcessor_GestureGetAuxiResults(int width, int height);


float GestureProcessor_GestureGetParam(const char *param_name);


void GestureProcessor_GestureSetParam(const char *param_name, float param_value);


bool GestureProcessor_GestureInitPosition(float relative_x, float relative_y);


bool GestureProcessor_GestureClear(float idle_time_second);


bool GestureProcessor_GestureFinalize(void);


bool GestureProcessor_GestureAddROI(int roi_left, int roi_top, int roi_right, int roi_bottom);


std::string GestureProcessor_GestureDetect
    (unsigned char *gray8, int width, int height, float hand_threshold);

std::string GestureProcessor_GestureTrack
    (unsigned char *gray8, int width, int height, 
     float time_interval, float moving_vel_x, float moving_vel_y, float moving_acc);

std::string GestureProcessor_GestureTrackFast
    (unsigned char *gray8, int width, int height, 
     float time_interval, float moving_vel_x, float moving_vel_y, float moving_acc);


std::string GestureProcessor_GestureTrackFastEx
    (unsigned char *gray8, int width, int height, 
     float time_interval, float moving_vel_x, float moving_vel_y, float moving_acc);


std::string GestureProcessor_GestureTrackFastROI
    (unsigned char *gray8, int width, int height, float time_interval);

int GestureProcessor_Version(void);
};



/*
 * 头肩检测接口。
 */
extern "C"
{
bool BodyFaceProcessor_BodyFaceInit
    (const char *fn_body, 
     const char *fn_face_front, 
     const char *fn_face_left, 
     const char *fn_face_right, 
     const char *fn_face_rollp, 
     const char *fn_face_rolln, 
     const char *fn_fdir_license, 
     void *context);


bool BodyFaceProcessor_BodyAddROI(int roi_left, int roi_top, int roi_right, int roi_bottom);



std::string BodyFaceProcessor_BodyDetect
    (unsigned char *gray8, int width, int height, 
     float time_interval, float body_threshold);



std::string BodyFaceProcessor_BodyFaceDetect
    (unsigned char *gray8, int width, int height, 
     float time_interval, float body_threshold, float face_threshold);


bool BodyFaceProcessor_BodyFaceFinalize(void);


int BodyFaceProcessor_Version(void);

};


extern "C"
{
/*
 * 这个比算法团队的接口多了两个参数：
 *   int normalized_width, int normalized_height
 * 因为Motion Detection不需要太多的图像细节，所以缩小一下图像效果更好；
 * 不过算法团队提供的实现中并没有做这一部分处理，所以我们进行一下封装，
 * 那么就需要外部指定应该把图像缩小到多大，这样更灵活一些。
 * 这两个值有任何一个小于或等于0，则我们这个接口不再进行归一化，直接
 * 在原始图像上处理，这个是为了支持有可能原始图像本身就比较小的应用场景
 * 下应用可以关闭图像归一化处理面减少CPU消耗。
 */
std::string MotionProcessor_MotionDetect
    (unsigned char *gray8, int width, int height, 
     float time_interval, float motion_threshold, 
     int normalized_width, int normalized_height);

bool MotionProcessor_MotionDetectFinalize(void);
};



/*
 * 头肩检测接口。这里的接口自动调用Motion Detection来确定运动区域，
 * 然后在运动区域上再做BodyFace的检测。当视频中画面中相对静止部分
 * 比较多时，这种方式可以提高检测的速度。
 *
 * 这几个接口主要是为了美的项目中Gesture、BodyFace集成做一个初步
 * 验证，相对而言应用场景不是很多。
 *
 * 参数和前面的头肩检测完全相同。
 */
extern "C"
{
bool BodyFaceVideoProcessor_BodyFaceVideoInit
    (const char *fn_body, 
     const char *fn_face_front, 
     const char *fn_face_left, 
     const char *fn_face_right, 
     const char *fn_face_rollp, 
     const char *fn_face_rolln, 
     const char *fn_fdir_license, 
     void *context);


std::string BodyFaceVideoProcessor_BodyFaceVideoDetect
    (unsigned char *gray8, int width, int height, float time_interval, 
     float body_threshold, float face_threshold);


// 释放相关的跟踪资源。
bool BodyFaceVideoProcessor_BodyFaceVideoClear(void);


bool BodyFaceVideoProcessor_BodyFaceVideoFinalize(void);


int BodyFaceVideoProcessor_Version(void);
};



/*
 * 集成手势和头肩人脸检测的接口，目前只用于美的（智能空调）项目。集成之后
 * 原来算法之间有共性的功能集中到一起，可以减省内存使用，另外因为公共部分
 * 只计算一次，也提高了性能。
 *
 * 带***Video***的接口目前都是自动带了Motion Detection的，这里的接口也不
 * 例外，会利用Motion Detection支获取运动区域，然后再在这个区域内检测。
 * 这样的好处是视频画面相对静止时性能会比较高，而且使用简单，不用再支单独
 * 处理Motion Detection；不过反过来讲这里的接口就不适合用于画面变化比较
 * 大的检测中。
 *
 *
 *
 * [2015-09-18]
 * 增加一个参数fn_para_ini，这个是一个.ini的配置文件，可以传NULL，表示不
 * 提供这个文件，内部使用缺省配置。
 *
 */
extern "C"
{
bool GestureBodyFaceVideoProcessor_GestureBodyFaceVideoInit
    (const char* fn_gest_open, 
     const char* fn_gest_close,
     const char *fn_body, 
     const char *fn_face_front, 
     const char *fn_face_left, 
     const char *fn_face_right, 
     const char *fn_body_cnn, 
     const char *fn_face_cnn, 
     const char *fn_gest_open_cnn, 
     const char *fn_gest_track_cnn, 
     const char *fn_para_ini, 
     const char *fn_fdir_license, 
     void *context);



/*
 * 这两个接口和com.reconova.processor.NativeGestureProcessor的两个接口功能
 * 一致，只是增加了参数。
 *
 * 相同的参数：
 *   param_name=="-hand_min_size" in the valid range (32,320) (-1 indicates the default value)
 *   param_name=="-hand_max_size" in the valid range (32,1280) (-1 indicates the default value)
 *   param_name=="-hand_min_prob" in the valid range (0,1) (-1 indicates the default value)
 *   param_name=="-hand_vel_size" in the valid range (32, 160) (-1 indicates the default value)
 *   param_name=="-hand_vel_coef" in the valid range (0.5, 2) (-1 indicates the default value)
 *   param_name=="-hand_num_thread" in the valid range (1,2,3,4)
 *   param_name=="-hand_mirror_face" in the valid range (0 or 1), whether mirror the face position
 * 针对三和一功能的新参数：
 *   param_name== "-hand_face_init" in the valid range (0,1)
 *
 * 查询接口成功返回参数值，失败返回-2.0。
 * 设置接口成功返回true，失败返回false。
 */
float GestureBodyFaceVideoProcessor_GestureGetParam(const char *param_name);
bool GestureBodyFaceVideoProcessor_GestureSetParam(const char *param_name, float param_value);


std::string GestureBodyFaceVideoProcessor_GestureBodyFaceVideoDetect
    (unsigned char *gray8, int width, int height, float time_interval, 
     float body_threshold, float face_threshold, 
     float hand_threshold, float motion_threshold);


// state==0: Gesture/Body/Face detection
// state==1: Gesture tracking
// state==2: Motion detection/Body detection
// state==3: Body/Face detection
// state==4: Motion/Body/Face detection
// state==5: Motion detection
bool GestureBodyFaceVideoProcessor_GestureBodyFaceSwitchState(int state);


/*
 * 设定一个黑名单区域，目前只针对手势，和黑名单矩形重叠面积达到50%的手势
 * 将不会进入识别跟踪状态。
 *
 * 参数rect_type目前为保留函数，使用时传入-1。
 * 前面4个参数指定一个区域，全部设置为-1时清除前面设置的黑名单区域。
 *
 */
bool GestureBodyFaceVideoProcessor_GestureBodyFaceAddBlackRect
    (int rect_left, int rect_top, int rect_right, int rect_bottom, int rect_type);


bool GestureBodyFaceVideoProcessor_GestureBodyFaceSetDetThresholds
    (float head_threshold, float face_threshold);


// 释放相关的跟踪资源。
bool GestureBodyFaceVideoProcessor_GestureBodyFaceVideoClear(void);


bool GestureBodyFaceVideoProcessor_GestureBodyFaceVideoFinalize(void);


int GestureBodyFaceVideoProcessor_Version(void);
};


#endif  /* __C_FACE_PROC_H__ */

