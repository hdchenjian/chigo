LOCAL_PATH:= $(call my-dir)/..

include $(CLEAR_VARS)  
LOCAL_MODULE := V4L2ImageCap
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/V4L2ImageCap/libs/armeabi-v7a/libV4L2ImageCap.so         
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libFaceProcC_NV906_Smartac_AC01
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/FaceRecov2/libs/libFaceProcC_NV906_Smartac_AC01.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := webrtc_simple
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/webrtc/libs/armeabi-v7a/libwebrtc_simple.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := jingle_peerconnection
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/webrtc/libs/armeabi-v7a/libjingle_peerconnection.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := boringssl
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/webrtc/libs/armeabi-v7a/libboringssl.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := avdevice
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libavdevice.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := avformat
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libavformat.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := avfilter
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libavfilter.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := avcodec
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libavcodec.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := swresample
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libswresample.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := avutil
LOCAL_SRC_FILES := $(LOCAL_PATH)/3rdParty/ffmpeg/lib/libavutil.a
include $(PREBUILT_STATIC_LIBRARY)


common_src_files := \
	main.cpp \
	md5_proc.cpp \
	cJSON.c \
	p2pThread.cpp \
	I2CTag.cpp \
	zThread.cpp \
	zUtil.cpp \
	zVideoThread.cpp \
	zAlgorithmManager.cpp \
	config_mgr.cpp \
	zEakonThread.cpp \
	tinystr.cpp \
	tinyxml.cpp \
	tinyxmlerror.cpp \
	tinyxmlparser.cpp \
	zAlgorithmDetectThread.cpp \
	zWifiStateThread.cpp \
	zWifiConnect.cpp \
	zLog.cpp \
	zSingletonThread.cpp \
	zRokugaThread.cpp \
	zRokuUpThread.cpp \
	zFile.cpp \
	zDataBuf.cpp \
	zTimer.cpp \
	zImage.cpp \
	device_test.cpp \
	devicePowerTest.cpp \
        process_start.cpp \
        file_date.cpp   \
        ZoneRecorder.cpp

common_c_includes := \
	$(KERNEL_HEADERS) \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/3rdParty/ffmpeg \
	$(LOCAL_PATH)/3rdParty/libjpeg-turbo/include

common_shared_libraries := \
	libV4L2ImageCap \
	libFaceProcC_NV906_Smartac_AC01 \
        libboringssl

common_static_libraries := \
        libwebrtc_simple \
        libjingle_peerconnection \
	libavdevice \
	libavformat \
	libavfilter \
	libavcodec \
	libswresample \
	libavutil

include $(CLEAR_VARS)

LOCAL_MODULE := facedis

LOCAL_SRC_FILES := $(common_src_files)

LOCAL_C_INCLUDES := $(common_c_includes)

LOCAL_SHARED_LIBRARIES := $(common_shared_libraries)

LOCAL_STATIC_LIBRARIES := $(common_static_libraries)

LOCAL_CPPFLAGS := -std=c++11 -pthread -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS -DPOSIX -Wall -Werror -Wno-literal-suffix
#LOCAL_CFLAGS += -ffunction-sections -fdata-sections  -fvisibility=hidden
LOCAL_LDLIBS += -llog -lz
#LOCAL_LDFLAGS += -Wl,--gc-sections

LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

include $(BUILD_EXECUTABLE)

$(call import-module,android/native_app_glue)
$(call import-module,cxx-stl/gnu-libstdc++)
