#ifndef __ZWIFISTATETHREAD_H__
#define __ZWIFISTATETHREAD_H__
#include "zSingletonThread.h"

class zEakonThread;
class zWifiStateThread : public zSingletonThread
{
public:
    static zWifiStateThread* getInstance();
    static void destroyInstance();

    void init(void* pParent);

    bool proc();

    unsigned char getWifiState();
    void eventNotify(void* pNoti);
    bool bSync();
    std::int64_t getTimeDiff();
    void reset_wifi_state();

protected:
    zWifiStateThread();
    ~zWifiStateThread();

private:
    bool wifi_connected();

private:
    bool bNeed_;
    unsigned char m_wifiState;
    int m_beatGap;
    void* notify_;                      // zAlgorithmManager here
    static zWifiStateThread* instance_;
    volatile std::int64_t timeDiff_;
    zEakonThread* m_pEakonThread;
    std::mutex ctrlMutex_;
	bool wifi_connected_flag = true; // true: wifi_connected执行成功
};

#endif