#ifndef __I2CTAG_H__
#define __I2CTAG_H__

#define UART_PROTOCOL_SIZE (29)         // THE MAX

#define MAX_HEAD_CNT (19)
#define SYUKAN_CEIL (6)

#define TOEAKON_BUF_SIZE (30)
#define FROMEAKON_BUF_SIZE (178)
#define RETURNINFO_SIZE (19)

#ifdef _WINDOWS
#define ZDEVICE void*
#else 
#define ZDEVICE int
#endif

namespace zI2C
{
#pragma pack(1)
    typedef struct tagToHeader
    {
        unsigned char chProHead;        //0xAA or 0xAB when chHead == 0xAB
    }toHeader, *pToHeader;

    typedef struct tagToInfo
    {
        toHeader header;
        unsigned char Buf[TOEAKON_BUF_SIZE - sizeof(toHeader)];
    }ToInfo, *pToInfo;

    typedef struct tagFromInfo
    {
        unsigned char unknown[44];
        unsigned char connectState;
        unsigned char eakon_normal[RETURNINFO_SIZE];
        unsigned char kosyou[RETURNINFO_SIZE];
        unsigned char samoninfo[RETURNINFO_SIZE];
        unsigned char ikooziinfo[RETURNINFO_SIZE];
        unsigned char nikooziinfo[RETURNINFO_SIZE];
        unsigned char rimokonn[RETURNINFO_SIZE];
        unsigned char backupinfo[RETURNINFO_SIZE];
    }fromInfo, *pFromInfo;

    union toAirConditioner
    {
        unsigned char buf[TOEAKON_BUF_SIZE];
        ToInfo tagInfo;
    };

    union fromAirConditioner
    {
        unsigned char buf[FROMEAKON_BUF_SIZE];
        fromInfo tagInfo;
    };
#pragma pack()
    void InitCMDInfo(ToInfo& cmd) ;
    void InitSendinfo(ToInfo& send) ;
}

#endif