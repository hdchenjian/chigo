
#include "tinystr.h"
#include "tinyxml.h"

#include "zLog.h"
#include "process_start.hpp"

ProcessStartCount* ProcessStartCount::instance_ = NULL;
const char * ProcessStartCount::persistence_store_ = "/data/process_start.xml";

bool ProcessStartCount::file_exist(const char * full_filename)
{
    FILE* pFile = fopen(full_filename, "r");
    if (NULL == pFile)
    {
        LOG_E("open file failed. filename=%s\n", full_filename);
        return false;
    }

    fclose(pFile);
    return true;
}

bool ProcessStartCount::create_xml()
{
    TiXmlDocument xmlDoc;

    TiXmlElement elemt("process");
    elemt.SetAttribute("times", count_);

    xmlDoc.InsertEndChild(elemt);
    return xmlDoc.SaveFile(persistence_store_);
}

bool ProcessStartCount::parse_xml()
{
    if (!file_exist(persistence_store_))
    {
        return create_xml();
    }

    TiXmlDocument xmlDoc(persistence_store_);
    if (!xmlDoc.LoadFile())
    {
        return create_xml();
    }

    TiXmlElement *elemt = xmlDoc.FirstChildElement("process");
    if (NULL == elemt)
    {
        return create_xml();
    }

    // read, modify, write.
    count_ = (long)atoi(elemt->Attribute("times")) + 1;
    elemt->SetAttribute("times", count_);

    xmlDoc.SaveFile();
    return true;
}

