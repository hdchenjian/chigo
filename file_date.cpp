#include <sstream>
#include <vector>
#include <string>
#include <chrono>
#include <time.h>

#include "p2pThread.h"
#include "zUtil.h"
#include "zLog.h"
#include "file_date.h"

namespace RW 
{

file_date::file_date(const std::string &date)
    : valid_( false)
{
    std::vector<std::string> opts;
    if (2 == zP2PThread::split_with_delimiter(date, '_', opts).size())
    {
        std::vector<std::string> ymd; 
        if (3 == zP2PThread::split_with_delimiter(opts[0], '-', ymd).size())
        {
            // 解析: 年月日
            year_ = zBaseUtil::to_int(ymd[0]);
            month_ = zBaseUtil::to_int(ymd[1]);
            day_ = zBaseUtil::to_int(ymd[2]);

            std::vector<std::string> hms;
            if (3 == zP2PThread::split_with_delimiter(opts[1], '-', hms).size())
            {
                // 解析: 时分秒
                hour_ = zBaseUtil::to_int(hms[0]);
                minute_ = zBaseUtil::to_int(hms[1]);
                second_ = zBaseUtil::to_int(hms[2]);

                valid_ = true;
            }
        }
    }
}

int file_date::compare(const file_date & date) const
{
    #define GT  1   // 大于
    #define LT -1   // 小于
    #define EQ  0   // 等于

    if      (year_ > date.year_)        {   return GT;  }
    else if (year_ < date.year_)        {   return LT;  }
    
    else if (month_ > date.month_)      {   return GT;  }
    else if (month_ < date.month_)      {   return LT;  }
    
    else if (day_ > date.day_)          {   return GT;  }
    else if (day_ < date.day_)          {   return LT;  }

    else if (hour_ > date.hour_)        {   return GT;  }
    else if (hour_ < date.hour_)        {   return LT;  }

    else if (minute_ > date.minute_)    {   return GT;  }
    else if (minute_ < date.minute_)    {   return LT;  }

    else if (second_ > date.second_)    {   return GT;  }
    else if (second_ < date.second_)    {   return LT;  }

    return EQ;
}

std::string file_date::get_date_string() const
{
    std::ostringstream stm;
    stm << year_ << "-" << month_ << "-" << day_ << "_"\
        << hour_ << "-" << minute_ << "-" << second_;
    return stm.str();
}

std::time_t file_date::to_time_t() const
{
    std::tm timeinfo = std::tm(); // timeinfo 必须初始化。
    (void)mk_timeinfo(timeinfo, year_, month_, day_, hour_, minute_, second_);
    return std::mktime (&timeinfo);
}

std::tm & file_date::mk_timeinfo( std::tm &timeinfo, 
    int year, int month, int day, int hour, int minute, int second)
{
    timeinfo.tm_year = year - 1900; // years since 1900
    timeinfo.tm_mon = month - 1;    // months since January, 0-11
    timeinfo.tm_mday = day;         // day of the month, 1-31
    timeinfo.tm_hour = hour;        // hours since midnight, 0-23
    timeinfo.tm_min = minute;       // minutes after the hour, 0-59
    timeinfo.tm_sec = second;       // seconds after the minute, 0-60*

    return timeinfo;
}

}

