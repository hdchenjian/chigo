#include "zTimer.h"
#include "zLog.h"

namespace zBaseUtil
{
    zTimer::zTimer()
    {

    }

    zTimer::~zTimer()
    {

    }

    /*****************************************************************************
     * 函 数 名  : zBaseUtil.zTimer.setTimer
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年11月5日
     * 函数功能  : 设置定时器
     * 输入参数  : int nID         ID值
                   int nType       为1，0，-1，为-1，一直执行，为1执行cnt次后退出，为0，执行一次退出
                   int nCnt        执行次数     : nType==1时, 该参数才有效
                   int nGap        定时时长，执行间隔
                   int wait        执行settimeer后，延迟多久才计时
                   timerFun func   起始函数
                   void* param     起始函数参数
                   onEnd endFunc   结束函数     : nType==0时,且起始函数返回值为真，就执行结束函数
                                                  nType==1时,执行次数到了，就执行结束函数
                                                  nType==-1, 从不执行结束函数
                   void* endParam  结束函数参数
     * 输出参数  : 无
     * 返 回 值  : int
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int zTimer::setTimer(int nID, int nType, int nCnt, int nGap, int wait
        , timerFun func, void* param /* = NULL */, onEnd endFunc /* = NULL */, void* endParam /* = NULL */)
    {
        // find
        std::list<zTimerEvent>::iterator it = timerList_.begin();
        while (it != timerList_.end())
        {
            if (it->id_ == nID) return -1;              // exist
            ++it;
        }
        zTimerEvent event = { nID, nType, nCnt, nGap, wait, 0, func, param, endFunc, endParam};
        LOG_D("settimer id == %d, type == %d, cnt == %d, gap == %d, wait == %d elapse == %d \n",
            event.id_, event.type_, event.cnt_, event.gap_, event.delay_, event.elapse_);
        timerList_.push_back(event);
        return 0;
    }

    int zTimer::removeTimer(int nID)
    {
        std::list<zTimerEvent>::iterator it = timerList_.begin();
        while (it != timerList_.end())
        {
            if (it->id_ == nID)
            {
                it = timerList_.erase(it);
                LOG_D(" remove timer %d \n", nID);
                break;
            }
            else ++it;
        }
        //if (it != timerList_.end()) timerList_.erase(it);
        return 0;
    }

    int zTimer::clearTimer()
    {
        timerList_.clear();
        return 0;
    }

    /*****************************************************************************
     * 函 数 名  : zBaseUtil.zTimer.elapse
     * 负 责 人  : 刘春龙
     * 创建日期  : 2015年11月5日
     * 函数功能  : 如果定时时间到，执行定时器函数，靠在线程的proc中执行来定时驱动。
     * 输入参数  : int nElapse  逝去的时长
     * 输出参数  : 无
     * 返 回 值  : int
     * 调用关系  : 
     * 其    它  : 

    *****************************************************************************/
    int zTimer::elapse(int nElapse)
    {
        int nCnt = 0;
        std::list<zTimerEvent>::iterator it = timerList_.begin();
        while (it != timerList_.end())
        {
            it->elapse_ += nElapse;
            bool bReturn = false;
            if (it->elapse_ > (it->delay_ + it->gap_) && NULL != it->EventFunc_)
            {
                bReturn = (it->EventFunc_)(it->param_);
                it->elapse_ = it->elapse_ - it->gap_;
                if (it->type_ > 0)
                {
                    --it->cnt_;
                    if (it->cnt_ <= 0)
                    {
                        if (NULL != it->endFunc_) (it->endFunc_)(it->param_);
                        it = timerList_.erase(it); // need remove
                    }
                    else ++it;
                }
                else if (0 == it->type_ && bReturn)
                {
                    if (NULL != it->endFunc_) (it->endFunc_)(it->param_);
                    it = timerList_.erase(it);
                }
                else /*if (-1 == it->type_)*/
                {
                    ++it;
                }
                ++nCnt;
            }
            else /*if (-1 == it->type_)*/
            {
                ++it;
            }
        }
        return nCnt;
    }
}