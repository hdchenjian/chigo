#include "zSingletonThread.h"
#include "zLog.h"
#include "zUtil.h"

zSingletonThread::zSingletonThread()
    : busy_(false)
    , nGap_(0)
    , m_pThread(NULL)
{
}

zSingletonThread::~zSingletonThread()
{

}

void zSingletonThread::begin()
{
    std::unique_lock<std::mutex> lock(mutex_);
    busy_ = true;
}

void zSingletonThread::end()
{
    std::unique_lock<std::mutex> lock(mutex_);
    busy_ = false;
}

void zSingletonThread::setThreadGap(std::uint64_t nGap)
{
    std::unique_lock<std::mutex> lock(mutex_);
    nGap_ = nGap;
}
std::uint64_t zSingletonThread::threadGap()
{
    std::unique_lock<std::mutex> lock(mutex_);
    return nGap_;
}

bool zSingletonThread::busy()
{
    std::unique_lock<std::mutex> lock(mutex_);
    return busy_; 
}

bool zSingletonThread::proc()
{
    if (!busy()) 
    {
        return false;
    }
    return true;
}

bool zSingletonThread::ThreadPro(void* param)
{
    LOG_D("zSingletonThread::ThreadPro param=%0x thread id == %u\n",
        param, zBaseUtil::getThreadId());

    zSingletonThread* pThread = (zSingletonThread*)param;
    if (pThread->busy()) return false;
    pThread->begin();
    LOG_D("zSingletonThread::ThreadPro %llu \n", pThread->threadGap());
    while (pThread->proc())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(pThread->threadGap()));
    }

    pThread->end();
    return true;
}

bool zSingletonThread::startThread( )
{
    m_pThread = new std::thread(ThreadPro, this);
    if (NULL == m_pThread)
    {
        LOG_E("zSingletonThread::Start() failed. \n");
    }
    return true;
}

int zSingletonThread::onMsg(const zBaseUtil::zMsg& msg)
{
    return 0;
}