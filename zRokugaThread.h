#ifndef __ZROKUGA_THREAD__
#define __ZROKUGA_THREAD__

#include <list>
#include <csignal>

#ifdef __cplusplus
extern "C" {
#endif
#include <3rdParty/ffmpeg/libavutil/avassert.h>
#include <3rdParty/ffmpeg/libavutil/channel_layout.h>
#include <3rdParty/ffmpeg/libavutil/opt.h>
#include <3rdParty/ffmpeg/libavutil/mathematics.h>
#include <3rdParty/ffmpeg/libavutil/timestamp.h>
#include <3rdParty/ffmpeg/libavformat/avformat.h>
#include <3rdParty/ffmpeg/libswscale/swscale.h>
#include <3rdParty/ffmpeg/libswresample/swresample.h>
#include <3rdParty/ffmpeg/libavutil/time.h>
#ifdef __cplusplus
}
#endif

#include "3rdParty/V4L2ImageCap/jni/cap_types.h"
#include "zThread.h"
#include "zUtil.h"
#include "zTimer.h"
#include "zAlgorithmManager.h"


// 录像线程
class zRokugaThread : public zThread
{
public:
    enum rokuCtrlCode
    {
        UNKNOWN_ROKU = 0,               // 初始化状态 未知状态
        STOPED_ROKU = 1,                // 已结束录制
        ROKU = 2,                       // 普通录制
        ROKU_TANINN = 4,                // 录制陌生人 这个状态可叠加到录制上、优先处理陌生人录制
        END_ROKU_TANIN = 8,             // 结束录制陌生人、可能来自其他地方强制结束的消息！
        END_ROKU = 16,                  // 结束普通录制
    };
    
    enum rokuStoreType
    {
        st_default =0 ,
        st_loc,
        st_usb,
        st_loc_normal,
        st_loc_alarm,
        st_usb_normal,
        st_usb_alarm,
        //st_image ,
        st_loc_image ,
        st_usb_image ,
        st_loc_gesture_image ,
        st_usb_gesture_image ,
    };

    enum queryType
    {
        query_all = 0,
        query_alarm,
        query_normal,
        query_image,
        query_loc_alarm,
        query_usb_alarm,
        query_loc_normal,
        query_usb_normal,
    };

    // 文件名及属性
    typedef struct file_property_s
    {
        std::string filename;
        rokuStoreType storage_type;

        file_property_s()
            : storage_type(rokuStoreType::st_default)
        {
        }
    }file_property_t;

    typedef enum 
    {
        VIDEO_ALARM     = 0,
        VIDEO_NORMAL    = 1,
        IMAGE_NORMAL    = 2,
        IMAGE_DEBUG     = 3,
        FILE_TYPE_BUTT  = 4
    }FILE_TYPE_E;

    typedef struct file_info_s
    {
        FILE_TYPE_E file_type = FILE_TYPE_BUTT;
        rokuStoreType store_type = st_default;
        std::string file_name;

        file_info_s() {
        }
    }file_info_t;

public:
    zRokugaThread(zAlgManager* palg, int nWidth, int nHeight);
    ~zRokugaThread();

    //int Ctrl(rokuCtrlCode code);
    int onMsg(const zBaseUtil::zMsg& msg);          // 带参数的用 OnMsg 其他的用还沿用之前的onEvent 以后合！
    int onEvent(int nEvent);
    void proc();

    int rokuState();
    //////////////////////////////////////////////////////////////////////////
    bool findFile(const std::string& fileName, std::string& fullPath);
    bool removeFile(const std::string& fileName);
    bool downloadFile(const zBaseUtil::DownloadParam& param);

    void saveImage();
    void findAndTryUSB();   
    //////////////////////////////////////////////////////////////////////////

    /* add by 刘春龙, 2015-10-31, 原因: 手势图片 */
    void save_gesture_image_thread(const S_VCBuf& buf, int gesture_type);
    int save_gesture_image(const S_VCBuf& buf, int gesture_type);
    rokuStoreType stType();

    /* add by 刘春龙, 2015-11-10, 原因: 分辨率切换 */
    void set_record_resolution(int width, int height);
    void storage_clear();

private:
    typedef struct OutputStream {
    public:
            void reset()
            {
                st = NULL;
                next_pts = 0;
                samples_count = 0;
                frame = NULL;
                tmp_frame = NULL;
                t = tincr = tincr2 = 0;
                swr_ctx = NULL;
                return;
            }

            OutputStream()
            {
                reset();
            }

    public: 
        AVStream *st;

        /* pts of the next frame that will be generated */
        int64_t next_pts;
        int samples_count;

        AVFrame *frame;
        AVFrame *tmp_frame;

        float t, tincr, tincr2;

        //struct SwsContext *sws_ctx;
        struct SwrContext *swr_ctx;
    } OutputStream;
    
    void dirChg(rokuStoreType nType);
    rokuStoreType getDir2Clear();
    bool checkDisk(const std::string& dir); 
    std::string stDir();
    std::string getDir(rokuStoreType nType);
    std::string setDir(const std::string& dir, rokuStoreType nType);
    //int getLatestId(std::list<std::string>& list);
    int updateImageList(std::string rootDir, std::list<std::string>& list, int& img_id);
    int updateVideolist(std::string rootDir, std::list<std::string>& list);
    int updateFilename(std::int64_t nGap, rokuStoreType type);                                  // 收到zWifiConnect的时间同步消息ZM_SYNCTIME之后，更新后缀为F的文件名
    int video2Jsonstr(std::string& str, int nTop = 10, int nSkip = 1);                              // 将m_listVedio 尾部（最新）的N个文件名存为 json 字符串
    //////////////////////////////////////////////////////////////////////////
    std::list<std::string> QueryVideoListByTime(time_t from, time_t to);

    void fill_file_list(
        std::list<std::string>& filename_list, FILE_TYPE_E file_type, 
        rokuStoreType store_type, std::list<file_info_t>& file_info_list);
    void get_list(const std::string &type, std::list<file_info_t> &temp_list);
    void reverse_query(
        std::reverse_iterator<std::list<file_info_t>::iterator> r_pos,
        std::reverse_iterator<std::list<file_info_t>::iterator> r_end,
        int max_count, 
        std::list<file_info_t>& file_info_list);
    void QueryVideoList(const zBaseUtil::QueryParam& param, std::list<file_info_t>& file_info_list);
    void QueryVideoListBaseOnTime(
        const zBaseUtil::QueryParamBaseOnTime& param, std::list<file_info_t>& file_info_list);
    void videoList2Jsonstr(const std::list<file_info_t>& list, std::string& str);

    //bool removeVideo(std::string str);
    //bool removeListVideo(std::list<std::string>& list,const std::string& str);
    bool removeFile_(const std::string& fileName);

    //////////////////////////////////////////////////////////////////////////
    void  save_curr_record_file_info();
    int initRokuga( const std::string& filename);               // 初始化录制需要的环境
    void uninitRokuga();                                        // 反初始化录制需要的环境
    bool roku(std::int64_t nElapse);                            // 录制、 nElapse proc 的 时间间隔 用这个做timer 判断
    bool rokuTaninn(std::int64_t nElapse);                      // 录制陌生人
    bool endRoku();                                             // 结束普通录制
    void onRokutaninnEnd();
    bool endTaninnRoku();                                       // 结束陌生人录制
    bool onDiskFull(rokuStoreType dirType);                     // 磁盘满之后的删除文件策略
    bool rokuNewfile();                                         // 关闭当前录制环境 新建一个录制环境（包括文件名）
    void addRokuFile(const std::string& filename);
    void update_all_filename();
    bool doAction(std::int64_t nElapse);                        // proc 处理相关录制状态的东西
    //
    void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt);
    int write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt);
    void add_stream(OutputStream *ost, AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id);
    AVFrame *alloc_audio_frame(enum AVSampleFormat sample_fmt, uint64_t channel_layout, int sample_rate, int nb_samples);
    void open_audio(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg);
    AVFrame *get_audio_frame(OutputStream *ost);
    int write_audio_frame(AVFormatContext *oc, OutputStream *ost);
    AVFrame *alloc_picture(enum AVPixelFormat pix_fmt, int width, int height);
    void open_video(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg);
    S_H264EncBuf *get_video_frame(OutputStream *ost);
    
    int write_muxer_header(const uint8_t *extradata, int extradata_size);
    int write_video_frame(AVFormatContext *oc, OutputStream *ost);
    int write_video_frame_impl(
        AVFormatContext *oc, OutputStream *ost, S_H264EncBuf &h264frame, std::int64_t TimeStamp);
    void close_stream(AVFormatContext *oc, OutputStream *ost);

    void joint_files(std::list<file_property_t>& dst_files, 
        std::list<std::string>& src_files, rokuStoreType storage_type );
    void sort_files(std::list<file_property_t>& files);
    void rename_file_ID(file_property_t property, int id);
    void fill_files(std::list<file_property_t>& files, FILE_TYPE_E file_type);
    void rename_files(std::list<file_property_t>& files, int& next_id);
    void update_all_file_ID();

    int get_alarm_image_info(
        zRokugaThread::rokuStoreType &roku_type,
        std::string& root_dir,
        std::list<std::string>** ppList);
    int get_gesture_image_info(
        zRokugaThread::rokuStoreType &roku_type,
        std::string& root_dir,
        std::list<std::string>** ppList);
    std::list<std::string>* getVideoList(zRokugaThread::rokuStoreType curType);
    std::list<std::string>* getImageList(zRokugaThread::rokuStoreType curType);
    std::list<std::string>* getList(zRokugaThread::rokuStoreType curType);

    void joint_files_2(
        std::list<file_property_t>& dst_files,
        std::list<std::string>& src_files, 
        enum rokuStoreType storage_type, int max_size);

    void video_storage_clear();
    void image_storage_clear(
        std::list<std::string>* image_list, std::string& root_dir,
        int max_image_count, int image_clear_size);

    void remove_null_files(std::string rootDir, std::list<std::string>& list);

private:
    time_t str2time(std::string str);
    template<class Iter>
    Iter lower_bound(Iter first, Iter last, time_t val)
    {
        while (first != last)
        {
            Iter mid = std::next(first, std::distance(first, last) / 2);
            time_t fileTime = str2time(*mid);
            if (val > fileTime) first = ++mid;
            else last = mid;
        }

        return first;
    }

    template<class Iter>
    Iter upper_bound(Iter first, Iter last, time_t val)
    {
        while (first != last)
        {
            Iter mid = std::next(first, std::distance(first, last) / 2);
            time_t fileTime = str2time(*mid);
            if (val >= fileTime) first = ++mid;
            else last = mid;
        }
        return first;
    }

public:
    sig_atomic_t gSaveImage;
private:
    zAlgManager* m_pManager;            // 从这里定时去读最新EncFrame

    bool m_bFirstFrame;
    bool m_bLocst;
    bool m_bNeedRename;                 // 时间已更新 需要rename一次
    //bool m_bSaveImage;
    int m_latestVideoid;                // 最近文件id（已存在的最大），根据枚举录制文件目录文件 获得id最大的文件
    int m_latestImageid;
    int m_nWidth;
    int m_nHeight;
    int m_ctrState;                     // 录制状态
    std::int64_t TimeStampBase_;//to do
    std::int64_t pts_;
    rokuStoreType m_curRokuType;        // 
    //FILE* m_pFile;                    // 无用

    std::string m_strFileName;          // 当前录像存储文件名
    std::string m_strVideoSuffix;           // 录像文件后缀，可通过配置文件配置， 在枚举文件的时候就是靠这个来辨认录像文件的,无"."

    std::int64_t m_preFrame;
    std::mutex m_ctrlMutex;
    
    std::uint64_t m_nRoku;              // 普通录制的时间（秒）， 也就是普通录制模式、一个文件最多录制多久（可配置）
    std::uint64_t m_nTaninnRoku;        // 陌生人录制最大时间
    //zFileManager* m_pFileManager;     
    std::int64_t m_nCurRokuTime;        // 普通录制 相对当前文件已录制时间（在uninit 录制环境的时候会清零）
    std::int64_t m_nCurTaninnRokuTime;  // 陌生人录制相对当前文件的时间
    std::string m_locDir;               // 录像文件目录（"\”结束），非路径 注意最后“\"
    std::string m_usbDir;
    std::string m_alarmImgDir;      
    std::string m_curDir;
    std::int64_t m_retainSize;          // 磁盘保留大小 M
    //std::string m_curFilename;
    std::list<std::string> m_locNormalVideo; // 根据 rootdir 以及文件后缀 在初始化时枚举出来的录像文件列表
    std::list<std::string> m_usbNormalVideo;
    std::list<std::string> m_locAlarmVideo;
    std::list<std::string> m_usbAlarmVideo;
    std::list<std::string> m_locAlarmImage;
    std::list<std::string> m_usbAlarmImage;
    std::list<std::string> m_local_gesture_image_list; // SD卡上的手势图片列表
    std::list<std::string> m_usb_gesture_image_list; // U盘上的手势图片列表
    int m_latest_gesture_image_id; // 最后一幅手势图片的ID号
    //std::list<std::string> m_alarmImage;
    //
    OutputStream video_st_;             // 录像环境相关
    //OutputStream audio_st_;
    AVFormatContext *oc_;               // 录像环境相关

    zBaseUtil::zTimer m_timer;
    std::thread* m_pSaveImageThread;

    int m_encode_width;     // 编码图像宽度
    int m_encode_height;    // 编码图像高度

    zRokugaThread::rokuStoreType m_curr_record_file_type;
    int m_curr_record_file_state;
    std::string m_curr_record_file_dir;

    encoded_frame_t m_frms[ENCODED_FRAMES_COUNT]; // 用于获取编码图像
    int m_frms_size;

    std::int64_t m_manual_record_begin_time = 0; // 手动录像起始时间, 单位: 毫秒
    std::int64_t m_manual_record_max_duration = 0;
};
#endif