#include <sys/wait.h>

#include "zLog.h"
#include "zFile.h"
#include "zUtil.h"

#ifndef LOG_TAG
#define  LOG_TAG  "FaceD"
#endif

namespace zLog
{

// 日志文件为两个，最大10M的文件
static const char * g_logfile0 = "/sdcard/facedis.log.1";
static const char * g_logfile1 = "/sdcard/facedis.log";
static const char * g_logfile_merge = "/sdcard/facedis-merge.log";
static const char * g_logfile_prev = "/sdcard/facedis.log.prev";

int zLogPrint(const char *format, ...)
{
    char buffer[DEBUG_BUFFER_MAX_LENGTH + 1] = { 0 };
    va_list arg;
    va_start(arg, format);
    vsnprintf(buffer, DEBUG_BUFFER_MAX_LENGTH, format, arg);
    va_end(arg);
    //printf("%s", buffer);
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "%s", buffer);
    //__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "%s", __VA_ARGS__);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.zLogPrint_debug
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 调试打印
 * 输入参数  : const char *filename  文件名
               const char *func      函数名
               const char *format    格式
               ...                   参数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zLogPrint_debug(const char *filename, const char *func, const int line, const char *format, ...)
{
    char buffer[DEBUG_BUFFER_MAX_LENGTH + 1] = { 0 };

    // filename example: D:/workspace/develop/Software/source/md_noScreen/jni/../zVideoThread.cpp
    char * sub_filename =  strrchr(filename, '/');

    int size = snprintf(buffer, DEBUG_BUFFER_MAX_LENGTH, "%s, %lld ms, %s, %s, %d: ",
        zFileUtil::getTimeString().c_str(), 
        (zBaseUtil::getMilliSecond()),
        (NULL != sub_filename ? (sub_filename + 1) : filename), 
        (NULL != func ? func : ""),
		line);

    va_list arg;
    va_start(arg, format);
    (void)vsnprintf(buffer + size, DEBUG_BUFFER_MAX_LENGTH - size, format, arg);
    va_end(arg);

    //printf("%s", buffer);
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "%s", buffer);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.zLogPrint_debug
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 调试打印
 * 输入参数  : const char *filename  文件名
               const char *func      函数名
               const char *format    格式
               ...                   参数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zLogPrint_info(const char *filename, const char *func, const int line, const char *format, ...)
{
    char buffer[DEBUG_BUFFER_MAX_LENGTH + 1] = { 0 };

    // filename example: D:/workspace/develop/Software/source/md_noScreen/jni/../zVideoThread.cpp
    char * sub_filename =  strrchr(filename, '/');
    int size = snprintf(buffer, DEBUG_BUFFER_MAX_LENGTH, "%s, %lld ms, %s, %s, %d: ",
        zFileUtil::getTimeString().c_str(), 
        (zBaseUtil::getMilliSecond()),
        (NULL != sub_filename ? (sub_filename + 1) : filename), 
        (NULL != func ? func : ""),
		line);

    va_list arg;
    va_start(arg, format);
    (void)vsnprintf(buffer + size, DEBUG_BUFFER_MAX_LENGTH - size, format, arg);
    va_end(arg);

    //printf("%s", buffer);
    __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "%s", buffer);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.zLogPrint_debug
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 调试打印
 * 输入参数  : const char *filename  文件名
               const char *func      函数名
               const char *format    格式
               ...                   参数
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int zLogPrint_error(const char *filename, const char *func, const int line, const char *format, ...)
{
    char buffer[DEBUG_BUFFER_MAX_LENGTH + 1] = { 0 };

    // filename example: D:/workspace/develop/Software/source/md_noScreen/jni/../zVideoThread.cpp
    char * sub_filename =  strrchr(filename, '/');
    int size = snprintf(buffer, DEBUG_BUFFER_MAX_LENGTH, "%s, %lld ms, %s, %s, %d: ",
        zFileUtil::getTimeString().c_str(), 
        zBaseUtil::getMilliSecond(),
        (NULL != sub_filename ? (sub_filename + 1) : filename), 
        (NULL != func ? func : ""),
		line);

    va_list arg;
    va_start(arg, format);
    (void)vsnprintf(buffer + size, DEBUG_BUFFER_MAX_LENGTH - size, format, arg);
    va_end(arg);

    //printf("%s", buffer);
    __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "%s", buffer);
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.zLog2File
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月23日
 * 函数功能  : 映射日志到文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : void
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zLog2File_begin()
{
    // 关闭已存在的LOGCAT 进程
    zLog2File_end();

    (void)save_prev_logfile();
    (void)remove_logfile();

    // 最大10M, 打印优先级大于等于“Info”的日志
    char buf[128] = {0};
    //(void)sprintf(buf, "logcat -f /sdcard/facedis.log -r 51200 -n 1 -b main -s %s:i &", LOG_TAG);
    (void)sprintf(buf, "logcat -f /sdcard/facedis.log -r 51200 -n 1 -b main -s %s:d &", LOG_TAG);
    LOG_D("%s\n", buf);

    int nReturn = zBaseUtil::RW_System(buf);
    if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
    {
        return;
    }

    LOG_E("system execute failed.\n");
    return;
}

/*****************************************************************************
 * 函 数 名  : zLog.zLog2File_end
 * 负 责 人  : 刘春龙
 * 创建日期  : 2016年3月9日
 * 函数功能  : 关闭写日志到文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
void zLog2File_end()
{
    const char *cmd = "busybox killall -9 logcat";
    LOG_D("cmd= %s\n", cmd);

    int nReturn = zBaseUtil::RW_System(cmd);
    if ((-1 != nReturn) && WIFEXITED(nReturn) && (0 == WEXITSTATUS(nReturn)))
    {
        return;
    }

    LOG_E("system execute failed.\n");
    return;
}


/*****************************************************************************
 * 函 数 名  : zLog.merge_logfile
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 合并两个日志文件为1个文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int merge_logfile()
{
    zFileUtil::zFileManager* pFm = zFileUtil::zFileManager::getInstance();
    if (!pFm->isFileExist(g_logfile1))
    {
        return -1;
    }

    if (pFm->isFileExist(g_logfile0))
    {
        char command[128];
        (void)sprintf(command, "cat %s %s > %s", 
            g_logfile0, g_logfile1, g_logfile_merge);
        zBaseUtil::RW_System(command);
    }
    else
    {
        char command[128];
        (void)sprintf(command, "cat %s > %s", 
            g_logfile1, g_logfile_merge);
        zBaseUtil::RW_System(command);
    }

    if (!pFm->isFileExist(g_logfile_merge))
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.save_prev_logfile
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年12月2日
 * 函数功能  : 保存上次的日志
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int save_prev_logfile()
{
    zFileUtil::zFileManager* pFm = zFileUtil::zFileManager::getInstance();
    
    // 删除合并的日志文件
    if (pFm->isFileExist(g_logfile_merge))
    {
        pFm->removeFile(g_logfile_merge);
    }
    
    // 合并日志文件
    if (-1 == merge_logfile())
    {
        LOG_E("merge logfile failed.\n");
        return -1;
    }

    // 删除前一个日志文件
    if (pFm->isFileExist(g_logfile_prev))
    {
        pFm->removeFile(g_logfile_prev);
    }

    // 保存前一个日志文件
    char command[128];
    (void)sprintf(command, "mv %s %s", g_logfile_merge, g_logfile_prev);
    zBaseUtil::RW_System(command);

    // 判断mv命令是否执行成功
    if (!pFm->isFileExist(g_logfile_prev))
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.remove_logfile
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 删除日志文件
 * 输入参数  : 无
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int remove_logfile()
{
    zFileUtil::zFileManager* pFm = zFileUtil::zFileManager::getInstance();
    if (pFm->isFileExist(g_logfile0))
    {
        pFm->removeFile(g_logfile0);
    }

    if (pFm->isFileExist(g_logfile1))
    {
        pFm->removeFile(g_logfile1);
    }

    if (pFm->isFileExist(g_logfile_merge))
    {
        pFm->removeFile(g_logfile_merge);
    }
    return 0;
}

/*****************************************************************************
 * 函 数 名  : zLog.upload_logfile
 * 负 责 人  : 刘春龙
 * 创建日期  : 2015年11月28日
 * 函数功能  : 上传日志文件
 * 输入参数  : const char *render  p2p客户端
 * 输出参数  : 无
 * 返 回 值  : 
 * 调用关系  : 
 * 其    它  : 

*****************************************************************************/
int upload_logfile(const std::string& render)
{
    if (    ("" == zP2PThread::m_deviceId)
        ||  ("" == render))
    {
        return -1;
    }

    if (-1 == merge_logfile())
    {
        LOG_E("webrtc merge logfile failed.\n");
        return -1;
    }

    int nResult = fileAcceptWebrtc(render, g_logfile_merge);
    if (0 != nResult)
    {
        LOG_E("webrtc fileAcceptWebrtc Failed! errorno == %d, render == %s, g_logfile_merge == %s \n",
            nResult, render.c_str(), g_logfile_merge);
        return -1;
    }

    return 0;
}

}
