#ifndef __ZALGORITHMDETECTTHREAD_H__
#define __ZALGORITHMDETECTTHREAD_H__

#include <mutex> 
#include <string>
#include <list>

#include <3rdParty/V4L2ImageCap/jni/cap_types.h>
#include "zThread.h"
#include "zUtil.h"
#include "zDataBuf.h"
#include "zTimer.h"
#include "zEakonThread.h"
#include "ZoneRecorder.h"


#define FRAME_COUNT 5

/* 
    add by 刘春龙, 2016-03-11, 原因: 
    开关打开后，不需要检测到人脸或者进入稳定态，只要有手存在时间到阈值(30秒)，
    就添加到黑名单，存在如下约束: 
    1, 手类型为GESTURE_TYPE_OPEN或GESTURE_TYPE_TRACK_LOSE，有手
    2. 手无移动，判断是否移动的条件为手掌宽/2.83.
    3, 无手掌时(result.nHand_ < 1)，如果没有进入稳定态，就认为无手，
       如果进入了稳定态，第一次收到无手，不认为手丢失，连续两次没有检测到手之间的时间小于2秒，才认为无手
    4. 多个手处理原则: 比较两个手的距离，如果没有移动，则认为是同一只手。不再同其他手进行比较。
*/
#define BLACKLIST_PROCESS_V2_ENABLE  1

#define MAX_BLACKLIST_CNT 100 // 最大黑名单数

typedef struct tagPosition
{
    int x_pos;
    int y_pos;

    tagPosition()
        :x_pos(0), y_pos(0)
    {
    }
}position_t;

#if BLACKLIST_PROCESS_V2_ENABLE
/* add by 刘春龙, 2016-03-11, 原因: 黑名单 */
typedef struct hand_time_s
{
    zBaseUtil::v2Hand hand;
    std::int64_t detect_start_time {0}; // 第一检测到手的时间，单位: 毫秒  
} hand_time_t;
#endif

class zAlgManager;
class zAlgorithmDetectThread : public zThread
{
public:
    enum zTesen
    {
        teIgnore = 0,
        teFind = 1,
        teLose = 2,
        openEakon = 4,
        closeEakon = 8,
    };

    zAlgorithmDetectThread(zAlgManager* pManager, int ctype, int nw = 640 , int nh = 480);
    ~zAlgorithmDetectThread();

    bool InitAlg(const std::string& mdir, const std::string& lcsDir);
    void proc();

    void onEvent(int nEvent);
    void onMsg(const zBaseUtil::zMsg& msg);

    // pro
    int eakonState();
    zBaseUtil::zEakonModel eakonModel();
    int switchState(int nState);
    int switchState_standby(int nState);

    bool autoLighting(bool bAuto);
    void chgLighting(bool bOpen);
    void get_lighting_mode(bool &auto_mode, bool &day);

    void onTaninnFind_bgnStranger();
    void taninnLose_endStranger();
    void onTaninnFind_notify(const zBaseUtil::v2ImgResult& result);
    void taninnLose_notify();
    void save_txt_result(const std::string &filename, const std::string &result);

    void get_gesture_flags(uint32_t &gesture, uint32_t &raise_count);
    std::int64_t get_lighting_duration();
    std::int64_t get_no_person_duration();
    bool is_hand_wind();
    bool is_free_time();

private:
    int analyzeHead(const zBaseUtil::v2ImgResult& result);
    int analyzeTemane(const zBaseUtil::v2ImgResult& result, const S_VCBuf& frame);
    void analyzeFrame(const S_VCBuf& frame);
    int algState();
    void reset();
    bool checkDiff(const std::list<zBaseUtil::v2Hand>& handList);

    void clear_context(bool wind_reset = true);

    int64_t get_daynight_change_gap();
    int daynight_mode_arbitrate(double image_light);
    
    bool set_daynight_mode(bool bDay);
    bool standby_mode_arbitrate(zBaseUtil::v2ImgResult& imgResult);
    void program_restart_arbitrate();

    void clear_taninn_context();
    void onTaninnFind(const zBaseUtil::v2ImgResult& result);
    void onTaninnLose();

    bool handRaise(int tesen, int gesture_flag);
    int hand_raise_wind();
    void clear_smart_wind_context();

    int stable_hand_enter(zBaseUtil::v2Hand& hand);
    void clear_gesture_context();
    void stable_hand_exit(bool is_play_voice = false);
    bool hand_move_check(zBaseUtil::v2Hand& hand, int gesture_track_lose_count);
    void hand_algrithm_ajust(zBaseUtil::v2Hand& hand);
    bool hand_lose_check();
    int save_gesture_image(const S_VCBuf& frame, int type);
    int gesture_analyze(const zBaseUtil::v2ImgResult& result);
    void reset_gesture_response_flags();
    void reset_hand_raise_response_count();

    #if BLACKLIST_PROCESS_V2_ENABLE
    bool has_been_in_blacklist(zBaseUtil::v2Hand &hand);
    void add_hand_blacklist(hand_time_t &hand);
    bool hand_lost(const zBaseUtil::v2ImgResult& result);
    int hand_compare(const zBaseUtil::v2Hand &h1, const zBaseUtil::v2Hand &h2);
    void hand_blacklist_arbitrate(const zBaseUtil::v2ImgResult& result);
    #else
    void add_hand_blacklist();
    #endif

    int gesture_response(const S_VCBuf& frame, int gesture_flag);
    void gesture_analyze_zone(const zBaseUtil::v2ImgResult& result);

    int device_test_prev_smart_wind(int focus);
    int wind_aviod_person(const zBaseUtil::v2ImgResult& result);
    int get_wind_angle(int lb, int rb, std::vector<zBaseUtil::Da> &vda, int &wind_angle, int &wind_distance);
    int get_curr_wind_angle(
        int lb, int rb, std::vector<zBaseUtil::Da> &vda, int &curr_angle_left, int &curr_angle_right);
    int blow_wind_angle(int lb, int rb, int left, int right);
    void clear_persons();
    int push_persons(const zBaseUtil::v2ImgResult& result, int lb, int rb, std::vector<zBaseUtil::Da> &da_vec, int &avg_person);
    int find_min_max(int lb, int rb, int &min_angle, int &max_angle);
    void wind_strength_adjust(int& wind_dist);

    void init_energy_config();
    int send_energy_config(bool open_saving);
    void save_energy_arbitrate(const zBaseUtil::v2ImgResult& result);
    int KazemukiCtrl_Energy(int* arr, bool gesture_flag);

    void add_fist_count(int cnt);
    void reset_fist_count();

    // 手势开关机相关函数
    bool InitGestureAlg(const std::string& mdir, const std::string& lcsDir);
    zBaseUtil::v2Hand & palm_expand(zBaseUtil::v2Hand &palm, int width, int height, int expand);
    bool set_gesture_roi(const zBaseUtil::v2Hand& hand);
    bool clear_gesture_roi();
    void swap_hand(zBaseUtil::v2ImgResult &dst, zBaseUtil::v2ImgResult &src);
    bool complex_detect_disable();

private:
    int algState_;
    zAlgManager* pManager_;

    bool bAutoLighting_;
    bool bDay_;
    std::mutex m_light_mutex; // 补光相关锁
    
    bool bFirstDet_;
    bool bUseHandRaise_;
    bool bUseFist_;
    bool bUseBlackList_;
    bool bUseHandFaceTrack;
    bool bUseFilterHeadType;
    bool bRobot;
    bool bTrackTaninn_;
    int m_taninn_frame_count;       // 连续检测到头肩的数量
    bool m_start_alarm_record_flag; // 告警录像启动标志
    int m_taninn_lose_count;
    bool m_taninn_lose_report;
    bool bTrackHand_;
    bool bAku_; // 前一次的手势状态，ture: 开手势，false: 关手势
    bool bFocus_;
    
    int nWidth_;
    int nHeight_;
    int cameraType_;
    int alarmGap_;                          // second
    int angleL_;
    int angleR_;
    int angleMin_;
    int angleMax_;
    int avDis_;
    double lightingF_;
    double lightingC_;
    int m_irled_brightness; // 补光亮度
    unsigned int nTe_ ; // 0: 无开手势状态， 其他: 有开手势状态 
    int latestCenterX_;
    int latestCenterY_;
    int pseudoHand_;
    int eakonCtrlGap_;                  // ms

    std::int64_t m_preFrame;
    std::int64_t latestKeikoku_;        // ms
    std::int64_t latestHandFind_;
    std::int64_t preEakonCtrl_;
    std::mutex ctrlMutex_;
    zDataBuf dataBuf_;

    bool m_bSleep;
    int64_t m_sleepPeriod;                      // 单位: ms,毫秒，默认10000 (10秒)
    int64_t m_preLightOnTime;
    int64_t m_last_daynight_change_time;        // 上一次白天/夜间 模式切换时间
    int m_light_change_count;                   // 连续灯光变化次数
    zBaseUtil::v2Hand pseudoHandPosition_;
    zBaseUtil::zTimer m_timer;

    unsigned int m_hand_raise_response_count;   // 举手停留一段时间后，执行相关的命令后，统计次数
    int m_prev_gesture_flag;
    int m_hand_raise_smart_wind_flag;           // 手势切换到，处理智能风时，置为 1
    zBaseUtil::zEakonModel m_prev_emodel;       // 离家模式配置标志位

    bool m_stable_hand_find_flag;               // 手势状态，false: 无手势，true: 有手势，手掌张开
    bool m_stable_hand_change_flag;             // 手势状态切换标志
    std::int64_t m_stable_hand_time;            // 手势稳定且手掌张开，的时间
    position_t m_stable_hand_left_top_pos;      // 左上角位置
    position_t m_stable_hand_right_bottom_pos;  // 右下角位置
    position_t m_stable_hand_center_pos;        // 手掌心位置
    uint m_stable_hand_width;                   // 手掌宽
    uint m_stable_hand_height;                  // 手掌高
    int m_stable_hand_width_square;             // 手掌宽的平方值
    bool m_hand_move_flag;                      // 手移动标志
    int m_gesture_track_lose_count;             // 连续跟踪失败次数
    position_t m_prev_hand_center_pos;          // 前一个手掌心位置
    int m_gesture_lose_count;                   // 手势丢失次数
    int m_hand_lose_count;                      // 手丢失(考虑了模式切换，也没有检测到)次数
    std::int64_t m_gesture_prev_lose_time;      // 第一次手势丢失时间
    ZoneRecorder recorder;                      // 悬停处理
    int m_center_fist_count = 0;                // 连续握拳次数

    std::int64_t m_prev_gesture_voice_time;     // 默认为0
    bool m_stable_hand_response_flag;           // 是否响应标志，默认为false
    bool m_stable_hand_blacklist_flag;          // 是否加入黑名单，默认为false

    std::vector<zBaseUtil::Da> m_vda[FRAME_COUNT];  // 保存多帧人头检测信息
    int m_vda_index;                                // m_vda 数组索引
    int m_last_wind_angle;                          // 0: 自动扫风，其他: 角度
    std::int64_t m_last_no_person_time;             // 上一次检测到没有人的时间
    std::int64_t m_prev_wind_time;                  // 前一个送吹风参数的时间
    int m_prev_wind_dist;                           // 前一次吹风距离
    int m_adjust_count;                             // 强弱风调整次数
    int m_image_nomotion;
    bool bMotionDetected;                           // true: 已检测到移动 false: 未检测到移动
    int64_t m_night_monitor_switch_time;

    // 节能相关上下文
    std::mutex m_energy_mutex;
    std::int64_t m_energy_time; // 第一次无人的时间，单位: 毫秒
    bool m_energy_have_persion;
    bool m_saving_energy_flag;
    int m_energy_buf_config[ SMART_WIND_CONFIG_SIZE ];
    zEakonThread::kazeMuki m_energy_muki;
    bool m_energy_gestrue_flag;
    bool m_itelligent_switch;

    std::string m_curr_analyze_result;

    #if BLACKLIST_PROCESS_V2_ENABLE
    std::list<hand_time_t> m_hands_for_blacklist;   // 保存手势，用于判断是否加入黑名单
    hand_time_t m_hands_blacklist[MAX_BLACKLIST_CNT]; // 保存加入黑名单的手
    unsigned int m_hands_blacklist_total;
    unsigned int m_hands_blacklist_index;
    std::int64_t m_prev_hand_lost_time;             // 稳定态手丢失时间
    #endif

    std::string m_model_dir;
    std::string m_licence_dir;
    bool m_algorithm_initialized_flag;  // 算法初始化标志

    int m_curr_smart_wind_flag; // 智能风标志, 和zEakonThread::kazeMuki定义一致
    bool m_intelligent_enable; // 智能使能标志

    // 手势开关机，相关变量
    double m_prev_lighting_ = 0.0;
    std::int64_t m_first_hand_lost_time = 0;
    bool m_temperature_operated = false; // true: 进入手势稳定态后，执行了控温命令。
    std::int64_t m_prev_switch_time = 0; // 前一次手势开关机时间。
    
    bool m_no_person_flag = false;         // true: 无人状态
    std::int64_t m_no_person_time = 0;     // 第一次无人时间, 单位: 毫秒

    std::int64_t m_reboot_duration_short = 0;
    std::int64_t m_reboot_duration_long  = 0;
    int m_no_person_thd = 0;
    int m_lighting_thd = 0; 
    int m_min_free_time = 0;
    int m_max_free_time = 0;
};


#endif
